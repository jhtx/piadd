package com.yff.tuan.exception;

import com.yff.tuan.api.Result;

/**
 * @author huili
 *
 */
public class AppException extends RuntimeException{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2565607886048376447L;

	/** The error code. */
	private Result result;
	
	/** The args. */
	private Object[] args;

	/**
	 * Instantiates a new api exception.
	 *
	 * @param result the error code
	 */
	public AppException(Result result){
		super(result.name());
		this.result = result;
	}
	
	/**
	 * Instantiates a new api exception.
	 *
	 * @param result the error code
	 * @param args the args
	 */
	public AppException(Result result,Object[] args) {
		super(result.name()+":"+ args);
		this.result = result;
		this.args=args;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public Result getResult(){
		return result;
	}
	
	/**
	 * Gets the extra args.
	 *
	 * @return the extra args
	 */
	public Object[] getExtraArgs() {
		return args;
	}
	public static void main(String[] args) {
		System.out.println("\uC9C1\uC6D0 \uC2E0\uADDC \uB4F1\uB85D");
	}
}
