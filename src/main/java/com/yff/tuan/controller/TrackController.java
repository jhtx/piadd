package com.yff.tuan.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.Brand;
import com.yff.tuan.model.CustomerExample;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.TrackTireCheck;
import com.yff.tuan.model.Tread;
import com.yff.tuan.model.TreadExample;
import com.yff.tuan.model.TreadExample.Criteria;
import com.yff.tuan.model.VehicleTypeExample;
import com.yff.tuan.service.CustomerService;
import com.yff.tuan.service.TrackService;
import com.yff.tuan.service.TreadService;
import com.yff.tuan.service.VehicleTypeService;
import com.yff.tuan.util.DateUtil;
import com.yff.tuan.util.ObjectUtils;
import com.yff.tuan.util.Page;
import com.yff.tuan.util.WordUtils;

@Controller
@RequestMapping("/track")
public class TrackController {
	@Autowired TrackService trackService;
	@Autowired CustomerService customerService;
	@Autowired VehicleTypeService vehicleTypeService;
	@Autowired TreadService treadService;
	
	@RequestMapping("/list")
	public String query(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response) {
		Page<Track> page = trackService.queryByPage(new TrackExample(), null == pageNum ? 1 : pageNum);
		model.addAttribute("page", page);
		return "/track_list";
	}
	
	@RequestMapping("/wxlist")
	@ResponseBody
	public Object wxlist(Model model,HttpServletRequest request,HttpServletResponse response) {
		return trackService.queryForWx();
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		
		Track track = trackService.findByPrimaryKey(id);
		if(null != track) {
			model.addAttribute("track", track);
			model.addAttribute("typeTires", vehicleTypeService.queryVehicleTypeTire(track.getVehicleTypeId()));
			List<Brand> brands = customerService.queryBrand();
			model.addAttribute("brands", brands);
			model.addAttribute("tyreSize", customerService.queryTyreSize());
			model.addAttribute("markets", customerService.queryMarket());
			model.addAttribute("evns", customerService.queryEvn());
			if(null != brands && !brands.isEmpty()) {
				TreadExample example = new TreadExample();
				Criteria createCriteria = example.createCriteria();
				createCriteria.andBrandIdEqualTo(brands.get(0).getId());
				List<Tread> treads = treadService.query(example);
				model.addAttribute("treads", treads);
			}
		}
		return "/track_detail";
	}
	
	@ResponseBody
	@RequestMapping("/wxdetail")
	public Object wxdetail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map =new HashMap<>();
		Track track = trackService.findByPrimaryKey(id);
		if(null != track) {
			if(null != track.getInDate()){
				track.setInDateStr(DateUtil.getFormatDate(track.getInDate(), DateUtil.FORMAT_YYMMDD));
			}else{
				track.setInDateStr("");
			}
			map.put("track", track);
			map.put("typeTires", vehicleTypeService.queryVehicleTypeTire(track.getVehicleTypeId()));
		
		}
		return map;
	}
	
	@RequestMapping("/del")
	@ResponseBody
	public Object del(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null == id) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		return ApiResult.SUCCESS(trackService.del(id));
	}
	
	@RequestMapping("/toAdd")
	public String toAdd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("customer", customerService.query(new CustomerExample()));
		model.addAttribute("vehicleType", vehicleTypeService.query(new VehicleTypeExample()));
		model.addAttribute("markets", customerService.queryMarket());
		model.addAttribute("evns", customerService.queryEvn());
		return "/track_add";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,@RequestBody Track track,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		track.setCreateTime(new Date());
		int i = trackService.insert(track);
		map.put("add", i>0?true:false);
		map.put("id", track.getId());
		return map;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public Object update(Model model,@RequestBody Track track,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		int i = trackService.updateByPrimaryKeySelective(track);
		map.put("update", i>0?true:false);
		map.put("id", track.getId());
		return map;
	}
	
	@RequestMapping("/addTire")
	@ResponseBody
	public Object addTire(Model model,@RequestBody TrackTire trackTire,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		int i = trackService.insertTrackTire(trackTire);
		map.put("add", i>0?true:false);
		return map;
	}
	
	@RequestMapping("/check")
	public String check(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		
		TrackTire trackTire = trackService.findTrackTireByPrimaryKey(id);
		if(null != trackTire) {
			model.addAttribute("trackTire", trackTire);
			model.addAttribute("track", trackService.findByPrimaryKey(trackTire.getTrackId()));
		}
		return "/track_tire";
	}
	
	@ResponseBody
	@RequestMapping("/wxtire")
	public Object wxtire(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		TrackTire trackTire = trackService.findTrackTireByPrimaryKey(id);
		if(null != trackTire) {
			map.put("trackTire", trackTire);
			map.put("track", trackService.findByPrimaryKey(trackTire.getTrackId()));
		}
		return map;
	}
	
	@RequestMapping("/addCheck")
	@ResponseBody
	public Object addCheck(Model model,@RequestBody TrackTireCheck trackTireCheck,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		if(null != trackTireCheck) {
			trackTireCheck.setCheckDate(new Date());
			int i = trackService.insertTrackTireCheck(trackTireCheck);
			map.put("add", i>0?true:false);
		}
		return map;
	}
	
	@RequestMapping("/tread")
	@ResponseBody
	public Object tread(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		TreadExample example = new TreadExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andBrandIdEqualTo(id);
		List<Tread> treads = treadService.query(example);
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("treads", treads);
		return map;
	}
	
	@RequestMapping("/export")
	public void export(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) throws Exception{
		Track track = trackService.exportByPrimaryKey(id);
        Map<String, Object> map = new HashMap<String,Object>();  
        map.put("t", ObjectUtils.objectToMap(track));
        WordUtils.exportMillCertificateWord(request,response,map,"track.ftl","里程跟踪");   
	}
	
}
