package com.yff.tuan.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CoUserExample;
import com.yff.tuan.service.CoUserService;
import com.yff.tuan.service.CustomerService;
import com.yff.tuan.service.TreadService;
import com.yff.tuan.util.AESUtil;
import com.yff.tuan.util.Page;
import com.yff.tuan.util.StringUtil;

@Controller
@RequestMapping("/couser")
public class CoUserController {
	@Autowired CoUserService coUserService;
	@Autowired CustomerService customerService;
	@Autowired TreadService treadService;
	
	@RequestMapping("/list")
	public String query(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response) {
		CoUserExample example = new CoUserExample();
		example.createCriteria().andRoleEqualTo(1);
		Page<CoUser> page = coUserService.queryByPage(example, null == pageNum ? 1 : pageNum);
		model.addAttribute("page", page);
		return "/couser_list";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,CoUser coUser,HttpServletRequest request,HttpServletResponse response){
		CoUserExample example = new CoUserExample();
		example.createCriteria().andNameEqualTo(coUser.getName());
		if(coUserService.checkExist(example)){
			return ApiResult.ERROR(Result.ALREADY_EXIST_USER_NAME);
		}
		coUser.setPassword(AESUtil.Encrypt(String.valueOf(coUser.getPassword())));
		coUser.setCreateDate(new Date());
		coUser.setRole(1);
		coUser.setParentId(0);
		return ApiResult.SUCCESS(coUserService.insert(coUser));
	}
	
	@RequestMapping("/toupdate")
	public String toadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("couser", coUserService.findByPrimaryKey(id));
		return "/couser_update";
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public Object updateUser(Model model,@RequestBody Map<String, Object> object,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resMap = new HashMap<String, Object>();
		if(null != object){
			try {
				CoUser user = new CoUser();
				int id = 0;
				if(object.containsKey("id")) {
					id = Integer.parseInt(String.valueOf(object.get("id")));
					user.setId(id);
				}
				if(object.containsKey("nickName")) {
					user.setNickName(String.valueOf(object.get("nickName")));
				}
				if(object.containsKey("password")) {
					user.setPassword(AESUtil.Encrypt(String.valueOf(object.get("password"))));
				}
				if(object.containsKey("no")) {
					user.setNo(String.valueOf(object.get("no")));
				}
				if(object.containsKey("address")) {
					user.setAddress(String.valueOf(object.get("address")));
				}
				if(object.containsKey("contactor")) {
					user.setContactor(String.valueOf(object.get("contactor")));
				}
				if(object.containsKey("phone")) {
					user.setPhone(String.valueOf(object.get("phone")));
				}
				coUserService.updateByPrimaryKeySelective(user);
				resMap.put("update", true);
			} catch (Exception e) {
				e.printStackTrace();
				resMap.put("update", false);
			}
		}else {
			resMap.put("update", false);
		}
		return resMap;
	}
	
	@RequestMapping("/del")
	@ResponseBody
	public Object del(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null == id) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		return ApiResult.SUCCESS(coUserService.del(id));
	}
	
	@RequestMapping("/updatepassword")
	@ResponseBody
	public Object updatepassword(Model model,Integer id,String password,HttpServletRequest request,HttpServletResponse response){
		CoUser user = new CoUser();
		user.setId(id);
		user.setPassword(StringUtil.isEmpty(password)?AESUtil.Encrypt("123456"):AESUtil.Encrypt(password));
		return ApiResult.SUCCESS(coUserService.updateByPrimaryKeySelective(user));
	}
}
