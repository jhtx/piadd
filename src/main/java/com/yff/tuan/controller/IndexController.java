package com.yff.tuan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.service.UserService;

import net.sf.json.JSONObject;

@Controller
public class IndexController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping("/index")
	public String index(){
		return "index";
	}
	
	@RequestMapping("/order/add")
	@ResponseBody
	public Object add(User user){
		int i = userService.insert(user);
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("isSaved", i>1);
		return map;
	}
}
