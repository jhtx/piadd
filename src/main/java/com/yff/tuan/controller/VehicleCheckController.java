package com.yff.tuan.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.service.VehicleCheckService;
import com.yff.tuan.util.Page;


@Controller
@RequestMapping("/vehicledata")
public class VehicleCheckController {
	private Logger logger = LoggerFactory.getLogger(VehicleCheckController.class);
	
	@Autowired VehicleCheckService checkService;
	
	@RequestMapping("/push")
	@ResponseBody
	public Object save(@Valid @RequestBody VehicleCheck check){
		logger.info(check.toString());
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		return new ApiResult(map.put("added", checkService.save(check)));
	}
	
	@RequestMapping("/query")
	public String query(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response) {
		Page<VehicleCheck> page = checkService.queryByPage(new VehicleCheckExample(), null == pageNum ? 1 : pageNum);
		model.addAttribute("page", page);
		return "/vehicle_check";
	}
}
