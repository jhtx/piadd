package com.yff.tuan.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.Check;
import com.yff.tuan.model.CheckExample;
import com.yff.tuan.model.CheckExample.Criteria;
import com.yff.tuan.model.CheckVehicle;
import com.yff.tuan.model.CheckVehicleExample;
import com.yff.tuan.model.CheckVehicleTire;
import com.yff.tuan.model.CustomerExample;
import com.yff.tuan.model.VehicleTypeExample;
import com.yff.tuan.model.VehicleTypeTire;
import com.yff.tuan.service.CheckService;
import com.yff.tuan.service.CustomerService;
import com.yff.tuan.service.VehicleTypeService;
import com.yff.tuan.util.DateUtil;
import com.yff.tuan.util.ObjectUtils;
import com.yff.tuan.util.Page;
import com.yff.tuan.util.WordUtils;

@Controller
@RequestMapping("/check")
public class CheckController {
	@Autowired CustomerService customerService;
	@Autowired CheckService checkService;
	@Autowired VehicleTypeService vehicleTypeService;
	
	@RequestMapping("/list")
	public String query(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response) {
		Page<Check> page = checkService.queryByPage(new CheckExample(), null == pageNum ? 1 : pageNum);
		model.addAttribute("page", page);
		model.addAttribute("customers", customerService.query(new CustomerExample()));
		return "/check_list";
	}
	
	@ResponseBody
	@RequestMapping("/wxlist")
	public Object wxlist(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response) {
		List<Check> checks =  checkService.querywx();
		for(Check check:checks){
			if(null != check.getDate()){
				check.setDateStr(DateUtil.getFormatDate(check.getDate(), DateUtil.FORMAT_YYMMDD));
			}else{
				check.setDateStr("");
			}
		}
		return checks;
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,Check check,HttpServletRequest request,HttpServletResponse response) {
		if(null == check) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		/*CheckExample example = new CheckExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andCustomerIdEqualTo(check.getCustomerId());
		List<Check> checks = checkService.query(example);
		if(null != checks && !checks.isEmpty()) {
			return ApiResult.ERROR(Result.ALREADY_EXIST_CUSTOMER);
		}*/
		check.setDate(new Date());
		return ApiResult.SUCCESS(checkService.insert(check));
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		
		Check check = checkService.findByPrimaryKey(id);
		if(null != check) {
			model.addAttribute("check", check);
		}
		model.addAttribute("vehicleTypes", vehicleTypeService.query(new VehicleTypeExample()));
		return "/check_detail";
	}
	
	@ResponseBody
	@RequestMapping("/wxdetail")
	public Object wxdetail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		Check check = checkService.findByPrimaryKey(id);
		if(null != check.getDate()){
			check.setDateStr(DateUtil.getFormatDate(check.getDate(), DateUtil.FORMAT_YYMMDD));
		}else{
			check.setDateStr("");
		}
		return check;
	}
	
	
	@RequestMapping("/addCheckVehicle")
	@ResponseBody
	public Object addCheckVehicle(Model model,CheckVehicle checkVehicle,HttpServletRequest request,HttpServletResponse response){
		if(null == checkVehicle) {
			return ApiResult.ERROR(Result.INVALID_PARAMETERS);
		}
		CheckVehicleExample example = new CheckVehicleExample();
		com.yff.tuan.model.CheckVehicleExample.Criteria createCriteria = example.createCriteria();
		createCriteria.andVehicleNoEqualTo(checkVehicle.getVehicleNo());
		createCriteria.andCheckIdEqualTo(checkVehicle.getCheckId());
		List<CheckVehicle> checkVehicles = checkService.queryCheckVehicle(example);
		if(null != checkVehicles && !checkVehicles.isEmpty()) {
			return ApiResult.ERROR(Result.ALREADY_EXIST_VEHICLENO);
		}
		return ApiResult.SUCCESS(checkService.insertCheckVehicle(checkVehicle));
	}
	
	@RequestMapping("/tire")
	public String checkTire(Model model,Integer id,Integer vehicleTypeId,HttpServletRequest request,HttpServletResponse response){
		
		List<CheckVehicleTire> checkVehicleTires = checkService.queryTireByVehicleId(id);
		if(null != checkVehicleTires) {
			model.addAttribute("checkVehicleTires", checkVehicleTires);
		}
		List<VehicleTypeTire> typeTires = vehicleTypeService.queryVehicleTypeTire(id);
		String typeTiresJson = new Gson().toJson(typeTires);
		model.addAttribute("typeTiresJson", typeTiresJson);
		model.addAttribute("id", id);
		return "/check_tire";
	}
	
	@RequestMapping("/wxcheck")
	@ResponseBody
	public Object wxcheck(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		return checkService.queryTireByVehicleId(id);
	}
	
	@RequestMapping("/toAddTire")
	public String toAddTire(Model model,Integer id,Integer vehicleTypeId,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("id", id);
		CheckVehicle vehicle = checkService.findCheckVehicle(id);
		model.addAttribute("vehicle", vehicle);
		List<VehicleTypeTire> typeTires = vehicleTypeService.queryVehicleTypeTire(vehicle.getVehicleTypeId());
		model.addAttribute("typeTires", typeTires);
		model.addAttribute("tyreSizes", customerService.queryTyreSize());
		model.addAttribute("brands", customerService.queryBrand());
		return "/check_tire_add";
	}
	
	@RequestMapping("/addTire")
	@ResponseBody
	public Object addTire(Model model,@RequestBody List<CheckVehicleTire> checkVehicleTires,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		int i =  checkService.insertCheckVehicleTire(checkVehicleTires);
		map.put("add", i>0?true:false);
		return map;
	}
	
	@RequestMapping("/tires")
	@ResponseBody
	public Object tires(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		return vehicleTypeService.queryVehicleTypeTire(id);
	}
	
	
	
	@RequestMapping("/report")
	public void report(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		Map<String, Object> map = new HashMap<String,Object>();  
        map.put("c", ObjectUtils.objectToMap(checkService.checkReport(id)));
        WordUtils.exportMillCertificateWord(request,response,map,"check.ftl","轮胎检查");   
	}
	
}
