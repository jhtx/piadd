package com.yff.tuan.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.api.Result;
import com.yff.tuan.model.Brand;
import com.yff.tuan.model.BrandExample;
import com.yff.tuan.model.RoleExample;
import com.yff.tuan.model.TyreSize;
import com.yff.tuan.model.TyreSizeExample;
import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.model.UserExample.Criteria;
import com.yff.tuan.model.VehicleTypeExample;
import com.yff.tuan.model.WxUserBind;
import com.yff.tuan.model.WxUserExample;
import com.yff.tuan.service.BrandService;
import com.yff.tuan.service.RoleService;
import com.yff.tuan.service.TyreSizeService;
import com.yff.tuan.service.UserService;
import com.yff.tuan.service.VehicleTypeService;
import com.yff.tuan.service.WxUserService;
import com.yff.tuan.util.AESUtil;
import com.yff.tuan.util.Page;
import com.yff.tuan.util.StringUtil;
import com.yff.tuan.vo.UserVo;

@Controller
@RequestMapping("/set")
public class SetController {
	
	private Logger LOG = LoggerFactory.getLogger(SetController.class);
	@Autowired
	VehicleTypeService vehicleTypeService;
	@Autowired
	BrandService brandService;
	@Autowired
	TyreSizeService tyreSizeService;
	
	@RequestMapping("/list")
	public String list(Model model,HttpServletRequest request,HttpServletResponse response){
		
		return "/set_list";
	}
	
	@RequestMapping("/vehcile")
	public String vehcile(Model model,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("vehciles", vehicleTypeService.query(new VehicleTypeExample()));
		return "/set_vehcile";
	}
	@RequestMapping("/brand")
	public String brand(Model model,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("brands", brandService.query(new BrandExample()));
		return "/set_brand";
	}
	@RequestMapping("/brand/toadd")
	public String brandToadd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response) {
		if(null != id) {
			model.addAttribute("brand", brandService.findByPrimaryKey(id));
		}
		return "/set_brand_add";
	}
	
	@RequestMapping("/brand/add")
	@ResponseBody
	public Object brandadd(Model model,@RequestBody Brand brand,HttpServletRequest request,HttpServletResponse response) {
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		if(null != brand) {
			if(null != brand.getId()) {
				brandService.update(brand);
			}else {
				brandService.add(brand);
			}
			map.put("add", true);
		}
		return map;
	}
	@RequestMapping("/tyreSize")
	public String tyreSize(Model model,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("tyreSizes", tyreSizeService.query(new TyreSizeExample()));
		return "/set_tyre_size";
	}
	
	@RequestMapping("/tyreSize/toadd")
	public String tyreSizeToAdd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		if(null != id) {
			model.addAttribute("tyreSize", tyreSizeService.findByPrimaryKey(id));
		}
		return "/set_tyre_size_add";
	}
	
	@RequestMapping("/tyreSize/add")
	@ResponseBody
	public Object tyreSizeAdd(@RequestBody TyreSize tyreSize,HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		if(null != tyreSize) {
			if(null != tyreSize.getId()) {
				tyreSizeService.update(tyreSize);
			}else {
				tyreSizeService.add(tyreSize);
			}
			map.put("add", true);
		}
		return map;
	}
}
