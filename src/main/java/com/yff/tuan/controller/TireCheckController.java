package com.yff.tuan.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.api.ApiResult;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.TireCheckExample;
import com.yff.tuan.service.TireCheckService;
import com.yff.tuan.util.Page;

@Controller
@RequestMapping("/checkdata")
public class TireCheckController {
	@Autowired TireCheckService checkService;
	
	@RequestMapping("/push")
	@ResponseBody
	public Object save(@Valid @RequestBody TireCheck check){
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		return new ApiResult(map.put("added", checkService.save(check)));
	}
	
	@RequestMapping("/query")
	public String query(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response) {
		Page<TireCheck> page = checkService.queryByPage(new TireCheckExample(), null == pageNum ? 1 : pageNum);
		model.addAttribute("page", page);
		return "/tire_check";
	}
}
