package com.yff.tuan.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yff.tuan.constants.Constants;
import com.yff.tuan.model.Brand;
import com.yff.tuan.model.CustomerComment;
import com.yff.tuan.model.Evn;
import com.yff.tuan.model.Info;
import com.yff.tuan.model.Market;
import com.yff.tuan.model.Point;
import com.yff.tuan.model.Problem;
import com.yff.tuan.model.Require;
import com.yff.tuan.model.Service;
import com.yff.tuan.model.TyreSize;
import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.model.UserExample.Criteria;
import com.yff.tuan.service.CustomerService;
import com.yff.tuan.service.UserService;
import com.yff.tuan.util.Page;
import com.yff.tuan.util.StringUtil;
import com.yff.tuan.vo.CustomerAu;
import com.yff.tuan.vo.CustomerVo;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    
	@Autowired
	CustomerService service;
	@Autowired
	UserService userService;
	
	@RequestMapping("/list")
	public String list(Model model,Integer pageNum,HttpServletRequest request,HttpServletResponse response){
		Integer role = (Integer) request.getSession().getAttribute("role");
		Integer userId = (Integer) request.getSession().getAttribute("userId");
		List<Integer> userIds = userService.queryUserIds(role,userId);
		
		Page<CustomerVo> page = service.queryByPage(null == pageNum ? 1 : pageNum,userIds);
		model.addAttribute("page", page);
		return "/customer_list";
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("customer", service.detail(id));
		model.addAttribute("brands", service.queryBrand());
		model.addAttribute("tyreSizes", service.queryTyreSize());
		model.addAttribute("markets", service.queryMarket());
		model.addAttribute("evns", service.queryEvn());
		model.addAttribute("problems", service.queryProblem());
		model.addAttribute("infos", service.queryInfo());
		model.addAttribute("requires", service.queryRequire());
		model.addAttribute("services", service.queryService());
		model.addAttribute("points", service.queryPoint());
		model.addAttribute("comments", service.queryComment(id));
		model.addAttribute("userId", Integer.parseInt(String.valueOf(request.getSession().getAttribute("userId"))));
		return "/customer_detail";
	}
	
	
	@RequestMapping("/toAdd")
	public String toAdd(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("brands", service.queryBrand());
		model.addAttribute("tyreSizes", service.queryTyreSize());
		model.addAttribute("markets", service.queryMarket());
		model.addAttribute("evns", service.queryEvn());
		model.addAttribute("problems", service.queryProblem());
		model.addAttribute("infos", service.queryInfo());
		model.addAttribute("requires", service.queryRequire());
		model.addAttribute("services", service.queryService());
		model.addAttribute("points", service.queryPoint());
		model.addAttribute("userId", Integer.parseInt(String.valueOf(request.getSession().getAttribute("userId"))));
		model.addAttribute("users", userService.query(new UserExample()));
		return "/customer_add";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Object add(Model model,@RequestBody CustomerAu au,HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		map.put("add", service.insertCustomerAu(au));
		return map;
	}
	
	
	@RequestMapping("/addComment")
	@ResponseBody
	public Object addComment(Model model,@RequestBody CustomerComment comment,HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		comment.setCreateTime(new Date());
		comment.setStatus(0);
		comment.setUserId(Integer.parseInt(String.valueOf(request.getSession().getAttribute("userId"))));
		map.put("add", service.insertCustomerComment(comment));
		return map;
	}
	
	@RequestMapping("/toUpdate")
	public String toUpdate(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		model.addAttribute("customer", service.detail(id));
		model.addAttribute("users", userService.query(new UserExample()));
		model.addAttribute("brands", service.queryBrand());
		model.addAttribute("tyreSizes", service.queryTyreSize());
		model.addAttribute("markets", service.queryMarket());
		model.addAttribute("evns", service.queryEvn());
		model.addAttribute("problems", service.queryProblem());
		model.addAttribute("infos", service.queryInfo());
		model.addAttribute("requires", service.queryRequire());
		model.addAttribute("services", service.queryService());
		model.addAttribute("points", service.queryPoint());
		model.addAttribute("comments", service.queryComment(id));
		return "/customer_update";
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public Object update(Model model,@RequestBody CustomerAu au,HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		map.put("add", service.updateCustomerAu(au));
		return map;
	}
	
	@RequestMapping("/wxlist")
	@ResponseBody
	public Object wxlist(Model model,Integer userId, HttpServletRequest request,HttpServletResponse response){
		return service.query(userId);
	}
	
	@RequestMapping("/wxtoadd")
	@ResponseBody
	public Object wxtoadd(Model model,Integer userId, HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("brands", service.queryBrand());
		map.put("tyreSizes", service.queryTyreSize());
		map.put("markets", service.queryMarket());
		map.put("evns", service.queryEvn());
		map.put("problems", service.queryProblem());
		map.put("infos", service.queryInfo());
		map.put("requires", service.queryRequire());
		map.put("services", service.queryService());
		map.put("points", service.queryPoint());
		map.put("userId", userId);
		return map;
	}
	
	@RequestMapping("/wxadd")
	@ResponseBody
	public Object wxadd(Model model,@RequestBody CustomerAu au, HttpServletRequest request,HttpServletResponse response){
		Map<String, Boolean> map = new HashMap<String,Boolean>();
		map.put("add", service.insertCustomerAu(au));
		return map;
	}
	
	@RequestMapping("/wxdetail")
	@ResponseBody
	public Object wxdetail(Model model,Integer id,HttpServletRequest request,HttpServletResponse response){
		return service.wxDetail(id);
	}
	
}
