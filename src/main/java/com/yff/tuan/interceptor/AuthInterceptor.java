package com.yff.tuan.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.yff.tuan.util.ApiConstants;

/**
 * The Class AuthInterceptor.
 */
public class AuthInterceptor extends HandlerInterceptorAdapter {
	private static final Logger LOG = LoggerFactory.getLogger(AuthInterceptor.class);
	
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception{
		String rnd =  (String) request.getAttribute(ApiConstants.RQID);
		//TODO AUTH VILIDATE
		request.setAttribute(ApiConstants.HEADER_RQID, rnd);
		response.setHeader("Access-Control-Allow-Origin", "*");
		Object isLogin = request.getSession().getAttribute("isLogin");
		if(null == isLogin) {
			response.sendRedirect(request.getContextPath()+"/login.html");
			return false;
		}
		
		LOG.debug(ApiConstants.HEADER_RQID+"="+rnd);
		
		return super.preHandle(request, response, handler);
	}
	
}
