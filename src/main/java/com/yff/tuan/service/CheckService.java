package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Check;
import com.yff.tuan.model.CheckExample;
import com.yff.tuan.model.CheckVehicle;
import com.yff.tuan.model.CheckVehicleExample;
import com.yff.tuan.model.CheckVehicleTire;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.BrokenReport;

public interface CheckService {
	
	public abstract Check find(Check check);
	
	public abstract Check findByPrimaryKey(Integer id);

	public abstract int insert(Check check);
	
	public abstract int updateByPrimaryKeySelective(Check check);
	
	public abstract List<Check> query(CheckExample example);
	
	public abstract Page<Check> queryByPage(CheckExample example,Integer pageNum);
	public abstract List<Check> querywx();
	public abstract List<CheckVehicleTire> queryTireByVehicleId(Integer vehicleId);
	public abstract int insertCheckVehicle(CheckVehicle checkVehicle);
	public abstract int insertCheckVehicleTire(List<CheckVehicleTire> checkVehicleTires);
	public abstract List<CheckVehicle> queryCheckVehicle(CheckVehicleExample example);
	public abstract CheckVehicle findCheckVehicle(Integer vehicleId);
	public abstract BrokenReport checkReport(Integer vehicleId);
	
}	
