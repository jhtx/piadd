package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CoUserExample;
import com.yff.tuan.util.Page;

public interface CoUserService {
	
	public abstract boolean checkExist(CoUserExample example);
	
	public abstract CoUser findByPrimaryKey(Integer id);

	public abstract int insert(CoUser coUser);
	
	public abstract int del(Integer id);
	
	public abstract int updateByPrimaryKeySelective(CoUser coUser);
	
	public abstract List<CoUser> query(CoUserExample example);
	
	
	public abstract Page<CoUser> queryByPage(CoUserExample example,Integer pageNum);
	
}	
