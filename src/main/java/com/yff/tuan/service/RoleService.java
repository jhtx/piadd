package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Role;
import com.yff.tuan.model.RoleExample;
import com.yff.tuan.util.Page;

public interface RoleService {
	public abstract Role findByPrimaryKey(Integer id);
	
	public abstract int insert(Role Role);
	
	public abstract int updateByPrimaryKeySelective(Role Role);
	
	public abstract  List<Role> query(RoleExample example);
	
	public abstract  Page<Role> queryByPage(RoleExample example,int pageNum);
}	
