package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.TyreSize;
import com.yff.tuan.model.TyreSizeExample;
public interface TyreSizeService {
	
	public abstract TyreSize find(TyreSize tyreSize);
	
	public abstract TyreSize findByPrimaryKey(Integer id);

	public abstract int insert(TyreSize tyreSize);
	
	public abstract int updateByPrimaryKeySelective(TyreSize tyreSize);
	
	public abstract List<TyreSize> query(TyreSizeExample example);
	
	public boolean add(TyreSize tyreSize);
	public boolean update(TyreSize tyreSize);
	
}	
