package com.yff.tuan.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor.GOLD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.yff.tuan.constants.Constants;
import com.yff.tuan.mapper.RoleMapper;
import com.yff.tuan.mapper.UserMapper;
import com.yff.tuan.mapper.WxUserMapper;
import com.yff.tuan.model.Role;
import com.yff.tuan.model.RoleExample;
import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.model.UserExample.Criteria;
import com.yff.tuan.model.WxUser;
import com.yff.tuan.model.WxUserExample;
import com.yff.tuan.service.RoleService;
import com.yff.tuan.service.UserService;
import com.yff.tuan.service.WxUserService;
import com.yff.tuan.util.Page;
import com.yff.tuan.util.StringUtil;
import com.yff.tuan.vo.UserVo;

import net.sf.json.JSONObject;

@Component
public class UserServiceImpl implements UserService{
	@Autowired
	UserMapper mapper;
	@Autowired
	RoleService roleService;
	@Autowired
	WxUserService wxUserService;
	
	@Override
	public User find(User user) {
		UserExample userExample = new UserExample();
		Criteria criteria = userExample.createCriteria();
		if(null != user.getName()){
			criteria.andNameEqualTo(user.getName());
		}
		if(null != user.getPassword()){
			criteria.andPasswordEqualTo(user.getPassword());
		}
		if(null != user.getWxId()){
			criteria.andWxIdEqualTo(user.getWxId());
		}
		List<User> users = mapper.selectByExample(userExample);
		if(!users.isEmpty()) {
			return users.get(0);
		}
		return null;
	}
	
	@Override
	public int del(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}
	
	@Override
	public int insert(User user) {
		return mapper.insert(user);
	}
	
	@Override
	public int updateByPrimaryKeySelective(User user) {
		return mapper.updateByPrimaryKeySelective(user);
	}
	
	@Override
	public List<User> query(UserExample example) {
		return mapper.selectByExample(example);
	}
	
	@Override
	public Page<UserVo> queryByPage(int pageNum ) {
		
		Page<UserVo> page = new Page<UserVo>(pageNum,mapper.selectByExample(new UserExample()).size());
		List<UserVo> users = mapper.queryByPage(page.getStartIndex(), page.getPageSize());
		//List<WxUser> wxUsers = wxUserService.queryWxUser(null);
		for(UserVo userVo:users) {
			userVo.setWxUsers(wxUserService.queryWxUser(userVo.getId()));
		}
		page.setList(users);
		return page;
	}

	@Override
	public UserVo find(Integer id) {
		UserVo userVo = mapper.findByPrimaryKey(id);
		if(null != userVo){
			userVo.setWxUsers(wxUserService.queryWxUser(userVo.getId()));
			userVo.setWxUsersJson(new Gson().toJson(wxUserService.queryWxUser(userVo.getId())));
		}
		return userVo;
	}

	@Override
	public List<Integer> queryUserIds(Integer role, Integer userId) {
		UserExample example = new UserExample();
		example.or().andIdEqualTo(userId);
		example.or().andFatherIdEqualTo(userId);
		List<Integer> userIds = new ArrayList<Integer>();
		if(role==Constants.ROLE.MD.getId()){		
			List<User> users = mapper.selectByExample(example);
			if(users.isEmpty()){
				userIds.add(userId);
				return userIds;
			}
			for(User user:users){
				userIds.add(user.getId());		
			}
			return userIds;
		}
		if(role==Constants.ROLE.OM.getId()){
			userIds.add(userId);
			return userIds;
		}
		return null;
	}
	
	
}
