package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.constants.Constants;
import com.yff.tuan.mapper.RoleMapper;
import com.yff.tuan.mapper.TrackMapper;
import com.yff.tuan.mapper.TrackTireCheckMapper;
import com.yff.tuan.mapper.TrackTireMapper;
import com.yff.tuan.mapper.TreadMapper;
import com.yff.tuan.mapper.UserMapper;
import com.yff.tuan.mapper.WxUserMapper;
import com.yff.tuan.model.CustomerExample;
import com.yff.tuan.model.Role;
import com.yff.tuan.model.RoleExample;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.TrackTireCheck;
import com.yff.tuan.model.TrackTireCheckExample;
import com.yff.tuan.model.Tread;
import com.yff.tuan.model.TreadExample;
import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.model.UserExample.Criteria;
import com.yff.tuan.model.WxUser;
import com.yff.tuan.model.WxUserExample;
import com.yff.tuan.service.RoleService;
import com.yff.tuan.service.TrackService;
import com.yff.tuan.service.TreadService;
import com.yff.tuan.service.UserService;
import com.yff.tuan.service.WxUserService;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.CustomerVo;
import com.yff.tuan.vo.UserVo;

@Component
public class TreadServiceImpl implements TreadService {
	@Autowired TreadMapper treadMapper;
	@Override
	public Tread find(Tread record) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Tread findByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int del(Integer id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(Tread record) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByPrimaryKeySelective(Tread record) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Tread> query(TreadExample example) {
		return treadMapper.selectByExample(example);
	}

}
