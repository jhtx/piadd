package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.RoleMapper;
import com.yff.tuan.model.Role;
import com.yff.tuan.model.RoleExample;
import com.yff.tuan.service.RoleService;
import com.yff.tuan.util.Page;

@Component
public class RoleServiceImpl implements RoleService{
	@Autowired
	RoleMapper roleMapper;

	@Override
	public Role findByPrimaryKey(Integer id) {
		if(null == id) {
			return null;
		}
		return roleMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insert(Role Role) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByPrimaryKeySelective(Role Role) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Role> query(RoleExample example) {
		return roleMapper.selectByExample(example);
	}

	@Override
	public Page<Role> queryByPage(RoleExample example, int pageNum) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
