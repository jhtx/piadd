package com.yff.tuan.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.yff.tuan.constants.Constants;
import com.yff.tuan.mapper.BrandMapper;
import com.yff.tuan.mapper.CustomerAddressMapper;
import com.yff.tuan.mapper.CustomerBrandMapper;
import com.yff.tuan.mapper.CustomerCommentMapper;
import com.yff.tuan.mapper.CustomerContactMapper;
import com.yff.tuan.mapper.CustomerEvnMapper;
import com.yff.tuan.mapper.CustomerInfoMapper;
import com.yff.tuan.mapper.CustomerMapper;
import com.yff.tuan.mapper.CustomerMarketMapper;
import com.yff.tuan.mapper.CustomerPointMapper;
import com.yff.tuan.mapper.CustomerProblemMapper;
import com.yff.tuan.mapper.CustomerRequireMapper;
import com.yff.tuan.mapper.CustomerServiceMapper;
import com.yff.tuan.mapper.CustomerTyreSizeMapper;
import com.yff.tuan.mapper.EvnMapper;
import com.yff.tuan.mapper.InfoMapper;
import com.yff.tuan.mapper.MarketMapper;
import com.yff.tuan.mapper.PointMapper;
import com.yff.tuan.mapper.ProblemMapper;
import com.yff.tuan.mapper.RequireMapper;
import com.yff.tuan.mapper.ServiceMapper;
import com.yff.tuan.mapper.TyreSizeMapper;
import com.yff.tuan.mapper.UserMapper;
import com.yff.tuan.model.Brand;
import com.yff.tuan.model.BrandExample;
import com.yff.tuan.model.Customer;
import com.yff.tuan.model.CustomerAddress;
import com.yff.tuan.model.CustomerAddressExample;
import com.yff.tuan.model.CustomerBrand;
import com.yff.tuan.model.CustomerBrandExample;
import com.yff.tuan.model.CustomerComment;
import com.yff.tuan.model.CustomerCommentExample;
import com.yff.tuan.model.CustomerContact;
import com.yff.tuan.model.CustomerContactExample;
import com.yff.tuan.model.CustomerEvn;
import com.yff.tuan.model.CustomerEvnExample;
import com.yff.tuan.model.CustomerExample;
import com.yff.tuan.model.CustomerInfo;
import com.yff.tuan.model.CustomerExample.Criteria;
import com.yff.tuan.model.CustomerInfoExample;
import com.yff.tuan.model.CustomerMarket;
import com.yff.tuan.model.CustomerMarketExample;
import com.yff.tuan.model.CustomerPoint;
import com.yff.tuan.model.CustomerPointExample;
import com.yff.tuan.model.CustomerProblem;
import com.yff.tuan.model.CustomerProblemExample;
import com.yff.tuan.model.CustomerRequire;
import com.yff.tuan.model.CustomerRequireExample;
import com.yff.tuan.model.CustomerServiceExample;
import com.yff.tuan.model.CustomerTyreSize;
import com.yff.tuan.model.CustomerTyreSizeExample;
import com.yff.tuan.model.Evn;
import com.yff.tuan.model.EvnExample;
import com.yff.tuan.model.Info;
import com.yff.tuan.model.InfoExample;
import com.yff.tuan.model.Market;
import com.yff.tuan.model.MarketExample;
import com.yff.tuan.model.Point;
import com.yff.tuan.model.PointExample;
import com.yff.tuan.model.Problem;
import com.yff.tuan.model.ProblemExample;
import com.yff.tuan.model.Require;
import com.yff.tuan.model.RequireExample;
import com.yff.tuan.model.Service;
import com.yff.tuan.model.ServiceExample;
import com.yff.tuan.model.TyreSize;
import com.yff.tuan.model.TyreSizeExample;
import com.yff.tuan.model.User;
import com.yff.tuan.service.CustomerService;
import com.yff.tuan.service.UserService;
import com.yff.tuan.util.DateUtil;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.CustomerAu;
import com.yff.tuan.vo.CustomerVo;

@Component
public class CustomerServiceImpl implements CustomerService {
	@Autowired CustomerMapper mapper;
	@Autowired UserMapper userMapper;
	@Autowired CustomerAddressMapper addressMapper;
	@Autowired CustomerContactMapper contactMapper;
	@Autowired CustomerTyreSizeMapper tyreSizeMapper;
	@Autowired CustomerBrandMapper brandMapper;
	@Autowired CustomerMarketMapper marketMapper;
	@Autowired CustomerEvnMapper evnMapper;
	@Autowired CustomerProblemMapper problemMapper;
	@Autowired CustomerRequireMapper requireMapper;
	@Autowired CustomerServiceMapper serviceMapper;
	@Autowired CustomerInfoMapper infoMapper;
	@Autowired CustomerPointMapper pointMapper;
	@Autowired BrandMapper bdMapper;
	@Autowired TyreSizeMapper teMapper;
	@Autowired MarketMapper mtMapper;
	@Autowired EvnMapper enMapper;
	@Autowired ProblemMapper pmMapper;
	@Autowired RequireMapper reMapper;
	@Autowired ServiceMapper seMapper;
	@Autowired InfoMapper ioMapper;
	@Autowired PointMapper ptMapper;
	@Autowired CustomerCommentMapper commentMapper;

	@Override
	public Customer findByPrimaryKey(Integer id) {
		if (null == id) {
			return null;
		}
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public CustomerVo detail(Integer id) {
		CustomerVo customerVo = null;
		if (null != id) {
			customerVo = mapper.findByPrimaryKey(id);
			if(null != customerVo.getStatus()) {
				customerVo.setStatusName(Constants.Status.getStatus(customerVo.getStatus()));
			}
			if(null != customerVo.getFailureReason()) {
				customerVo.setFailureReasonName(Constants.FailureReason.getFailureReason(customerVo.getFailureReason()));
			}
			if (null != customerVo) {
				CustomerAddressExample addressExample = new CustomerAddressExample();
				com.yff.tuan.model.CustomerAddressExample.Criteria addressCriteria = addressExample.createCriteria();
				addressCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setAddresses(addressMapper.selectByExample(addressExample));
				customerVo.setAddressesJson(new Gson().toJson(addressMapper.selectByExample(addressExample)));

				CustomerContactExample contactExample = new CustomerContactExample();
				com.yff.tuan.model.CustomerContactExample.Criteria contactCriteria = contactExample.createCriteria();
				contactCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setContacts(contactMapper.selectByExample(contactExample));
				customerVo.setContactsJson(new Gson().toJson(contactMapper.selectByExample(contactExample)));

				CustomerTyreSizeExample tyreSizeExample = new CustomerTyreSizeExample();
				com.yff.tuan.model.CustomerTyreSizeExample.Criteria tyreSizeCriteria = tyreSizeExample.createCriteria();
				tyreSizeCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setTyreSizes(tyreSizeMapper.selectByExample(tyreSizeExample));
				customerVo.setTyreSizesJson(new Gson().toJson(tyreSizeMapper.selectByExample(tyreSizeExample)));

				CustomerBrandExample brandExample = new CustomerBrandExample();
				com.yff.tuan.model.CustomerBrandExample.Criteria brandCriteria = brandExample.createCriteria();
				brandCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setBrands(brandMapper.selectByExample(brandExample));
				customerVo.setBrandsJson(new Gson().toJson(brandMapper.selectByExample(brandExample)));

				CustomerMarketExample marketExample = new CustomerMarketExample();
				com.yff.tuan.model.CustomerMarketExample.Criteria marketCriteria = marketExample.createCriteria();
				marketCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setMarkets(marketMapper.selectByExample(marketExample));
				customerVo.setMarketsJson(new Gson().toJson(marketMapper.selectByExample(marketExample)));

				CustomerEvnExample evnExample = new CustomerEvnExample();
				com.yff.tuan.model.CustomerEvnExample.Criteria evnCriteria = evnExample.createCriteria();
				evnCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setEvns(evnMapper.selectByExample(evnExample));
				customerVo.setEvnsJson(new Gson().toJson(evnMapper.selectByExample(evnExample)));

				CustomerProblemExample problemExample = new CustomerProblemExample();
				com.yff.tuan.model.CustomerProblemExample.Criteria problemCriteria = problemExample.createCriteria();
				problemCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setProblems(problemMapper.selectByExample(problemExample));
				customerVo.setProblemsJson(new Gson().toJson(problemMapper.selectByExample(problemExample)));

				CustomerRequireExample requireExample = new CustomerRequireExample();
				com.yff.tuan.model.CustomerRequireExample.Criteria requireCriteria = requireExample.createCriteria();
				requireCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setRequires(requireMapper.selectByExample(requireExample));
				customerVo.setRequiresJson(new Gson().toJson(requireMapper.selectByExample(requireExample)));

				CustomerServiceExample serviceExample = new CustomerServiceExample();
				com.yff.tuan.model.CustomerServiceExample.Criteria serviceCriteria = serviceExample.createCriteria();
				serviceCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setServices(serviceMapper.selectByExample(serviceExample));
				customerVo.setServicesJson(new Gson().toJson(serviceMapper.selectByExample(serviceExample)));

				CustomerInfoExample infoExample = new CustomerInfoExample();
				com.yff.tuan.model.CustomerInfoExample.Criteria infoCriteria = infoExample.createCriteria();
				infoCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setInfos(infoMapper.selectByExample(infoExample));
				customerVo.setInfosJson(new Gson().toJson(infoMapper.selectByExample(infoExample)));

				CustomerPointExample pointExample = new CustomerPointExample();
				com.yff.tuan.model.CustomerPointExample.Criteria pointCriteria = pointExample.createCriteria();
				pointCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setPoints(pointMapper.selectByExample(pointExample));
				customerVo.setPointsJson(new Gson().toJson(pointMapper.selectByExample(pointExample)));
			}

		}
		return customerVo;
	}

	@Override
	public CustomerVo wxDetail(Integer id) {
		CustomerVo customerVo = null;
		if (null != id) {
			customerVo = mapper.findByPrimaryKey(id);
			if(null != customerVo.getStatus()) {
				customerVo.setStatusName(Constants.Status.getStatus(customerVo.getStatus()));
			}
			if(null != customerVo.getFailureReason()) {
				customerVo.setFailureReasonName(Constants.FailureReason.getFailureReason(customerVo.getFailureReason()));
			}
			if (null != customerVo) {
				CustomerAddressExample addressExample = new CustomerAddressExample();
				com.yff.tuan.model.CustomerAddressExample.Criteria addressCriteria = addressExample.createCriteria();
				addressCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setAddresses(addressMapper.selectByExample(addressExample));

				CustomerContactExample contactExample = new CustomerContactExample();
				com.yff.tuan.model.CustomerContactExample.Criteria contactCriteria = contactExample.createCriteria();
				contactCriteria.andCustomerIdEqualTo(customerVo.getId());
				customerVo.setContacts(contactMapper.selectByExample(contactExample));

				CustomerTyreSize  tyreSize = new CustomerTyreSize();
				tyreSize.setCustomerId(customerVo.getId());
				customerVo.setTyreSizes(tyreSizeMapper.selectByExampleForName(tyreSize));

				CustomerBrand brand = new CustomerBrand();
				brand.setCustomerId(customerVo.getId());
				customerVo.setBrands(brandMapper.selectByExampleForName(brand));

				CustomerMarket market = new CustomerMarket();
				market.setCustomerId(customerVo.getId());
				customerVo.setMarkets(marketMapper.selectByExampleForName(market));

				CustomerEvn evn = new CustomerEvn();
				evn.setCustomerId(customerVo.getId());
				customerVo.setEvns(evnMapper.selectByExampleForName(evn));

				CustomerProblem problem = new CustomerProblem();
				problem.setCustomerId(customerVo.getId());
				customerVo.setProblems(problemMapper.selectByExampleForName(problem));

				CustomerRequire require = new CustomerRequire();
				require.setCustomerId(customerVo.getId());
				customerVo.setRequires(requireMapper.selectByExampleForName(require));

				com.yff.tuan.model.CustomerService service = new com.yff.tuan.model.CustomerService();
				service.setCustomerId(customerVo.getId());
				customerVo.setServices(serviceMapper.selectByExampleForName(service));

				CustomerInfo info = new CustomerInfo();
				info.setCustomerId(customerVo.getId());
				customerVo.setInfos(infoMapper.selectByExampleForName(info));

				CustomerPoint point = new CustomerPoint();
				point.setCustomerId(customerVo.getId());
				customerVo.setPoints(pointMapper.selectByExampleForName(point));
			}

		}
		return customerVo;
	}
	
	@Override
	public int insert(Customer Customer) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByPrimaryKeySelective(Customer Customer) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Page<CustomerVo> queryByPage(int pageNum,List<Integer> userIds) {
		String ids = "";
		CustomerExample example = new CustomerExample();
		if(null != userIds && !userIds.isEmpty()){
			example.createCriteria().andUserIdIn(userIds);
			for(int userId:userIds){
				ids = ids + userId+",";
			}
			ids = ids.substring(0, ids.length()-1);
		}
		int totalRecord = mapper.countByExample(example);
		
		Page<CustomerVo> page = new Page<CustomerVo>(pageNum, totalRecord);
		List<CustomerVo> customers = mapper.queryByPage(page.getStartIndex(), page.getPageSize(),ids);
		for (CustomerVo vo : customers) {
			if (null != vo.getStatus()) {
				vo.setStatusName(Constants.Status.getStatus(vo.getStatus()));
			}
		}
		page.setList(customers);
		return page;
	}

	@Override
	public List<Customer> query(Integer userId) {
		if (null == userId) {
			return new ArrayList<Customer>();
		}
		CustomerExample example = new CustomerExample();
		User user = userMapper.findByPrimaryKey(userId);
		if(user.getRoleId() != null && user.getRoleId()>1){
			Criteria criteria = example.createCriteria();
			criteria.andUserIdEqualTo(userId);
			example.setOrderByClause(" id desc ");
		}
		List<Customer> customers = mapper.selectByExample(example);
		for (Customer customer : customers) {
			customer.setStatusName(Constants.Status.getStatus(customer.getStatus()));
			customer.setTime(DateUtil.getFormatDate(customer.getCreateTime(), DateUtil.FORMAT_YYMMDD));
		}
		return customers;
	}

	@Override
	@Transactional
	public boolean add(CustomerVo vo) {
		try {
			if(null != vo) {
				Customer customer = new Customer();
				if(vo.getName() != null) customer.setName(vo.getName()) ;
				if(vo.getUserId() != null) customer.setUserId(vo.getUserId());
				if(vo.getStatus() != null) customer.setStatus(vo.getStatus()) ;
				if(vo.getFailureReason() != null) customer.setFailureReason(vo.getFailureReason()) ;
				if(vo.getSupplier() != null) customer.setSupplier(vo.getSupplier()) ;
				if(vo.getWholeCar42() != null) customer.setWholeCar42(vo.getWholeCar42()) ;
				if(vo.getWholeCar62() != null) customer.setWholeCar42(vo.getWholeCar62()) ;
				if(vo.getWholeCar84() != null) customer.setWholeCar42(vo.getWholeCar84()) ;
				if(vo.getDrawCar42() != null) customer.setDrawCar42(vo.getDrawCar42()) ;
				if(vo.getDrawCar62() != null) customer.setDrawCar62(vo.getDrawCar62()) ;
				if(vo.getDrawCar64() != null) customer.setDrawCar64(vo.getDrawCar64()) ;
				if(vo.getDragCar40() != null) customer.setDragCar40(vo.getDragCar40()) ;
				if(vo.getDragCar40D() != null) customer.setDragCar40D(vo.getDragCar40D()) ;
				if(vo.getDragCar60() != null) customer.setDragCar60(vo.getDragCar60()) ;
				if(vo.getDragCar60D() != null) customer.setDragCar60D(vo.getDragCar60D()) ;
				if(vo.getOldNewRate() != null) customer.setOldNewRate(vo.getOldNewRate()) ;
				if(vo.getFrontMiles() != null) customer.setFrontMiles(vo.getFrontMiles()) ;
				if(vo.getBackMiles() != null) customer.setBackMiles(vo.getBackMiles()) ;
				if(vo.getFrontPrice() != null) customer.setFrontPrice(vo.getFrontPrice()) ;
				if(vo.getBackPrice() != null) customer.setBackPrice(vo.getBackPrice()) ;
				if(vo.getFrontTread() != null) customer.setFrontTread(vo.getFrontTread()) ;
				if(vo.getBackTread() != null) customer.setBackTread(vo.getBackTread()) ;
				if(vo.getDayChangeAmt() != null) customer.setDayChangeAmt(vo.getDayChangeAmt()) ;
				if(vo.getDayDropAmt() != null) customer.setDayDropAmt(vo.getDayDropAmt());
				if(vo.getMonthAvgFee() != null) customer.setMonthAvgFee(vo.getMonthAvgFee()) ;
				if(vo.getAvgMiles() != null) customer.setAvgMiles(vo.getAvgMiles()) ;
				if(vo.getAvgOilFee() != null) customer.setAvgOilFee(vo.getAvgOilFee());
				if(vo.getLikeBrand() != null) customer.setLikeBrand(vo.getLikeBrand()) ;
				if(vo.getDoubleMatch() != null) customer.setDoubleMatch(vo.getDoubleMatch()) ;
				if(vo.getHasTest() != null) customer.setHasTest(vo.getHasTest()) ;
				if(vo.getCheckRate() != null) customer.setCheckRate(vo.getCheckRate()) ;
				if(vo.getPressStandard() != null) customer.setPressStandard(vo.getPressStandard()) ;
				if(vo.getFleetAmt() != null) customer.setFleetAmt(vo.getFleetAmt()) ;
				customer.setCreateTime(new Date()) ;
				customer.setUpdateTime(new Date()) ;
				int i = mapper.insert(customer);
				if(i>0) {
					List<CustomerAddress> addresses = vo.getAddresses();
					if(null != addresses) {
						for(CustomerAddress address:addresses) {
							address.setCustomerId(customer.getId());
							addressMapper.insert(address);
						}
					}
					
					List<CustomerContact> contacts = vo.getContacts();
					if(null != contacts) {
						for(CustomerContact contact:contacts) {
							contact.setCustomerId(customer.getId());
							contactMapper.insert(contact);
						}
					}
					
					List<CustomerTyreSize> tyreSizes = vo.getTyreSizes();
					if(null != tyreSizes) {
						for(CustomerTyreSize tyreSize:tyreSizes) {
							tyreSize.setCustomerId(customer.getId());
							tyreSizeMapper.insert(tyreSize);
						}
					}
					
					List<CustomerBrand> brands = vo.getBrands();
					if(null != brands) {
						for(CustomerBrand brand:brands) {
							brand.setCustomerId(customer.getId());
							brandMapper.insert(brand);
						}
					}
					
					List<CustomerMarket> markets = vo.getMarkets();
					if(null != markets) {
						for(CustomerMarket market:markets) {
							market.setCustomerId(customer.getId());
							marketMapper.insert(market);
						}
					}
					
					List<CustomerEvn> evns = vo.getEvns();
					if(null != evns) {
						for(CustomerEvn evn:evns) {
							evn.setCustomerId(customer.getId());
							evnMapper.insert(evn);
						}
					}
					
					List<CustomerInfo> infos = vo.getInfos();
					if(null != infos) {
						for(CustomerInfo info:infos) {
							info.setCustomerId(customer.getId());
							infoMapper.insert(info);
						}
					}
					
					List<CustomerProblem> problems = vo.getProblems();
					if(null != problems) {
						for(CustomerProblem problem:problems) {
							problem.setCustomerId(customer.getId());
							problemMapper.insert(problem);
						}
					}
					
					List<CustomerRequire> requires = vo.getRequires();
					if(null != problems) {
						for(CustomerRequire require:requires) {
							require.setCustomerId(customer.getId());
							requireMapper.insert(require);
						}
					}
					
					List<com.yff.tuan.model.CustomerService> services = vo.getServices();
					if(null != services) {
						for(com.yff.tuan.model.CustomerService csService:services) {
							csService.setCustomerId(customer.getId());
							serviceMapper.insert(csService);
						}
					}

					List<CustomerPoint> points = vo.getPoints();
					if(null != points) {
						for(CustomerPoint point:points) {
							point.setCustomerId(customer.getId());
							pointMapper.insert(point);
						}
					}
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Brand> queryBrand() {
		return bdMapper.selectByExample(new BrandExample());
	}

	@Override
	public List<TyreSize> queryTyreSize() {
		return teMapper.selectByExample(new TyreSizeExample());
	}

	@Override
	public List<Market> queryMarket() {
		return mtMapper.selectByExample(new MarketExample());
	}

	@Override
	public List<Evn> queryEvn() {
		return enMapper.selectByExample(new EvnExample());
	}

	@Override
	public List<Problem> queryProblem() {
		return pmMapper.selectByExample(new ProblemExample());
	}

	@Override
	public List<Info> queryInfo() {
		return ioMapper.selectByExample(new InfoExample());
	}

	@Override
	public List<Require> queryRequire() {
		return reMapper.selectByExample(new RequireExample());
	}

	@Override
	public List<Service> queryService() {
		return seMapper.selectByExample(new ServiceExample());
	}

	@Override
	public List<Point> queryPoint() {
		return ptMapper.selectByExample(new PointExample());
	}
	
	@Override
	public List<CustomerComment> queryComment(Integer id) {
		CustomerCommentExample example = new CustomerCommentExample();
		com.yff.tuan.model.CustomerCommentExample.Criteria criteria = example.createCriteria();
		criteria.andCustomerIdEqualTo(id);
		return commentMapper.queryComment(example);
	}

	@Override
	public List<Customer> query(CustomerExample example) {
		return mapper.selectByExample(example);
	}

	@Override
	@Transactional
	public boolean insertCustomerAu(CustomerAu au) {
		if(null != au) {
			int i = 0;
			Customer customer = au.getCustomer();
			if(null != customer) {
				customer.setCreateTime(new Date());
				customer.setUpdateTime(new Date());
				i = mapper.insert(customer);
				if(i>0) {
					List<CustomerAddress> addresses = au.getAddresses();
					if(null != addresses) {
						for(CustomerAddress address:addresses) {
							address.setCustomerId(customer.getId());
							addressMapper.insert(address);
						}
					}
					
					List<CustomerContact> contacts = au.getContacts();
					if(null != contacts) {
						for(CustomerContact contact:contacts) {
							contact.setCustomerId(customer.getId());
							contactMapper.insert(contact);
						}
					}
					
					List<CustomerTyreSize> tyreSizes = au.getTyreSizes();
					if(null != tyreSizes) {
						for(CustomerTyreSize tyreSize:tyreSizes) {
							tyreSize.setCustomerId(customer.getId());
							tyreSizeMapper.insert(tyreSize);
						}
					}
					
					List<CustomerBrand> brands = au.getBrands();
					if(null != brands) {
						for(CustomerBrand brand:brands) {
							brand.setCustomerId(customer.getId());
							brandMapper.insert(brand);
						}
					}
					
					List<CustomerMarket> markets = au.getMarkets();
					if(null != markets) {
						for(CustomerMarket market:markets) {
							market.setCustomerId(customer.getId());
							marketMapper.insert(market);
						}
					}
					
					List<CustomerEvn> evns = au.getEvns();
					if(null != evns) {
						for(CustomerEvn evn:evns) {
							evn.setCustomerId(customer.getId());
							evnMapper.insert(evn);
						}
					}
					
					List<CustomerInfo> infos = au.getInfos();
					if(null != infos) {
						for(CustomerInfo info:infos) {
							info.setCustomerId(customer.getId());
							infoMapper.insert(info);
						}
					}
					
					List<CustomerProblem> problems = au.getProblems();
					if(null != problems) {
						for(CustomerProblem problem:problems) {
							problem.setCustomerId(customer.getId());
							problemMapper.insert(problem);
						}
					}
					
					List<CustomerRequire> requires = au.getRequires();
					if(null != problems) {
						for(CustomerRequire require:requires) {
							require.setCustomerId(customer.getId());
							requireMapper.insert(require);
						}
					}
					
					List<com.yff.tuan.model.CustomerService> services = au.getServices();
					if(null != services) {
						for(com.yff.tuan.model.CustomerService csService:services) {
							csService.setCustomerId(customer.getId());
							serviceMapper.insert(csService);
						}
					}

					List<CustomerPoint> points = au.getPoints();
					if(null != points) {
						for(CustomerPoint point:points) {
							point.setCustomerId(customer.getId());
							pointMapper.insert(point);
						}
					}
				}
			}
			return true;
		}	
		return false;
	}
	
	@Override
	@Transactional
	public boolean updateCustomerAu(CustomerAu au) {
		if(null != au) {
			int i = 0;
			Customer customer = au.getCustomer();
			if(null != customer) {
				customer.setCreateTime(new Date());
				customer.setUpdateTime(new Date());
				i = mapper.updateByPrimaryKeySelective(customer);
				if(i>0) {
					CustomerAddressExample addressExample = new CustomerAddressExample();
					com.yff.tuan.model.CustomerAddressExample.Criteria addressCriteria = addressExample.createCriteria();
					addressCriteria.andCustomerIdEqualTo(customer.getId());
					addressMapper.deleteByExample(addressExample);
					List<CustomerAddress> addresses = au.getAddresses();
					if(null != addresses) {
						for(CustomerAddress address:addresses) {
							address.setCustomerId(customer.getId());
							addressMapper.insert(address);
						}
					}
					
					CustomerContactExample contactExample = new CustomerContactExample();
					com.yff.tuan.model.CustomerContactExample.Criteria contactCriteria = contactExample.createCriteria();
					contactCriteria.andCustomerIdEqualTo(customer.getId());
					contactMapper.deleteByExample(contactExample);
					List<CustomerContact> contacts = au.getContacts();
					if(null != contacts) {
						for(CustomerContact contact:contacts) {
							contact.setCustomerId(customer.getId());
							contactMapper.insert(contact);
						}
					}
					
					CustomerTyreSizeExample tyreSizeExample = new CustomerTyreSizeExample();
					com.yff.tuan.model.CustomerTyreSizeExample.Criteria tyreSizeCriteria = tyreSizeExample.createCriteria();
					tyreSizeCriteria.andCustomerIdEqualTo(customer.getId());
					tyreSizeMapper.deleteByExample(tyreSizeExample);
					List<CustomerTyreSize> tyreSizes = au.getTyreSizes();
					if(null != tyreSizes) {
						for(CustomerTyreSize tyreSize:tyreSizes) {
							tyreSize.setCustomerId(customer.getId());
							tyreSizeMapper.insert(tyreSize);
						}
					}
					
					CustomerBrandExample brandExample = new CustomerBrandExample();
					com.yff.tuan.model.CustomerBrandExample.Criteria brandCriteria = brandExample.createCriteria();
					brandCriteria.andCustomerIdEqualTo(customer.getId());
					brandMapper.deleteByExample(brandExample);
					List<CustomerBrand> brands = au.getBrands();
					if(null != brands) {
						for(CustomerBrand brand:brands) {
							brand.setCustomerId(customer.getId());
							brandMapper.insert(brand);
						}
					}
					
					CustomerMarketExample marketExample = new CustomerMarketExample();
					com.yff.tuan.model.CustomerMarketExample.Criteria marketCriteria = marketExample.createCriteria();
					marketCriteria.andCustomerIdEqualTo(customer.getId());
					marketMapper.deleteByExample(marketExample);
					List<CustomerMarket> markets = au.getMarkets();
					if(null != markets) {
						for(CustomerMarket market:markets) {
							market.setCustomerId(customer.getId());
							marketMapper.insert(market);
						}
					}
					
					CustomerEvnExample evnExample = new CustomerEvnExample();
					com.yff.tuan.model.CustomerEvnExample.Criteria evnCriteria = evnExample.createCriteria();
					evnCriteria.andCustomerIdEqualTo(customer.getId());
					evnMapper.deleteByExample(evnExample);
					List<CustomerEvn> evns = au.getEvns();
					if(null != evns) {
						for(CustomerEvn evn:evns) {
							evn.setCustomerId(customer.getId());
							evnMapper.insert(evn);
						}
					}
					
					CustomerInfoExample infoExample = new CustomerInfoExample();
					com.yff.tuan.model.CustomerInfoExample.Criteria infoCriteria = infoExample.createCriteria();
					infoCriteria.andCustomerIdEqualTo(customer.getId());
					infoMapper.deleteByExample(infoExample);
					List<CustomerInfo> infos = au.getInfos();
					if(null != infos) {
						for(CustomerInfo info:infos) {
							info.setCustomerId(customer.getId());
							infoMapper.insert(info);
						}
					}
					
					CustomerProblemExample problemExample = new CustomerProblemExample();
					com.yff.tuan.model.CustomerProblemExample.Criteria problemCriteria = problemExample.createCriteria();
					problemCriteria.andCustomerIdEqualTo(customer.getId());
					problemMapper.deleteByExample(problemExample);
					List<CustomerProblem> problems = au.getProblems();
					if(null != problems) {
						for(CustomerProblem problem:problems) {
							problem.setCustomerId(customer.getId());
							problemMapper.insert(problem);
						}
					}
					
					CustomerRequireExample requireExample = new CustomerRequireExample();
					com.yff.tuan.model.CustomerRequireExample.Criteria requireCriteria = requireExample.createCriteria();
					requireCriteria.andCustomerIdEqualTo(customer.getId());
					requireMapper.deleteByExample(requireExample);
					List<CustomerRequire> requires = au.getRequires();
					if(null != problems) {
						for(CustomerRequire require:requires) {
							require.setCustomerId(customer.getId());
							requireMapper.insert(require);
						}
					}
					
					CustomerServiceExample csServiceExample = new CustomerServiceExample();
					com.yff.tuan.model.CustomerServiceExample.Criteria csServiceCriteria = csServiceExample.createCriteria();
					csServiceCriteria.andCustomerIdEqualTo(customer.getId());
					serviceMapper.deleteByExample(csServiceExample);
					List<com.yff.tuan.model.CustomerService> services = au.getServices();
					if(null != services) {
						for(com.yff.tuan.model.CustomerService csService:services) {
							csService.setCustomerId(customer.getId());
							serviceMapper.insert(csService);
						}
					}
					
					CustomerPointExample pointExample = new CustomerPointExample();
					com.yff.tuan.model.CustomerPointExample.Criteria pointCriteria = pointExample.createCriteria();
					pointCriteria.andCustomerIdEqualTo(customer.getId());
					pointMapper.deleteByExample(pointExample);
					List<CustomerPoint> points = au.getPoints();
					if(null != points) {
						for(CustomerPoint point:points) {
							point.setCustomerId(customer.getId());
							pointMapper.insert(point);
						}
					}
				}
			}
			return true;
		}	
		return false;
	}

	@Override
	public boolean insertCustomerComment(CustomerComment comment) {
		int i = commentMapper.insert(comment);
		return i>0?true:false;
	}
}
