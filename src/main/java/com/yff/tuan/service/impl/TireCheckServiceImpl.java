package com.yff.tuan.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CarBindMapper;
import com.yff.tuan.mapper.CarBindTyreMapper;
import com.yff.tuan.mapper.CarMapper;
import com.yff.tuan.mapper.CarWarnIndexMapper;
import com.yff.tuan.mapper.CarWarnMapper;
import com.yff.tuan.mapper.CarWarningMapper;
import com.yff.tuan.mapper.CoUserMapper;
import com.yff.tuan.mapper.TireCheckMapper;
import com.yff.tuan.mapper.TireCheckWarningMapper;
import com.yff.tuan.model.Car;
import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarBindTyreExample;
import com.yff.tuan.model.CarWarn;
import com.yff.tuan.model.CarWarnExample;
import com.yff.tuan.model.CarWarning;
import com.yff.tuan.model.CarWarningExample;
import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.TireCheckExample;
import com.yff.tuan.model.TireCheckExample.Criteria;
import com.yff.tuan.service.TireCheckService;
import com.yff.tuan.util.DateUtil;
import com.yff.tuan.util.Page;

@Component
public class TireCheckServiceImpl implements TireCheckService{
	@Autowired
	TireCheckMapper mapper;
	@Autowired
	TireCheckWarningMapper checkWarningMapper;
	@Autowired
	CarWarnMapper carWarnMapper;
	@Autowired
	CarBindTyreMapper carBindTyreMapper;
	@Autowired
	CarBindMapper bindMapper;
	@Autowired
	CarMapper carMapper;
	@Autowired
	CarWarningMapper carWarningMapper;
	@Autowired
	CarWarnIndexMapper carWarnIndexMapper;
	@Autowired
	CoUserMapper userMapper;
	
	@Override
	public boolean save(TireCheck tireCheck) {
		Date date = tireCheck.getCheckTime();
		if(DateUtil.getFormatDate(date, DateUtil.FORMAT_YYMMDDHHMMSS).equals("2017-01-01 00:00:00") ) {
			return false;
		}
		TireCheckExample example = new TireCheckExample();
		Criteria criteria = example.createCriteria();
		criteria.andProductIdEqualTo(tireCheck.getProductId());
		List<TireCheck> checks = mapper.selectByExample(example);
		CarBindTyreExample bindTyreExample = new CarBindTyreExample();
		bindTyreExample.createCriteria().andProductIdEqualTo(tireCheck.getProductId());
		List<CarBindTyre> bindTyres = carBindTyreMapper.selectByExample(bindTyreExample);
		Car car = null;
		if(null != bindTyres && !bindTyres.isEmpty()) {
			tireCheck.setTyreId(bindTyres.get(0).getTireId());
			tireCheck.setCarId(bindTyres.get(0).getCarId());
			car = carMapper.selectByPrimaryKey(bindTyres.get(0).getCarId());
		}
		
		/*if(tireCheck.getAirWarning()>0 || tireCheck.getPressureWarning()>0 || tireCheck.getSignalWarning()>0 || tireCheck.getTemperatureWarning()>0) {
			TireCheckWarning record = new TireCheckWarning();
			record.setAirWarning(tireCheck.getAirWarning());
			record.setCheckTime(tireCheck.getCheckTime());
			record.setPressure(tireCheck.getPressure());
			record.setPressureWarning(tireCheck.getPressureWarning());
			record.setProductId(tireCheck.getProductId());
			record.setSignalWarning(tireCheck.getSignalWarning());
			record.setTemperature(tireCheck.getTemperature());
			record.setTemperatureWarning(tireCheck.getTemperatureWarning());
			record.setTyreNo(tireCheck.getTyreNo());
			checkWarningMapper.insert(record);
		}*/
		
		boolean isInsert = false;
		if(checks.isEmpty()) {
			isInsert = mapper.insert(tireCheck)>0?true:false;
		}else {
			tireCheck.setId(checks.get(0).getId());
			isInsert = mapper.updateByPrimaryKey(tireCheck)>0?true:false;
		}
		if(isInsert && car != null) {
			insertCarWarn(car.getCoUserId(), car.getId(), tireCheck.getProductId(), tireCheck.getPressure(), tireCheck.getPressureWarning());
		}
		return isInsert;
	}

	private void insertCarWarn(Integer userId,Integer carId ,String productId,double pressure,double temperature) {
		try {
			CoUser coUser = userMapper.selectByPrimaryKey(userId);
			if(coUser.getParentId() != null && coUser.getParentId() != 0) {
				userId = coUser.getParentId();
			}
			
			CarWarnExample example = new CarWarnExample();
			example.createCriteria().andUserIdEqualTo(userId);
			List<CarWarn> carWarns = carWarnMapper.selectByExample(example );
			if(null != carWarns && !carWarns.isEmpty()) {
				Double sPressure = carWarns.get(0).getPressure();
				Double sTemperature = carWarns.get(0).getTemperature();
				Double sTemperatureWarning = carWarns.get(0).getTemperatureWarning();
				
				/*int pressureUnder10Warning = pressure < (sPressure-sPressure*0.1)?1:0;*/
				int pressureUnder20Warning = pressure < (sPressure-sPressure*0.2)?1:0;
				int pressureUnder30Warning = pressure < (sPressure-sPressure*0.3)?1:0;
				int pressureUpper10Warning = pressure > (sPressure+sPressure*0.2)?1:0;
				int pressureUpper20Warning = pressure > (sPressure+sPressure*0.3)?1:0;
				int temperatureUpper10Warning = temperature > sTemperature?1:0;
			    int temperatureUpper20Warning = temperature > sTemperatureWarning?1:0;
						
				if(/*pressureUnder10Warning+*/pressureUnder20Warning+pressureUnder30Warning+pressureUpper10Warning+pressureUpper20Warning+temperatureUpper10Warning+temperatureUpper20Warning != 0 ) {
					CarWarning carWarning = new CarWarning();
					carWarning.setProductId(productId);
					carWarning.setCarId(carId);
					carWarning.setWarnDate(new Date());
					carWarning.setPressureUnder10(""+pressure);
					carWarning.setPressureUnder10Warning(0);
					carWarning.setPressureUnder20(""+pressure);
					carWarning.setPressureUnder20Warning(pressureUnder20Warning);
					carWarning.setPressureUnder30(""+pressure);
					carWarning.setPressureUnder30Warning(pressureUnder30Warning);
					carWarning.setPressureUpper10(""+pressure);
					carWarning.setPressureUpper10Warning(pressureUpper10Warning);
					carWarning.setPressureUpper20(""+pressure);
					carWarning.setPressureUpper20Warning(pressureUpper20Warning);
					carWarning.setTemperatureUpper10(""+temperature);
					carWarning.setTemperatureUpper10Warning(temperatureUpper10Warning);
					carWarning.setTemperatureUpper20(""+temperature);
					carWarning.setTemperatureUpper20Warning(temperatureUpper20Warning);
					
					CarWarningExample carWarningExample = new CarWarningExample();
					carWarningExample.createCriteria().andProductIdEqualTo(productId);
					List<CarWarning> carWarnings = carWarningMapper.selectByExample(carWarningExample);
					if(null != carWarnings && !carWarnings.isEmpty()) {
						carWarning.setId(carWarnings.get(0).getId());
						carWarningMapper.updateByPrimaryKey(carWarning);
					}else {
						carWarningMapper.insert(carWarning);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public Page<TireCheck> queryByPage(TireCheckExample example, Integer pageNum) {
		int count = mapper.countByExample(example);
		Page<TireCheck> page = new Page<TireCheck>(pageNum, count);
		List<TireCheck> tireChecks = mapper.queryByPage(page.getStartIndex(), page.getPageSize());
		page.setList(tireChecks);
		return page;
	}
	
}
