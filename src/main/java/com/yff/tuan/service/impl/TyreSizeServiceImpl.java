package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.TyreSizeMapper;
import com.yff.tuan.mapper.BrokenCheckMapper;
import com.yff.tuan.mapper.BrokenMapper;
import com.yff.tuan.mapper.BrokenProblemMapper;
import com.yff.tuan.mapper.TreadMapper;
import com.yff.tuan.model.TyreSize;
import com.yff.tuan.model.TyreSizeExample;
import com.yff.tuan.model.Broken;
import com.yff.tuan.model.BrokenCheck;
import com.yff.tuan.model.BrokenExample;
import com.yff.tuan.model.BrokenProblem;
import com.yff.tuan.model.BrokenProblemExample;
import com.yff.tuan.model.TyreSize;
import com.yff.tuan.model.Tread;
import com.yff.tuan.model.TreadExample;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.Tread;
import com.yff.tuan.service.TyreSizeService;
import com.yff.tuan.service.BrokenService;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.BrokenObjectCheck;

@Component
public class TyreSizeServiceImpl implements TyreSizeService {
	@Autowired
	TyreSizeMapper tyreSizeMapper;
	@Autowired
	TreadMapper treadMapper;
	
	@Override
	public TyreSize find(TyreSize broken) {
		
		return tyreSizeMapper.selectByPrimaryKey(broken.getId());
	}
	@Override
	public int insert(TyreSize broken) {
		return tyreSizeMapper.insert(broken);
	}
	@Override
	public int updateByPrimaryKeySelective(TyreSize broken) {
		return tyreSizeMapper.updateByPrimaryKeySelective(broken);
	}
	@Override
	public List<TyreSize> query(TyreSizeExample example) {
		return tyreSizeMapper.selectByExample(example);
	}
	@Override
	public TyreSize findByPrimaryKey(Integer id) {
		return tyreSizeMapper.selectByPrimaryKey(id);
	}
	@Override
	public boolean add(TyreSize tyreSize) {
		return tyreSizeMapper.insert(tyreSize)>0?true:false;
	}
	@Override
	public boolean update(TyreSize tyreSize) {
		return tyreSizeMapper.updateByPrimaryKeySelective(tyreSize)>0?true:false;
	}

}
