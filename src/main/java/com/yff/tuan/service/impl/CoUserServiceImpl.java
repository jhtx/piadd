package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CoUserMapper;
import com.yff.tuan.model.CoUser;
import com.yff.tuan.model.CoUserExample;
import com.yff.tuan.service.CoUserService;
import com.yff.tuan.util.Page;

@Component
public class CoUserServiceImpl implements CoUserService {
	@Autowired
	CoUserMapper coUserMapper;

	@Override
	public boolean checkExist(CoUserExample example) {
		return coUserMapper.countByExample(example)>0?true:false;
	}


	@Override
	public CoUser findByPrimaryKey(Integer id) {
		return coUserMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insert(CoUser coUser) {
		return coUserMapper.insert(coUser);
	}

	@Override
	public int updateByPrimaryKeySelective(CoUser coUser) {
		return coUserMapper.updateByPrimaryKeySelective(coUser);
	}

	@Override
	public List<CoUser> query(CoUserExample example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<CoUser> queryByPage(CoUserExample example, Integer pageNum) {
		int count = coUserMapper.countByExample(example);
		Page<CoUser> page = new Page<CoUser>(pageNum, count);
		List<CoUser> coUsers = coUserMapper.queryByPage(page.getStartIndex(), page.getPageSize());
		page.setList(coUsers);
		return page;
	}


	@Override
	public int del(Integer id) {
		return coUserMapper.deleteByPrimaryKey(id);
	}

}
