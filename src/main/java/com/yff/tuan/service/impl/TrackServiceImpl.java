package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.constants.Constants;
import com.yff.tuan.mapper.RoleMapper;
import com.yff.tuan.mapper.TrackMapper;
import com.yff.tuan.mapper.TrackTireCheckMapper;
import com.yff.tuan.mapper.TrackTireMapper;
import com.yff.tuan.mapper.UserMapper;
import com.yff.tuan.mapper.WxUserMapper;
import com.yff.tuan.model.CustomerExample;
import com.yff.tuan.model.Role;
import com.yff.tuan.model.RoleExample;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.TrackTireCheck;
import com.yff.tuan.model.TrackTireCheckExample;
import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.model.UserExample.Criteria;
import com.yff.tuan.model.WxUser;
import com.yff.tuan.model.WxUserExample;
import com.yff.tuan.service.RoleService;
import com.yff.tuan.service.TrackService;
import com.yff.tuan.service.UserService;
import com.yff.tuan.service.WxUserService;
import com.yff.tuan.util.MathUtil;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.CustomerVo;
import com.yff.tuan.vo.UserVo;

@Component
public class TrackServiceImpl implements TrackService {
	@Autowired
	TrackMapper trackMapper;
	@Autowired
	TrackTireMapper trackTireMapper;
	@Autowired
	TrackTireCheckMapper trackTireCheckMapper;

	@Override
	public Track find(Track Track) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Track findByPrimaryKey(Integer id) {
		if (null == id) {
			return null;
		}
		Track track = trackMapper.findByPrimaryKey(id);
		if (null != track) {
			List<TrackTire> trackTires = trackTireMapper.queryByTrackId(id);
			track.setTires(trackTires);
		}
		return track;
	}
	
	
	@Override
	public int del(Integer id) {
		return trackMapper.deleteByPrimaryKey(id);
	}
	
	@Override
	public int insert(Track record) {
		return trackMapper.insert(record);
	}

	@Override
	public int updateByPrimaryKeySelective(Track record) {
		return trackMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<Track> query(TrackExample example) {
		return trackMapper.selectByExample(new TrackExample());
	}

	@Override
	public Page<Track> queryByPage(TrackExample example, Integer pageNum) {
		List<Track> list = trackMapper.selectByExample(new TrackExample());
		Page<Track> page = new Page<Track>(pageNum, null == list ? 0 : list.size());
		List<Track> tracks = trackMapper.queryByPage(page.getStartIndex(), page.getPageSize());
		/*
		 * for (Track vo : tracks) { if (null != vo.getStatus()) {
		 * vo.setStatusName(Constants.Status.getStatus(vo.getStatus())); } }
		 */
		page.setList(tracks);
		return page;
	}

	@Override
	public TrackTire findTrackTireByPrimaryKey(Integer id) {
		if (null == id) {
			return null;
		}
		TrackTire trackTire = trackTireMapper.findTrackTireByPrimaryKey(id);
		if (null != trackTire) {
			TrackTireCheckExample tireCheckExample = new TrackTireCheckExample();
			com.yff.tuan.model.TrackTireCheckExample.Criteria criteria = tireCheckExample.createCriteria();
			criteria.andTireIdEqualTo(id);
			List<TrackTireCheck> trackTireChecks = trackTireCheckMapper.selectByExample(tireCheckExample);
			if (null != trackTireChecks) {
				trackTire.setTrackTireChecks(trackTireChecks);
			}
		}
		return trackTire;
	}

	@Override
	public int insertTrackTire(TrackTire trackTire) {
		return trackTireMapper.insert(trackTire);
	}

	@Override
	public int insertTrackTireCheck(TrackTireCheck trackTireCheck) {
		TrackTire trackTire = trackTireMapper.findTrackTireByPrimaryKey(trackTireCheck.getTireId());
		Track track = trackMapper.findByPrimaryKey(trackTire.getTrackId());
		double expectMiles = MathUtil.parseDouble( (trackTireCheck.getMiles()-track.getBaseMiles())/(trackTire.getTread()-trackTireCheck.getTread()))*(trackTireCheck.getTread()-2);
		double baseExpectMiles = MathUtil.parseDouble((trackTireCheck.getMiles()-track.getBaseMiles())/(trackTire.getTread()-trackTireCheck.getTread()))*(trackTire.getTread()-2);
		trackTire.setMiles(baseExpectMiles);
		trackTireCheck.setExpectMiles(expectMiles);
		trackTireMapper.updateByPrimaryKey(trackTire);
		return trackTireCheckMapper.insert(trackTireCheck);
	}
	
	
	@Override
	public Track exportByPrimaryKey(Integer id) {
		if (null == id) {
			return null;
		}
		Track track = trackMapper.findByPrimaryKey(id);
		if (null != track) {
			List<TrackTire> trackTires = trackTireMapper.queryByTrackId(id);
			for(TrackTire trackTire: trackTires) {
				TrackTireCheckExample tireCheckExample = new TrackTireCheckExample();
				com.yff.tuan.model.TrackTireCheckExample.Criteria criteria = tireCheckExample.createCriteria();
				criteria.andTireIdEqualTo(trackTire.getId());
				List<TrackTireCheck> trackTireChecks = trackTireCheckMapper.selectByExample(tireCheckExample);
				if (null != trackTireChecks) {
					trackTire.setTrackTireChecks(trackTireChecks);
				}
			}
			track.setTires(trackTires);
		}
		return track;
	}

	@Override
	public List<Track> queryForWx() {
		return trackMapper.queryForWx();
	}
}
