package com.yff.tuan.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CheckMapper;
import com.yff.tuan.mapper.CheckVehicleMapper;
import com.yff.tuan.mapper.CheckVehicleTireMapper;
import com.yff.tuan.mapper.CheckVehicleTireWarnMapper;
import com.yff.tuan.model.Check;
import com.yff.tuan.model.CheckExample;
import com.yff.tuan.model.CheckVehicle;
import com.yff.tuan.model.CheckVehicleExample;
import com.yff.tuan.model.CheckVehicleTire;
import com.yff.tuan.model.CheckVehicleTireWarn;
import com.yff.tuan.model.CheckVehicleTireWarnExample;
import com.yff.tuan.model.CheckVehicleTireWarnExample.Criteria;
import com.yff.tuan.model.Customer;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.VehicleTypeTire;
import com.yff.tuan.service.CheckService;
import com.yff.tuan.service.CustomerService;
import com.yff.tuan.service.VehicleTypeService;
import com.yff.tuan.util.DateUtil;
import com.yff.tuan.util.MathUtil;
import com.yff.tuan.util.Page;
import com.yff.tuan.util.StringUtil;
import com.yff.tuan.vo.BrokenReport;
import com.yff.tuan.vo.CVehicle;
import com.yff.tuan.vo.checkcar;

@Component
public class CheckServiceImpl implements CheckService {
	@Autowired
	CheckMapper checkMapper;
	@Autowired
	CheckVehicleMapper checkVehicleMapper;
	@Autowired
	CheckVehicleTireMapper checkVehicleTireMapper;
	@Autowired
	CheckVehicleTireWarnMapper checkVehicleTireWarnMapper;
	@Autowired
	CustomerService customerService;
	@Autowired
	VehicleTypeService vehicleTypeService;
	
	
	@Override
	public Check find(Check check) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Check findByPrimaryKey(Integer id) {
		 Check check = checkMapper.findByPrimaryKey(id);
		 if(null != check) {
			 check.setCheckVehicles(checkVehicleMapper.queryByCheckId(check.getId()));
		 }
		 return check;
	}

	@Override
	public int insert(Check check) {
		return checkMapper.insert(check);
	}

	@Override
	public int updateByPrimaryKeySelective(Check check) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Check> query(CheckExample example) {
		return checkMapper.selectByExample(example);
	}

	@Override
	public Page<Check> queryByPage(CheckExample example, Integer pageNum) {
		int count = checkMapper.countByExample(example);
		Page<Check> page = new Page<Check>(pageNum, count);
		List<Check> checks = checkMapper.queryByPage(page.getStartIndex(), page.getPageSize());
		page.setList(checks);
		return page;
	}
	
	@Override
	public List<Check> querywx() {
		List<Check> checks = checkMapper.querywx();
		return checks;
	}
	
	@Override
	public List<CheckVehicleTire> queryTireByVehicleId(Integer vehicleId) {
		List<CheckVehicleTire> checkVehicleTires = checkVehicleTireMapper.queryTireByVehicleId(vehicleId);
		for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
			CheckVehicleTireWarnExample example = new CheckVehicleTireWarnExample();
			Criteria createCriteria = example.createCriteria();
			createCriteria.andCheckVehicleTireIdEqualTo(checkVehicleTire.getId());
			List<CheckVehicleTireWarn> tireWarns = checkVehicleTireWarnMapper.selectByExample(example);
			if(null != tireWarns && !tireWarns.isEmpty()) {
				checkVehicleTire.setVehicleTypeTireWarns(tireWarns);
			}
		}
		return checkVehicleTires;
	}

	@Override
	public int insertCheckVehicle(CheckVehicle checkVehicle) {
		return checkVehicleMapper.insert(checkVehicle);
	}

	@Override
	public List<CheckVehicle> queryCheckVehicle(CheckVehicleExample example) {
		return checkVehicleMapper.selectByExample(example);
	}
	
	@Override
	public CheckVehicle findCheckVehicle(Integer id) {
		return checkVehicleMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insertCheckVehicleTire(List<CheckVehicleTire> checkVehicleTires) {
		int i =0;
		if(null != checkVehicleTires && !checkVehicleTires.isEmpty()) {
			for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
				checkVehicleTireMapper.insert(checkVehicleTire);
				Integer id = checkVehicleTire.getId();
				List<CheckVehicleTireWarn> vehicleTypeTireWarns = checkVehicleTire.getVehicleTypeTireWarns();
				if(null != vehicleTypeTireWarns && !vehicleTypeTireWarns.isEmpty()) {
					for(CheckVehicleTireWarn checkVehicleTireWarn:vehicleTypeTireWarns) {
						checkVehicleTireWarn.setCheckVehicleTireId(id);
						checkVehicleTireWarnMapper.insert(checkVehicleTireWarn);
					}
				}
				i ++;
			}
		}
		return i;
	}

	@Override
	public BrokenReport checkReport(Integer id) {
		Check check = checkMapper.findByPrimaryKey(id);
		int vType1Count = 0;
		int vType2Count = 0;
		int vType3Count = 0;
		int vDrive1Count = 0;
		int vDrive2Count = 0;
		int vDrive3Count = 0;
		int c6 = 0;
		int c8 = 0;
		int c10 = 0;
		int c12 = 0;
		double sDPress = 0;
		double sqPress = 0;
		double stPress = 0;
		int rUnder10Count = 0;
		int rUnder20Count = 0;
		int rUnder30Count = 0;
		int cUnder10Count = 0;
		int cUnder20Count = 0;
		int cPassCount = 0;
		int notCheckCount = 0;
		int bNCount= 0;
		int b24Count= 0;
		int b4Count= 0;
		int l0Count= 0;
		int l4Count= 0;
		int l8Count= 0;
		int l12Count= 0;
		int sX= 0;
		int sN= 0;
		int sL= 0;
		int sT= 0;
		int sP= 0;
		int sTire= 0;
		List<CVehicle> list = new ArrayList<CVehicle>();
		if(null != check) {
			sDPress = check.getGuideAirStandard();
			sqPress = check.getDriveAirStandard();
			stPress = check.getDragAirStandard();
			List<CheckVehicle> checkVehicles = checkVehicleMapper.queryByCheckId(check.getId());
			check.setCheckVehicles(checkVehicles);
			List<VehicleTypeTire> typeTires = vehicleTypeService.queryVehicleTypeTire(id);
			if(!checkVehicles.isEmpty()) {
				for(CheckVehicle checkVehicle:checkVehicles) {
					CVehicle cVehicle = new CVehicle();
					vType1Count += vehicleCout(checkVehicle.getVehicleTypeId(), 1);
					vType2Count += vehicleCout(checkVehicle.getVehicleTypeId(), 2);
					vType3Count += vehicleCout(checkVehicle.getVehicleTypeId(), 3);
					c6 += vehicleTireCount(checkVehicle.getVehicleTypeId(), 6);
					c8 += vehicleTireCount(checkVehicle.getVehicleTypeId(), 8);
					c10 += vehicleTireCount(checkVehicle.getVehicleTypeId(), 10);
					c12 += vehicleTireCount(checkVehicle.getVehicleTypeId(), 12);
					List<CheckVehicleTire> checkVehicleTires = checkVehicleTireMapper.queryTireByVehicleId(checkVehicle.getId());
					List<checkcar> checkcars = new ArrayList<checkcar>();
					for(CheckVehicleTire vehicleTire:checkVehicleTires) {
						checkcar cc = new checkcar();
						cc.setBrand(vehicleTire.getBrandName());
						cc.setPr(vehicleTire.getAirPress());
						cc.setP1(vehicleTire.getTreadDeep1());
						cc.setP2(vehicleTire.getTreadDeep2());
						cc.setP3(vehicleTire.getTreadDeep3());
						cc.setTN(vehicleTire.getVehicleTypeTireName());
						cc.setTread(vehicleTire.getTreadName());
						cc.setTy(vehicleTire.getTreadType());
						checkcars.add(cc);
						vDrive1Count += tireCount(typeTires, 1, vehicleTire.getVehicleTypeTireId());
						vDrive2Count += tireCount(typeTires, 2, vehicleTire.getVehicleTypeTireId());
						vDrive3Count += tireCount(typeTires, 3, vehicleTire.getVehicleTypeTireId());
						if(vehicleTire.getAirPress() == null) notCheckCount ++;
						double airPress = vehicleTire.getAirPress();
						
						if(getDriveType(typeTires, vehicleTire.getId())==1) {
							double  as = (airPress - sDPress);
							if(as>0) {
								if(as>sDPress*0.1 && as<=sDPress*0.2) {
									cUnder10Count ++;
								}else if(as>sDPress*0.2) {
									cUnder20Count ++;
								}else {
									cPassCount ++;
								}
							}else if (as<0){
								if(as<-sDPress*0.1 && as>=-sDPress*0.2) {
									rUnder10Count ++;
								}else if(as<-sDPress*0.2 && as>=-sDPress*0.3) {
									rUnder20Count ++;
								}else if(as>-sDPress*0.3) {
									rUnder30Count ++;
								}else {
									cPassCount ++;
								}
							}else {
								cPassCount ++;
							}
						}
						if(getDriveType(typeTires, vehicleTire.getId())==2) {
							double  as = (airPress - sqPress);
							if(as>0) {
								if(as>sqPress*0.1 && as<=sqPress*0.2) {
									cUnder10Count ++;
								}else if(as>sqPress*0.2) {
									cUnder20Count ++;
								}else {
									cPassCount ++;
								}
							}else if (as<0){
								if(as<-sqPress*0.1 && as>=-sqPress*0.2) {
									rUnder10Count ++;
								}else if(as<-sqPress*0.2 && as>=-sqPress*0.3) {
									rUnder20Count ++;
								}else if(as>-sqPress*0.3) {
									rUnder30Count ++;
								}else {
									cPassCount ++;
								}
							}else {
								cPassCount ++;
							}
							
						}
						if(getDriveType(typeTires, vehicleTire.getId())==3) {
							double  as = (airPress - stPress);
							if(as>0) {
								if(as>stPress*0.1 && as<=stPress*0.2) {
									cUnder10Count ++;
								}else if(as>stPress*0.2) {
									cUnder20Count ++;
								}else {
									cPassCount ++;
								}
							}else if (as<0){
								if(as<-stPress*0.1 && as>=-stPress*0.2) {
									rUnder10Count ++;
								}else if(as<-stPress*0.2 && as>=-stPress*0.3) {
									rUnder20Count ++;
								}else if(as>-stPress*0.3) {
									rUnder30Count ++;
								}else {
									cPassCount ++;
								}
							}else {
								cPassCount ++;
							}
						}
						if(getTreadStatus(vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep2(), vehicleTire.getTreadDeep3())==1) bNCount ++;
						if(getTreadStatus(vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep2(), vehicleTire.getTreadDeep3())==2) b24Count ++;
						if(getTreadStatus(vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep2(), vehicleTire.getTreadDeep3())==3) b4Count ++;
						if(getTreadDeep(vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep2(), vehicleTire.getTreadDeep3())==1) l0Count ++;
						if(getTreadDeep(vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep2(), vehicleTire.getTreadDeep3())==2) l4Count ++;
						if(getTreadDeep(vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep2(), vehicleTire.getTreadDeep3())==3) l8Count ++;
						if(getTreadDeep(vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep2(), vehicleTire.getTreadDeep3())==4) l12Count ++;
						
						sX += match(1, vehicleTire.getVehicleTypeTireId(), vehicleTire.getrX(), vehicleTire.getRetreadType(), vehicleTire.getTyreSizeId(), vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep3(), vehicleTire.getTreadDeep3(), vehicleTire.getAirPress(), vehicleTire.getTreadType(), checkVehicleTires);
						sN += match(2, vehicleTire.getVehicleTypeTireId(), vehicleTire.getrX(), vehicleTire.getRetreadType(), vehicleTire.getTyreSizeId(), vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep3(), vehicleTire.getTreadDeep3(), vehicleTire.getAirPress(), vehicleTire.getTreadType(), checkVehicleTires);
						sL += match(3, vehicleTire.getVehicleTypeTireId(), vehicleTire.getrX(), vehicleTire.getRetreadType(), vehicleTire.getTyreSizeId(), vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep3(), vehicleTire.getTreadDeep3(), vehicleTire.getAirPress(), vehicleTire.getTreadType(), checkVehicleTires);
						sT += match(4, vehicleTire.getVehicleTypeTireId(), vehicleTire.getrX(), vehicleTire.getRetreadType(), vehicleTire.getTyreSizeId(), vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep3(), vehicleTire.getTreadDeep3(), vehicleTire.getAirPress(), vehicleTire.getTreadType(), checkVehicleTires);
						sP += match(5, vehicleTire.getVehicleTypeTireId(), vehicleTire.getrX(), vehicleTire.getRetreadType(), vehicleTire.getTyreSizeId(), vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep3(), vehicleTire.getTreadDeep3(), vehicleTire.getAirPress(), vehicleTire.getTreadType(), checkVehicleTires);
						sTire += match(6, vehicleTire.getVehicleTypeTireId(), vehicleTire.getrX(), vehicleTire.getRetreadType(), vehicleTire.getTyreSizeId(), vehicleTire.getTreadDeep1(), vehicleTire.getTreadDeep3(), vehicleTire.getTreadDeep3(), vehicleTire.getAirPress(), vehicleTire.getTreadType(), checkVehicleTires);
					}
					cVehicle.setCces(checkcars);
					list.add(cVehicle);
				}
			}
		}
		Customer customer = customerService.findByPrimaryKey(check.getCustomerId());
		BrokenReport brokenReport = new BrokenReport();
		brokenReport.setCustomerName(StringUtil.dealNull(check.getCustomerName()));
		brokenReport.setCreateTime(DateUtil.getFormatDate(check.getDate(), DateUtil.FORMAT_YYMMDDHHMMSS));
		
		
		brokenReport.setVType1Count(vType1Count);
		brokenReport.setVType2Count(vType2Count);
		brokenReport.setVType3Count(vType3Count);
		int vTypeTotalCount = vType1Count+vType2Count+vType3Count;
		brokenReport.setVTypeTotalCount(vTypeTotalCount);
		brokenReport.setVDrive1Count(vDrive1Count);
		brokenReport.setVDrive2Count(vDrive2Count);
		brokenReport.setVDrive3Count(vDrive3Count);
		int vDriveTotalCount = vDrive1Count+vDrive2Count+vDrive3Count;
		brokenReport.setVDriveTotalCount(vDriveTotalCount);
		double vType1Rate = vTypeTotalCount==0?0:MathUtil.parseDouble(vType1Count*100/vTypeTotalCount);
		double vType2Rate = vTypeTotalCount==0?0:MathUtil.parseDouble(vType2Count*100/vTypeTotalCount);
		double vType3Rate = vTypeTotalCount==0?0:MathUtil.parseDouble(vType3Count*100/vTypeTotalCount);
		brokenReport.setVType1Rate(vType1Rate);
		brokenReport.setVType2Rate(vType2Rate);
		brokenReport.setVType3Rate(vType3Rate);
		double vDrive1Rate = vDriveTotalCount==0?0:MathUtil.parseDouble(vDrive1Count*100/vDriveTotalCount);
		double vDrive2Rate = vDriveTotalCount==0?0:MathUtil.parseDouble(vDrive2Count*100/vDriveTotalCount);
		double vDrive3Rate = vDriveTotalCount==0?0:MathUtil.parseDouble(vDrive3Count*100/vDriveTotalCount);
		brokenReport.setVDrive1Rate(vDrive1Rate);
		brokenReport.setVDrive2Rate(vDrive2Rate);
		brokenReport.setVDrive3Rate(vDrive3Rate);
		
		//TODO
		double sZBRate = 5.2;
		int sZBcount = 0;
		int sBcount = 0;
		//TODO
		double sBRate = 16.4;
		brokenReport.setSZBRate(sZBRate);
		brokenReport.setSBRate(sBRate);
		double sTotalRate = sZBRate+sBRate;
		brokenReport.setSTotalRate(sTotalRate);
		
		
		int a6 = customer.getDragCar60()+customer.getDragCar60D()+customer.getWholeCar42()+customer.getDrawCar42();
		int a8 = customer.getDrawCar62()+customer.getWholeCar62()+customer.getDragCar40()+customer.getDragCar40D();
		int a10= customer.getDrawCar64();
		int a12= customer.getWholeCar84();
		int aTotal= a6+a8+a10+a12;
		brokenReport.setA6(a6);
		brokenReport.setA8(a8);
		brokenReport.setA10(a10);
		brokenReport.setA12(a12);
		brokenReport.setATotal(aTotal);
		int aW = a6*6 + a8*8 + a10*10 + a12*12;
		brokenReport.setAW(aW);
		
		brokenReport.setC6(c6);
		brokenReport.setC8(c8);
		brokenReport.setC10(c10);
		brokenReport.setC12(c12);
		int cTotal = c6 + c8 + c10 +c12;
		brokenReport.setCTotal(cTotal);
		int aWC =  c6*6 + c8*8 + c10*10 + c12*12;
		brokenReport.setAWC(aWC);
		
		double tamp = cTotal==0?0: MathUtil.parseDouble(aTotal*100/cTotal);
		brokenReport.setTamp(tamp);
		
		double vamp = aWC==0?0: MathUtil.parseDouble(aW*100/aWC);
		brokenReport.setVamp(vamp);
		
		double c6Rate = a6==0?0: MathUtil.parseDouble(c6*100/a6);
		double c8Rate = a8==0?0: MathUtil.parseDouble(c8*100/a8);
		double c10Rate = a10==0?0: MathUtil.parseDouble(c10*100/a10);
		double c12Rate = a12==0?0: MathUtil.parseDouble(c12*100/a12);
		brokenReport.setC6Rate(c6Rate);
		brokenReport.setC8Rate(c8Rate);
		brokenReport.setC10Rate(c10Rate);
		brokenReport.setC12Rate(c12Rate);
		
		double cTotalRate =  cTotal==0?0: MathUtil.parseDouble(aTotal*100/cTotal);
		brokenReport.setCTotalRate(cTotalRate);
		double aWCRate =  aWC==0?0: MathUtil.parseDouble(aW*100/aWC);
		brokenReport.setAWCRate(aWCRate);
		
		
		double ddPress = sDPress-(sDPress*0.1);
		double dqPress = sqPress-(sqPress*0.1);
		double dtPress = stPress-(stPress*0.1);
		double udPress = sDPress+(sDPress*0.1);
		double uqPress = sqPress+(sqPress*0.1);
		double utPress = stPress+(stPress*0.1);
		brokenReport.setDdPress(ddPress);
		brokenReport.setDqPress(dqPress);
		brokenReport.setDtPress(dtPress);
		brokenReport.setUdPress(udPress);
		brokenReport.setUqPress(uqPress);
		brokenReport.setUtPress(utPress);
		brokenReport.setSDPress(sDPress);
		brokenReport.setSqPress(sqPress);
		brokenReport.setStPress(stPress);
		
		
		brokenReport.setRUnder10Count(rUnder10Count);
		brokenReport.setRUnder20Count(rUnder20Count);
		brokenReport.setRUnder30Count(rUnder30Count);
		brokenReport.setCUnder10Count(cUnder10Count);
		brokenReport.setCUnder20Count(cUnder20Count);
		brokenReport.setCPassCount(cPassCount);
		brokenReport.setNotCheckCount(notCheckCount);
		int totalPressCount = rUnder10Count + rUnder20Count +rUnder30Count +cUnder10Count +cUnder20Count +cPassCount +notCheckCount;
		brokenReport.setTotalPressCount(totalPressCount);
		double rUnder10Rate = totalPressCount==0?0: MathUtil.parseDouble(rUnder10Count*100/totalPressCount);
		double rUnder20Rate = totalPressCount==0?0: MathUtil.parseDouble(rUnder20Count*100/totalPressCount);
		double rUnder30Rate = totalPressCount==0?0: MathUtil.parseDouble(rUnder30Count*100/totalPressCount);
		double cUnder10Rate = totalPressCount==0?0: MathUtil.parseDouble(cUnder10Count*100/totalPressCount);
		double cUnder20Rate = totalPressCount==0?0: MathUtil.parseDouble(cUnder20Count*100/totalPressCount);
		double cPassRate = totalPressCount==0?0: MathUtil.parseDouble(cPassCount*100/totalPressCount);
		double notCheckRate = totalPressCount==0?0: MathUtil.parseDouble(notCheckCount*100/totalPressCount);
		brokenReport.setRUnder10Rate(rUnder10Rate);
		brokenReport.setRUnder20Rate(rUnder20Rate);
		brokenReport.setRUnder30Rate(rUnder30Rate);
		brokenReport.setCUnder10Rate(cUnder10Rate);
		brokenReport.setCUnder20Rate(cUnder20Rate);
		brokenReport.setCPassRate(cPassRate);
		brokenReport.setNotCheckRate(notCheckRate);
		brokenReport.setTotalPressRate(100);
		int rUnder10Total =  (int) (rUnder10Count*vamp);
		int rUnder20Total =  (int) (rUnder20Count*vamp);
		int rUnder30Total =  (int) (rUnder30Count*vamp);
		int cUnder10Total =  (int) (cUnder10Count*vamp);
		int cUnder20Total =  (int) (cUnder20Count*vamp);
		int cPassTotal =  (int) (cPassCount*vamp);
		int notCheckTotal =  (int) (notCheckCount*vamp);
		int totalPressTotal =  (int) (totalPressCount*vamp);
		brokenReport.setRUnder10Total(rUnder10Total);
		brokenReport.setRUnder20Total(rUnder20Total);
		brokenReport.setRUnder30Total(rUnder30Total);
		brokenReport.setCUnder10Total(cUnder10Total);
		brokenReport.setCUnder20Total(cUnder20Total);
		brokenReport.setCPassTotal(cPassTotal);
		brokenReport.setNotCheckTotal(notCheckTotal);
		brokenReport.setTotalPressTotal(totalPressTotal);
		
		brokenReport.setBNCount(bNCount);
		brokenReport.setB24Count(b24Count);
		brokenReport.setB4Count(b4Count);
		int bTotalCount = bNCount + b24Count + b4Count;
		brokenReport.setBTotalCount(bTotalCount);
		double bNRate = bTotalCount==0?0: MathUtil.parseDouble(bNCount*100/bTotalCount);
		double b24Rate = bTotalCount==0?0: MathUtil.parseDouble(b24Count*100/bTotalCount);
		double b4Rate = bTotalCount==0?0: MathUtil.parseDouble(b4Count*100/bTotalCount);
		double bTotalRate = 100;
		brokenReport.setBNRate(bNRate);
		brokenReport.setB24Rate(b24Rate);
		brokenReport.setB4Rate(b4Rate);
		brokenReport.setBTotalRate(bTotalRate);
		
		int bNTotal = (int) (bNCount*vamp);
		int b24Total = (int) (b24Count*vamp);
		int b4Total = (int) (b4Count*vamp);
		int bTotal = (int) (bTotalCount*vamp);
		brokenReport.setBNTotal(bNTotal);
		brokenReport.setB24Total( b24Total);
		brokenReport.setB4Total(b4Total);
		brokenReport.setBTotal(bTotal);
		
		brokenReport.setL0Count(l0Count);
		brokenReport.setL4Count(l4Count);
		brokenReport.setL8Count(l8Count);
		brokenReport.setL12Count(l12Count);
		int lTotalCount = l0Count +l4Count  +l8Count +l12Count;
		brokenReport.setLTotalCount(lTotalCount);
		double l0Rate = lTotalCount==0?0: MathUtil.parseDouble(l0Count*100/lTotalCount);
		double l4Rate = lTotalCount==0?0: MathUtil.parseDouble(l4Count*100/lTotalCount);
		double l8Rate = lTotalCount==0?0: MathUtil.parseDouble(l8Count*100/lTotalCount);
		double l12Rate = lTotalCount==0?0: MathUtil.parseDouble(l12Count*100/lTotalCount);
		
		brokenReport.setL0Rate(l0Rate);
		brokenReport.setL4Rate(l4Rate);
		brokenReport.setL8Rate(l8Rate);
		brokenReport.setL12Rate(l12Rate);
		brokenReport.setLTotalRate(100);
		
		int l0Total = (int) (l0Count*vamp);
		int l4Total  = (int) (l4Count*vamp);
		int l8Total = (int) (l8Count*vamp);
		int l12Total = (int) (l12Count*vamp);
		int lTotal = (int) (lTotalCount*vamp);
		brokenReport.setL0Total(l0Total);
		brokenReport.setL4Total(l4Total);
		brokenReport.setL8Total(l8Total);
		brokenReport.setL12Total(l12Total);
		brokenReport.setLTotal(lTotal);
		brokenReport.setSX(sX);
		brokenReport.setSN(sN);
		brokenReport.setSL(sL);
		brokenReport.setST(sT);
		brokenReport.setSP(sP);
		brokenReport.setSTire(sTire);
		int sCount =sX + sN+ sL +sT+ sP+sTire;
		brokenReport.setSCount(sCount);
		double sXRate  = sCount==0?0: MathUtil.parseDouble(sX*100/sCount);
		double sNRate  = sCount==0?0: MathUtil.parseDouble(sN*100/sCount);
		double sLRate  = sCount==0?0: MathUtil.parseDouble(sL*100/sCount);
		double sTRate  = sCount==0?0: MathUtil.parseDouble(sT*100/sCount);
		double sPRate  = sCount==0?0: MathUtil.parseDouble(sP*100/sCount);
		double sTireRate  = sCount==0?0: MathUtil.parseDouble(sTire*100/sCount);
		double sCountRate  = 100;
		brokenReport.setSXRate(sXRate);
		brokenReport.setSNRate(sNRate);
		brokenReport.setSLRate(sLRate);
		brokenReport.setSTRate(sTRate);
		brokenReport.setSPRate(sPRate);
		brokenReport.setSTireRate(sTireRate);
		brokenReport.setSCountRate(sCountRate);
		int sXTotal =  (int) (sX*vamp);
		int sNTotal=  (int) (sN*vamp);
		int sLTotal=  (int) (sL*vamp);
		int sTTotal=  (int) (sT*vamp);
		int sPTotal=  (int) (sP*vamp);
		int sTireTotal=  (int) (sTire*vamp);
		int sCountTotal=  (int) (sCount*vamp);
		int j1Count = rUnder30Count + cUnder20Count+b4Count+sZBcount;
		int j2Count = rUnder10Count+rUnder20Count+cUnder10Count+b24Count+sBcount;
		int j3Count = cPassCount+bNCount;
		int j4Count = b24Count+b4Count;
		int j5Count = rUnder10Count+rUnder20Count+rUnder30Count;
		int j6Count = l0Count;
		int jTotal = j1Count+j2Count+j3Count+j4Count+j5Count+j6Count;
		brokenReport.setJ1Count(j1Count);
		brokenReport.setJ2Count(j2Count);
		brokenReport.setJ3Count(j3Count);
		brokenReport.setJ4Count(j4Count);
		brokenReport.setJ5Count(j5Count);
		brokenReport.setJ6Count(j6Count);
		brokenReport.setJTotal(jTotal);
		brokenReport.setJ1Rate(jTotal==0?0: MathUtil.parseDouble(j1Count*100/jTotal));
		brokenReport.setJ2Rate(jTotal==0?0: MathUtil.parseDouble(j2Count*100/jTotal));
		brokenReport.setJ3Rate(jTotal==0?0: MathUtil.parseDouble(j3Count*100/jTotal));
		brokenReport.setJ4Rate(jTotal==0?0: MathUtil.parseDouble(j4Count*100/jTotal));
		brokenReport.setJ5Rate(jTotal==0?0: MathUtil.parseDouble(j5Count*100/jTotal));
		brokenReport.setJ6Rate(jTotal==0?0: MathUtil.parseDouble(j6Count*100/jTotal));
		brokenReport.setJTotalRate(jTotal==0?0: MathUtil.parseDouble(jTotal*100/jTotal));
		
		brokenReport.setSXTotal(sXTotal);
		brokenReport.setSNTotal(sNTotal);
		brokenReport.setSLTotal(sLTotal);
		brokenReport.setSTTotal(sTTotal);
		brokenReport.setSPTotal(sPTotal);
		brokenReport.setSTireTotal(sTireTotal);
		brokenReport.setSCountTotal(sCountTotal);
		brokenReport.setGuideAir(sDPress);
		brokenReport.setDiriveAir(sqPress);
		brokenReport.setDragAir(stPress);
		brokenReport.setCv(list);
		
		
		
		return brokenReport;
	}
	
	private int vehicleCout(int typeId,int type) {
		if(type == 1) {//一体车
			if(typeId == 1 || typeId == 2 || typeId == 3) {
				return 1;
			}
		}
		if(type == 2) {//牵引车
			if(typeId == 4 || typeId == 5 || typeId == 6) {
				return 1;
			}
		}
		if(type == 1) {//拖车
			if(typeId == 7 || typeId == 8 || typeId == 9 || typeId == 10) {
				return 1;
			}
		}
		return 0;
	}
	
	private int tireCount(List<VehicleTypeTire> typeTires,int type,int typeId) {
		for(VehicleTypeTire tire:typeTires) {
			if(type==tire.getDriveTypeId() && typeId == tire.getId() ) {
				return 1;
			}
		}
		return 0;
	}
	private int vehicleTireCount(int typeId,int type) {
		
		if(type == 6) {//牵引车
			if(typeId == 1 || typeId == 4 || typeId == 9 || typeId == 10) {
				return 1;
			}
		}
		if(type == 8 ) {//牵引车
			if(typeId == 2 || typeId == 5 || typeId == 7 || typeId == 8) {
				return 1;
			}
		}
		if(type == 10) {//拖车
			if(typeId == 6 ) {
				return 1;
			}
		}
		if(type == 12) {//拖车
			if(typeId == 3) {
				return 1;
			}
		}
		return 0;
	}
	
	private int getDriveType(List<VehicleTypeTire> typeTires,int tireId) {
		for(VehicleTypeTire tire:typeTires) {
			if(tireId == tire.getId() ) {
				return tire.getDriveTypeId();
			}
		}
		return 0;
	}
	
	
	private int getTreadStatus(Double press1 ,Double press2,Double press3) {
		if(null == press1) press1 = 0.0; 
		if(null == press2) press2 = 0.0; 
		if(null == press3) press3 = 0.0; 
		double p1 = Math.abs(press1 - press2);
		double p2 = Math.abs(press2 - press3);
		double p3 = Math.abs(press3 - press1);
		double p = p1;
		if(p2<p) {
			p = p2;
		}
		if(p3<p) {
			p = p3;
		}
		if(p<2) {
			return 1;
		}
		if(p>2 && p<=4) {
			return 2;
		}
		return 3;
	}
	private int getTreadDeep(Double press1 ,Double press2,Double press3) {
		if(null == press1) press1 = 0.0; 
		if(null == press2) press2 = 0.0; 
		if(null == press3) press3 = 0.0; 
		double p = press1;
		if(press2<p) {
			p = press2;
		}
		if(press3<p) {
			p = press3;
		}
		if(p>=0 && p<4 ) {
			return 1;
		}
		if(p>=4 && p<8) {
			return 2;
		}
		if(p>=8 && p<12) {
			return 3;
		}
		if(p>=12) {
			return 4;
		}
		return 1;
	}
	
	private int match(int type ,int id ,int rx,String retreadType,int tyreSize,Double treadDeep1,Double treadDeep2,Double treadDeep3,Double airPress,String treadType,List<CheckVehicleTire> checkVehicleTires) {
		if(type == 1) {
			for(Integer i : getList(1)) {
				if(i == id) {
					for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
						if(checkVehicleTire.getVehicleTypeTireId() == (id+1)) {
							if(rx != checkVehicleTire.getrX()) {
								return 2;
							}else {
								return 0;
							}
						}
					}
				}
			}
		}
		if(type == 2) {
			for(Integer i : getList(1)) {
				if(i == id) {
					for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
						if(checkVehicleTire.getVehicleTypeTireId() == (id+1)) {
							if(!retreadType.equals(checkVehicleTire.getRetreadType())) {
								return 2;
							}else {
								return 0;
							}
						}
					}
				}
			}
		}
		
		if(type == 3) {
			for(Integer i : getList(1)) {
				if(i == id) {
					for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
						if(checkVehicleTire.getVehicleTypeTireId() == (id+1)) {
							if(tyreSize!=checkVehicleTire.getTyreSizeId()) {
								return 2;
							}else {
								return 0;
							}
						}
					}
				}
			}
		}
		
		if(type == 4) {
			for(Integer i : getList(2)) {
				if(i == id) {
					for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
						if(checkVehicleTire.getVehicleTypeTireId() == (id+1)) {
							double deep = Math.abs((treadDeep1 +treadDeep2+treadDeep3)/3-(checkVehicleTire.getTreadDeep1()+checkVehicleTire.getTreadDeep2()+checkVehicleTire.getTreadDeep3())/3);
							if(deep>2) {
								return 2;
							}else {
								return 0;
							}
						}
					}
				}
			}
		}
		if(type == 5) {
			for(Integer i : getList(2)) {
				if(i == id) {
					for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
						if(checkVehicleTire.getVehicleTypeTireId() == (id+1)) {
							boolean is0= checkVehicleTire.getAirPress()==null||checkVehicleTire.getAirPress()==0;
							double deep = is0?1:Math.abs(checkVehicleTire.getAirPress()-airPress)/checkVehicleTire.getAirPress();
							if(deep>0.2) {
								return 2;
							}else {
								return 0;
							}
						}
					}
				}
			}
		}
		if(type == 6) {
			for(Integer i : getList(2)) {
				if(i == id) {
					for(CheckVehicleTire checkVehicleTire:checkVehicleTires) {
						if(checkVehicleTire.getVehicleTypeTireId() == (id+1)) {
							boolean is0= checkVehicleTire.getTreadType()==null;
							return is0?2:treadType.equals(checkVehicleTire.getTreadType())?0:2;
						}
					}
				}
			}
		}
		return 0;
	}
	
	private List<Integer> getList(int type){
		List<Integer> ids1 = new ArrayList<Integer>();
		ids1.add(1);
		ids1.add(2);
		ids1.add(7);
		ids1.add(8);
		ids1.add(9);
		ids1.add(10);
		ids1.add(11);
		ids1.add(12);
		ids1.add(13);
		ids1.add(14);
		ids1.add(15);
		ids1.add(16);
		ids1.add(17);
		ids1.add(18);
		ids1.add(27);
		ids1.add(28);
		ids1.add(33);
		ids1.add(34);
		ids1.add(35);
		ids1.add(36);
		ids1.add(41);
		ids1.add(42);
		ids1.add(59);
		ids1.add(60);
		ids1.add(61);
		ids1.add(62);
		ids1.add(75);
		ids1.add(76);
		ids1.add(77);
		ids1.add(78);
		ids1.add(79);
		ids1.add(80);
		
		List<Integer> ids2 = new ArrayList<Integer>();
		ids2.add(3);
		ids2.add(4);
		ids2.add(5);
		ids2.add(6);
		ids2.add(19);
		ids2.add(20);
		ids2.add(21);
		ids2.add(22);
		ids2.add(23);
		ids2.add(24);
		ids2.add(25);
		ids2.add(26);
		ids2.add(29);
		ids2.add(30);
		ids2.add(31);
		ids2.add(32);
		ids2.add(37);
		ids2.add(38);
		ids2.add(39);
		ids2.add(40);
		ids2.add(43);
		ids2.add(44);
		ids2.add(45);
		ids2.add(46);
		ids2.add(47);
		ids2.add(48);
		ids2.add(49);
		ids2.add(50);
		ids2.add(51);
		ids2.add(52);
		ids2.add(53);
		ids2.add(54);
		ids2.add(55);
		ids2.add(56);
		ids2.add(57);
		ids2.add(58);
		ids2.add(63);
		ids2.add(64);
		ids2.add(65);
		ids2.add(66);
		ids2.add(67);
		ids2.add(68);
		ids2.add(69);
		ids2.add(70);
		ids2.add(71);
		ids2.add(72);
		ids2.add(73);
		ids2.add(74);
		if(type == 1) return ids1;
		if(type == 2) return ids2;
		return null;
	}
}
