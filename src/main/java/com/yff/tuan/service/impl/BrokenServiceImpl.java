package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.BrokenCheckMapper;
import com.yff.tuan.mapper.BrokenMapper;
import com.yff.tuan.mapper.BrokenProblemMapper;
import com.yff.tuan.model.Broken;
import com.yff.tuan.model.BrokenCheck;
import com.yff.tuan.model.BrokenExample;
import com.yff.tuan.model.BrokenProblem;
import com.yff.tuan.model.BrokenProblemExample;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.service.BrokenService;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.BrokenObjectCheck;

@Component
public class BrokenServiceImpl implements BrokenService {
	@Autowired
	BrokenMapper brokenMapper;
	@Autowired
	BrokenCheckMapper brokenCheckMapper;
	@Autowired
	BrokenProblemMapper brokenProblemMapper;

	@Override
	public Broken find(Broken broken) {
		return brokenMapper.findByPrimaryKey(broken.getId());
	}

	@Override
	public BrokenCheck findByPrimaryKey(Integer id) {
		return brokenCheckMapper.findByPrimaryKey(id);
	}

	@Override
	public int insert(Broken broken) {
		return brokenMapper.insert(broken);
	}

	@Override
	public int updateByPrimaryKeySelective(Broken broken) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Broken> queryforwx() {
		return brokenMapper.queryforwx();
	}

	@Override
	public Page<Broken> queryByPage(BrokenExample example, Integer pageNum) {
		int count = brokenMapper.countByExample(example);
		Page<Broken> page = new Page<Broken>(pageNum, count);
		List<Broken> brokens = brokenMapper.queryByPage(page.getStartIndex(), page.getPageSize());
		page.setList(brokens);
		return page;
	}

	@Override
	public List<BrokenCheck> queryByBrokenId(Integer brokenId) {
		return brokenCheckMapper.queryByBrokenId(brokenId);
	}

	@Override
	public List<BrokenProblem> queryBrokenProblem(BrokenProblemExample example) {
		return brokenProblemMapper.selectByExample(example);
	}

	@Override
	public int insertBrokenCheck(BrokenCheck brokenCheck) {
		return brokenCheckMapper.insert(brokenCheck);
	}

	@Override
	public List<BrokenObjectCheck> queryProblem(Integer brokenId, String key) {
		return brokenCheckMapper.queryProblem(brokenId,key);
	}

	@Override
	public List<BrokenObjectCheck> queryBrand(Integer brokenId) {
		return brokenCheckMapper.queryBrand(brokenId);
	}

	@Override
	public List<BrokenObjectCheck> queryTyreSize(Integer brokenId) {
		return brokenCheckMapper.queryTyreSize(brokenId);
	}
}
