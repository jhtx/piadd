package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.yff.tuan.mapper.RoleMapper;
import com.yff.tuan.mapper.UserMapper;
import com.yff.tuan.mapper.WxUserBindMapper;
import com.yff.tuan.mapper.WxUserMapper;
import com.yff.tuan.model.Role;
import com.yff.tuan.model.RoleExample;
import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.model.UserExample.Criteria;
import com.yff.tuan.model.WxUser;
import com.yff.tuan.model.WxUserBind;
import com.yff.tuan.model.WxUserBindExample;
import com.yff.tuan.model.WxUserExample;
import com.yff.tuan.service.RoleService;
import com.yff.tuan.service.UserService;
import com.yff.tuan.service.WxUserService;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.UserVo;

@Component
public class WxUserServiceImpl implements WxUserService{
	
	@Autowired
	WxUserMapper wxUserMapper;
	@Autowired
	WxUserBindMapper wxUserBindMapper;
	
	@Override
	public int insert(WxUser user) {
		return wxUserMapper.insert(user);
	}
	
	@Override
	public WxUser find(WxUser user) {
		WxUserExample example = new WxUserExample();
		com.yff.tuan.model.WxUserExample.Criteria criteria = example.createCriteria();
		criteria.andOpenIdEqualTo(user.getOpenId());
		List<WxUser> users = wxUserMapper.selectByExample(example);
		return users.isEmpty()?null:users.get(0);
	}
	@Override
	public List<WxUser> query(WxUserExample example) {
		return wxUserMapper.selectByExample(example);
	}

	@Override
	public WxUser findByPrimaryKey(Integer id) {
		return wxUserMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(WxUser wxUser) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

	@Override
	public List<WxUser> queryWxUser(Integer userId) {
		return wxUserMapper.queryWxUser(userId);
	}

	@Override
	@Transactional
	public Integer updateWxUser(Integer userId, List<WxUserBind> binds) {
		WxUserBindExample example = new WxUserBindExample();
		com.yff.tuan.model.WxUserBindExample.Criteria createCriteria = example.createCriteria();
		createCriteria.andUserIdEqualTo(userId);
		int i = wxUserBindMapper.deleteByExample(example);
		for(WxUserBind record:binds) {
			wxUserBindMapper.insert(record);
		}
		return i;
	}

	@Override
	public Integer countWxUser(Integer userId, Integer wxUserId) {
		if(null == wxUserId || null == userId){
			return 0;
		}
		WxUserBindExample example = new WxUserBindExample();
		com.yff.tuan.model.WxUserBindExample.Criteria createCriteria = example.createCriteria();
		createCriteria.andUserIdEqualTo(userId);
		createCriteria.andWxIdEqualTo(wxUserId);
		int count = wxUserBindMapper.countByExample(example);
		return count;
	}
	
	
}
