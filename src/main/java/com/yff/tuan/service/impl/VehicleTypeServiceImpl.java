package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.CheckMapper;
import com.yff.tuan.mapper.CheckVehicleMapper;
import com.yff.tuan.mapper.CheckVehicleTireMapper;
import com.yff.tuan.mapper.CheckVehicleTireWarnMapper;
import com.yff.tuan.mapper.VehicleTypeMapper;
import com.yff.tuan.mapper.VehicleTypeTireMapper;
import com.yff.tuan.model.Check;
import com.yff.tuan.model.CheckExample;
import com.yff.tuan.model.CheckVehicleTire;
import com.yff.tuan.model.CheckVehicleTireWarn;
import com.yff.tuan.model.CheckVehicleTireWarnExample;
import com.yff.tuan.model.CheckVehicleTireWarnExample.Criteria;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.VehicleType;
import com.yff.tuan.model.VehicleTypeExample;
import com.yff.tuan.model.VehicleTypeTire;
import com.yff.tuan.model.VehicleTypeTireExample;
import com.yff.tuan.service.CheckService;
import com.yff.tuan.service.VehicleTypeService;
import com.yff.tuan.util.Page;

@Component
public class VehicleTypeServiceImpl implements VehicleTypeService {
	@Autowired
	VehicleTypeMapper vehicleTypeMapper;
	@Autowired
	VehicleTypeTireMapper vehicleTypeTireMapper;
	
	@Override
	public VehicleType find(VehicleType vehicleType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VehicleType findByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int insert(VehicleType vehicleType) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByPrimaryKeySelective(VehicleType vehicleType) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<VehicleType> query(VehicleTypeExample example) {
		return vehicleTypeMapper.selectByExample(example);
	}

	@Override
	public List<VehicleTypeTire> queryVehicleTypeTire(Integer vehicleTypeId) {
		VehicleTypeTireExample example = new VehicleTypeTireExample();
		com.yff.tuan.model.VehicleTypeTireExample.Criteria createCriteria = example.createCriteria();
		createCriteria.andTypeIdEqualTo(vehicleTypeId);
		List<VehicleTypeTire> vehicleTypeTires = vehicleTypeTireMapper.selectByExample(example);
		return vehicleTypeTires;
	}

}
