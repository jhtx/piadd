package com.yff.tuan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yff.tuan.mapper.BrandMapper;
import com.yff.tuan.mapper.BrokenCheckMapper;
import com.yff.tuan.mapper.BrokenMapper;
import com.yff.tuan.mapper.BrokenProblemMapper;
import com.yff.tuan.mapper.TreadMapper;
import com.yff.tuan.model.Brand;
import com.yff.tuan.model.BrandExample;
import com.yff.tuan.model.Broken;
import com.yff.tuan.model.BrokenCheck;
import com.yff.tuan.model.BrokenExample;
import com.yff.tuan.model.BrokenProblem;
import com.yff.tuan.model.BrokenProblemExample;
import com.yff.tuan.model.Brand;
import com.yff.tuan.model.Tread;
import com.yff.tuan.model.TreadExample;
import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.Tread;
import com.yff.tuan.service.BrandService;
import com.yff.tuan.service.BrokenService;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.BrokenObjectCheck;

@Component
public class BrandServiceImpl implements BrandService {
	@Autowired
	BrandMapper brandMapper;
	@Autowired
	TreadMapper treadMapper;
	
	@Override
	public Brand find(Brand broken) {
		
		return brandMapper.selectByPrimaryKey(broken.getId());
	}
	@Override
	public Brand findByPrimaryKey(Integer id) {
		Brand brand = brandMapper.selectByPrimaryKey(id);
		if(null != brand) {
			TreadExample example = new TreadExample();
			example.createCriteria().andBrandIdEqualTo(brand.getId());
			List<Tread> treads = treadMapper.selectByExample(example);
			brand.setTreads(treads);
		}
		return brand;
	}
	@Override
	public int insert(Brand broken) {
		return brandMapper.insert(broken);
	}
	@Override
	public int updateByPrimaryKeySelective(Brand broken) {
		return brandMapper.updateByPrimaryKeySelective(broken);
	}
	@Override
	public List<Brand> query(BrandExample example) {
		return brandMapper.selectByExample(example);
	}

	@Override
	public boolean add(Brand brand) {
		int id = brandMapper.insert(brand);
		List<Tread> treads = brand.getmAdd();
		if(null != treads && !treads.isEmpty()) {
			for(Tread tread:treads) {
				tread.setBrandId(brand.getId());
				treadMapper.insert(tread);
			}
		}
		return id>0?true:false;
	}


	@Override
	public boolean update(Brand brand) {
		int id = brandMapper.updateByPrimaryKey(brand);
		List<Tread> treadAdd = brand.getmAdd();
		List<Tread> treadUpdate = brand.getmUpdate();
		if(null != treadAdd && !treadAdd.isEmpty()) {
			for(Tread tread:treadAdd) {
				tread.setBrandId(brand.getId());
				treadMapper.insert(tread);
			}
		}
		if(null != treadUpdate && !treadUpdate.isEmpty()) {
			for(Tread tread:treadUpdate) {
				treadMapper.updateByPrimaryKey(tread);
			}
		}
		return id>0?true:false;
	}
	
}
