package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Brand;
import com.yff.tuan.model.BrandExample;
public interface BrandService {
	
	public abstract Brand find(Brand broken);
	
	public abstract Brand findByPrimaryKey(Integer id);

	public abstract int insert(Brand broken);
	
	public abstract int updateByPrimaryKeySelective(Brand broken);
	
	public abstract List<Brand> query(BrandExample example);
	
	public boolean add(Brand brand);
	public boolean update(Brand brand);
	
}	
