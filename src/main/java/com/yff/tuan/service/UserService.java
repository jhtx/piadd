package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.User;
import com.yff.tuan.model.UserExample;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.UserVo;

public interface UserService {
	public abstract User find(User user);
	
	public abstract int del(Integer id);

	public abstract int insert(User user);
	
	public abstract int updateByPrimaryKeySelective(User user);
	
	public abstract  List<User> query(UserExample example);
	
	public abstract UserVo find(Integer id);
	
	public abstract  Page<UserVo> queryByPage(int pageNum);
	
	public abstract List<Integer> queryUserIds(Integer role,Integer userId);
	
}	
