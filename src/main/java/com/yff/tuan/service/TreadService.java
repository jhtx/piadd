package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Tread;
import com.yff.tuan.model.TreadExample;

public interface TreadService {
	
	public abstract Tread find(Tread record);
	
	public abstract Tread findByPrimaryKey(Integer id);

	public abstract int del(Integer id);
	
	public abstract int insert(Tread record);
	
	public abstract int updateByPrimaryKeySelective(Tread record);
	
	public abstract List<Tread> query(TreadExample example);
	
}	
