package com.yff.tuan.service;

import com.yff.tuan.model.VehicleCheck;
import com.yff.tuan.model.VehicleCheckExample;
import com.yff.tuan.util.Page;

public interface VehicleCheckService {
	public abstract boolean save(VehicleCheck vehicleCheck);
	public abstract Page<VehicleCheck> queryByPage(VehicleCheckExample example,Integer pageNum);
}
