package com.yff.tuan.service;

import com.yff.tuan.model.TireCheck;
import com.yff.tuan.model.TireCheckExample;
import com.yff.tuan.util.Page;

public interface TireCheckService {
	
	public abstract boolean save(TireCheck tireCheck);
	public abstract Page<TireCheck> queryByPage(TireCheckExample example,Integer pageNum);
	
}	
