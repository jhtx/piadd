package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.TrackTireCheck;
import com.yff.tuan.util.Page;

public interface TrackService {
	
	public abstract Track find(Track record);
	
	public abstract Track findByPrimaryKey(Integer id);
	
	public abstract Track exportByPrimaryKey(Integer id);

	public abstract int del(Integer id);
	
	public abstract int insert(Track record);
	
	public abstract int insertTrackTire(TrackTire trackTire);
	
	public abstract int insertTrackTireCheck(TrackTireCheck trackTireCheck);
	
	public abstract int updateByPrimaryKeySelective(Track record);
	
	public abstract List<Track> query(TrackExample example);
	
	
	public abstract TrackTire findTrackTireByPrimaryKey(Integer id);
	
	public abstract Page<Track> queryByPage(TrackExample example,Integer pageNum);
	
	public abstract List<Track> queryForWx();
	
}	
