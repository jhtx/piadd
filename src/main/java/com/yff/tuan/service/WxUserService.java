package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.WxUser;
import com.yff.tuan.model.WxUserBind;
import com.yff.tuan.model.WxUserExample;

public interface WxUserService {
	
	public abstract WxUser findByPrimaryKey(Integer id);
	
	public abstract WxUser find(WxUser wxUser);
	
	public abstract int insert(WxUser wxUser);
	
	public abstract int updateByPrimaryKeySelective(WxUser wxUser);
	
	public abstract  List<WxUser> query(WxUserExample example);
	
	public abstract  List<WxUser> queryWxUser(Integer userId);
	
	public abstract  Integer updateWxUser(Integer userId,List<WxUserBind> binds);
	
	public abstract  Integer countWxUser(Integer userId,Integer wxUserId);
}	
