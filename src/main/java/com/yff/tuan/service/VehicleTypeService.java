package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.VehicleType;
import com.yff.tuan.model.VehicleTypeExample;
import com.yff.tuan.model.VehicleTypeTire;

public interface VehicleTypeService {
	
	public abstract VehicleType find(VehicleType vehicleType);
	
	public abstract VehicleType findByPrimaryKey(Integer id);

	public abstract int insert(VehicleType vehicleType);
	
	public abstract int updateByPrimaryKeySelective(VehicleType vehicleType);
	
	public abstract List<VehicleType> query(VehicleTypeExample example);
	
	public abstract List<VehicleTypeTire> queryVehicleTypeTire(Integer vehicleTypeId);
}	
