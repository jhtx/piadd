package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Brand;
import com.yff.tuan.model.Customer;
import com.yff.tuan.model.CustomerComment;
import com.yff.tuan.model.CustomerExample;
import com.yff.tuan.model.Evn;
import com.yff.tuan.model.Info;
import com.yff.tuan.model.Market;
import com.yff.tuan.model.Point;
import com.yff.tuan.model.Problem;
import com.yff.tuan.model.Require;
import com.yff.tuan.model.Service;
import com.yff.tuan.model.TyreSize;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.CustomerAu;
import com.yff.tuan.vo.CustomerVo;

public interface CustomerService {
	public abstract Customer findByPrimaryKey(Integer id);
	
	public abstract CustomerVo detail(Integer id);
	public abstract CustomerVo wxDetail(Integer id);
	
	public abstract int insert(Customer Customer);
	
	public abstract int updateByPrimaryKeySelective(Customer Customer);
	
	public abstract  Page<CustomerVo> queryByPage(int pageNum,List<Integer> userIds);
	
	public abstract  List<Customer> query(Integer userId);
	
	public abstract  boolean add(CustomerVo vo);
	
	public abstract  boolean insertCustomerAu(CustomerAu au);
	public abstract  boolean insertCustomerComment(CustomerComment comment);
	
	public abstract  boolean updateCustomerAu(CustomerAu au);
	
	public abstract List<Customer> query(CustomerExample example);
	public abstract List<Brand> queryBrand();
	public abstract List<TyreSize> queryTyreSize();
	public abstract List<Market> queryMarket();
	public abstract List<Evn> queryEvn();
	public abstract List<Problem> queryProblem();
	public abstract List<Info> queryInfo();
	public abstract List<Require> queryRequire();
	public abstract List<Service> queryService();
	public abstract List<Point> queryPoint();
	public abstract List<CustomerComment> queryComment(Integer id);
}	
