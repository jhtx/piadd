package com.yff.tuan.service;

import java.util.List;

import com.yff.tuan.model.Broken;
import com.yff.tuan.model.BrokenCheck;
import com.yff.tuan.model.BrokenExample;
import com.yff.tuan.model.BrokenProblem;
import com.yff.tuan.model.BrokenProblemExample;
import com.yff.tuan.util.Page;
import com.yff.tuan.vo.BrokenObjectCheck;

public interface BrokenService {
	
	public abstract Broken find(Broken broken);
	
	public abstract BrokenCheck findByPrimaryKey(Integer id);

	public abstract int insert(Broken broken);
	public abstract int insertBrokenCheck(BrokenCheck brokenCheck);
	
	public abstract int updateByPrimaryKeySelective(Broken broken);
	
	public abstract List<Broken> queryforwx();
	
	public abstract List<BrokenProblem> queryBrokenProblem(BrokenProblemExample example);
	
	
	
	public abstract Page<Broken> queryByPage(BrokenExample example,Integer pageNum);
	public abstract List<BrokenCheck> queryByBrokenId(Integer brokenId);
	public abstract List<BrokenObjectCheck> queryProblem(Integer brokenId,String key);
	public abstract List<BrokenObjectCheck> queryBrand(Integer brokenId);
	public abstract List<BrokenObjectCheck> queryTyreSize(Integer brokenId);
	
}	
