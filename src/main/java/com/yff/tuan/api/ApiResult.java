package com.yff.tuan.api;

import com.fasterxml.jackson.annotation.JsonInclude;

import net.sf.json.JSONObject;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResult {

	private String resCode;

	private String resMsg;
	
	private Object data;

	public ApiResult() {

	}
	public ApiResult(Object data) {
		this.resCode = Result.SUCCESS.getCode();
		this.resMsg = Result.SUCCESS.getMsg();
		this.data = data;
	}
	public ApiResult(String resMsg) {
		this.resMsg = resMsg;
	}

	public ApiResult(String resCode, String resMsg) {
		this.resCode = resCode;
		this.resMsg = resMsg;
	}
	
	public ApiResult(String resCode, String resMsg,Object data) {
		this.resCode = resCode;
		this.resMsg = resMsg;
		this.data = data;
	}
	
	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	
	public static JSONObject SUCCESS(String resMsg,Object data) {
		return JSONObject.fromObject(new ApiResult(Result.SUCCESS.getCode(),resMsg, data));
	}
	public static JSONObject ERROR(Result result) {
		return JSONObject.fromObject(new ApiResult(result.getCode(),result.getMsg(),""));
	}
	public static JSONObject SUCCESS(Object data) {
		return JSONObject.fromObject(new ApiResult(Result.SUCCESS.getCode(),Result.SUCCESS.getMsg(), data));
	}
	
}
