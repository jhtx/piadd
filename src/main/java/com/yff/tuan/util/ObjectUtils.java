package com.yff.tuan.util;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by lonecloud on 17/3/12.
 * 用于对Object进行解析并且转换成Map键值对的形式
 *
 * @author lonecloud
 * @version 1.0
 */
public class ObjectUtils {

    private static final String JAVAP = "java.";
    private static final String JAVADATESTR = "java.util.Date";

    /**
     * 获取利用反射获取类里面的值和名称
     *
     * @param obj
     * @return
     * @throws IllegalAccessException
     */
    public static Map<String, Object> objectToMap(Object obj) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        Class<?> clazz = obj.getClass();
        System.out.println(clazz);
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object value = field.get(obj);
            map.put(fieldName, value);
        }
        return map;
    }

    /**
     * 利用递归调用将Object中的值全部进行获取
     *
     * @param timeFormatStr 格式化时间字符串默认<strong>2017-03-10 10:21</strong>
     * @param obj           对象
     * @param excludeFields 排除的属性
     * @return
     * @throws IllegalAccessException
     */
    public static Map<String, String> objectToMapString(String timeFormatStr, Object obj, String... excludeFields) throws IllegalAccessException {
        Map<String, String> map = new HashMap<>();

        if (excludeFields.length!=0){
            List<String> list = Arrays.asList(excludeFields);
            objectTransfer(timeFormatStr, obj, map, list);
        }else{
            objectTransfer(timeFormatStr, obj, map,null);
        }
        return map;
    }


    /**
     * 递归调用函数
     *
     * @param obj           对象
     * @param map           map
     * @param excludeFields 对应参数
     * @return
     * @throws IllegalAccessException
     */
    private static Map<String, String> objectTransfer(String timeFormatStr, Object obj, Map<String, String> map, List<String> excludeFields) throws IllegalAccessException {
        boolean isExclude=false;
        //默认字符串
        String formatStr = "YYYY-MM-dd HH:mm:ss";
        //设置格式化字符串
        if (timeFormatStr != null && !timeFormatStr.isEmpty()) {
            formatStr = timeFormatStr;
        }
        if (excludeFields!=null){
            isExclude=true;
        }
        Class<?> clazz = obj.getClass();
        //获取值
        for (Field field : clazz.getDeclaredFields()) {
            String fieldName = clazz.getSimpleName() + "." + field.getName();
            //判断是不是需要跳过某个属性
            if (isExclude&&excludeFields.contains(fieldName)){
                continue;
            }
            //设置属性可以被访问
            field.setAccessible(true);
            Object value = field.get(obj);
            Class<?> valueClass = value.getClass();
            if (valueClass.isPrimitive()) {
                map.put(fieldName, value.toString());

            } else if (valueClass.getName().contains(JAVAP)) {//判断是不是基本类型
                if (valueClass.getName().equals(JAVADATESTR)) {
                    //格式化Date类型
                    SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
                    Date date = (Date) value;
                    String dataStr = sdf.format(date);
                    map.put(fieldName, dataStr);
                } else {
                    map.put(fieldName, value.toString());
                }
            } else {
                objectTransfer(timeFormatStr, value, map,excludeFields);
            }
        }
        return map;
    }
    
    
    public static void main(String[] args) {
    	String taillight = "000000001919008600000000001F15000000";
    	String dx1 = taillight.substring(0, 2);
		String dx2 = taillight.substring(2, 4);
		String dx3 = taillight.substring(4, 6);
		String dx4 = taillight.substring(6, 8);
		String dx5 = taillight.substring(8, 10);
		String dx6 = taillight.substring(10, 12);
		String dx7 = taillight.substring(12, 14);
		String dx8 = taillight.substring(14, 16);
		
		String dy1 = taillight.substring(18, 20);
		String dy2 = taillight.substring(20, 22);
		String dy3 = taillight.substring(22, 24);
		String dy4 = taillight.substring(24, 26);
		String dy5 = taillight.substring(26, 28);
		String dy6 = taillight.substring(28, 30);
		String dy7 = taillight.substring(30, 32);
		String dy8 = taillight.substring(32, 34);
		System.out.println(dx1);
		System.out.println(dy1);
		System.out.println("--------------");
		System.out.println(dx2);
		System.out.println(dy2);
		System.out.println("--------------");
		System.out.println(dx3);
		System.out.println(dy3);
		System.out.println("--------------");
		System.out.println(dx4);
		System.out.println(dy4);
		System.out.println("--------------");
		System.out.println(dx5);
		System.out.println(dy5);
		System.out.println("--------------");
		System.out.println(dx6);
		System.out.println(dy6);
		System.out.println("--------------");
		System.out.println(dx7);
		System.out.println(dy7);
		System.out.println("--------------");
		System.out.println(dx8);
		System.out.println(dy8);
		
		double d8= Double.valueOf(Integer.parseInt(dx8,16))/10;
		
	}
}