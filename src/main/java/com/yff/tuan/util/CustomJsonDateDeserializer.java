package com.yff.tuan.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.yff.tuan.api.Result;
import com.yff.tuan.exception.AppException;

public class CustomJsonDateDeserializer extends JsonDeserializer<Date> {  
  
    @Override  
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws AppException {  
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        try { 
        	String date = jp.getText();  
            return format.parse(date);  
        } catch (Exception e) {  
            throw new AppException(Result.INVALID_PARAMETERS,new Object[]{"Date Format Error , Require patten is yyyy-MM-dd HH:mm:ss"});  
        }  
    }  
  
} 