package com.yff.tuan.util;

/**
 * The Class ErrorMessage.
 */
public class ErrorMessage{

	/** The code. */
	private String resCode;
	
	/** The message. */
	private String resMsg;
	
	/**
	 * Gets the result code.
	 *
	 * @return the result code
	 */
	public String getResCode(){
		return resCode;
	}

	/**
	 * Sets the result code.
	 *
	 * @param resultCode the new result code
	 */
	public void setResCode(String resCode){
		this.resCode = resCode;
	}

	/**
	 * Gets the result msg.
	 *
	 * @return the result msg
	 */
	public String getResMsg(){
		return resMsg;
	}

	/**
	 * Sets the result msg.
	 *
	 * @param resultMsg the new result msg
	 */
	public void setResultMsg(String resMsg){
		this.resMsg = resMsg;
	}


	/** The suss. */
	private static ErrorMessage SUSS = new ErrorMessage("0", "request success");
	
	/**
	 * Add dummy constructor to fix the No suitable constructor found Exception
	 * for type From Jackson.
	 */
	public ErrorMessage() {		
	}
	
	/**
	 * Instantiates a new error message.
	 *
	 * @param resultCode the result code
	 * @param message            the message
	 */
	public ErrorMessage(String resCode, String resMsg) {
		this.resCode = resCode;
		this.resMsg = resMsg;
	}

	
	/**
	 * No error message.
	 *
	 * @return the error message
	 */
	public static ErrorMessage noErrorMessage(){
		return SUSS;
	}
}
