package com.yff.tuan.util;

import org.springframework.http.HttpStatus;

/**
 * The class ErrorCode.
 */
public enum ErrorCode{

	/** The success. */
	SUCCESS("0", HttpStatus.OK),

	/** The invalid parameters. */
	INVALID_PARAMETERS("YFF1001", HttpStatus.BAD_REQUEST),
	
	/** The wrong json format. */
	WRONG_JSON_FORMAT("YFF1002", HttpStatus.BAD_REQUEST),
	
	/** The http do not has paramter. */
	MISSING_REQUEST_PARAMETER("YFF1003", HttpStatus.BAD_REQUEST),
	
	/** The not registered phone number. */
	NOT_REGISTERED_USER("YFF1004", HttpStatus.BAD_REQUEST),
	
	MISSING_REQUIRED_PHONE_NUMBER("YFF1005", HttpStatus.BAD_REQUEST),
	
	/** The already registered phone number. */
	ALREADY_REGISTERED_PHONE_NUMBER("YFF1006", HttpStatus.BAD_REQUEST),
	
	/** The not registered phone number. */
	NOT_REGISTERED_PHONE_NUMBER("YFF1007", HttpStatus.BAD_REQUEST),
	
	/** The password not equal. */
	PASSWORD_NOT_EQUAL("YFF1008", HttpStatus.BAD_REQUEST),
	
	/** The hash password failed. */
	HASH_PASSWORD_FAILED("YFF1009", HttpStatus.INTERNAL_SERVER_ERROR),
	
	/** The wrong phone number. */
	WRONG_PHONE_NUMBER("YFF1010", HttpStatus.BAD_REQUEST),
	
	/** The wrong password. */
	WRONG_PASSWORD("YFF1011", HttpStatus.BAD_REQUEST),
	
	/** The wrong vcode. */
	WRONG_VCODE("YFF1012", HttpStatus.BAD_REQUEST),
	
	/** The expired vcode. */
	EXPIRED_VCODE("YFF1013", HttpStatus.FORBIDDEN),//?
	
	
	/** The invalid header. */
	INVALID_HEADER("YFF2001",  HttpStatus.BAD_REQUEST),
	
	/** The missing header. */
	MISSING_HEADER("YFF2002",  HttpStatus.BAD_REQUEST),
	
	/** The http method not supported exception. */
	HTTP_METHOD_NOT_SUPPORTED_EXCEPTION("YFF2003", HttpStatus.METHOD_NOT_ALLOWED),

	/** The too frequent request. */
	TOO_FREQUENT_REQUEST("YFF2004", HttpStatus.FORBIDDEN),

	
	WECHAT_BIND_FAIL("YFF5001", HttpStatus.BAD_REQUEST),
	
	ALREADY_BIND_FAIL("YFF5002", HttpStatus.BAD_REQUEST),
	
	/** The internal server error. */
	INTERNAL_SERVER_ERROR("YFF9999", HttpStatus.INTERNAL_SERVER_ERROR),
	;
	
	
	/** The code. */
	private String code;
	
	/** The status code. */
	private HttpStatus statusCode;
	
	/**
	 * Instantiates a new error code.
	 *
	 * @param code the code
	 * @param statusCode the status code
	 */
	private ErrorCode(String code, HttpStatus statusCode) {
		this.code = code;
		this.statusCode = statusCode;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode(){
		return code;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public HttpStatus getStatusCode(){
		return statusCode;
	}
}
