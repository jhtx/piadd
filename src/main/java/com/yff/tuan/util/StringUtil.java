package com.yff.tuan.util;

import java.util.Arrays;
import java.util.Random;

public class StringUtil {
	private static final String BLANK = "";

	public static boolean isEmpty(String s) {
		return s == null || BLANK.compareTo(s) == 0;
	}

	public static boolean isEmpty(Object str) {
		return str == null || BLANK.equals(str);
	}

	public static boolean isEmpty(Object... strs) {
		for (Object str : strs) {
			if (isEmpty(str)) {
				return true;
			}
		}
		return false;
	}

	public static String dealNull(String str) {
		return str != null ? str.trim() : BLANK;
	}

	public static String dealNull(String str, String defaultVal) {
		return str != null ? str.trim() : defaultVal;
	}

	public static String dealNull(Object str, String defaultValue) {
		return str != null ? str.toString().trim() : defaultValue;
	}

	public static String dealNull(Object obj) {
		String str = BLANK;
		if (obj != null) {
			if (obj instanceof String) {
				str = (String) obj;
			} else {
				str = obj.toString();
			}
		}
		return str;
	}

	/**
	 * ת������ΪString
	 * 
	 * @param o
	 * @return
	 */
	public static String toString(Object o) {
		if (o == null) {
			return "";
		} else {
			return o.toString();
		}

	}

	public static boolean equals(Object value1, Object value2) {
		boolean is = false;
		if (value1 == value2) { // is null or self
			return is = true;
		}
		if (value1 != null && value2 != null) { // is not null;
			return value1.equals(value2);
		}
		return is;
	}

	public static int random(int n) {
		if (n == 0)
			return 0;
		Random random = new Random();
		return Math.abs(random.nextInt()) % n;
	}

	public static void main(String[] args) {
		String astr = "info+simple+detail+cohabiters";
		String bstr = "info,simple,detail,cohabiters";
		String[] a = astr.contains("+")?astr.split("\\+"):astr.split(" ");
		String[] b = bstr.split(",");
		System.out.println(compare(a, b));

	}
	
	private static String[] PARTTENS = {"[","]","="};

	public static String toUnicode(String s){
		String uncodeString = "";
		if(s==null || s.isEmpty())  return "";
		for(String partten : PARTTENS){
			if(s.contains(partten)){
				uncodeString = s.replaceAll(partten, StringUtil.string2unicode(partten));
			}
		}
		return uncodeString;
	}
	
	public static String string2unicode(String str) {
		str = (str == null ? "" : str);
		String tmp;
		StringBuffer sb = new StringBuffer(1000);
		char c;
		int i, j;
		sb.setLength(0);
		for (i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			sb.append("\\u");
			j = (c >>> 8);
			tmp = Integer.toHexString(j);
			if (tmp.length() == 1)
				sb.append("0");
			sb.append(tmp);
			j = (c & 0xFF);
			tmp = Integer.toHexString(j);
			if (tmp.length() == 1)
				sb.append("0");
			sb.append(tmp);

		}
		return sb.toString();
	}

	public static String unicode2string(String str) {
		str = (str == null ? "" : str);
		if (str.indexOf("\\u") == -1)
			return str;

		StringBuffer sb = new StringBuffer(1000);

		for (int i = 0; i <= str.length() - 6;) {
			String strTemp = str.substring(i, i + 6);
			String value = strTemp.substring(2);
			int c = 0;
			for (int j = 0; j < value.length(); j++) {
				char tempChar = value.charAt(j);
				int t = 0;
				switch (tempChar) {
				case 'a':
					t = 10;
					break;
				case 'b':
					t = 11;
					break;
				case 'c':
					t = 12;
					break;
				case 'd':
					t = 13;
					break;
				case 'e':
					t = 14;
					break;
				case 'f':
					t = 15;
					break;
				default:
					t = tempChar - 48;
					break;
				}

				c += t * ((int) Math.pow(16, (value.length() - j - 1)));
			}
			sb.append((char) c);
			i = i + 6;
		}
		return sb.toString();
	}
	
	public static boolean compare(String[] a,String[] b){
		Arrays.sort(a);
		Arrays.sort(b);
		return Arrays.equals(a, b);
	}

}