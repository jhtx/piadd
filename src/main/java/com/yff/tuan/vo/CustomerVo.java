package com.yff.tuan.vo;

import java.util.List;

import com.yff.tuan.model.Customer;
import com.yff.tuan.model.CustomerAddress;
import com.yff.tuan.model.CustomerBrand;
import com.yff.tuan.model.CustomerComment;
import com.yff.tuan.model.CustomerContact;
import com.yff.tuan.model.CustomerEvn;
import com.yff.tuan.model.CustomerInfo;
import com.yff.tuan.model.CustomerMarket;
import com.yff.tuan.model.CustomerPoint;
import com.yff.tuan.model.CustomerProblem;
import com.yff.tuan.model.CustomerRequire;
import com.yff.tuan.model.CustomerService;
import com.yff.tuan.model.CustomerTyreSize;

public class CustomerVo extends Customer{
	
	private String userName;
	private String statusName;
	private String failureReasonName;
	private String time;
	
	private List<CustomerAddress> addresses;
	private List<CustomerContact> contacts;
	private List<CustomerTyreSize> tyreSizes;
	private List<CustomerBrand> brands;
	private List<CustomerMarket> markets;
	private List<CustomerEvn> evns;
	private List<CustomerInfo> infos;
	private List<CustomerProblem> problems;
	private List<CustomerRequire> requires;
	private List<CustomerService> services;
	private List<CustomerPoint> points;
	private List<CustomerComment> comments;
	
	
	private String addressesJson;
	private String contactsJson;
	private String tyreSizesJson;
	private String brandsJson;
	private String marketsJson;
	private String evnsJson;
	private String infosJson;
	private String problemsJson;
	private String requiresJson;
	private String servicesJson;
	private String pointsJson;
	private String commentsJson;
	
	
	
	
	public List<CustomerBrand> getBrands() {
		return brands;
	}
	public void setBrands(List<CustomerBrand> brands) {
		this.brands = brands;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public List<CustomerAddress> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<CustomerAddress> addresses) {
		this.addresses = addresses;
	}
	public List<CustomerContact> getContacts() {
		return contacts;
	}
	public void setContacts(List<CustomerContact> contacts) {
		this.contacts = contacts;
	}
	public List<CustomerEvn> getEvns() {
		return evns;
	}
	public void setEvns(List<CustomerEvn> evns) {
		this.evns = evns;
	}
	public List<CustomerInfo> getInfos() {
		return infos;
	}
	public void setInfos(List<CustomerInfo> infos) {
		this.infos = infos;
	}
	public List<CustomerMarket> getMarkets() {
		return markets;
	}
	public void setMarkets(List<CustomerMarket> markets) {
		this.markets = markets;
	}
	public List<CustomerProblem> getProblems() {
		return problems;
	}
	public void setProblems(List<CustomerProblem> problems) {
		this.problems = problems;
	}
	public List<CustomerRequire> getRequires() {
		return requires;
	}
	public void setRequires(List<CustomerRequire> requires) {
		this.requires = requires;
	}
	public List<CustomerService> getServices() {
		return services;
	}
	public void setServices(List<CustomerService> services) {
		this.services = services;
	}
	public List<CustomerTyreSize> getTyreSizes() {
		return tyreSizes;
	}
	public void setTyreSizes(List<CustomerTyreSize> tyreSizes) {
		this.tyreSizes = tyreSizes;
	}
	public List<CustomerPoint> getPoints() {
		return points;
	}
	public void setPoints(List<CustomerPoint> points) {
		this.points = points;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFailureReasonName() {
		return failureReasonName;
	}
	public void setFailureReasonName(String failureReasonName) {
		this.failureReasonName = failureReasonName;
	}
	public List<CustomerComment> getComments() {
		return comments;
	}
	public void setComments(List<CustomerComment> comments) {
		this.comments = comments;
	}
	public String getAddressesJson() {
		return addressesJson;
	}
	public void setAddressesJson(String addressesJson) {
		this.addressesJson = addressesJson;
	}
	public String getContactsJson() {
		return contactsJson;
	}
	public void setContactsJson(String contactsJson) {
		this.contactsJson = contactsJson;
	}
	public String getTyreSizesJson() {
		return tyreSizesJson;
	}
	public void setTyreSizesJson(String tyreSizesJson) {
		this.tyreSizesJson = tyreSizesJson;
	}
	public String getBrandsJson() {
		return brandsJson;
	}
	public void setBrandsJson(String brandsJson) {
		this.brandsJson = brandsJson;
	}
	public String getMarketsJson() {
		return marketsJson;
	}
	public void setMarketsJson(String marketsJson) {
		this.marketsJson = marketsJson;
	}
	public String getEvnsJson() {
		return evnsJson;
	}
	public void setEvnsJson(String evnsJson) {
		this.evnsJson = evnsJson;
	}
	public String getInfosJson() {
		return infosJson;
	}
	public void setInfosJson(String infosJson) {
		this.infosJson = infosJson;
	}
	public String getProblemsJson() {
		return problemsJson;
	}
	public void setProblemsJson(String problemsJson) {
		this.problemsJson = problemsJson;
	}
	public String getRequiresJson() {
		return requiresJson;
	}
	public void setRequiresJson(String requiresJson) {
		this.requiresJson = requiresJson;
	}
	public String getServicesJson() {
		return servicesJson;
	}
	public void setServicesJson(String servicesJson) {
		this.servicesJson = servicesJson;
	}
	public String getPointsJson() {
		return pointsJson;
	}
	public void setPointsJson(String pointsJson) {
		this.pointsJson = pointsJson;
	}
	public String getCommentsJson() {
		return commentsJson;
	}
	public void setCommentsJson(String commentsJson) {
		this.commentsJson = commentsJson;
	}
	
	
}
