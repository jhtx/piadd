package com.yff.tuan.vo;

import java.util.List;

public class BrokenReport {
	private String CustomerName;
	private String CreateTime;
	private int VType1Count;
	private int VType2Count;
	private int VType3Count;
	private int VTypeTotalCount;
	
	private int VDrive1Count;
	private int VDrive2Count;
	private int VDrive3Count;
	private int VDriveTotalCount;
	private double VType1Rate;
	private double VType2Rate;
	private double VType3Rate;

	private double VDrive1Rate;
	private double VDrive2Rate;
	private double VDrive3Rate;
	
	private double SZBRate;
	private double SBRate;
	private double STotalRate;
	
	private int J1Count;
	private int J2Count;
	private int J3Count;
	private int J4Count;
	private int J5Count;
	private int J6Count;
	private int JTotal;
	
	private double J1Rate;
	private double J2Rate;
	private double J3Rate;
	private double J4Rate;
	private double J5Rate;
	private double J6Rate;
	private double JTotalRate;
	
	
	private int A6;
	private int A8;
	private int A10;
	private int A12;
	private int ATotal;
	private int AW;
	
	private int C6;
	private int C8;
	private int C10;
	private int C12;
	private int CTotal;
	private int AWC;
	
	private double C6Rate;
	private double C8Rate;
	private double C10Rate;
	private double 	C12Rate;
	private double 	CTotalRate;
	private double 	AWCRate;
	
	private double 	DdPress;
	private double DqPress;
	private double DtPress;
	
	private double UdPress;
	private double UqPress;
	private double UtPress;
	
	private double SDPress;
	private double SqPress;
	private double StPress;
	
	
	private int RUnder10Count;
	private int RUnder20Count;
	private int RUnder30Count;
	private int CUnder10Count;
	private int CUnder20Count;
	private int CPassCount;
	private int NotCheckCount;
	private int TotalPressCount;
	
	private double RUnder10Rate;
	private double RUnder20Rate;
	private double RUnder30Rate;
	private double CUnder10Rate;
	private double 	CUnder20Rate;
	private double 	CPassRate;
	private double 	NotCheckRate;
	private double 	TotalPressRate;
	
	private int RUnder10Total;
	private int RUnder20Total;
	private int RUnder30Total;
	private int CUnder10Total;
	private int CUnder20Total;
	private int CPassTotal;
	private int NotCheckTotal;
	private int TotalPressTotal;
	
	private int BNCount;
	private int B24Count;
	private int B4Count;
	private int BTotalCount;
	private double BNRate;
	private double B24Rate;
	private double B4Rate;
	private double BTotalRate;
	
	private int BNTotal;
	private int B24Total;
	private int B4Total;
	private int BTotal;
	
	private int L0Count;
	private int L4Count;
	private int L8Count;
	private int L12Count;
	private int LTotalCount;
	
	private double L0Rate;
	private double 	L4Rate;
	private double 	L8Rate;
	private double 	L12Rate;
	private double 	LTotalRate;
	
	private int L0Total;
	private int L4Total;
	private int L8Total;
	private int L12Total;
	private int LTotal;
	
	private int SX;
	private int SN;
	private int SL;
	private int ST;
	private int SP;
	private int 	STire;
	private int 	SCount;
	
	private double 	SXRate;
	private double 	SNRate;
	private double 	SLRate;
	private double 	STRate;
	private double 	SPRate;
	private double 	STireRate;
	private double 	SCountRate;
	
	private int SXTotal;
	private int SNTotal;
	private int SLTotal;
	private int STTotal;
	private int SPTotal;
	private int STireTotal;
	private int SCountTotal;
	
	private double 	GuideAir;
	private double 	DiriveAir;
	private double 	DragAir;
	private double 	vamp;
	private double 	tamp;
	
	List<CVehicle> cv;

	public String getCustomerName() {
		return CustomerName;
	}

	public String getCreateTime() {
		return CreateTime;
	}

	public int getVType1Count() {
		return VType1Count;
	}

	public int getVType2Count() {
		return VType2Count;
	}

	public int getVType3Count() {
		return VType3Count;
	}

	public int getVTypeTotalCount() {
		return VTypeTotalCount;
	}

	public int getVDrive1Count() {
		return VDrive1Count;
	}

	public int getVDrive2Count() {
		return VDrive2Count;
	}

	public int getVDrive3Count() {
		return VDrive3Count;
	}

	public int getVDriveTotalCount() {
		return VDriveTotalCount;
	}

	public double getVType1Rate() {
		return VType1Rate;
	}

	public double getVType2Rate() {
		return VType2Rate;
	}

	public double getVType3Rate() {
		return VType3Rate;
	}

	public double getVDrive1Rate() {
		return VDrive1Rate;
	}

	public double getVDrive2Rate() {
		return VDrive2Rate;
	}

	public double getVDrive3Rate() {
		return VDrive3Rate;
	}

	public double getSZBRate() {
		return SZBRate;
	}

	public double getSBRate() {
		return SBRate;
	}

	public double getSTotalRate() {
		return STotalRate;
	}

	public int getJ1Count() {
		return J1Count;
	}

	public int getJ2Count() {
		return J2Count;
	}

	public int getJ3Count() {
		return J3Count;
	}

	public int getJ4Count() {
		return J4Count;
	}

	public int getJ5Count() {
		return J5Count;
	}

	public int getJ6Count() {
		return J6Count;
	}

	public int getJTotal() {
		return JTotal;
	}

	public double getJ1Rate() {
		return J1Rate;
	}

	public double getJ2Rate() {
		return J2Rate;
	}

	public double getJ3Rate() {
		return J3Rate;
	}

	public double getJ4Rate() {
		return J4Rate;
	}

	public double getJ5Rate() {
		return J5Rate;
	}

	public double getJ6Rate() {
		return J6Rate;
	}

	public double getJTotalRate() {
		return JTotalRate;
	}

	public int getA6() {
		return A6;
	}

	public int getA8() {
		return A8;
	}

	public int getA10() {
		return A10;
	}

	public int getA12() {
		return A12;
	}

	public int getATotal() {
		return ATotal;
	}

	public int getAW() {
		return AW;
	}

	public int getC6() {
		return C6;
	}

	public int getC8() {
		return C8;
	}

	public int getC10() {
		return C10;
	}

	public int getC12() {
		return C12;
	}

	public int getCTotal() {
		return CTotal;
	}

	public int getAWC() {
		return AWC;
	}

	public double getC6Rate() {
		return C6Rate;
	}

	public double getC8Rate() {
		return C8Rate;
	}

	public double getC10Rate() {
		return C10Rate;
	}

	public double getC12Rate() {
		return C12Rate;
	}

	public double getCTotalRate() {
		return CTotalRate;
	}

	public double getAWCRate() {
		return AWCRate;
	}

	public double getDdPress() {
		return DdPress;
	}

	public double getDqPress() {
		return DqPress;
	}

	public double getDtPress() {
		return DtPress;
	}

	public double getUdPress() {
		return UdPress;
	}

	public double getUqPress() {
		return UqPress;
	}

	public double getUtPress() {
		return UtPress;
	}

	public double getSDPress() {
		return SDPress;
	}

	public double getSqPress() {
		return SqPress;
	}

	public double getStPress() {
		return StPress;
	}

	public int getRUnder10Count() {
		return RUnder10Count;
	}

	public int getRUnder20Count() {
		return RUnder20Count;
	}

	public int getRUnder30Count() {
		return RUnder30Count;
	}

	public int getCUnder10Count() {
		return CUnder10Count;
	}

	public int getCUnder20Count() {
		return CUnder20Count;
	}

	public int getCPassCount() {
		return CPassCount;
	}

	public int getNotCheckCount() {
		return NotCheckCount;
	}

	public int getTotalPressCount() {
		return TotalPressCount;
	}

	public double getRUnder10Rate() {
		return RUnder10Rate;
	}

	public double getRUnder20Rate() {
		return RUnder20Rate;
	}

	public double getRUnder30Rate() {
		return RUnder30Rate;
	}

	public double getCUnder10Rate() {
		return CUnder10Rate;
	}

	public double getCUnder20Rate() {
		return CUnder20Rate;
	}

	public double getCPassRate() {
		return CPassRate;
	}

	public double getNotCheckRate() {
		return NotCheckRate;
	}

	public double getTotalPressRate() {
		return TotalPressRate;
	}

	public int getRUnder10Total() {
		return RUnder10Total;
	}

	public int getRUnder20Total() {
		return RUnder20Total;
	}

	public int getRUnder30Total() {
		return RUnder30Total;
	}

	public int getCUnder10Total() {
		return CUnder10Total;
	}

	public int getCUnder20Total() {
		return CUnder20Total;
	}

	public int getCPassTotal() {
		return CPassTotal;
	}

	public int getNotCheckTotal() {
		return NotCheckTotal;
	}

	public int getTotalPressTotal() {
		return TotalPressTotal;
	}

	public int getBNCount() {
		return BNCount;
	}

	public int getB24Count() {
		return B24Count;
	}

	public int getB4Count() {
		return B4Count;
	}

	public int getBTotalCount() {
		return BTotalCount;
	}

	public double getBNRate() {
		return BNRate;
	}

	public double getB24Rate() {
		return B24Rate;
	}

	public double getB4Rate() {
		return B4Rate;
	}

	public double getBTotalRate() {
		return BTotalRate;
	}

	public int getBNTotal() {
		return BNTotal;
	}

	public int getB24Total() {
		return B24Total;
	}

	public int getB4Total() {
		return B4Total;
	}

	public int getBTotal() {
		return BTotal;
	}

	public int getL0Count() {
		return L0Count;
	}

	public int getL4Count() {
		return L4Count;
	}

	public int getL8Count() {
		return L8Count;
	}

	public int getL12Count() {
		return L12Count;
	}

	public int getLTotalCount() {
		return LTotalCount;
	}

	public double getL0Rate() {
		return L0Rate;
	}

	public double getL4Rate() {
		return L4Rate;
	}

	public double getL8Rate() {
		return L8Rate;
	}

	public double getL12Rate() {
		return L12Rate;
	}

	public double getLTotalRate() {
		return LTotalRate;
	}

	public int getL0Total() {
		return L0Total;
	}

	public int getL4Total() {
		return L4Total;
	}

	public int getL8Total() {
		return L8Total;
	}

	public int getL12Total() {
		return L12Total;
	}

	public int getLTotal() {
		return LTotal;
	}

	public int getSX() {
		return SX;
	}

	public int getSN() {
		return SN;
	}

	public int getSL() {
		return SL;
	}

	public int getST() {
		return ST;
	}

	public int getSP() {
		return SP;
	}

	public int getSTire() {
		return STire;
	}

	public int getSCount() {
		return SCount;
	}

	public double getSXRate() {
		return SXRate;
	}

	public double getSNRate() {
		return SNRate;
	}

	public double getSLRate() {
		return SLRate;
	}

	public double getSTRate() {
		return STRate;
	}

	public double getSPRate() {
		return SPRate;
	}

	public double getSTireRate() {
		return STireRate;
	}

	public double getSCountRate() {
		return SCountRate;
	}

	public int getSXTotal() {
		return SXTotal;
	}

	public int getSNTotal() {
		return SNTotal;
	}

	public int getSLTotal() {
		return SLTotal;
	}

	public int getSTTotal() {
		return STTotal;
	}

	public int getSPTotal() {
		return SPTotal;
	}

	public int getSTireTotal() {
		return STireTotal;
	}

	public int getSCountTotal() {
		return SCountTotal;
	}

	public double getGuideAir() {
		return GuideAir;
	}

	public double getDiriveAir() {
		return DiriveAir;
	}

	public double getDragAir() {
		return DragAir;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public void setVType1Count(int vType1Count) {
		VType1Count = vType1Count;
	}

	public void setVType2Count(int vType2Count) {
		VType2Count = vType2Count;
	}

	public void setVType3Count(int vType3Count) {
		VType3Count = vType3Count;
	}

	public void setVTypeTotalCount(int vTypeTotalCount) {
		VTypeTotalCount = vTypeTotalCount;
	}

	public void setVDrive1Count(int vDrive1Count) {
		VDrive1Count = vDrive1Count;
	}

	public void setVDrive2Count(int vDrive2Count) {
		VDrive2Count = vDrive2Count;
	}

	public void setVDrive3Count(int vDrive3Count) {
		VDrive3Count = vDrive3Count;
	}

	public void setVDriveTotalCount(int vDriveTotalCount) {
		VDriveTotalCount = vDriveTotalCount;
	}

	public void setVType1Rate(double vType1Rate) {
		VType1Rate = vType1Rate;
	}

	public void setVType2Rate(double vType2Rate) {
		VType2Rate = vType2Rate;
	}

	public void setVType3Rate(double vType3Rate) {
		VType3Rate = vType3Rate;
	}

	public void setVDrive1Rate(double vDrive1Rate) {
		VDrive1Rate = vDrive1Rate;
	}

	public void setVDrive2Rate(double vDrive2Rate) {
		VDrive2Rate = vDrive2Rate;
	}

	public void setVDrive3Rate(double vDrive3Rate) {
		VDrive3Rate = vDrive3Rate;
	}

	public void setSZBRate(double sZBRate) {
		SZBRate = sZBRate;
	}

	public void setSBRate(double sBRate) {
		SBRate = sBRate;
	}

	public void setSTotalRate(double sTotalRate) {
		STotalRate = sTotalRate;
	}

	public void setJ1Count(int j1Count) {
		J1Count = j1Count;
	}

	public void setJ2Count(int j2Count) {
		J2Count = j2Count;
	}

	public void setJ3Count(int j3Count) {
		J3Count = j3Count;
	}

	public void setJ4Count(int j4Count) {
		J4Count = j4Count;
	}

	public void setJ5Count(int j5Count) {
		J5Count = j5Count;
	}

	public void setJ6Count(int j6Count) {
		J6Count = j6Count;
	}

	public void setJTotal(int jTotal) {
		JTotal = jTotal;
	}

	public void setJ1Rate(double j1Rate) {
		J1Rate = j1Rate;
	}

	public void setJ2Rate(double j2Rate) {
		J2Rate = j2Rate;
	}

	public void setJ3Rate(double j3Rate) {
		J3Rate = j3Rate;
	}

	public void setJ4Rate(double j4Rate) {
		J4Rate = j4Rate;
	}

	public void setJ5Rate(double j5Rate) {
		J5Rate = j5Rate;
	}

	public void setJ6Rate(double j6Rate) {
		J6Rate = j6Rate;
	}

	public void setJTotalRate(double jTotalRate) {
		JTotalRate = jTotalRate;
	}

	public void setA6(int a6) {
		A6 = a6;
	}

	public void setA8(int a8) {
		A8 = a8;
	}

	public void setA10(int a10) {
		A10 = a10;
	}

	public void setA12(int a12) {
		A12 = a12;
	}

	public void setATotal(int aTotal) {
		ATotal = aTotal;
	}

	public void setAW(int aW) {
		AW = aW;
	}

	public void setC6(int c6) {
		C6 = c6;
	}

	public void setC8(int c8) {
		C8 = c8;
	}

	public void setC10(int c10) {
		C10 = c10;
	}

	public void setC12(int c12) {
		C12 = c12;
	}

	public void setCTotal(int cTotal) {
		CTotal = cTotal;
	}

	public void setAWC(int aWC) {
		AWC = aWC;
	}

	public void setC6Rate(double c6Rate) {
		C6Rate = c6Rate;
	}

	public void setC8Rate(double c8Rate) {
		C8Rate = c8Rate;
	}

	public void setC10Rate(double c10Rate) {
		C10Rate = c10Rate;
	}

	public void setC12Rate(double c12Rate) {
		C12Rate = c12Rate;
	}

	public void setCTotalRate(double cTotalRate) {
		CTotalRate = cTotalRate;
	}

	public void setAWCRate(double aWCRate) {
		AWCRate = aWCRate;
	}

	public void setDdPress(double ddPress) {
		DdPress = ddPress;
	}

	public void setDqPress(double dqPress) {
		DqPress = dqPress;
	}

	public void setDtPress(double dtPress) {
		DtPress = dtPress;
	}

	public void setUdPress(double udPress) {
		UdPress = udPress;
	}

	public void setUqPress(double uqPress) {
		UqPress = uqPress;
	}

	public void setUtPress(double utPress) {
		UtPress = utPress;
	}

	public void setSDPress(double sDPress) {
		SDPress = sDPress;
	}

	public void setSqPress(double sqPress) {
		SqPress = sqPress;
	}

	public void setStPress(double stPress) {
		StPress = stPress;
	}

	public void setRUnder10Count(int rUnder10Count) {
		RUnder10Count = rUnder10Count;
	}

	public void setRUnder20Count(int rUnder20Count) {
		RUnder20Count = rUnder20Count;
	}

	public void setRUnder30Count(int rUnder30Count) {
		RUnder30Count = rUnder30Count;
	}

	public void setCUnder10Count(int cUnder10Count) {
		CUnder10Count = cUnder10Count;
	}

	public void setCUnder20Count(int cUnder20Count) {
		CUnder20Count = cUnder20Count;
	}

	public void setCPassCount(int cPassCount) {
		CPassCount = cPassCount;
	}

	public void setNotCheckCount(int notCheckCount) {
		NotCheckCount = notCheckCount;
	}

	public void setTotalPressCount(int totalPressCount) {
		TotalPressCount = totalPressCount;
	}

	public void setRUnder10Rate(double rUnder10Rate) {
		RUnder10Rate = rUnder10Rate;
	}

	public void setRUnder20Rate(double rUnder20Rate) {
		RUnder20Rate = rUnder20Rate;
	}

	public void setRUnder30Rate(double rUnder30Rate) {
		RUnder30Rate = rUnder30Rate;
	}

	public void setCUnder10Rate(double cUnder10Rate) {
		CUnder10Rate = cUnder10Rate;
	}

	public void setCUnder20Rate(double cUnder20Rate) {
		CUnder20Rate = cUnder20Rate;
	}

	public void setCPassRate(double cPassRate) {
		CPassRate = cPassRate;
	}

	public void setNotCheckRate(double notCheckRate) {
		NotCheckRate = notCheckRate;
	}

	public void setTotalPressRate(double totalPressRate) {
		TotalPressRate = totalPressRate;
	}

	public void setRUnder10Total(int rUnder10Total) {
		RUnder10Total = rUnder10Total;
	}

	public void setRUnder20Total(int rUnder20Total) {
		RUnder20Total = rUnder20Total;
	}

	public void setRUnder30Total(int rUnder30Total) {
		RUnder30Total = rUnder30Total;
	}

	public void setCUnder10Total(int cUnder10Total) {
		CUnder10Total = cUnder10Total;
	}

	public void setCUnder20Total(int cUnder20Total) {
		CUnder20Total = cUnder20Total;
	}

	public void setCPassTotal(int cPassTotal) {
		CPassTotal = cPassTotal;
	}

	public void setNotCheckTotal(int notCheckTotal) {
		NotCheckTotal = notCheckTotal;
	}

	public void setTotalPressTotal(int totalPressTotal) {
		TotalPressTotal = totalPressTotal;
	}

	public void setBNCount(int bNCount) {
		BNCount = bNCount;
	}

	public void setB24Count(int b24Count) {
		B24Count = b24Count;
	}

	public void setB4Count(int b4Count) {
		B4Count = b4Count;
	}

	public void setBTotalCount(int bTotalCount) {
		BTotalCount = bTotalCount;
	}

	public void setBNRate(int bNRate) {
		BNRate = bNRate;
	}

	public void setB24Rate(int b24Rate) {
		B24Rate = b24Rate;
	}

	public void setB4Rate(int b4Rate) {
		B4Rate = b4Rate;
	}

	public void setBTotalRate(int bTotalRate) {
		BTotalRate = bTotalRate;
	}

	public void setBNTotal(int bNTotal) {
		BNTotal = bNTotal;
	}

	public void setB24Total(int b24Total) {
		B24Total = b24Total;
	}

	public void setB4Total(int b4Total) {
		B4Total = b4Total;
	}

	public void setBTotal(int bTotal) {
		BTotal = bTotal;
	}

	public void setL0Count(int l0Count) {
		L0Count = l0Count;
	}

	public void setL4Count(int l4Count) {
		L4Count = l4Count;
	}

	public void setL8Count(int l8Count) {
		L8Count = l8Count;
	}

	public void setL12Count(int l12Count) {
		L12Count = l12Count;
	}

	public void setLTotalCount(int lTotalCount) {
		LTotalCount = lTotalCount;
	}

	public void setL0Rate(double l0Rate) {
		L0Rate = l0Rate;
	}

	public void setL4Rate(double l4Rate) {
		L4Rate = l4Rate;
	}

	public void setL8Rate(double l8Rate) {
		L8Rate = l8Rate;
	}

	public void setL12Rate(double l12Rate) {
		L12Rate = l12Rate;
	}

	public void setLTotalRate(double lTotalRate) {
		LTotalRate = lTotalRate;
	}

	public void setL0Total(int l0Total) {
		L0Total = l0Total;
	}

	public void setL4Total(int l4Total) {
		L4Total = l4Total;
	}

	public void setL8Total(int l8Total) {
		L8Total = l8Total;
	}

	public void setL12Total(int l12Total) {
		L12Total = l12Total;
	}

	public void setLTotal(int lTotal) {
		LTotal = lTotal;
	}

	public void setSX(int sX) {
		SX = sX;
	}

	public void setSN(int sN) {
		SN = sN;
	}

	public void setSL(int sL) {
		SL = sL;
	}

	public void setST(int sT) {
		ST = sT;
	}

	public void setSP(int sP) {
		SP = sP;
	}

	public void setSTire(int sTire) {
		STire = sTire;
	}

	public void setSCount(int sCount) {
		SCount = sCount;
	}

	public void setSXRate(double sXRate) {
		SXRate = sXRate;
	}

	public void setSNRate(double sNRate) {
		SNRate = sNRate;
	}

	public void setSLRate(double sLRate) {
		SLRate = sLRate;
	}

	public void setSTRate(double sTRate) {
		STRate = sTRate;
	}

	public void setSPRate(double sPRate) {
		SPRate = sPRate;
	}

	public void setSTireRate(double sTireRate) {
		STireRate = sTireRate;
	}

	public void setSCountRate(double sCountRate) {
		SCountRate = sCountRate;
	}

	public void setSXTotal(int sXTotal) {
		SXTotal = sXTotal;
	}

	public void setSNTotal(int sNTotal) {
		SNTotal = sNTotal;
	}

	public void setSLTotal(int sLTotal) {
		SLTotal = sLTotal;
	}

	public void setSTTotal(int sTTotal) {
		STTotal = sTTotal;
	}

	public void setSPTotal(int sPTotal) {
		SPTotal = sPTotal;
	}

	public void setSTireTotal(int sTireTotal) {
		STireTotal = sTireTotal;
	}

	public void setSCountTotal(int sCountTotal) {
		SCountTotal = sCountTotal;
	}

	public void setGuideAir(double guideAir) {
		GuideAir = guideAir;
	}

	public void setDiriveAir(double diriveAir) {
		DiriveAir = diriveAir;
	}

	public void setDragAir(double dragAir) {
		DragAir = dragAir;
	}

	public double getVamp() {
		return vamp;
	}

	public double getTamp() {
		return tamp;
	}

	public void setVamp(double vamp) {
		this.vamp = vamp;
	}

	public void setTamp(double tamp) {
		this.tamp = tamp;
	}

	public void setBNRate(double bNRate) {
		BNRate = bNRate;
	}

	public void setB24Rate(double b24Rate) {
		B24Rate = b24Rate;
	}

	public void setB4Rate(double b4Rate) {
		B4Rate = b4Rate;
	}

	public void setBTotalRate(double bTotalRate) {
		BTotalRate = bTotalRate;
	}

	public List<CVehicle> getCv() {
		return cv;
	}

	public void setCv(List<CVehicle> cv) {
		this.cv = cv;
	}

	
}
