package com.yff.tuan.vo;

import java.util.List;

import com.yff.tuan.model.User;
import com.yff.tuan.model.WxUser;

public class UserVo extends User{
	
	private String openId;
	private String roleName;
	private Integer pageNum;
	
	private List<WxUser> wxUsers;
	private String wxUsersJson;
	
	
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getPageNum() {
		return pageNum;
	}
	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}
	public List<WxUser> getWxUsers() {
		return wxUsers;
	}
	public void setWxUsers(List<WxUser> wxUsers) {
		this.wxUsers = wxUsers;
	}
	public String getWxUsersJson() {
		return wxUsersJson;
	}
	public void setWxUsersJson(String wxUsersJson) {
		this.wxUsersJson = wxUsersJson;
	}
	
}
