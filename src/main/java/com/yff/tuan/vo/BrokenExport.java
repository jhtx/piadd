package com.yff.tuan.vo;

import java.util.List;

public class BrokenExport {
	private String customerName;
	private String createTime;
	private String image1;
    private String image2;
    private String image3;
    private String image4;
    private String image5;
    private String image6;
    private String image7;
    private String image8;
    
    private String comment1;
    private String comment2;
    private String comment3;
    private String comment4;
    private List<BrokenObject> problems;
    private int problemCount;
    private int tread02Count;
    private double tread02Rate;
    private int tread35Count;
    private double tread35Rate;
    private int tread60Count;
    private double tread60Rate;
    private int tread11Count;
    private double tread11Rate;
    
    private List<BrokenObjectCheck> brands;
    private List<BrokenObjectCheck> tyreSizes;
    
    private int xCount;
    private double xRate;
    private int rCount;
    private double rRate;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
	public String getImage3() {
		return image3;
	}
	public void setImage3(String image3) {
		this.image3 = image3;
	}
	public String getImage4() {
		return image4;
	}
	public void setImage4(String image4) {
		this.image4 = image4;
	}
	public String getImage5() {
		return image5;
	}
	public void setImage5(String image5) {
		this.image5 = image5;
	}
	public String getImage6() {
		return image6;
	}
	public void setImage6(String image6) {
		this.image6 = image6;
	}
	public String getImage7() {
		return image7;
	}
	public void setImage7(String image7) {
		this.image7 = image7;
	}
	public String getImage8() {
		return image8;
	}
	public void setImage8(String image8) {
		this.image8 = image8;
	}
	public String getComment1() {
		return comment1;
	}
	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}
	public String getComment2() {
		return comment2;
	}
	public void setComment2(String comment2) {
		this.comment2 = comment2;
	}
	public String getComment3() {
		return comment3;
	}
	public void setComment3(String comment3) {
		this.comment3 = comment3;
	}
	public String getComment4() {
		return comment4;
	}
	public void setComment4(String comment4) {
		this.comment4 = comment4;
	}
	public List<BrokenObject> getProblems() {
		return problems;
	}
	public void setProblems(List<BrokenObject> problems) {
		this.problems = problems;
	}
	public int getProblemCount() {
		return problemCount;
	}
	public void setProblemCount(int problemCount) {
		this.problemCount = problemCount;
	}
	public int getTread02Count() {
		return tread02Count;
	}
	public void setTread02Count(int tread02Count) {
		this.tread02Count = tread02Count;
	}
	public double getTread02Rate() {
		return tread02Rate;
	}
	public void setTread02Rate(double tread02Rate) {
		this.tread02Rate = tread02Rate;
	}
	public int getTread35Count() {
		return tread35Count;
	}
	public void setTread35Count(int tread35Count) {
		this.tread35Count = tread35Count;
	}
	public double getTread35Rate() {
		return tread35Rate;
	}
	public void setTread35Rate(double tread35Rate) {
		this.tread35Rate = tread35Rate;
	}
	public int getTread60Count() {
		return tread60Count;
	}
	public void setTread60Count(int tread60Count) {
		this.tread60Count = tread60Count;
	}
	public double getTread60Rate() {
		return tread60Rate;
	}
	public void setTread60Rate(double tread60Rate) {
		this.tread60Rate = tread60Rate;
	}
	public int getTread11Count() {
		return tread11Count;
	}
	public void setTread11Count(int tread11Count) {
		this.tread11Count = tread11Count;
	}
	public double getTread11Rate() {
		return tread11Rate;
	}
	public void setTread11Rate(double tread11Rate) {
		this.tread11Rate = tread11Rate;
	}
	public List<BrokenObjectCheck> getBrands() {
		return brands;
	}
	public void setBrands(List<BrokenObjectCheck> brands) {
		this.brands = brands;
	}
	public List<BrokenObjectCheck> getTyreSizes() {
		return tyreSizes;
	}
	public void setTyreSizes(List<BrokenObjectCheck> tyreSizes) {
		this.tyreSizes = tyreSizes;
	}
	public int getxCount() {
		return xCount;
	}
	public void setxCount(int xCount) {
		this.xCount = xCount;
	}
	public double getxRate() {
		return xRate;
	}
	public void setxRate(double xRate) {
		this.xRate = xRate;
	}
	public int getrCount() {
		return rCount;
	}
	public void setrCount(int rCount) {
		this.rCount = rCount;
	}
	public double getrRate() {
		return rRate;
	}
	public void setrRate(double rRate) {
		this.rRate = rRate;
	}
    
     
}
