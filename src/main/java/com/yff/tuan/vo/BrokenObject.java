package com.yff.tuan.vo;

import java.util.List;

public class BrokenObject {
	private String name;
	private String count;
	private String rate;
	private List<BrokenObjectCheck> checks;
	
	public String getName() {
		return name;
	}

	public List<BrokenObjectCheck> getChecks() {
		return checks;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setChecks(List<BrokenObjectCheck> checks) {
		this.checks = checks;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}
	
}
