package com.yff.tuan.vo;

public class BrokenObjectCheck {
	private String name;
	private Integer count;
	private String rate;
	public String getName() {
		return name;
	}
	public Integer getCount() {
		return count;
	}
	public String getRate() {
		return rate;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	
	
}
