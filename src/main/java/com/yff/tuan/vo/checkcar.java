package com.yff.tuan.vo;

public class checkcar {
	private String 	TN;
	private double 	Pr;
	private String 	Tread;
	private String 	Brand;
	private double 	P1;
	private double 	P2;
	private double 	P3;
	private String 	ty;

	
	public String getTN() {
		return TN;
	}


	public void setTN(String tN) {
		TN = tN;
	}

	public String getTread() {
		return Tread;
	}

	public String getBrand() {
		return Brand;
	}

	public double getP1() {
		return P1;
	}

	public double getP2() {
		return P2;
	}

	public double getP3() {
		return P3;
	}

	public String getTy() {
		return ty;
	}

	public void setTread(String tread) {
		Tread = tread;
	}

	public void setBrand(String brand) {
		Brand = brand;
	}

	public void setP1(double p1) {
		P1 = p1;
	}

	public void setP2(double p2) {
		P2 = p2;
	}

	public void setP3(double p3) {
		P3 = p3;
	}

	public void setTy(String ty) {
		this.ty = ty;
	}


	public double getPr() {
		return Pr;
	}


	public void setPr(double pr) {
		Pr = pr;
	}
	
	
}
