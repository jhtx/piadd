package com.yff.tuan.vo;

import java.util.List;

import com.yff.tuan.model.Customer;
import com.yff.tuan.model.CustomerAddress;
import com.yff.tuan.model.CustomerBrand;
import com.yff.tuan.model.CustomerComment;
import com.yff.tuan.model.CustomerContact;
import com.yff.tuan.model.CustomerEvn;
import com.yff.tuan.model.CustomerInfo;
import com.yff.tuan.model.CustomerMarket;
import com.yff.tuan.model.CustomerPoint;
import com.yff.tuan.model.CustomerProblem;
import com.yff.tuan.model.CustomerRequire;
import com.yff.tuan.model.CustomerService;
import com.yff.tuan.model.CustomerTyreSize;

public class CustomerAu {
	private Customer customer;
	private List<CustomerAddress> addresses;
	private List<CustomerContact> contacts;
	private List<CustomerTyreSize> tyreSizes;
	private List<CustomerBrand> brands;
	private List<CustomerMarket> markets;
	private List<CustomerEvn> evns;
	private List<CustomerInfo> infos;
	private List<CustomerProblem> problems;
	private List<CustomerRequire> requires;
	private List<CustomerService> services;
	private List<CustomerPoint> points;
	private List<CustomerComment> comments;
	public Customer getCustomer() {
		return customer;
	}
	public List<CustomerAddress> getAddresses() {
		return addresses;
	}
	public List<CustomerContact> getContacts() {
		return contacts;
	}
	public List<CustomerTyreSize> getTyreSizes() {
		return tyreSizes;
	}
	public List<CustomerBrand> getBrands() {
		return brands;
	}
	public List<CustomerMarket> getMarkets() {
		return markets;
	}
	public List<CustomerEvn> getEvns() {
		return evns;
	}
	public List<CustomerInfo> getInfos() {
		return infos;
	}
	public List<CustomerProblem> getProblems() {
		return problems;
	}
	public List<CustomerRequire> getRequires() {
		return requires;
	}
	public List<CustomerService> getServices() {
		return services;
	}
	public List<CustomerPoint> getPoints() {
		return points;
	}
	public List<CustomerComment> getComments() {
		return comments;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public void setAddresses(List<CustomerAddress> addresses) {
		this.addresses = addresses;
	}
	public void setContacts(List<CustomerContact> contacts) {
		this.contacts = contacts;
	}
	public void setTyreSizes(List<CustomerTyreSize> tyreSizes) {
		this.tyreSizes = tyreSizes;
	}
	public void setBrands(List<CustomerBrand> brands) {
		this.brands = brands;
	}
	public void setMarkets(List<CustomerMarket> markets) {
		this.markets = markets;
	}
	public void setEvns(List<CustomerEvn> evns) {
		this.evns = evns;
	}
	public void setInfos(List<CustomerInfo> infos) {
		this.infos = infos;
	}
	public void setProblems(List<CustomerProblem> problems) {
		this.problems = problems;
	}
	public void setRequires(List<CustomerRequire> requires) {
		this.requires = requires;
	}
	public void setServices(List<CustomerService> services) {
		this.services = services;
	}
	public void setPoints(List<CustomerPoint> points) {
		this.points = points;
	}
	public void setComments(List<CustomerComment> comments) {
		this.comments = comments;
	}
	
}
