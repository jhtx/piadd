package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerBrand;
import com.yff.tuan.model.CustomerBrandExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerBrandMapper {
    int countByExample(CustomerBrandExample example);

    int deleteByExample(CustomerBrandExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerBrand record);

    int insertSelective(CustomerBrand record);

    List<CustomerBrand> selectByExample(CustomerBrandExample example);
    List<CustomerBrand> selectByExampleForName(CustomerBrand brand);

    CustomerBrand selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerBrand record, @Param("example") CustomerBrandExample example);

    int updateByExample(@Param("record") CustomerBrand record, @Param("example") CustomerBrandExample example);

    int updateByPrimaryKeySelective(CustomerBrand record);

    int updateByPrimaryKey(CustomerBrand record);
}