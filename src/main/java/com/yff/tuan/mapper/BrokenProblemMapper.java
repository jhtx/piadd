package com.yff.tuan.mapper;

import com.yff.tuan.model.BrokenProblem;
import com.yff.tuan.model.BrokenProblemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BrokenProblemMapper {
    int countByExample(BrokenProblemExample example);

    int deleteByExample(BrokenProblemExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BrokenProblem record);

    int insertSelective(BrokenProblem record);

    List<BrokenProblem> selectByExample(BrokenProblemExample example);

    BrokenProblem selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BrokenProblem record, @Param("example") BrokenProblemExample example);

    int updateByExample(@Param("record") BrokenProblem record, @Param("example") BrokenProblemExample example);

    int updateByPrimaryKeySelective(BrokenProblem record);

    int updateByPrimaryKey(BrokenProblem record);
}