package com.yff.tuan.mapper;

import com.yff.tuan.model.Broken;
import com.yff.tuan.model.BrokenExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BrokenMapper {
	int countByExample(BrokenExample example);

    int deleteByExample(BrokenExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Broken record);

    int insertSelective(Broken record);

    List<Broken> selectByExample(BrokenExample example);

    Broken selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Broken record, @Param("example") BrokenExample example);

    int updateByExample(@Param("record") Broken record, @Param("example") BrokenExample example);

    int updateByPrimaryKeySelective(Broken record);

    int updateByPrimaryKey(Broken record);
    
    /**以下自定义**/
    List<Broken> queryByPage(@Param("startIndex")int startIndex ,@Param("pageSize")int pageSize);
    List<Broken> queryforwx();
    Broken findByPrimaryKey(Integer id);
}