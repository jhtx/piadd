package com.yff.tuan.mapper;

import com.yff.tuan.model.WxUserBind;
import com.yff.tuan.model.WxUserBindExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WxUserBindMapper {
    int countByExample(WxUserBindExample example);

    int deleteByExample(WxUserBindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WxUserBind record);

    int insertSelective(WxUserBind record);

    List<WxUserBind> selectByExample(WxUserBindExample example);

    WxUserBind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WxUserBind record, @Param("example") WxUserBindExample example);

    int updateByExample(@Param("record") WxUserBind record, @Param("example") WxUserBindExample example);

    int updateByPrimaryKeySelective(WxUserBind record);

    int updateByPrimaryKey(WxUserBind record);
    
}