package com.yff.tuan.mapper;

import com.yff.tuan.model.Evn;
import com.yff.tuan.model.EvnExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EvnMapper {
    int countByExample(EvnExample example);

    int deleteByExample(EvnExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Evn record);

    int insertSelective(Evn record);

    List<Evn> selectByExample(EvnExample example);

    Evn selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Evn record, @Param("example") EvnExample example);

    int updateByExample(@Param("record") Evn record, @Param("example") EvnExample example);

    int updateByPrimaryKeySelective(Evn record);

    int updateByPrimaryKey(Evn record);
}