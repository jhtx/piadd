package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerTyreSize;
import com.yff.tuan.model.CustomerTyreSizeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerTyreSizeMapper {
    int countByExample(CustomerTyreSizeExample example);

    int deleteByExample(CustomerTyreSizeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerTyreSize record);

    int insertSelective(CustomerTyreSize record);

    List<CustomerTyreSize> selectByExample(CustomerTyreSizeExample example);

    CustomerTyreSize selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerTyreSize record, @Param("example") CustomerTyreSizeExample example);

    int updateByExample(@Param("record") CustomerTyreSize record, @Param("example") CustomerTyreSizeExample example);

    int updateByPrimaryKeySelective(CustomerTyreSize record);

    int updateByPrimaryKey(CustomerTyreSize record);
    
    
    List<CustomerTyreSize> selectByExampleForName(CustomerTyreSize tyreSize);
}