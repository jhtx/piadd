package com.yff.tuan.mapper;

import com.yff.tuan.model.VehicleDriveType;
import com.yff.tuan.model.VehicleDriveTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VehicleDriveTypeMapper {
    int countByExample(VehicleDriveTypeExample example);

    int deleteByExample(VehicleDriveTypeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VehicleDriveType record);

    int insertSelective(VehicleDriveType record);

    List<VehicleDriveType> selectByExample(VehicleDriveTypeExample example);

    VehicleDriveType selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VehicleDriveType record, @Param("example") VehicleDriveTypeExample example);

    int updateByExample(@Param("record") VehicleDriveType record, @Param("example") VehicleDriveTypeExample example);

    int updateByPrimaryKeySelective(VehicleDriveType record);

    int updateByPrimaryKey(VehicleDriveType record);
}