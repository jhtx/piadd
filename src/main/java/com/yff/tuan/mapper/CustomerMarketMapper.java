package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerMarket;
import com.yff.tuan.model.CustomerMarketExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerMarketMapper {
    int countByExample(CustomerMarketExample example);

    int deleteByExample(CustomerMarketExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerMarket record);

    int insertSelective(CustomerMarket record);

    List<CustomerMarket> selectByExample(CustomerMarketExample example);
    List<CustomerMarket> selectByExampleForName(CustomerMarket market);

    CustomerMarket selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerMarket record, @Param("example") CustomerMarketExample example);

    int updateByExample(@Param("record") CustomerMarket record, @Param("example") CustomerMarketExample example);

    int updateByPrimaryKeySelective(CustomerMarket record);

    int updateByPrimaryKey(CustomerMarket record);
}