package com.yff.tuan.mapper;

import com.yff.tuan.model.CheckVehicle;
import com.yff.tuan.model.CheckVehicleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CheckVehicleMapper {
    int countByExample(CheckVehicleExample example);

    int deleteByExample(CheckVehicleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CheckVehicle record);

    int insertSelective(CheckVehicle record);

    List<CheckVehicle> selectByExample(CheckVehicleExample example);

    CheckVehicle selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CheckVehicle record, @Param("example") CheckVehicleExample example);

    int updateByExample(@Param("record") CheckVehicle record, @Param("example") CheckVehicleExample example);

    int updateByPrimaryKeySelective(CheckVehicle record);

    int updateByPrimaryKey(CheckVehicle record);
    
    /** 以下自定义 **/
    List<CheckVehicle> queryByCheckId(Integer id);
}