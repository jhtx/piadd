package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerComment;
import com.yff.tuan.model.CustomerCommentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerCommentMapper {
    int countByExample(CustomerCommentExample example);

    int deleteByExample(CustomerCommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerComment record);

    int insertSelective(CustomerComment record);

    List<CustomerComment> selectByExample(CustomerCommentExample example);

    CustomerComment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerComment record, @Param("example") CustomerCommentExample example);

    int updateByExample(@Param("record") CustomerComment record, @Param("example") CustomerCommentExample example);

    int updateByPrimaryKeySelective(CustomerComment record);

    int updateByPrimaryKey(CustomerComment record);
    
    List<CustomerComment> queryComment(CustomerCommentExample example);
}