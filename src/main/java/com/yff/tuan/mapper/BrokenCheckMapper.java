package com.yff.tuan.mapper;

import com.yff.tuan.model.BrokenCheck;
import com.yff.tuan.model.BrokenCheckExample;
import com.yff.tuan.vo.BrokenObjectCheck;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BrokenCheckMapper {
    int countByExample(BrokenCheckExample example);

    int deleteByExample(BrokenCheckExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BrokenCheck record);

    int insertSelective(BrokenCheck record);

    List<BrokenCheck> selectByExample(BrokenCheckExample example);

    BrokenCheck selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BrokenCheck record, @Param("example") BrokenCheckExample example);

    int updateByExample(@Param("record") BrokenCheck record, @Param("example") BrokenCheckExample example);

    int updateByPrimaryKeySelective(BrokenCheck record);

    int updateByPrimaryKey(BrokenCheck record);
    
    BrokenCheck findByPrimaryKey(Integer id);
    List<BrokenCheck> queryByBrokenId(Integer brokenId);
    List<BrokenObjectCheck> queryProblem(@Param("brokenId")Integer brokenId,@Param("key")String key);
    List<BrokenObjectCheck> queryBrand(Integer brokenId);  
    List<BrokenObjectCheck> queryTyreSize(Integer brokenId);
}