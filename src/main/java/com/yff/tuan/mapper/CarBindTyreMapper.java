package com.yff.tuan.mapper;

import com.yff.tuan.model.CarBindTyre;
import com.yff.tuan.model.CarBindTyreExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarBindTyreMapper {
    int countByExample(CarBindTyreExample example);

    int deleteByExample(CarBindTyreExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarBindTyre record);

    int insertSelective(CarBindTyre record);

    List<CarBindTyre> selectByExample(CarBindTyreExample example);

    CarBindTyre selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarBindTyre record, @Param("example") CarBindTyreExample example);

    int updateByExample(@Param("record") CarBindTyre record, @Param("example") CarBindTyreExample example);

    int updateByPrimaryKeySelective(CarBindTyre record);

    int updateByPrimaryKey(CarBindTyre record);
}