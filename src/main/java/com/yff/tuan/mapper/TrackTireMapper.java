package com.yff.tuan.mapper;

import com.yff.tuan.model.TrackTire;
import com.yff.tuan.model.TrackTireExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TrackTireMapper {
	int countByExample(TrackTireExample example);

    int deleteByExample(TrackTireExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TrackTire record);

    int insertSelective(TrackTire record);

    List<TrackTire> selectByExample(TrackTireExample example);

    TrackTire selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TrackTire record, @Param("example") TrackTireExample example);

    int updateByExample(@Param("record") TrackTire record, @Param("example") TrackTireExample example);

    int updateByPrimaryKeySelective(TrackTire record);

    int updateByPrimaryKey(TrackTire record);
    
    /** 以下自定义 **/
    List<TrackTire> queryByTrackId(Integer trackId);
    
    TrackTire findTrackTireByPrimaryKey(Integer id);
}