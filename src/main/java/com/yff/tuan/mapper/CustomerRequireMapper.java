package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerRequire;
import com.yff.tuan.model.CustomerRequireExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerRequireMapper {
    int countByExample(CustomerRequireExample example);

    int deleteByExample(CustomerRequireExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerRequire record);

    int insertSelective(CustomerRequire record);

    List<CustomerRequire> selectByExample(CustomerRequireExample example);
    List<CustomerRequire> selectByExampleForName(CustomerRequire require);

    CustomerRequire selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerRequire record, @Param("example") CustomerRequireExample example);

    int updateByExample(@Param("record") CustomerRequire record, @Param("example") CustomerRequireExample example);

    int updateByPrimaryKeySelective(CustomerRequire record);

    int updateByPrimaryKey(CustomerRequire record);
}