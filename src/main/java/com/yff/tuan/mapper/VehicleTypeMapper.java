package com.yff.tuan.mapper;

import com.yff.tuan.model.VehicleType;
import com.yff.tuan.model.VehicleTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VehicleTypeMapper {
    int countByExample(VehicleTypeExample example);

    int deleteByExample(VehicleTypeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VehicleType record);

    int insertSelective(VehicleType record);

    List<VehicleType> selectByExample(VehicleTypeExample example);

    VehicleType selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VehicleType record, @Param("example") VehicleTypeExample example);

    int updateByExample(@Param("record") VehicleType record, @Param("example") VehicleTypeExample example);

    int updateByPrimaryKeySelective(VehicleType record);

    int updateByPrimaryKey(VehicleType record);
}