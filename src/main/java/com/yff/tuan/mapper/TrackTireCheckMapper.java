package com.yff.tuan.mapper;

import com.yff.tuan.model.TrackTireCheck;
import com.yff.tuan.model.TrackTireCheckExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TrackTireCheckMapper {
    int countByExample(TrackTireCheckExample example);

    int deleteByExample(TrackTireCheckExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TrackTireCheck record);

    int insertSelective(TrackTireCheck record);

    List<TrackTireCheck> selectByExample(TrackTireCheckExample example);

    TrackTireCheck selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TrackTireCheck record, @Param("example") TrackTireCheckExample example);

    int updateByExample(@Param("record") TrackTireCheck record, @Param("example") TrackTireCheckExample example);

    int updateByPrimaryKeySelective(TrackTireCheck record);

    int updateByPrimaryKey(TrackTireCheck record);
}