package com.yff.tuan.mapper;

import com.yff.tuan.model.TireCheckWarning;
import com.yff.tuan.model.TireCheckWarningExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TireCheckWarningMapper {
    int countByExample(TireCheckWarningExample example);

    int deleteByExample(TireCheckWarningExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TireCheckWarning record);

    int insertSelective(TireCheckWarning record);

    List<TireCheckWarning> selectByExample(TireCheckWarningExample example);

    TireCheckWarning selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TireCheckWarning record, @Param("example") TireCheckWarningExample example);

    int updateByExample(@Param("record") TireCheckWarning record, @Param("example") TireCheckWarningExample example);

    int updateByPrimaryKeySelective(TireCheckWarning record);

    int updateByPrimaryKey(TireCheckWarning record);
}