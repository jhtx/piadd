package com.yff.tuan.mapper;

import com.yff.tuan.model.Customer;
import com.yff.tuan.model.CustomerExample;
import com.yff.tuan.vo.CustomerVo;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerMapper {
	int countByExample(CustomerExample example);

    int deleteByExample(CustomerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Customer record);

    int insertSelective(Customer record);

    List<Customer> selectByExample(CustomerExample example);

    Customer selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Customer record, @Param("example") CustomerExample example);

    int updateByExample(@Param("record") Customer record, @Param("example") CustomerExample example);

    int updateByPrimaryKeySelective(Customer record);

    int updateByPrimaryKey(Customer record);
    
    /**以下自定义**/
    List<CustomerVo> queryByPage(@Param("startIndex")int startIndex ,@Param("pageSize")int pageSize,@Param("ids")String ids);
    CustomerVo findByPrimaryKey(Integer id);
    List<CustomerVo> query(Integer userId);
}