package com.yff.tuan.mapper;

import com.yff.tuan.model.CheckVehicleTire;
import com.yff.tuan.model.CheckVehicleTireExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CheckVehicleTireMapper {
    int countByExample(CheckVehicleTireExample example);

    int deleteByExample(CheckVehicleTireExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CheckVehicleTire record);

    int insertSelective(CheckVehicleTire record);

    List<CheckVehicleTire> selectByExample(CheckVehicleTireExample example);

    CheckVehicleTire selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CheckVehicleTire record, @Param("example") CheckVehicleTireExample example);

    int updateByExample(@Param("record") CheckVehicleTire record, @Param("example") CheckVehicleTireExample example);

    int updateByPrimaryKeySelective(CheckVehicleTire record);

    int updateByPrimaryKey(CheckVehicleTire record);
    
    /** 以下自定义 **/
    
    List<CheckVehicleTire> queryTireByVehicleId(Integer vehicleId);
}