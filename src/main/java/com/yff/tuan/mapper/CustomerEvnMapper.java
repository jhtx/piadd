package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerEvn;
import com.yff.tuan.model.CustomerEvnExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerEvnMapper {
    int countByExample(CustomerEvnExample example);

    int deleteByExample(CustomerEvnExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerEvn record);

    int insertSelective(CustomerEvn record);

    List<CustomerEvn> selectByExample(CustomerEvnExample example);
    List<CustomerEvn> selectByExampleForName(CustomerEvn evn);

    CustomerEvn selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerEvn record, @Param("example") CustomerEvnExample example);

    int updateByExample(@Param("record") CustomerEvn record, @Param("example") CustomerEvnExample example);

    int updateByPrimaryKeySelective(CustomerEvn record);

    int updateByPrimaryKey(CustomerEvn record);
}