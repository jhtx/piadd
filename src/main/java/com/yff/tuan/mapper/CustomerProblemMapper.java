package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerProblem;
import com.yff.tuan.model.CustomerProblemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerProblemMapper {
    int countByExample(CustomerProblemExample example);

    int deleteByExample(CustomerProblemExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerProblem record);

    int insertSelective(CustomerProblem record);

    List<CustomerProblem> selectByExample(CustomerProblemExample example);
    List<CustomerProblem> selectByExampleForName(CustomerProblem problem);

    CustomerProblem selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerProblem record, @Param("example") CustomerProblemExample example);

    int updateByExample(@Param("record") CustomerProblem record, @Param("example") CustomerProblemExample example);

    int updateByPrimaryKeySelective(CustomerProblem record);

    int updateByPrimaryKey(CustomerProblem record);
}