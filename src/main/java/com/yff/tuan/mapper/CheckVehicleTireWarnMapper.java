package com.yff.tuan.mapper;

import com.yff.tuan.model.CheckVehicleTireWarn;
import com.yff.tuan.model.CheckVehicleTireWarnExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CheckVehicleTireWarnMapper {
    int countByExample(CheckVehicleTireWarnExample example);

    int deleteByExample(CheckVehicleTireWarnExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CheckVehicleTireWarn record);

    int insertSelective(CheckVehicleTireWarn record);

    List<CheckVehicleTireWarn> selectByExample(CheckVehicleTireWarnExample example);

    CheckVehicleTireWarn selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CheckVehicleTireWarn record, @Param("example") CheckVehicleTireWarnExample example);

    int updateByExample(@Param("record") CheckVehicleTireWarn record, @Param("example") CheckVehicleTireWarnExample example);

    int updateByPrimaryKeySelective(CheckVehicleTireWarn record);

    int updateByPrimaryKey(CheckVehicleTireWarn record);
}