package com.yff.tuan.mapper;

import com.yff.tuan.model.Check;
import com.yff.tuan.model.CheckExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CheckMapper {
	int countByExample(CheckExample example);

    int deleteByExample(CheckExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Check record);

    int insertSelective(Check record);

    List<Check> selectByExample(CheckExample example);

    Check selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Check record, @Param("example") CheckExample example);

    int updateByExample(@Param("record") Check record, @Param("example") CheckExample example);

    int updateByPrimaryKeySelective(Check record);

    int updateByPrimaryKey(Check record);
    
    /** 以下自定义 **/
    List<Check> queryByPage(@Param("startIndex")int startIndex ,@Param("pageSize")int pageSize);
    List<Check> querywx();
    Check findByPrimaryKey(Integer id);
}