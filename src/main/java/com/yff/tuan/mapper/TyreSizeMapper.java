package com.yff.tuan.mapper;

import com.yff.tuan.model.TyreSize;
import com.yff.tuan.model.TyreSizeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TyreSizeMapper {
    int countByExample(TyreSizeExample example);

    int deleteByExample(TyreSizeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TyreSize record);

    int insertSelective(TyreSize record);

    List<TyreSize> selectByExample(TyreSizeExample example);

    TyreSize selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TyreSize record, @Param("example") TyreSizeExample example);

    int updateByExample(@Param("record") TyreSize record, @Param("example") TyreSizeExample example);

    int updateByPrimaryKeySelective(TyreSize record);

    int updateByPrimaryKey(TyreSize record);
}