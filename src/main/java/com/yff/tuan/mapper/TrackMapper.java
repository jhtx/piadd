package com.yff.tuan.mapper;

import com.yff.tuan.model.Track;
import com.yff.tuan.model.TrackExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TrackMapper {
	int countByExample(TrackExample example);

    int deleteByExample(TrackExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Track record);

    int insertSelective(Track record);

    List<Track> selectByExample(TrackExample example);

    Track selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Track record, @Param("example") TrackExample example);

    int updateByExample(@Param("record") Track record, @Param("example") TrackExample example);

    int updateByPrimaryKeySelective(Track record);

    int updateByPrimaryKey(Track record);
    
    /**以下自定义**/
    List<Track> queryByPage(@Param("startIndex")int startIndex ,@Param("pageSize")int pageSize);
    Track findByPrimaryKey(Integer id);
    List<Track> queryForWx();
}