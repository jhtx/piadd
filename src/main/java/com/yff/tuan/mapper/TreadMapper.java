package com.yff.tuan.mapper;

import com.yff.tuan.model.Tread;
import com.yff.tuan.model.TreadExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TreadMapper {
    int countByExample(TreadExample example);

    int deleteByExample(TreadExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Tread record);

    int insertSelective(Tread record);

    List<Tread> selectByExample(TreadExample example);

    Tread selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Tread record, @Param("example") TreadExample example);

    int updateByExample(@Param("record") Tread record, @Param("example") TreadExample example);

    int updateByPrimaryKeySelective(Tread record);

    int updateByPrimaryKey(Tread record);
}