package com.yff.tuan.mapper;

import com.yff.tuan.model.CustomerPoint;
import com.yff.tuan.model.CustomerPointExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerPointMapper {
    int countByExample(CustomerPointExample example);

    int deleteByExample(CustomerPointExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CustomerPoint record);

    int insertSelective(CustomerPoint record);

    List<CustomerPoint> selectByExample(CustomerPointExample example);
    List<CustomerPoint> selectByExampleForName(CustomerPoint point);

    CustomerPoint selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CustomerPoint record, @Param("example") CustomerPointExample example);

    int updateByExample(@Param("record") CustomerPoint record, @Param("example") CustomerPointExample example);

    int updateByPrimaryKeySelective(CustomerPoint record);

    int updateByPrimaryKey(CustomerPoint record);
}