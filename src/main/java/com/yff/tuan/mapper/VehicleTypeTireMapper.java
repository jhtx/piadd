package com.yff.tuan.mapper;

import com.yff.tuan.model.VehicleTypeTire;
import com.yff.tuan.model.VehicleTypeTireExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VehicleTypeTireMapper {
    int countByExample(VehicleTypeTireExample example);

    int deleteByExample(VehicleTypeTireExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VehicleTypeTire record);

    int insertSelective(VehicleTypeTire record);

    List<VehicleTypeTire> selectByExample(VehicleTypeTireExample example);

    VehicleTypeTire selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VehicleTypeTire record, @Param("example") VehicleTypeTireExample example);

    int updateByExample(@Param("record") VehicleTypeTire record, @Param("example") VehicleTypeTireExample example);

    int updateByPrimaryKeySelective(VehicleTypeTire record);

    int updateByPrimaryKey(VehicleTypeTire record);
}