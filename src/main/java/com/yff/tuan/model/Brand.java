package com.yff.tuan.model;

import java.util.List;

public class Brand {
    private Integer id;

    private String name;
    
    private List<Tread> mAdd;
    private List<Tread> mUpdate;
    List<Tread> treads;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public List<Tread> getmAdd() {
		return mAdd;
	}

	public List<Tread> getmUpdate() {
		return mUpdate;
	}

	public List<Tread> getTreads() {
		return treads;
	}

	public void setmAdd(List<Tread> mAdd) {
		this.mAdd = mAdd;
	}

	public void setmUpdate(List<Tread> mUpdate) {
		this.mUpdate = mUpdate;
	}

	public void setTreads(List<Tread> treads) {
		this.treads = treads;
	}

	private String cla;

	public String getCla() {
		return cla;
	}

	public void setCla(String cla) {
		this.cla = cla;
	}

	
	
    
}