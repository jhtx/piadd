package com.yff.tuan.model;

public class CustomerEvn {
    private Integer id;

    private Integer customerId;

    private Integer evnId;
    
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getEvnId() {
        return evnId;
    }

    public void setEvnId(Integer evnId) {
        this.evnId = evnId;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    
}