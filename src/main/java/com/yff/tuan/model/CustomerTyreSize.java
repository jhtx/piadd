package com.yff.tuan.model;

public class CustomerTyreSize {
    private Integer id;

    private Integer customerId;

    private Integer tyreSizeId;
    
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getTyreSizeId() {
        return tyreSizeId;
    }

    public void setTyreSizeId(Integer tyreSizeId) {
        this.tyreSizeId = tyreSizeId;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
}