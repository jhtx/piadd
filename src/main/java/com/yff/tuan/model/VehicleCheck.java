package com.yff.tuan.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.yff.tuan.util.CustomJsonDateDeserializer;

public class VehicleCheck {
    private Integer id;
    @NotNull(message="{产品编号不能为空}")
    private String productId;

    private Integer carId;
    @NotNull(message="{纬度不能为空}")
    private String lat;
    @NotNull(message="{经度不能为空}")
    private String lng;

    private String matchProductId;

    private Integer matchCarId;
    @NotNull(message="{速度不能为空}")
    private Double speed;

    private Double miles;

    private Double oneMile;
    @NotNull(message="{电流电压信息不能为空}")
    private String taillight;
    @NotNull(message="{剩余电量不能为空}")
    private Double charge;

    private Integer state;
    @NotNull(message="{检测时间不能为空}")
    private Date checkTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMatchProductId() {
        return matchProductId;
    }

    public void setMatchProductId(String matchProductId) {
        this.matchProductId = matchProductId;
    }

    public Integer getMatchCarId() {
        return matchCarId;
    }

    public void setMatchCarId(Integer matchCarId) {
        this.matchCarId = matchCarId;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getMiles() {
        return miles;
    }

    public void setMiles(Double miles) {
        this.miles = miles;
    }

    public Double getOneMile() {
        return oneMile;
    }

    public void setOneMile(Double oneMile) {
        this.oneMile = oneMile;
    }

    public String getTaillight() {
        return taillight;
    }

    public void setTaillight(String taillight) {
        this.taillight = taillight;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCheckTime() {
        return checkTime;
    }
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

	@Override
	public String toString() {
		return "VehicleCheck [id=" + id + ", productId=" + productId + ", carId=" + carId + ", lat=" + lat + ", lng="
				+ lng + ", matchProductId=" + matchProductId + ", matchCarId=" + matchCarId + ", speed=" + speed
				+ ", miles=" + miles + ", oneMile=" + oneMile + ", taillight=" + taillight + ", charge=" + charge
				+ ", state=" + state + ", checkTime=" + checkTime + "]";
	}
    
    
}