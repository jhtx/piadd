package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarWarningExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarWarningExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(String value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(String value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(String value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(String value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(String value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLike(String value) {
            addCriterion("product_id like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotLike(String value) {
            addCriterion("product_id not like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<String> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<String> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(String value1, String value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(String value1, String value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andCarIdIsNull() {
            addCriterion("car_id is null");
            return (Criteria) this;
        }

        public Criteria andCarIdIsNotNull() {
            addCriterion("car_id is not null");
            return (Criteria) this;
        }

        public Criteria andCarIdEqualTo(Integer value) {
            addCriterion("car_id =", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotEqualTo(Integer value) {
            addCriterion("car_id <>", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdGreaterThan(Integer value) {
            addCriterion("car_id >", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("car_id >=", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdLessThan(Integer value) {
            addCriterion("car_id <", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdLessThanOrEqualTo(Integer value) {
            addCriterion("car_id <=", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdIn(List<Integer> values) {
            addCriterion("car_id in", values, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotIn(List<Integer> values) {
            addCriterion("car_id not in", values, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdBetween(Integer value1, Integer value2) {
            addCriterion("car_id between", value1, value2, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotBetween(Integer value1, Integer value2) {
            addCriterion("car_id not between", value1, value2, "carId");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10IsNull() {
            addCriterion("pressure_under10 is null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10IsNotNull() {
            addCriterion("pressure_under10 is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10EqualTo(String value) {
            addCriterion("pressure_under10 =", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10NotEqualTo(String value) {
            addCriterion("pressure_under10 <>", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10GreaterThan(String value) {
            addCriterion("pressure_under10 >", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10GreaterThanOrEqualTo(String value) {
            addCriterion("pressure_under10 >=", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10LessThan(String value) {
            addCriterion("pressure_under10 <", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10LessThanOrEqualTo(String value) {
            addCriterion("pressure_under10 <=", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10Like(String value) {
            addCriterion("pressure_under10 like", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10NotLike(String value) {
            addCriterion("pressure_under10 not like", value, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10In(List<String> values) {
            addCriterion("pressure_under10 in", values, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10NotIn(List<String> values) {
            addCriterion("pressure_under10 not in", values, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10Between(String value1, String value2) {
            addCriterion("pressure_under10 between", value1, value2, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10NotBetween(String value1, String value2) {
            addCriterion("pressure_under10 not between", value1, value2, "pressureUnder10");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningIsNull() {
            addCriterion("pressure_under10_warning is null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningIsNotNull() {
            addCriterion("pressure_under10_warning is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningEqualTo(Integer value) {
            addCriterion("pressure_under10_warning =", value, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningNotEqualTo(Integer value) {
            addCriterion("pressure_under10_warning <>", value, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningGreaterThan(Integer value) {
            addCriterion("pressure_under10_warning >", value, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("pressure_under10_warning >=", value, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningLessThan(Integer value) {
            addCriterion("pressure_under10_warning <", value, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningLessThanOrEqualTo(Integer value) {
            addCriterion("pressure_under10_warning <=", value, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningIn(List<Integer> values) {
            addCriterion("pressure_under10_warning in", values, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningNotIn(List<Integer> values) {
            addCriterion("pressure_under10_warning not in", values, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningBetween(Integer value1, Integer value2) {
            addCriterion("pressure_under10_warning between", value1, value2, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder10WarningNotBetween(Integer value1, Integer value2) {
            addCriterion("pressure_under10_warning not between", value1, value2, "pressureUnder10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20IsNull() {
            addCriterion("pressure_under20 is null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20IsNotNull() {
            addCriterion("pressure_under20 is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20EqualTo(String value) {
            addCriterion("pressure_under20 =", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20NotEqualTo(String value) {
            addCriterion("pressure_under20 <>", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20GreaterThan(String value) {
            addCriterion("pressure_under20 >", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20GreaterThanOrEqualTo(String value) {
            addCriterion("pressure_under20 >=", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20LessThan(String value) {
            addCriterion("pressure_under20 <", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20LessThanOrEqualTo(String value) {
            addCriterion("pressure_under20 <=", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20Like(String value) {
            addCriterion("pressure_under20 like", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20NotLike(String value) {
            addCriterion("pressure_under20 not like", value, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20In(List<String> values) {
            addCriterion("pressure_under20 in", values, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20NotIn(List<String> values) {
            addCriterion("pressure_under20 not in", values, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20Between(String value1, String value2) {
            addCriterion("pressure_under20 between", value1, value2, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20NotBetween(String value1, String value2) {
            addCriterion("pressure_under20 not between", value1, value2, "pressureUnder20");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningIsNull() {
            addCriterion("pressure_under20_warning is null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningIsNotNull() {
            addCriterion("pressure_under20_warning is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningEqualTo(Integer value) {
            addCriterion("pressure_under20_warning =", value, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningNotEqualTo(Integer value) {
            addCriterion("pressure_under20_warning <>", value, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningGreaterThan(Integer value) {
            addCriterion("pressure_under20_warning >", value, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("pressure_under20_warning >=", value, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningLessThan(Integer value) {
            addCriterion("pressure_under20_warning <", value, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningLessThanOrEqualTo(Integer value) {
            addCriterion("pressure_under20_warning <=", value, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningIn(List<Integer> values) {
            addCriterion("pressure_under20_warning in", values, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningNotIn(List<Integer> values) {
            addCriterion("pressure_under20_warning not in", values, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningBetween(Integer value1, Integer value2) {
            addCriterion("pressure_under20_warning between", value1, value2, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder20WarningNotBetween(Integer value1, Integer value2) {
            addCriterion("pressure_under20_warning not between", value1, value2, "pressureUnder20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30IsNull() {
            addCriterion("pressure_under30 is null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30IsNotNull() {
            addCriterion("pressure_under30 is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30EqualTo(String value) {
            addCriterion("pressure_under30 =", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30NotEqualTo(String value) {
            addCriterion("pressure_under30 <>", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30GreaterThan(String value) {
            addCriterion("pressure_under30 >", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30GreaterThanOrEqualTo(String value) {
            addCriterion("pressure_under30 >=", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30LessThan(String value) {
            addCriterion("pressure_under30 <", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30LessThanOrEqualTo(String value) {
            addCriterion("pressure_under30 <=", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30Like(String value) {
            addCriterion("pressure_under30 like", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30NotLike(String value) {
            addCriterion("pressure_under30 not like", value, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30In(List<String> values) {
            addCriterion("pressure_under30 in", values, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30NotIn(List<String> values) {
            addCriterion("pressure_under30 not in", values, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30Between(String value1, String value2) {
            addCriterion("pressure_under30 between", value1, value2, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30NotBetween(String value1, String value2) {
            addCriterion("pressure_under30 not between", value1, value2, "pressureUnder30");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningIsNull() {
            addCriterion("pressure_under30_warning is null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningIsNotNull() {
            addCriterion("pressure_under30_warning is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningEqualTo(Integer value) {
            addCriterion("pressure_under30_warning =", value, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningNotEqualTo(Integer value) {
            addCriterion("pressure_under30_warning <>", value, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningGreaterThan(Integer value) {
            addCriterion("pressure_under30_warning >", value, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("pressure_under30_warning >=", value, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningLessThan(Integer value) {
            addCriterion("pressure_under30_warning <", value, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningLessThanOrEqualTo(Integer value) {
            addCriterion("pressure_under30_warning <=", value, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningIn(List<Integer> values) {
            addCriterion("pressure_under30_warning in", values, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningNotIn(List<Integer> values) {
            addCriterion("pressure_under30_warning not in", values, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningBetween(Integer value1, Integer value2) {
            addCriterion("pressure_under30_warning between", value1, value2, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUnder30WarningNotBetween(Integer value1, Integer value2) {
            addCriterion("pressure_under30_warning not between", value1, value2, "pressureUnder30Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10IsNull() {
            addCriterion("pressure_upper10 is null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10IsNotNull() {
            addCriterion("pressure_upper10 is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10EqualTo(String value) {
            addCriterion("pressure_upper10 =", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10NotEqualTo(String value) {
            addCriterion("pressure_upper10 <>", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10GreaterThan(String value) {
            addCriterion("pressure_upper10 >", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10GreaterThanOrEqualTo(String value) {
            addCriterion("pressure_upper10 >=", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10LessThan(String value) {
            addCriterion("pressure_upper10 <", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10LessThanOrEqualTo(String value) {
            addCriterion("pressure_upper10 <=", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10Like(String value) {
            addCriterion("pressure_upper10 like", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10NotLike(String value) {
            addCriterion("pressure_upper10 not like", value, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10In(List<String> values) {
            addCriterion("pressure_upper10 in", values, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10NotIn(List<String> values) {
            addCriterion("pressure_upper10 not in", values, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10Between(String value1, String value2) {
            addCriterion("pressure_upper10 between", value1, value2, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10NotBetween(String value1, String value2) {
            addCriterion("pressure_upper10 not between", value1, value2, "pressureUpper10");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningIsNull() {
            addCriterion("pressure_upper10_warning is null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningIsNotNull() {
            addCriterion("pressure_upper10_warning is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningEqualTo(Integer value) {
            addCriterion("pressure_upper10_warning =", value, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningNotEqualTo(Integer value) {
            addCriterion("pressure_upper10_warning <>", value, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningGreaterThan(Integer value) {
            addCriterion("pressure_upper10_warning >", value, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("pressure_upper10_warning >=", value, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningLessThan(Integer value) {
            addCriterion("pressure_upper10_warning <", value, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningLessThanOrEqualTo(Integer value) {
            addCriterion("pressure_upper10_warning <=", value, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningIn(List<Integer> values) {
            addCriterion("pressure_upper10_warning in", values, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningNotIn(List<Integer> values) {
            addCriterion("pressure_upper10_warning not in", values, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningBetween(Integer value1, Integer value2) {
            addCriterion("pressure_upper10_warning between", value1, value2, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper10WarningNotBetween(Integer value1, Integer value2) {
            addCriterion("pressure_upper10_warning not between", value1, value2, "pressureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20IsNull() {
            addCriterion("pressure_upper20 is null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20IsNotNull() {
            addCriterion("pressure_upper20 is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20EqualTo(String value) {
            addCriterion("pressure_upper20 =", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20NotEqualTo(String value) {
            addCriterion("pressure_upper20 <>", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20GreaterThan(String value) {
            addCriterion("pressure_upper20 >", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20GreaterThanOrEqualTo(String value) {
            addCriterion("pressure_upper20 >=", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20LessThan(String value) {
            addCriterion("pressure_upper20 <", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20LessThanOrEqualTo(String value) {
            addCriterion("pressure_upper20 <=", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20Like(String value) {
            addCriterion("pressure_upper20 like", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20NotLike(String value) {
            addCriterion("pressure_upper20 not like", value, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20In(List<String> values) {
            addCriterion("pressure_upper20 in", values, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20NotIn(List<String> values) {
            addCriterion("pressure_upper20 not in", values, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20Between(String value1, String value2) {
            addCriterion("pressure_upper20 between", value1, value2, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20NotBetween(String value1, String value2) {
            addCriterion("pressure_upper20 not between", value1, value2, "pressureUpper20");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningIsNull() {
            addCriterion("pressure_upper20_warning is null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningIsNotNull() {
            addCriterion("pressure_upper20_warning is not null");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningEqualTo(Integer value) {
            addCriterion("pressure_upper20_warning =", value, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningNotEqualTo(Integer value) {
            addCriterion("pressure_upper20_warning <>", value, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningGreaterThan(Integer value) {
            addCriterion("pressure_upper20_warning >", value, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("pressure_upper20_warning >=", value, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningLessThan(Integer value) {
            addCriterion("pressure_upper20_warning <", value, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningLessThanOrEqualTo(Integer value) {
            addCriterion("pressure_upper20_warning <=", value, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningIn(List<Integer> values) {
            addCriterion("pressure_upper20_warning in", values, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningNotIn(List<Integer> values) {
            addCriterion("pressure_upper20_warning not in", values, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningBetween(Integer value1, Integer value2) {
            addCriterion("pressure_upper20_warning between", value1, value2, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andPressureUpper20WarningNotBetween(Integer value1, Integer value2) {
            addCriterion("pressure_upper20_warning not between", value1, value2, "pressureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10IsNull() {
            addCriterion("temperature_upper10 is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10IsNotNull() {
            addCriterion("temperature_upper10 is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10EqualTo(String value) {
            addCriterion("temperature_upper10 =", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10NotEqualTo(String value) {
            addCriterion("temperature_upper10 <>", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10GreaterThan(String value) {
            addCriterion("temperature_upper10 >", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10GreaterThanOrEqualTo(String value) {
            addCriterion("temperature_upper10 >=", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10LessThan(String value) {
            addCriterion("temperature_upper10 <", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10LessThanOrEqualTo(String value) {
            addCriterion("temperature_upper10 <=", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10Like(String value) {
            addCriterion("temperature_upper10 like", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10NotLike(String value) {
            addCriterion("temperature_upper10 not like", value, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10In(List<String> values) {
            addCriterion("temperature_upper10 in", values, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10NotIn(List<String> values) {
            addCriterion("temperature_upper10 not in", values, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10Between(String value1, String value2) {
            addCriterion("temperature_upper10 between", value1, value2, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10NotBetween(String value1, String value2) {
            addCriterion("temperature_upper10 not between", value1, value2, "temperatureUpper10");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningIsNull() {
            addCriterion("temperature_upper10_warning is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningIsNotNull() {
            addCriterion("temperature_upper10_warning is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningEqualTo(Integer value) {
            addCriterion("temperature_upper10_warning =", value, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningNotEqualTo(Integer value) {
            addCriterion("temperature_upper10_warning <>", value, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningGreaterThan(Integer value) {
            addCriterion("temperature_upper10_warning >", value, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("temperature_upper10_warning >=", value, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningLessThan(Integer value) {
            addCriterion("temperature_upper10_warning <", value, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningLessThanOrEqualTo(Integer value) {
            addCriterion("temperature_upper10_warning <=", value, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningIn(List<Integer> values) {
            addCriterion("temperature_upper10_warning in", values, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningNotIn(List<Integer> values) {
            addCriterion("temperature_upper10_warning not in", values, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningBetween(Integer value1, Integer value2) {
            addCriterion("temperature_upper10_warning between", value1, value2, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper10WarningNotBetween(Integer value1, Integer value2) {
            addCriterion("temperature_upper10_warning not between", value1, value2, "temperatureUpper10Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20IsNull() {
            addCriterion("temperature_upper20 is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20IsNotNull() {
            addCriterion("temperature_upper20 is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20EqualTo(String value) {
            addCriterion("temperature_upper20 =", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20NotEqualTo(String value) {
            addCriterion("temperature_upper20 <>", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20GreaterThan(String value) {
            addCriterion("temperature_upper20 >", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20GreaterThanOrEqualTo(String value) {
            addCriterion("temperature_upper20 >=", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20LessThan(String value) {
            addCriterion("temperature_upper20 <", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20LessThanOrEqualTo(String value) {
            addCriterion("temperature_upper20 <=", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20Like(String value) {
            addCriterion("temperature_upper20 like", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20NotLike(String value) {
            addCriterion("temperature_upper20 not like", value, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20In(List<String> values) {
            addCriterion("temperature_upper20 in", values, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20NotIn(List<String> values) {
            addCriterion("temperature_upper20 not in", values, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20Between(String value1, String value2) {
            addCriterion("temperature_upper20 between", value1, value2, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20NotBetween(String value1, String value2) {
            addCriterion("temperature_upper20 not between", value1, value2, "temperatureUpper20");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningIsNull() {
            addCriterion("temperature_upper20_warning is null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningIsNotNull() {
            addCriterion("temperature_upper20_warning is not null");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningEqualTo(Integer value) {
            addCriterion("temperature_upper20_warning =", value, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningNotEqualTo(Integer value) {
            addCriterion("temperature_upper20_warning <>", value, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningGreaterThan(Integer value) {
            addCriterion("temperature_upper20_warning >", value, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("temperature_upper20_warning >=", value, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningLessThan(Integer value) {
            addCriterion("temperature_upper20_warning <", value, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningLessThanOrEqualTo(Integer value) {
            addCriterion("temperature_upper20_warning <=", value, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningIn(List<Integer> values) {
            addCriterion("temperature_upper20_warning in", values, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningNotIn(List<Integer> values) {
            addCriterion("temperature_upper20_warning not in", values, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningBetween(Integer value1, Integer value2) {
            addCriterion("temperature_upper20_warning between", value1, value2, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andTemperatureUpper20WarningNotBetween(Integer value1, Integer value2) {
            addCriterion("temperature_upper20_warning not between", value1, value2, "temperatureUpper20Warning");
            return (Criteria) this;
        }

        public Criteria andChargeIsNull() {
            addCriterion("charge is null");
            return (Criteria) this;
        }

        public Criteria andChargeIsNotNull() {
            addCriterion("charge is not null");
            return (Criteria) this;
        }

        public Criteria andChargeEqualTo(String value) {
            addCriterion("charge =", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotEqualTo(String value) {
            addCriterion("charge <>", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThan(String value) {
            addCriterion("charge >", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThanOrEqualTo(String value) {
            addCriterion("charge >=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThan(String value) {
            addCriterion("charge <", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThanOrEqualTo(String value) {
            addCriterion("charge <=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLike(String value) {
            addCriterion("charge like", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotLike(String value) {
            addCriterion("charge not like", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeIn(List<String> values) {
            addCriterion("charge in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotIn(List<String> values) {
            addCriterion("charge not in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeBetween(String value1, String value2) {
            addCriterion("charge between", value1, value2, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotBetween(String value1, String value2) {
            addCriterion("charge not between", value1, value2, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeWarningIsNull() {
            addCriterion("charge_warning is null");
            return (Criteria) this;
        }

        public Criteria andChargeWarningIsNotNull() {
            addCriterion("charge_warning is not null");
            return (Criteria) this;
        }

        public Criteria andChargeWarningEqualTo(Integer value) {
            addCriterion("charge_warning =", value, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningNotEqualTo(Integer value) {
            addCriterion("charge_warning <>", value, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningGreaterThan(Integer value) {
            addCriterion("charge_warning >", value, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("charge_warning >=", value, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningLessThan(Integer value) {
            addCriterion("charge_warning <", value, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningLessThanOrEqualTo(Integer value) {
            addCriterion("charge_warning <=", value, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningIn(List<Integer> values) {
            addCriterion("charge_warning in", values, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningNotIn(List<Integer> values) {
            addCriterion("charge_warning not in", values, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningBetween(Integer value1, Integer value2) {
            addCriterion("charge_warning between", value1, value2, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeWarningNotBetween(Integer value1, Integer value2) {
            addCriterion("charge_warning not between", value1, value2, "chargeWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmIsNull() {
            addCriterion("charge_alarm is null");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmIsNotNull() {
            addCriterion("charge_alarm is not null");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmEqualTo(String value) {
            addCriterion("charge_alarm =", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmNotEqualTo(String value) {
            addCriterion("charge_alarm <>", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmGreaterThan(String value) {
            addCriterion("charge_alarm >", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmGreaterThanOrEqualTo(String value) {
            addCriterion("charge_alarm >=", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmLessThan(String value) {
            addCriterion("charge_alarm <", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmLessThanOrEqualTo(String value) {
            addCriterion("charge_alarm <=", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmLike(String value) {
            addCriterion("charge_alarm like", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmNotLike(String value) {
            addCriterion("charge_alarm not like", value, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmIn(List<String> values) {
            addCriterion("charge_alarm in", values, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmNotIn(List<String> values) {
            addCriterion("charge_alarm not in", values, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmBetween(String value1, String value2) {
            addCriterion("charge_alarm between", value1, value2, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmNotBetween(String value1, String value2) {
            addCriterion("charge_alarm not between", value1, value2, "chargeAlarm");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningIsNull() {
            addCriterion("charge_alarm_warning is null");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningIsNotNull() {
            addCriterion("charge_alarm_warning is not null");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningEqualTo(Integer value) {
            addCriterion("charge_alarm_warning =", value, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningNotEqualTo(Integer value) {
            addCriterion("charge_alarm_warning <>", value, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningGreaterThan(Integer value) {
            addCriterion("charge_alarm_warning >", value, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningGreaterThanOrEqualTo(Integer value) {
            addCriterion("charge_alarm_warning >=", value, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningLessThan(Integer value) {
            addCriterion("charge_alarm_warning <", value, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningLessThanOrEqualTo(Integer value) {
            addCriterion("charge_alarm_warning <=", value, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningIn(List<Integer> values) {
            addCriterion("charge_alarm_warning in", values, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningNotIn(List<Integer> values) {
            addCriterion("charge_alarm_warning not in", values, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningBetween(Integer value1, Integer value2) {
            addCriterion("charge_alarm_warning between", value1, value2, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andChargeAlarmWarningNotBetween(Integer value1, Integer value2) {
            addCriterion("charge_alarm_warning not between", value1, value2, "chargeAlarmWarning");
            return (Criteria) this;
        }

        public Criteria andD1IsNull() {
            addCriterion("d1 is null");
            return (Criteria) this;
        }

        public Criteria andD1IsNotNull() {
            addCriterion("d1 is not null");
            return (Criteria) this;
        }

        public Criteria andD1EqualTo(Integer value) {
            addCriterion("d1 =", value, "d1");
            return (Criteria) this;
        }

        public Criteria andD1NotEqualTo(Integer value) {
            addCriterion("d1 <>", value, "d1");
            return (Criteria) this;
        }

        public Criteria andD1GreaterThan(Integer value) {
            addCriterion("d1 >", value, "d1");
            return (Criteria) this;
        }

        public Criteria andD1GreaterThanOrEqualTo(Integer value) {
            addCriterion("d1 >=", value, "d1");
            return (Criteria) this;
        }

        public Criteria andD1LessThan(Integer value) {
            addCriterion("d1 <", value, "d1");
            return (Criteria) this;
        }

        public Criteria andD1LessThanOrEqualTo(Integer value) {
            addCriterion("d1 <=", value, "d1");
            return (Criteria) this;
        }

        public Criteria andD1In(List<Integer> values) {
            addCriterion("d1 in", values, "d1");
            return (Criteria) this;
        }

        public Criteria andD1NotIn(List<Integer> values) {
            addCriterion("d1 not in", values, "d1");
            return (Criteria) this;
        }

        public Criteria andD1Between(Integer value1, Integer value2) {
            addCriterion("d1 between", value1, value2, "d1");
            return (Criteria) this;
        }

        public Criteria andD1NotBetween(Integer value1, Integer value2) {
            addCriterion("d1 not between", value1, value2, "d1");
            return (Criteria) this;
        }

        public Criteria andD2IsNull() {
            addCriterion("d2 is null");
            return (Criteria) this;
        }

        public Criteria andD2IsNotNull() {
            addCriterion("d2 is not null");
            return (Criteria) this;
        }

        public Criteria andD2EqualTo(Integer value) {
            addCriterion("d2 =", value, "d2");
            return (Criteria) this;
        }

        public Criteria andD2NotEqualTo(Integer value) {
            addCriterion("d2 <>", value, "d2");
            return (Criteria) this;
        }

        public Criteria andD2GreaterThan(Integer value) {
            addCriterion("d2 >", value, "d2");
            return (Criteria) this;
        }

        public Criteria andD2GreaterThanOrEqualTo(Integer value) {
            addCriterion("d2 >=", value, "d2");
            return (Criteria) this;
        }

        public Criteria andD2LessThan(Integer value) {
            addCriterion("d2 <", value, "d2");
            return (Criteria) this;
        }

        public Criteria andD2LessThanOrEqualTo(Integer value) {
            addCriterion("d2 <=", value, "d2");
            return (Criteria) this;
        }

        public Criteria andD2In(List<Integer> values) {
            addCriterion("d2 in", values, "d2");
            return (Criteria) this;
        }

        public Criteria andD2NotIn(List<Integer> values) {
            addCriterion("d2 not in", values, "d2");
            return (Criteria) this;
        }

        public Criteria andD2Between(Integer value1, Integer value2) {
            addCriterion("d2 between", value1, value2, "d2");
            return (Criteria) this;
        }

        public Criteria andD2NotBetween(Integer value1, Integer value2) {
            addCriterion("d2 not between", value1, value2, "d2");
            return (Criteria) this;
        }

        public Criteria andD3IsNull() {
            addCriterion("d3 is null");
            return (Criteria) this;
        }

        public Criteria andD3IsNotNull() {
            addCriterion("d3 is not null");
            return (Criteria) this;
        }

        public Criteria andD3EqualTo(Integer value) {
            addCriterion("d3 =", value, "d3");
            return (Criteria) this;
        }

        public Criteria andD3NotEqualTo(Integer value) {
            addCriterion("d3 <>", value, "d3");
            return (Criteria) this;
        }

        public Criteria andD3GreaterThan(Integer value) {
            addCriterion("d3 >", value, "d3");
            return (Criteria) this;
        }

        public Criteria andD3GreaterThanOrEqualTo(Integer value) {
            addCriterion("d3 >=", value, "d3");
            return (Criteria) this;
        }

        public Criteria andD3LessThan(Integer value) {
            addCriterion("d3 <", value, "d3");
            return (Criteria) this;
        }

        public Criteria andD3LessThanOrEqualTo(Integer value) {
            addCriterion("d3 <=", value, "d3");
            return (Criteria) this;
        }

        public Criteria andD3In(List<Integer> values) {
            addCriterion("d3 in", values, "d3");
            return (Criteria) this;
        }

        public Criteria andD3NotIn(List<Integer> values) {
            addCriterion("d3 not in", values, "d3");
            return (Criteria) this;
        }

        public Criteria andD3Between(Integer value1, Integer value2) {
            addCriterion("d3 between", value1, value2, "d3");
            return (Criteria) this;
        }

        public Criteria andD3NotBetween(Integer value1, Integer value2) {
            addCriterion("d3 not between", value1, value2, "d3");
            return (Criteria) this;
        }

        public Criteria andD4IsNull() {
            addCriterion("d4 is null");
            return (Criteria) this;
        }

        public Criteria andD4IsNotNull() {
            addCriterion("d4 is not null");
            return (Criteria) this;
        }

        public Criteria andD4EqualTo(Integer value) {
            addCriterion("d4 =", value, "d4");
            return (Criteria) this;
        }

        public Criteria andD4NotEqualTo(Integer value) {
            addCriterion("d4 <>", value, "d4");
            return (Criteria) this;
        }

        public Criteria andD4GreaterThan(Integer value) {
            addCriterion("d4 >", value, "d4");
            return (Criteria) this;
        }

        public Criteria andD4GreaterThanOrEqualTo(Integer value) {
            addCriterion("d4 >=", value, "d4");
            return (Criteria) this;
        }

        public Criteria andD4LessThan(Integer value) {
            addCriterion("d4 <", value, "d4");
            return (Criteria) this;
        }

        public Criteria andD4LessThanOrEqualTo(Integer value) {
            addCriterion("d4 <=", value, "d4");
            return (Criteria) this;
        }

        public Criteria andD4In(List<Integer> values) {
            addCriterion("d4 in", values, "d4");
            return (Criteria) this;
        }

        public Criteria andD4NotIn(List<Integer> values) {
            addCriterion("d4 not in", values, "d4");
            return (Criteria) this;
        }

        public Criteria andD4Between(Integer value1, Integer value2) {
            addCriterion("d4 between", value1, value2, "d4");
            return (Criteria) this;
        }

        public Criteria andD4NotBetween(Integer value1, Integer value2) {
            addCriterion("d4 not between", value1, value2, "d4");
            return (Criteria) this;
        }

        public Criteria andD5IsNull() {
            addCriterion("d5 is null");
            return (Criteria) this;
        }

        public Criteria andD5IsNotNull() {
            addCriterion("d5 is not null");
            return (Criteria) this;
        }

        public Criteria andD5EqualTo(Integer value) {
            addCriterion("d5 =", value, "d5");
            return (Criteria) this;
        }

        public Criteria andD5NotEqualTo(Integer value) {
            addCriterion("d5 <>", value, "d5");
            return (Criteria) this;
        }

        public Criteria andD5GreaterThan(Integer value) {
            addCriterion("d5 >", value, "d5");
            return (Criteria) this;
        }

        public Criteria andD5GreaterThanOrEqualTo(Integer value) {
            addCriterion("d5 >=", value, "d5");
            return (Criteria) this;
        }

        public Criteria andD5LessThan(Integer value) {
            addCriterion("d5 <", value, "d5");
            return (Criteria) this;
        }

        public Criteria andD5LessThanOrEqualTo(Integer value) {
            addCriterion("d5 <=", value, "d5");
            return (Criteria) this;
        }

        public Criteria andD5In(List<Integer> values) {
            addCriterion("d5 in", values, "d5");
            return (Criteria) this;
        }

        public Criteria andD5NotIn(List<Integer> values) {
            addCriterion("d5 not in", values, "d5");
            return (Criteria) this;
        }

        public Criteria andD5Between(Integer value1, Integer value2) {
            addCriterion("d5 between", value1, value2, "d5");
            return (Criteria) this;
        }

        public Criteria andD5NotBetween(Integer value1, Integer value2) {
            addCriterion("d5 not between", value1, value2, "d5");
            return (Criteria) this;
        }

        public Criteria andD6IsNull() {
            addCriterion("d6 is null");
            return (Criteria) this;
        }

        public Criteria andD6IsNotNull() {
            addCriterion("d6 is not null");
            return (Criteria) this;
        }

        public Criteria andD6EqualTo(Integer value) {
            addCriterion("d6 =", value, "d6");
            return (Criteria) this;
        }

        public Criteria andD6NotEqualTo(Integer value) {
            addCriterion("d6 <>", value, "d6");
            return (Criteria) this;
        }

        public Criteria andD6GreaterThan(Integer value) {
            addCriterion("d6 >", value, "d6");
            return (Criteria) this;
        }

        public Criteria andD6GreaterThanOrEqualTo(Integer value) {
            addCriterion("d6 >=", value, "d6");
            return (Criteria) this;
        }

        public Criteria andD6LessThan(Integer value) {
            addCriterion("d6 <", value, "d6");
            return (Criteria) this;
        }

        public Criteria andD6LessThanOrEqualTo(Integer value) {
            addCriterion("d6 <=", value, "d6");
            return (Criteria) this;
        }

        public Criteria andD6In(List<Integer> values) {
            addCriterion("d6 in", values, "d6");
            return (Criteria) this;
        }

        public Criteria andD6NotIn(List<Integer> values) {
            addCriterion("d6 not in", values, "d6");
            return (Criteria) this;
        }

        public Criteria andD6Between(Integer value1, Integer value2) {
            addCriterion("d6 between", value1, value2, "d6");
            return (Criteria) this;
        }

        public Criteria andD6NotBetween(Integer value1, Integer value2) {
            addCriterion("d6 not between", value1, value2, "d6");
            return (Criteria) this;
        }

        public Criteria andD7IsNull() {
            addCriterion("d7 is null");
            return (Criteria) this;
        }

        public Criteria andD7IsNotNull() {
            addCriterion("d7 is not null");
            return (Criteria) this;
        }

        public Criteria andD7EqualTo(Integer value) {
            addCriterion("d7 =", value, "d7");
            return (Criteria) this;
        }

        public Criteria andD7NotEqualTo(Integer value) {
            addCriterion("d7 <>", value, "d7");
            return (Criteria) this;
        }

        public Criteria andD7GreaterThan(Integer value) {
            addCriterion("d7 >", value, "d7");
            return (Criteria) this;
        }

        public Criteria andD7GreaterThanOrEqualTo(Integer value) {
            addCriterion("d7 >=", value, "d7");
            return (Criteria) this;
        }

        public Criteria andD7LessThan(Integer value) {
            addCriterion("d7 <", value, "d7");
            return (Criteria) this;
        }

        public Criteria andD7LessThanOrEqualTo(Integer value) {
            addCriterion("d7 <=", value, "d7");
            return (Criteria) this;
        }

        public Criteria andD7In(List<Integer> values) {
            addCriterion("d7 in", values, "d7");
            return (Criteria) this;
        }

        public Criteria andD7NotIn(List<Integer> values) {
            addCriterion("d7 not in", values, "d7");
            return (Criteria) this;
        }

        public Criteria andD7Between(Integer value1, Integer value2) {
            addCriterion("d7 between", value1, value2, "d7");
            return (Criteria) this;
        }

        public Criteria andD7NotBetween(Integer value1, Integer value2) {
            addCriterion("d7 not between", value1, value2, "d7");
            return (Criteria) this;
        }

        public Criteria andWarnTimeIsNull() {
            addCriterion("warn_time is null");
            return (Criteria) this;
        }

        public Criteria andWarnTimeIsNotNull() {
            addCriterion("warn_time is not null");
            return (Criteria) this;
        }

        public Criteria andWarnTimeEqualTo(Integer value) {
            addCriterion("warn_time =", value, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeNotEqualTo(Integer value) {
            addCriterion("warn_time <>", value, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeGreaterThan(Integer value) {
            addCriterion("warn_time >", value, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("warn_time >=", value, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeLessThan(Integer value) {
            addCriterion("warn_time <", value, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeLessThanOrEqualTo(Integer value) {
            addCriterion("warn_time <=", value, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeIn(List<Integer> values) {
            addCriterion("warn_time in", values, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeNotIn(List<Integer> values) {
            addCriterion("warn_time not in", values, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeBetween(Integer value1, Integer value2) {
            addCriterion("warn_time between", value1, value2, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("warn_time not between", value1, value2, "warnTime");
            return (Criteria) this;
        }

        public Criteria andWarnDateIsNull() {
            addCriterion("warn_date is null");
            return (Criteria) this;
        }

        public Criteria andWarnDateIsNotNull() {
            addCriterion("warn_date is not null");
            return (Criteria) this;
        }

        public Criteria andWarnDateEqualTo(Date value) {
            addCriterion("warn_date =", value, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateNotEqualTo(Date value) {
            addCriterion("warn_date <>", value, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateGreaterThan(Date value) {
            addCriterion("warn_date >", value, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateGreaterThanOrEqualTo(Date value) {
            addCriterion("warn_date >=", value, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateLessThan(Date value) {
            addCriterion("warn_date <", value, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateLessThanOrEqualTo(Date value) {
            addCriterion("warn_date <=", value, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateIn(List<Date> values) {
            addCriterion("warn_date in", values, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateNotIn(List<Date> values) {
            addCriterion("warn_date not in", values, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateBetween(Date value1, Date value2) {
            addCriterion("warn_date between", value1, value2, "warnDate");
            return (Criteria) this;
        }

        public Criteria andWarnDateNotBetween(Date value1, Date value2) {
            addCriterion("warn_date not between", value1, value2, "warnDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}