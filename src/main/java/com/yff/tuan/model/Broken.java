package com.yff.tuan.model;

import java.util.Date;

public class Broken {
    private Integer id;

    private Integer customerId;

    private Integer userId;

    private String checkerName;

    private Date checkDate;
    
    private String checkDateStr;
    
    private String customerName;
    
    private String userName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCheckerName() {
        return checkerName;
    }

    public void setCheckerName(String checkerName) {
        this.checkerName = checkerName;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

	public String getCustomerName() {
		return customerName;
	}

	public String getUserName() {
		return userName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCheckDateStr() {
		return checkDateStr;
	}

	public void setCheckDateStr(String checkDateStr) {
		this.checkDateStr = checkDateStr;
	}
    
    
}