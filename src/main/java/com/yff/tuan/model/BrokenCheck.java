package com.yff.tuan.model;

import java.util.Date;

public class BrokenCheck {
    private Integer id;

    private Integer brokenId;

    private Integer tyreSizeId;

    private String tireNo;

    private Integer brandId;

    private Integer primaryRetread;

    private Double tppTread;

    private Integer retreadBrandId;

    private Integer retreadCount;

    private Integer retreadTreadId;

    private Integer primaryTreadId;

    private Double primaryTreadDeep;

    private Double remainTreadDeep1;

    private Double remainTreadDeep2;

    private Double remainTreadDeep3;

    private Integer rX;

    private Integer useProblemId;

    private Integer tireProblemId;

    private Integer manageProblemId;

    private Integer repairProblemId;

    private Integer retreadProblemId;

    private Date updateTime;
    private String updateTimeStr;
    
    private String tyreSizeName;
    private String brandName;
    private String tppTreadName;
    private String retreadBrandName;
    private String retreadTreadName;
    private String primaryTreadName;
    private String useProblemName;
    private String tireProblemName;
    private String manageProblemName;
    private String repairProblemName;
    private String retreadProblemName;
    private Double price;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBrokenId() {
        return brokenId;
    }

    public void setBrokenId(Integer brokenId) {
        this.brokenId = brokenId;
    }

    public Integer getTyreSizeId() {
        return tyreSizeId;
    }

    public void setTyreSizeId(Integer tyreSizeId) {
        this.tyreSizeId = tyreSizeId;
    }

    public String getTireNo() {
        return tireNo;
    }

    public void setTireNo(String tireNo) {
        this.tireNo = tireNo;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getPrimaryRetread() {
        return primaryRetread;
    }

    public void setPrimaryRetread(Integer primaryRetread) {
        this.primaryRetread = primaryRetread;
    }

    

    public Double getTppTread() {
		return tppTread;
	}

	public void setTppTread(Double tppTread) {
		this.tppTread = tppTread;
	}

	public Integer getRetreadBrandId() {
        return retreadBrandId;
    }

    public void setRetreadBrandId(Integer retreadBrandId) {
        this.retreadBrandId = retreadBrandId;
    }

    public Integer getRetreadCount() {
        return retreadCount;
    }

    public void setRetreadCount(Integer retreadCount) {
        this.retreadCount = retreadCount;
    }

    public Integer getRetreadTreadId() {
        return retreadTreadId;
    }

    public void setRetreadTreadId(Integer retreadTreadId) {
        this.retreadTreadId = retreadTreadId;
    }

    public Integer getPrimaryTreadId() {
        return primaryTreadId;
    }

    public void setPrimaryTreadId(Integer primaryTreadId) {
        this.primaryTreadId = primaryTreadId;
    }

    public Double getPrimaryTreadDeep() {
        return primaryTreadDeep;
    }

    public void setPrimaryTreadDeep(Double primaryTreadDeep) {
        this.primaryTreadDeep = primaryTreadDeep;
    }

    public Double getRemainTreadDeep1() {
        return remainTreadDeep1;
    }

    public void setRemainTreadDeep1(Double remainTreadDeep1) {
        this.remainTreadDeep1 = remainTreadDeep1;
    }

    public Double getRemainTreadDeep2() {
        return remainTreadDeep2;
    }

    public void setRemainTreadDeep2(Double remainTreadDeep2) {
        this.remainTreadDeep2 = remainTreadDeep2;
    }

    public Double getRemainTreadDeep3() {
        return remainTreadDeep3;
    }

    public void setRemainTreadDeep3(Double remainTreadDeep3) {
        this.remainTreadDeep3 = remainTreadDeep3;
    }

    public Integer getrX() {
        return rX;
    }

    public void setrX(Integer rX) {
        this.rX = rX;
    }

    public Integer getUseProblemId() {
        return useProblemId;
    }

    public void setUseProblemId(Integer useProblemId) {
        this.useProblemId = useProblemId;
    }

    public Integer getTireProblemId() {
        return tireProblemId;
    }

    public void setTireProblemId(Integer tireProblemId) {
        this.tireProblemId = tireProblemId;
    }

    public Integer getManageProblemId() {
        return manageProblemId;
    }

    public void setManageProblemId(Integer manageProblemId) {
        this.manageProblemId = manageProblemId;
    }

    public Integer getRepairProblemId() {
        return repairProblemId;
    }

    public void setRepairProblemId(Integer repairProblemId) {
        this.repairProblemId = repairProblemId;
    }

    public Integer getRetreadProblemId() {
        return retreadProblemId;
    }

    public void setRetreadProblemId(Integer retreadProblemId) {
        this.retreadProblemId = retreadProblemId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	public String getTyreSizeName() {
		return tyreSizeName;
	}

	public String getBrandName() {
		return brandName;
	}

	public String getTppTreadName() {
		return tppTreadName;
	}

	public String getRetreadBrandName() {
		return retreadBrandName;
	}

	public String getRetreadTreadName() {
		return retreadTreadName;
	}

	public String getPrimaryTreadName() {
		return primaryTreadName;
	}

	public String getUseProblemName() {
		return useProblemName;
	}

	public String getTireProblemName() {
		return tireProblemName;
	}

	public String getManageProblemName() {
		return manageProblemName;
	}

	public String getRepairProblemName() {
		return repairProblemName;
	}

	public String getRetreadProblemName() {
		return retreadProblemName;
	}

	public void setTyreSizeName(String tyreSizeName) {
		this.tyreSizeName = tyreSizeName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public void setTppTreadName(String tppTreadName) {
		this.tppTreadName = tppTreadName;
	}

	public void setRetreadBrandName(String retreadBrandName) {
		this.retreadBrandName = retreadBrandName;
	}

	public void setRetreadTreadName(String retreadTreadName) {
		this.retreadTreadName = retreadTreadName;
	}

	public void setPrimaryTreadName(String primaryTreadName) {
		this.primaryTreadName = primaryTreadName;
	}

	public void setUseProblemName(String useProblemName) {
		this.useProblemName = useProblemName;
	}

	public void setTireProblemName(String tireProblemName) {
		this.tireProblemName = tireProblemName;
	}

	public void setManageProblemName(String manageProblemName) {
		this.manageProblemName = manageProblemName;
	}

	public void setRepairProblemName(String repairProblemName) {
		this.repairProblemName = repairProblemName;
	}

	public void setRetreadProblemName(String retreadProblemName) {
		this.retreadProblemName = retreadProblemName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getUpdateTimeStr() {
		return updateTimeStr;
	}

	public void setUpdateTimeStr(String updateTimeStr) {
		this.updateTimeStr = updateTimeStr;
	}

    
}