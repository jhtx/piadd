package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BrokenCheckExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BrokenCheckExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBrokenIdIsNull() {
            addCriterion("broken_id is null");
            return (Criteria) this;
        }

        public Criteria andBrokenIdIsNotNull() {
            addCriterion("broken_id is not null");
            return (Criteria) this;
        }

        public Criteria andBrokenIdEqualTo(Integer value) {
            addCriterion("broken_id =", value, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdNotEqualTo(Integer value) {
            addCriterion("broken_id <>", value, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdGreaterThan(Integer value) {
            addCriterion("broken_id >", value, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("broken_id >=", value, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdLessThan(Integer value) {
            addCriterion("broken_id <", value, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdLessThanOrEqualTo(Integer value) {
            addCriterion("broken_id <=", value, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdIn(List<Integer> values) {
            addCriterion("broken_id in", values, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdNotIn(List<Integer> values) {
            addCriterion("broken_id not in", values, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdBetween(Integer value1, Integer value2) {
            addCriterion("broken_id between", value1, value2, "brokenId");
            return (Criteria) this;
        }

        public Criteria andBrokenIdNotBetween(Integer value1, Integer value2) {
            addCriterion("broken_id not between", value1, value2, "brokenId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIsNull() {
            addCriterion("tyre_size_id is null");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIsNotNull() {
            addCriterion("tyre_size_id is not null");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdEqualTo(Integer value) {
            addCriterion("tyre_size_id =", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotEqualTo(Integer value) {
            addCriterion("tyre_size_id <>", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdGreaterThan(Integer value) {
            addCriterion("tyre_size_id >", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tyre_size_id >=", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdLessThan(Integer value) {
            addCriterion("tyre_size_id <", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdLessThanOrEqualTo(Integer value) {
            addCriterion("tyre_size_id <=", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIn(List<Integer> values) {
            addCriterion("tyre_size_id in", values, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotIn(List<Integer> values) {
            addCriterion("tyre_size_id not in", values, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdBetween(Integer value1, Integer value2) {
            addCriterion("tyre_size_id between", value1, value2, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tyre_size_id not between", value1, value2, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTireNoIsNull() {
            addCriterion("tire_no is null");
            return (Criteria) this;
        }

        public Criteria andTireNoIsNotNull() {
            addCriterion("tire_no is not null");
            return (Criteria) this;
        }

        public Criteria andTireNoEqualTo(String value) {
            addCriterion("tire_no =", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoNotEqualTo(String value) {
            addCriterion("tire_no <>", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoGreaterThan(String value) {
            addCriterion("tire_no >", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoGreaterThanOrEqualTo(String value) {
            addCriterion("tire_no >=", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoLessThan(String value) {
            addCriterion("tire_no <", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoLessThanOrEqualTo(String value) {
            addCriterion("tire_no <=", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoLike(String value) {
            addCriterion("tire_no like", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoNotLike(String value) {
            addCriterion("tire_no not like", value, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoIn(List<String> values) {
            addCriterion("tire_no in", values, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoNotIn(List<String> values) {
            addCriterion("tire_no not in", values, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoBetween(String value1, String value2) {
            addCriterion("tire_no between", value1, value2, "tireNo");
            return (Criteria) this;
        }

        public Criteria andTireNoNotBetween(String value1, String value2) {
            addCriterion("tire_no not between", value1, value2, "tireNo");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNull() {
            addCriterion("brand_id is null");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNotNull() {
            addCriterion("brand_id is not null");
            return (Criteria) this;
        }

        public Criteria andBrandIdEqualTo(Integer value) {
            addCriterion("brand_id =", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotEqualTo(Integer value) {
            addCriterion("brand_id <>", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThan(Integer value) {
            addCriterion("brand_id >", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("brand_id >=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThan(Integer value) {
            addCriterion("brand_id <", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThanOrEqualTo(Integer value) {
            addCriterion("brand_id <=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIn(List<Integer> values) {
            addCriterion("brand_id in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotIn(List<Integer> values) {
            addCriterion("brand_id not in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdBetween(Integer value1, Integer value2) {
            addCriterion("brand_id between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotBetween(Integer value1, Integer value2) {
            addCriterion("brand_id not between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadIsNull() {
            addCriterion("primary_retread is null");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadIsNotNull() {
            addCriterion("primary_retread is not null");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadEqualTo(Integer value) {
            addCriterion("primary_retread =", value, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadNotEqualTo(Integer value) {
            addCriterion("primary_retread <>", value, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadGreaterThan(Integer value) {
            addCriterion("primary_retread >", value, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadGreaterThanOrEqualTo(Integer value) {
            addCriterion("primary_retread >=", value, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadLessThan(Integer value) {
            addCriterion("primary_retread <", value, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadLessThanOrEqualTo(Integer value) {
            addCriterion("primary_retread <=", value, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadIn(List<Integer> values) {
            addCriterion("primary_retread in", values, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadNotIn(List<Integer> values) {
            addCriterion("primary_retread not in", values, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadBetween(Integer value1, Integer value2) {
            addCriterion("primary_retread between", value1, value2, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andPrimaryRetreadNotBetween(Integer value1, Integer value2) {
            addCriterion("primary_retread not between", value1, value2, "primaryRetread");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdIsNull() {
            addCriterion("tpp_tread_id is null");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdIsNotNull() {
            addCriterion("tpp_tread_id is not null");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdEqualTo(Double value) {
            addCriterion("tpp_tread_id =", value, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdNotEqualTo(Double value) {
            addCriterion("tpp_tread_id <>", value, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdGreaterThan(Double value) {
            addCriterion("tpp_tread_id >", value, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdGreaterThanOrEqualTo(Double value) {
            addCriterion("tpp_tread_id >=", value, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdLessThan(Double value) {
            addCriterion("tpp_tread_id <", value, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdLessThanOrEqualTo(Double value) {
            addCriterion("tpp_tread_id <=", value, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdIn(List<Double> values) {
            addCriterion("tpp_tread_id in", values, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdNotIn(List<Double> values) {
            addCriterion("tpp_tread_id not in", values, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdBetween(Double value1, Double value2) {
            addCriterion("tpp_tread_id between", value1, value2, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andTppTreadIdNotBetween(Double value1, Double value2) {
            addCriterion("tpp_tread_id not between", value1, value2, "tppTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdIsNull() {
            addCriterion("retread_brand_id is null");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdIsNotNull() {
            addCriterion("retread_brand_id is not null");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdEqualTo(Integer value) {
            addCriterion("retread_brand_id =", value, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdNotEqualTo(Integer value) {
            addCriterion("retread_brand_id <>", value, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdGreaterThan(Integer value) {
            addCriterion("retread_brand_id >", value, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("retread_brand_id >=", value, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdLessThan(Integer value) {
            addCriterion("retread_brand_id <", value, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdLessThanOrEqualTo(Integer value) {
            addCriterion("retread_brand_id <=", value, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdIn(List<Integer> values) {
            addCriterion("retread_brand_id in", values, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdNotIn(List<Integer> values) {
            addCriterion("retread_brand_id not in", values, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdBetween(Integer value1, Integer value2) {
            addCriterion("retread_brand_id between", value1, value2, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadBrandIdNotBetween(Integer value1, Integer value2) {
            addCriterion("retread_brand_id not between", value1, value2, "retreadBrandId");
            return (Criteria) this;
        }

        public Criteria andRetreadCountIsNull() {
            addCriterion("retread_count is null");
            return (Criteria) this;
        }

        public Criteria andRetreadCountIsNotNull() {
            addCriterion("retread_count is not null");
            return (Criteria) this;
        }

        public Criteria andRetreadCountEqualTo(Integer value) {
            addCriterion("retread_count =", value, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountNotEqualTo(Integer value) {
            addCriterion("retread_count <>", value, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountGreaterThan(Integer value) {
            addCriterion("retread_count >", value, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("retread_count >=", value, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountLessThan(Integer value) {
            addCriterion("retread_count <", value, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountLessThanOrEqualTo(Integer value) {
            addCriterion("retread_count <=", value, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountIn(List<Integer> values) {
            addCriterion("retread_count in", values, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountNotIn(List<Integer> values) {
            addCriterion("retread_count not in", values, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountBetween(Integer value1, Integer value2) {
            addCriterion("retread_count between", value1, value2, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadCountNotBetween(Integer value1, Integer value2) {
            addCriterion("retread_count not between", value1, value2, "retreadCount");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdIsNull() {
            addCriterion("retread_tread_id is null");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdIsNotNull() {
            addCriterion("retread_tread_id is not null");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdEqualTo(Integer value) {
            addCriterion("retread_tread_id =", value, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdNotEqualTo(Integer value) {
            addCriterion("retread_tread_id <>", value, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdGreaterThan(Integer value) {
            addCriterion("retread_tread_id >", value, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("retread_tread_id >=", value, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdLessThan(Integer value) {
            addCriterion("retread_tread_id <", value, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdLessThanOrEqualTo(Integer value) {
            addCriterion("retread_tread_id <=", value, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdIn(List<Integer> values) {
            addCriterion("retread_tread_id in", values, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdNotIn(List<Integer> values) {
            addCriterion("retread_tread_id not in", values, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdBetween(Integer value1, Integer value2) {
            addCriterion("retread_tread_id between", value1, value2, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andRetreadTreadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("retread_tread_id not between", value1, value2, "retreadTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdIsNull() {
            addCriterion("primary_tread_id is null");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdIsNotNull() {
            addCriterion("primary_tread_id is not null");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdEqualTo(Integer value) {
            addCriterion("primary_tread_id =", value, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdNotEqualTo(Integer value) {
            addCriterion("primary_tread_id <>", value, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdGreaterThan(Integer value) {
            addCriterion("primary_tread_id >", value, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("primary_tread_id >=", value, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdLessThan(Integer value) {
            addCriterion("primary_tread_id <", value, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdLessThanOrEqualTo(Integer value) {
            addCriterion("primary_tread_id <=", value, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdIn(List<Integer> values) {
            addCriterion("primary_tread_id in", values, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdNotIn(List<Integer> values) {
            addCriterion("primary_tread_id not in", values, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdBetween(Integer value1, Integer value2) {
            addCriterion("primary_tread_id between", value1, value2, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("primary_tread_id not between", value1, value2, "primaryTreadId");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepIsNull() {
            addCriterion("primary_tread_deep is null");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepIsNotNull() {
            addCriterion("primary_tread_deep is not null");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepEqualTo(Double value) {
            addCriterion("primary_tread_deep =", value, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepNotEqualTo(Double value) {
            addCriterion("primary_tread_deep <>", value, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepGreaterThan(Double value) {
            addCriterion("primary_tread_deep >", value, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepGreaterThanOrEqualTo(Double value) {
            addCriterion("primary_tread_deep >=", value, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepLessThan(Double value) {
            addCriterion("primary_tread_deep <", value, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepLessThanOrEqualTo(Double value) {
            addCriterion("primary_tread_deep <=", value, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepIn(List<Double> values) {
            addCriterion("primary_tread_deep in", values, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepNotIn(List<Double> values) {
            addCriterion("primary_tread_deep not in", values, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepBetween(Double value1, Double value2) {
            addCriterion("primary_tread_deep between", value1, value2, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andPrimaryTreadDeepNotBetween(Double value1, Double value2) {
            addCriterion("primary_tread_deep not between", value1, value2, "primaryTreadDeep");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1IsNull() {
            addCriterion("remain_tread_deep1 is null");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1IsNotNull() {
            addCriterion("remain_tread_deep1 is not null");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1EqualTo(Double value) {
            addCriterion("remain_tread_deep1 =", value, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1NotEqualTo(Double value) {
            addCriterion("remain_tread_deep1 <>", value, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1GreaterThan(Double value) {
            addCriterion("remain_tread_deep1 >", value, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1GreaterThanOrEqualTo(Double value) {
            addCriterion("remain_tread_deep1 >=", value, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1LessThan(Double value) {
            addCriterion("remain_tread_deep1 <", value, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1LessThanOrEqualTo(Double value) {
            addCriterion("remain_tread_deep1 <=", value, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1In(List<Double> values) {
            addCriterion("remain_tread_deep1 in", values, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1NotIn(List<Double> values) {
            addCriterion("remain_tread_deep1 not in", values, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1Between(Double value1, Double value2) {
            addCriterion("remain_tread_deep1 between", value1, value2, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep1NotBetween(Double value1, Double value2) {
            addCriterion("remain_tread_deep1 not between", value1, value2, "remainTreadDeep1");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2IsNull() {
            addCriterion("remain_tread_deep2 is null");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2IsNotNull() {
            addCriterion("remain_tread_deep2 is not null");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2EqualTo(Double value) {
            addCriterion("remain_tread_deep2 =", value, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2NotEqualTo(Double value) {
            addCriterion("remain_tread_deep2 <>", value, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2GreaterThan(Double value) {
            addCriterion("remain_tread_deep2 >", value, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2GreaterThanOrEqualTo(Double value) {
            addCriterion("remain_tread_deep2 >=", value, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2LessThan(Double value) {
            addCriterion("remain_tread_deep2 <", value, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2LessThanOrEqualTo(Double value) {
            addCriterion("remain_tread_deep2 <=", value, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2In(List<Double> values) {
            addCriterion("remain_tread_deep2 in", values, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2NotIn(List<Double> values) {
            addCriterion("remain_tread_deep2 not in", values, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2Between(Double value1, Double value2) {
            addCriterion("remain_tread_deep2 between", value1, value2, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep2NotBetween(Double value1, Double value2) {
            addCriterion("remain_tread_deep2 not between", value1, value2, "remainTreadDeep2");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3IsNull() {
            addCriterion("remain_tread_deep3 is null");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3IsNotNull() {
            addCriterion("remain_tread_deep3 is not null");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3EqualTo(Double value) {
            addCriterion("remain_tread_deep3 =", value, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3NotEqualTo(Double value) {
            addCriterion("remain_tread_deep3 <>", value, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3GreaterThan(Double value) {
            addCriterion("remain_tread_deep3 >", value, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3GreaterThanOrEqualTo(Double value) {
            addCriterion("remain_tread_deep3 >=", value, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3LessThan(Double value) {
            addCriterion("remain_tread_deep3 <", value, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3LessThanOrEqualTo(Double value) {
            addCriterion("remain_tread_deep3 <=", value, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3In(List<Double> values) {
            addCriterion("remain_tread_deep3 in", values, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3NotIn(List<Double> values) {
            addCriterion("remain_tread_deep3 not in", values, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3Between(Double value1, Double value2) {
            addCriterion("remain_tread_deep3 between", value1, value2, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRemainTreadDeep3NotBetween(Double value1, Double value2) {
            addCriterion("remain_tread_deep3 not between", value1, value2, "remainTreadDeep3");
            return (Criteria) this;
        }

        public Criteria andRXIsNull() {
            addCriterion("r_x is null");
            return (Criteria) this;
        }

        public Criteria andRXIsNotNull() {
            addCriterion("r_x is not null");
            return (Criteria) this;
        }

        public Criteria andRXEqualTo(Integer value) {
            addCriterion("r_x =", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXNotEqualTo(Integer value) {
            addCriterion("r_x <>", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXGreaterThan(Integer value) {
            addCriterion("r_x >", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXGreaterThanOrEqualTo(Integer value) {
            addCriterion("r_x >=", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXLessThan(Integer value) {
            addCriterion("r_x <", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXLessThanOrEqualTo(Integer value) {
            addCriterion("r_x <=", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXIn(List<Integer> values) {
            addCriterion("r_x in", values, "rX");
            return (Criteria) this;
        }

        public Criteria andRXNotIn(List<Integer> values) {
            addCriterion("r_x not in", values, "rX");
            return (Criteria) this;
        }

        public Criteria andRXBetween(Integer value1, Integer value2) {
            addCriterion("r_x between", value1, value2, "rX");
            return (Criteria) this;
        }

        public Criteria andRXNotBetween(Integer value1, Integer value2) {
            addCriterion("r_x not between", value1, value2, "rX");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdIsNull() {
            addCriterion("use_problem_id is null");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdIsNotNull() {
            addCriterion("use_problem_id is not null");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdEqualTo(Integer value) {
            addCriterion("use_problem_id =", value, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdNotEqualTo(Integer value) {
            addCriterion("use_problem_id <>", value, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdGreaterThan(Integer value) {
            addCriterion("use_problem_id >", value, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("use_problem_id >=", value, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdLessThan(Integer value) {
            addCriterion("use_problem_id <", value, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdLessThanOrEqualTo(Integer value) {
            addCriterion("use_problem_id <=", value, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdIn(List<Integer> values) {
            addCriterion("use_problem_id in", values, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdNotIn(List<Integer> values) {
            addCriterion("use_problem_id not in", values, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdBetween(Integer value1, Integer value2) {
            addCriterion("use_problem_id between", value1, value2, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andUseProblemIdNotBetween(Integer value1, Integer value2) {
            addCriterion("use_problem_id not between", value1, value2, "useProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdIsNull() {
            addCriterion("tire_problem_id is null");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdIsNotNull() {
            addCriterion("tire_problem_id is not null");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdEqualTo(Integer value) {
            addCriterion("tire_problem_id =", value, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdNotEqualTo(Integer value) {
            addCriterion("tire_problem_id <>", value, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdGreaterThan(Integer value) {
            addCriterion("tire_problem_id >", value, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tire_problem_id >=", value, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdLessThan(Integer value) {
            addCriterion("tire_problem_id <", value, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdLessThanOrEqualTo(Integer value) {
            addCriterion("tire_problem_id <=", value, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdIn(List<Integer> values) {
            addCriterion("tire_problem_id in", values, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdNotIn(List<Integer> values) {
            addCriterion("tire_problem_id not in", values, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdBetween(Integer value1, Integer value2) {
            addCriterion("tire_problem_id between", value1, value2, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andTireProblemIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tire_problem_id not between", value1, value2, "tireProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdIsNull() {
            addCriterion("manage_problem_id is null");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdIsNotNull() {
            addCriterion("manage_problem_id is not null");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdEqualTo(Integer value) {
            addCriterion("manage_problem_id =", value, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdNotEqualTo(Integer value) {
            addCriterion("manage_problem_id <>", value, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdGreaterThan(Integer value) {
            addCriterion("manage_problem_id >", value, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("manage_problem_id >=", value, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdLessThan(Integer value) {
            addCriterion("manage_problem_id <", value, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdLessThanOrEqualTo(Integer value) {
            addCriterion("manage_problem_id <=", value, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdIn(List<Integer> values) {
            addCriterion("manage_problem_id in", values, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdNotIn(List<Integer> values) {
            addCriterion("manage_problem_id not in", values, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdBetween(Integer value1, Integer value2) {
            addCriterion("manage_problem_id between", value1, value2, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andManageProblemIdNotBetween(Integer value1, Integer value2) {
            addCriterion("manage_problem_id not between", value1, value2, "manageProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdIsNull() {
            addCriterion("repair_problem_id is null");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdIsNotNull() {
            addCriterion("repair_problem_id is not null");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdEqualTo(Integer value) {
            addCriterion("repair_problem_id =", value, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdNotEqualTo(Integer value) {
            addCriterion("repair_problem_id <>", value, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdGreaterThan(Integer value) {
            addCriterion("repair_problem_id >", value, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("repair_problem_id >=", value, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdLessThan(Integer value) {
            addCriterion("repair_problem_id <", value, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdLessThanOrEqualTo(Integer value) {
            addCriterion("repair_problem_id <=", value, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdIn(List<Integer> values) {
            addCriterion("repair_problem_id in", values, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdNotIn(List<Integer> values) {
            addCriterion("repair_problem_id not in", values, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdBetween(Integer value1, Integer value2) {
            addCriterion("repair_problem_id between", value1, value2, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRepairProblemIdNotBetween(Integer value1, Integer value2) {
            addCriterion("repair_problem_id not between", value1, value2, "repairProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdIsNull() {
            addCriterion("retread__problem_id is null");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdIsNotNull() {
            addCriterion("retread__problem_id is not null");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdEqualTo(Integer value) {
            addCriterion("retread__problem_id =", value, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdNotEqualTo(Integer value) {
            addCriterion("retread__problem_id <>", value, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdGreaterThan(Integer value) {
            addCriterion("retread__problem_id >", value, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("retread__problem_id >=", value, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdLessThan(Integer value) {
            addCriterion("retread__problem_id <", value, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdLessThanOrEqualTo(Integer value) {
            addCriterion("retread__problem_id <=", value, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdIn(List<Integer> values) {
            addCriterion("retread__problem_id in", values, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdNotIn(List<Integer> values) {
            addCriterion("retread__problem_id not in", values, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdBetween(Integer value1, Integer value2) {
            addCriterion("retread__problem_id between", value1, value2, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andRetreadProblemIdNotBetween(Integer value1, Integer value2) {
            addCriterion("retread__problem_id not between", value1, value2, "retreadProblemId");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}