package com.yff.tuan.model;

import java.util.Date;

public class Customer {
	private Integer id;

    private String name;

    private Integer userId;
    
    private Integer status;

    private Integer failureReason;

    private String supplier;

    private Integer wholeCar42;

    private Integer wholeCar62;

    private Integer wholeCar84;

    private Integer drawCar42;

    private Integer drawCar62;

    private Integer drawCar64;

    private Integer dragCar40;

    private Integer dragCar40D;

    private Integer dragCar60;

    private Integer dragCar60D;

    private Double oldNewRate;

    private Double frontMiles;

    private Integer backMiles;

    private Long frontPrice;

    private Long backPrice;

    private String frontTread;

    private String backTread;

    private String dayChangeAmt;

    private String dayDropAmt;

    private String monthAvgFee;

    private String avgMiles;

    private String avgOilFee;

    private String likeBrand;

    private Integer doubleMatch;

    private Integer hasTest;

    private String checkRate;

    private String pressStandard;

    private String fleetAmt;

    private Date createTime;

    private Date updateTime;
    private String statusName;
    private String time;

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Integer getUserId() {
		return userId;
	}

	public Integer getStatus() {
		return status;
	}

	public Integer getFailureReason() {
		return failureReason;
	}

	public String getSupplier() {
		return supplier;
	}

	public Integer getWholeCar42() {
		return wholeCar42;
	}

	public Integer getWholeCar62() {
		return wholeCar62;
	}

	public Integer getWholeCar84() {
		return wholeCar84;
	}

	public Integer getDrawCar42() {
		return drawCar42;
	}

	public Integer getDrawCar62() {
		return drawCar62;
	}

	public Integer getDrawCar64() {
		return drawCar64;
	}

	public Integer getDragCar40() {
		return dragCar40;
	}

	public Integer getDragCar40D() {
		return dragCar40D;
	}

	public Integer getDragCar60() {
		return dragCar60;
	}

	public Integer getDragCar60D() {
		return dragCar60D;
	}

	public Double getOldNewRate() {
		return oldNewRate;
	}

	public Double getFrontMiles() {
		return frontMiles;
	}

	public Integer getBackMiles() {
		return backMiles;
	}

	public Long getFrontPrice() {
		return frontPrice;
	}

	public Long getBackPrice() {
		return backPrice;
	}

	public String getFrontTread() {
		return frontTread;
	}

	public String getBackTread() {
		return backTread;
	}

	public String getDayChangeAmt() {
		return dayChangeAmt;
	}

	public String getDayDropAmt() {
		return dayDropAmt;
	}

	public String getMonthAvgFee() {
		return monthAvgFee;
	}

	public String getAvgMiles() {
		return avgMiles;
	}

	public String getAvgOilFee() {
		return avgOilFee;
	}

	public String getLikeBrand() {
		return likeBrand;
	}

	public Integer getDoubleMatch() {
		return doubleMatch;
	}

	public Integer getHasTest() {
		return hasTest;
	}

	public String getCheckRate() {
		return checkRate;
	}

	public String getPressStandard() {
		return pressStandard;
	}

	public String getFleetAmt() {
		return fleetAmt;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setFailureReason(Integer failureReason) {
		this.failureReason = failureReason;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public void setWholeCar42(Integer wholeCar42) {
		this.wholeCar42 = wholeCar42;
	}

	public void setWholeCar62(Integer wholeCar62) {
		this.wholeCar62 = wholeCar62;
	}

	public void setWholeCar84(Integer wholeCar84) {
		this.wholeCar84 = wholeCar84;
	}

	public void setDrawCar42(Integer drawCar42) {
		this.drawCar42 = drawCar42;
	}

	public void setDrawCar62(Integer drawCar62) {
		this.drawCar62 = drawCar62;
	}

	public void setDrawCar64(Integer drawCar64) {
		this.drawCar64 = drawCar64;
	}

	public void setDragCar40(Integer dragCar40) {
		this.dragCar40 = dragCar40;
	}

	public void setDragCar40D(Integer dragCar40D) {
		this.dragCar40D = dragCar40D;
	}

	public void setDragCar60(Integer dragCar60) {
		this.dragCar60 = dragCar60;
	}

	public void setDragCar60D(Integer dragCar60D) {
		this.dragCar60D = dragCar60D;
	}

	public void setOldNewRate(Double oldNewRate) {
		this.oldNewRate = oldNewRate;
	}

	public void setFrontMiles(Double frontMiles) {
		this.frontMiles = frontMiles;
	}

	public void setBackMiles(Integer backMiles) {
		this.backMiles = backMiles;
	}

	public void setFrontPrice(Long frontPrice) {
		this.frontPrice = frontPrice;
	}

	public void setBackPrice(Long backPrice) {
		this.backPrice = backPrice;
	}

	public void setFrontTread(String frontTread) {
		this.frontTread = frontTread;
	}

	public void setBackTread(String backTread) {
		this.backTread = backTread;
	}

	public void setDayChangeAmt(String dayChangeAmt) {
		this.dayChangeAmt = dayChangeAmt;
	}

	public void setDayDropAmt(String dayDropAmt) {
		this.dayDropAmt = dayDropAmt;
	}

	public void setMonthAvgFee(String monthAvgFee) {
		this.monthAvgFee = monthAvgFee;
	}

	public void setAvgMiles(String avgMiles) {
		this.avgMiles = avgMiles;
	}

	public void setAvgOilFee(String avgOilFee) {
		this.avgOilFee = avgOilFee;
	}

	public void setLikeBrand(String likeBrand) {
		this.likeBrand = likeBrand;
	}

	public void setDoubleMatch(Integer doubleMatch) {
		this.doubleMatch = doubleMatch;
	}

	public void setHasTest(Integer hasTest) {
		this.hasTest = hasTest;
	}

	public void setCheckRate(String checkRate) {
		this.checkRate = checkRate;
	}

	public void setPressStandard(String pressStandard) {
		this.pressStandard = pressStandard;
	}

	public void setFleetAmt(String fleetAmt) {
		this.fleetAmt = fleetAmt;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	
    
}