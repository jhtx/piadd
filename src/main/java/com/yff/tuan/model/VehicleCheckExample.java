package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VehicleCheckExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VehicleCheckExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(String value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(String value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(String value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(String value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(String value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLike(String value) {
            addCriterion("product_id like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotLike(String value) {
            addCriterion("product_id not like", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<String> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<String> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(String value1, String value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(String value1, String value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andCarIdIsNull() {
            addCriterion("car_id is null");
            return (Criteria) this;
        }

        public Criteria andCarIdIsNotNull() {
            addCriterion("car_id is not null");
            return (Criteria) this;
        }

        public Criteria andCarIdEqualTo(Integer value) {
            addCriterion("car_id =", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotEqualTo(Integer value) {
            addCriterion("car_id <>", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdGreaterThan(Integer value) {
            addCriterion("car_id >", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("car_id >=", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdLessThan(Integer value) {
            addCriterion("car_id <", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdLessThanOrEqualTo(Integer value) {
            addCriterion("car_id <=", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdIn(List<Integer> values) {
            addCriterion("car_id in", values, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotIn(List<Integer> values) {
            addCriterion("car_id not in", values, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdBetween(Integer value1, Integer value2) {
            addCriterion("car_id between", value1, value2, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotBetween(Integer value1, Integer value2) {
            addCriterion("car_id not between", value1, value2, "carId");
            return (Criteria) this;
        }

        public Criteria andLatIsNull() {
            addCriterion("lat is null");
            return (Criteria) this;
        }

        public Criteria andLatIsNotNull() {
            addCriterion("lat is not null");
            return (Criteria) this;
        }

        public Criteria andLatEqualTo(String value) {
            addCriterion("lat =", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotEqualTo(String value) {
            addCriterion("lat <>", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThan(String value) {
            addCriterion("lat >", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThanOrEqualTo(String value) {
            addCriterion("lat >=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThan(String value) {
            addCriterion("lat <", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThanOrEqualTo(String value) {
            addCriterion("lat <=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLike(String value) {
            addCriterion("lat like", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotLike(String value) {
            addCriterion("lat not like", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatIn(List<String> values) {
            addCriterion("lat in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotIn(List<String> values) {
            addCriterion("lat not in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatBetween(String value1, String value2) {
            addCriterion("lat between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotBetween(String value1, String value2) {
            addCriterion("lat not between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andLngIsNull() {
            addCriterion("lng is null");
            return (Criteria) this;
        }

        public Criteria andLngIsNotNull() {
            addCriterion("lng is not null");
            return (Criteria) this;
        }

        public Criteria andLngEqualTo(String value) {
            addCriterion("lng =", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotEqualTo(String value) {
            addCriterion("lng <>", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThan(String value) {
            addCriterion("lng >", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThanOrEqualTo(String value) {
            addCriterion("lng >=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThan(String value) {
            addCriterion("lng <", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThanOrEqualTo(String value) {
            addCriterion("lng <=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLike(String value) {
            addCriterion("lng like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotLike(String value) {
            addCriterion("lng not like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngIn(List<String> values) {
            addCriterion("lng in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotIn(List<String> values) {
            addCriterion("lng not in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngBetween(String value1, String value2) {
            addCriterion("lng between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotBetween(String value1, String value2) {
            addCriterion("lng not between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdIsNull() {
            addCriterion("match_product_id is null");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdIsNotNull() {
            addCriterion("match_product_id is not null");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdEqualTo(String value) {
            addCriterion("match_product_id =", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdNotEqualTo(String value) {
            addCriterion("match_product_id <>", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdGreaterThan(String value) {
            addCriterion("match_product_id >", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdGreaterThanOrEqualTo(String value) {
            addCriterion("match_product_id >=", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdLessThan(String value) {
            addCriterion("match_product_id <", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdLessThanOrEqualTo(String value) {
            addCriterion("match_product_id <=", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdLike(String value) {
            addCriterion("match_product_id like", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdNotLike(String value) {
            addCriterion("match_product_id not like", value, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdIn(List<String> values) {
            addCriterion("match_product_id in", values, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdNotIn(List<String> values) {
            addCriterion("match_product_id not in", values, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdBetween(String value1, String value2) {
            addCriterion("match_product_id between", value1, value2, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchProductIdNotBetween(String value1, String value2) {
            addCriterion("match_product_id not between", value1, value2, "matchProductId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdIsNull() {
            addCriterion("match_car_id is null");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdIsNotNull() {
            addCriterion("match_car_id is not null");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdEqualTo(Integer value) {
            addCriterion("match_car_id =", value, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdNotEqualTo(Integer value) {
            addCriterion("match_car_id <>", value, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdGreaterThan(Integer value) {
            addCriterion("match_car_id >", value, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("match_car_id >=", value, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdLessThan(Integer value) {
            addCriterion("match_car_id <", value, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdLessThanOrEqualTo(Integer value) {
            addCriterion("match_car_id <=", value, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdIn(List<Integer> values) {
            addCriterion("match_car_id in", values, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdNotIn(List<Integer> values) {
            addCriterion("match_car_id not in", values, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdBetween(Integer value1, Integer value2) {
            addCriterion("match_car_id between", value1, value2, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andMatchCarIdNotBetween(Integer value1, Integer value2) {
            addCriterion("match_car_id not between", value1, value2, "matchCarId");
            return (Criteria) this;
        }

        public Criteria andSpeedIsNull() {
            addCriterion("speed is null");
            return (Criteria) this;
        }

        public Criteria andSpeedIsNotNull() {
            addCriterion("speed is not null");
            return (Criteria) this;
        }

        public Criteria andSpeedEqualTo(Double value) {
            addCriterion("speed =", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotEqualTo(Double value) {
            addCriterion("speed <>", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedGreaterThan(Double value) {
            addCriterion("speed >", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedGreaterThanOrEqualTo(Double value) {
            addCriterion("speed >=", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedLessThan(Double value) {
            addCriterion("speed <", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedLessThanOrEqualTo(Double value) {
            addCriterion("speed <=", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedIn(List<Double> values) {
            addCriterion("speed in", values, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotIn(List<Double> values) {
            addCriterion("speed not in", values, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedBetween(Double value1, Double value2) {
            addCriterion("speed between", value1, value2, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotBetween(Double value1, Double value2) {
            addCriterion("speed not between", value1, value2, "speed");
            return (Criteria) this;
        }

        public Criteria andMilesIsNull() {
            addCriterion("miles is null");
            return (Criteria) this;
        }

        public Criteria andMilesIsNotNull() {
            addCriterion("miles is not null");
            return (Criteria) this;
        }

        public Criteria andMilesEqualTo(Double value) {
            addCriterion("miles =", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotEqualTo(Double value) {
            addCriterion("miles <>", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesGreaterThan(Double value) {
            addCriterion("miles >", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesGreaterThanOrEqualTo(Double value) {
            addCriterion("miles >=", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesLessThan(Double value) {
            addCriterion("miles <", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesLessThanOrEqualTo(Double value) {
            addCriterion("miles <=", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesIn(List<Double> values) {
            addCriterion("miles in", values, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotIn(List<Double> values) {
            addCriterion("miles not in", values, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesBetween(Double value1, Double value2) {
            addCriterion("miles between", value1, value2, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotBetween(Double value1, Double value2) {
            addCriterion("miles not between", value1, value2, "miles");
            return (Criteria) this;
        }

        public Criteria andOneMileIsNull() {
            addCriterion("one_mile is null");
            return (Criteria) this;
        }

        public Criteria andOneMileIsNotNull() {
            addCriterion("one_mile is not null");
            return (Criteria) this;
        }

        public Criteria andOneMileEqualTo(Double value) {
            addCriterion("one_mile =", value, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileNotEqualTo(Double value) {
            addCriterion("one_mile <>", value, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileGreaterThan(Double value) {
            addCriterion("one_mile >", value, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileGreaterThanOrEqualTo(Double value) {
            addCriterion("one_mile >=", value, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileLessThan(Double value) {
            addCriterion("one_mile <", value, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileLessThanOrEqualTo(Double value) {
            addCriterion("one_mile <=", value, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileIn(List<Double> values) {
            addCriterion("one_mile in", values, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileNotIn(List<Double> values) {
            addCriterion("one_mile not in", values, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileBetween(Double value1, Double value2) {
            addCriterion("one_mile between", value1, value2, "oneMile");
            return (Criteria) this;
        }

        public Criteria andOneMileNotBetween(Double value1, Double value2) {
            addCriterion("one_mile not between", value1, value2, "oneMile");
            return (Criteria) this;
        }

        public Criteria andTaillightIsNull() {
            addCriterion("taillight is null");
            return (Criteria) this;
        }

        public Criteria andTaillightIsNotNull() {
            addCriterion("taillight is not null");
            return (Criteria) this;
        }

        public Criteria andTaillightEqualTo(String value) {
            addCriterion("taillight =", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightNotEqualTo(String value) {
            addCriterion("taillight <>", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightGreaterThan(String value) {
            addCriterion("taillight >", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightGreaterThanOrEqualTo(String value) {
            addCriterion("taillight >=", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightLessThan(String value) {
            addCriterion("taillight <", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightLessThanOrEqualTo(String value) {
            addCriterion("taillight <=", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightLike(String value) {
            addCriterion("taillight like", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightNotLike(String value) {
            addCriterion("taillight not like", value, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightIn(List<String> values) {
            addCriterion("taillight in", values, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightNotIn(List<String> values) {
            addCriterion("taillight not in", values, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightBetween(String value1, String value2) {
            addCriterion("taillight between", value1, value2, "taillight");
            return (Criteria) this;
        }

        public Criteria andTaillightNotBetween(String value1, String value2) {
            addCriterion("taillight not between", value1, value2, "taillight");
            return (Criteria) this;
        }

        public Criteria andChargeIsNull() {
            addCriterion("charge is null");
            return (Criteria) this;
        }

        public Criteria andChargeIsNotNull() {
            addCriterion("charge is not null");
            return (Criteria) this;
        }

        public Criteria andChargeEqualTo(Double value) {
            addCriterion("charge =", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotEqualTo(Double value) {
            addCriterion("charge <>", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThan(Double value) {
            addCriterion("charge >", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThanOrEqualTo(Double value) {
            addCriterion("charge >=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThan(Double value) {
            addCriterion("charge <", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThanOrEqualTo(Double value) {
            addCriterion("charge <=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeIn(List<Double> values) {
            addCriterion("charge in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotIn(List<Double> values) {
            addCriterion("charge not in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeBetween(Double value1, Double value2) {
            addCriterion("charge between", value1, value2, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotBetween(Double value1, Double value2) {
            addCriterion("charge not between", value1, value2, "charge");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIsNull() {
            addCriterion("check_time is null");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIsNotNull() {
            addCriterion("check_time is not null");
            return (Criteria) this;
        }

        public Criteria andCheckTimeEqualTo(Date value) {
            addCriterion("check_time =", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotEqualTo(Date value) {
            addCriterion("check_time <>", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeGreaterThan(Date value) {
            addCriterion("check_time >", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("check_time >=", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeLessThan(Date value) {
            addCriterion("check_time <", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeLessThanOrEqualTo(Date value) {
            addCriterion("check_time <=", value, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeIn(List<Date> values) {
            addCriterion("check_time in", values, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotIn(List<Date> values) {
            addCriterion("check_time not in", values, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeBetween(Date value1, Date value2) {
            addCriterion("check_time between", value1, value2, "checkTime");
            return (Criteria) this;
        }

        public Criteria andCheckTimeNotBetween(Date value1, Date value2) {
            addCriterion("check_time not between", value1, value2, "checkTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}