package com.yff.tuan.model;

public class WxUserBind {
    private Integer id;

    private Integer userId;

    private Integer wxId;
    
    private String wxName;
    
    private String openId;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getWxId() {
        return wxId;
    }

    public void setWxId(Integer wxId) {
        this.wxId = wxId;
    }

	public String getWxName() {
		return wxName;
	}

	public String getOpenId() {
		return openId;
	}

	public void setWxName(String wxName) {
		this.wxName = wxName;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
    
}