package com.yff.tuan.model;

public class CheckVehicleTireWarn {
    private Integer id;

    private Integer checkVehicleTireId;

    private Integer warnType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCheckVehicleTireId() {
        return checkVehicleTireId;
    }

    public void setCheckVehicleTireId(Integer checkVehicleTireId) {
        this.checkVehicleTireId = checkVehicleTireId;
    }

    public Integer getWarnType() {
        return warnType;
    }

    public void setWarnType(Integer warnType) {
        this.warnType = warnType;
    }
}