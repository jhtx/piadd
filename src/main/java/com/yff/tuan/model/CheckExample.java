package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CheckExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIsNull() {
            addCriterion("customer_id is null");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIsNotNull() {
            addCriterion("customer_id is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerIdEqualTo(Integer value) {
            addCriterion("customer_id =", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotEqualTo(Integer value) {
            addCriterion("customer_id <>", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdGreaterThan(Integer value) {
            addCriterion("customer_id >", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("customer_id >=", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdLessThan(Integer value) {
            addCriterion("customer_id <", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdLessThanOrEqualTo(Integer value) {
            addCriterion("customer_id <=", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIn(List<Integer> values) {
            addCriterion("customer_id in", values, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotIn(List<Integer> values) {
            addCriterion("customer_id not in", values, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdBetween(Integer value1, Integer value2) {
            addCriterion("customer_id between", value1, value2, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("customer_id not between", value1, value2, "customerId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andCheckerNameIsNull() {
            addCriterion("checker_name is null");
            return (Criteria) this;
        }

        public Criteria andCheckerNameIsNotNull() {
            addCriterion("checker_name is not null");
            return (Criteria) this;
        }

        public Criteria andCheckerNameEqualTo(String value) {
            addCriterion("checker_name =", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameNotEqualTo(String value) {
            addCriterion("checker_name <>", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameGreaterThan(String value) {
            addCriterion("checker_name >", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameGreaterThanOrEqualTo(String value) {
            addCriterion("checker_name >=", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameLessThan(String value) {
            addCriterion("checker_name <", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameLessThanOrEqualTo(String value) {
            addCriterion("checker_name <=", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameLike(String value) {
            addCriterion("checker_name like", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameNotLike(String value) {
            addCriterion("checker_name not like", value, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameIn(List<String> values) {
            addCriterion("checker_name in", values, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameNotIn(List<String> values) {
            addCriterion("checker_name not in", values, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameBetween(String value1, String value2) {
            addCriterion("checker_name between", value1, value2, "checkerName");
            return (Criteria) this;
        }

        public Criteria andCheckerNameNotBetween(String value1, String value2) {
            addCriterion("checker_name not between", value1, value2, "checkerName");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardIsNull() {
            addCriterion("guide_air_standard is null");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardIsNotNull() {
            addCriterion("guide_air_standard is not null");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardEqualTo(Double value) {
            addCriterion("guide_air_standard =", value, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardNotEqualTo(Double value) {
            addCriterion("guide_air_standard <>", value, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardGreaterThan(Double value) {
            addCriterion("guide_air_standard >", value, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardGreaterThanOrEqualTo(Double value) {
            addCriterion("guide_air_standard >=", value, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardLessThan(Double value) {
            addCriterion("guide_air_standard <", value, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardLessThanOrEqualTo(Double value) {
            addCriterion("guide_air_standard <=", value, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardIn(List<Double> values) {
            addCriterion("guide_air_standard in", values, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardNotIn(List<Double> values) {
            addCriterion("guide_air_standard not in", values, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardBetween(Double value1, Double value2) {
            addCriterion("guide_air_standard between", value1, value2, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andGuideAirStandardNotBetween(Double value1, Double value2) {
            addCriterion("guide_air_standard not between", value1, value2, "guideAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardIsNull() {
            addCriterion("drive_air_standard is null");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardIsNotNull() {
            addCriterion("drive_air_standard is not null");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardEqualTo(Double value) {
            addCriterion("drive_air_standard =", value, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardNotEqualTo(Double value) {
            addCriterion("drive_air_standard <>", value, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardGreaterThan(Double value) {
            addCriterion("drive_air_standard >", value, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardGreaterThanOrEqualTo(Double value) {
            addCriterion("drive_air_standard >=", value, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardLessThan(Double value) {
            addCriterion("drive_air_standard <", value, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardLessThanOrEqualTo(Double value) {
            addCriterion("drive_air_standard <=", value, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardIn(List<Double> values) {
            addCriterion("drive_air_standard in", values, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardNotIn(List<Double> values) {
            addCriterion("drive_air_standard not in", values, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardBetween(Double value1, Double value2) {
            addCriterion("drive_air_standard between", value1, value2, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDriveAirStandardNotBetween(Double value1, Double value2) {
            addCriterion("drive_air_standard not between", value1, value2, "driveAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardIsNull() {
            addCriterion("drag_air_standard is null");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardIsNotNull() {
            addCriterion("drag_air_standard is not null");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardEqualTo(Double value) {
            addCriterion("drag_air_standard =", value, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardNotEqualTo(Double value) {
            addCriterion("drag_air_standard <>", value, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardGreaterThan(Double value) {
            addCriterion("drag_air_standard >", value, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardGreaterThanOrEqualTo(Double value) {
            addCriterion("drag_air_standard >=", value, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardLessThan(Double value) {
            addCriterion("drag_air_standard <", value, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardLessThanOrEqualTo(Double value) {
            addCriterion("drag_air_standard <=", value, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardIn(List<Double> values) {
            addCriterion("drag_air_standard in", values, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardNotIn(List<Double> values) {
            addCriterion("drag_air_standard not in", values, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardBetween(Double value1, Double value2) {
            addCriterion("drag_air_standard between", value1, value2, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDragAirStandardNotBetween(Double value1, Double value2) {
            addCriterion("drag_air_standard not between", value1, value2, "dragAirStandard");
            return (Criteria) this;
        }

        public Criteria andDateIsNull() {
            addCriterion("date is null");
            return (Criteria) this;
        }

        public Criteria andDateIsNotNull() {
            addCriterion("date is not null");
            return (Criteria) this;
        }

        public Criteria andDateEqualTo(Date value) {
            addCriterion("date =", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotEqualTo(Date value) {
            addCriterion("date <>", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThan(Date value) {
            addCriterion("date >", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThanOrEqualTo(Date value) {
            addCriterion("date >=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThan(Date value) {
            addCriterion("date <", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThanOrEqualTo(Date value) {
            addCriterion("date <=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateIn(List<Date> values) {
            addCriterion("date in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotIn(List<Date> values) {
            addCriterion("date not in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateBetween(Date value1, Date value2) {
            addCriterion("date between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotBetween(Date value1, Date value2) {
            addCriterion("date not between", value1, value2, "date");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}