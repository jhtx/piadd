package com.yff.tuan.model;

import java.util.List;

public class TrackTire {
    private Integer id;

    private Integer trackId;

    private Integer vehicleTypeTireId;

    private Integer brandId;

    private Integer tyreSizeId;

    private Integer treadId;

    private String leftNo;

    private String rightNo;

    private Double airPress;

    private Double tread;

    private Double miles;
    
    private Double tppTread;
    
    private String vehicleTypeTireName;
    private String brandName;
    private String tyreSizeName;
    private String treadName;
    
    private List<TrackTireCheck> trackTireChecks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTrackId() {
        return trackId;
    }

    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    public Integer getVehicleTypeTireId() {
        return vehicleTypeTireId;
    }

    public void setVehicleTypeTireId(Integer vehicleTypeTireId) {
        this.vehicleTypeTireId = vehicleTypeTireId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getTyreSizeId() {
        return tyreSizeId;
    }

    public void setTyreSizeId(Integer tyreSizeId) {
        this.tyreSizeId = tyreSizeId;
    }

    public Integer getTreadId() {
        return treadId;
    }

    public void setTreadId(Integer treadId) {
        this.treadId = treadId;
    }

    public String getLeftNo() {
        return leftNo;
    }

    public void setLeftNo(String leftNo) {
        this.leftNo = leftNo;
    }

    public String getRightNo() {
        return rightNo;
    }

    public void setRightNo(String rightNo) {
        this.rightNo = rightNo;
    }

    public Double getAirPress() {
        return airPress;
    }

    public void setAirPress(Double airPress) {
        this.airPress = airPress;
    }

    public Double getTread() {
        return tread;
    }

    public void setTread(Double tread) {
        this.tread = tread;
    }

    public Double getMiles() {
        return miles;
    }

    public void setMiles(Double miles) {
        this.miles = miles;
    }

	public String getVehicleTypeTireName() {
		return vehicleTypeTireName;
	}

	public String getBrandName() {
		return brandName;
	}

	public String getTyreSizeName() {
		return tyreSizeName;
	}

	public String getTreadName() {
		return treadName;
	}

	public void setVehicleTypeTireName(String vehicleTypeTireName) {
		this.vehicleTypeTireName = vehicleTypeTireName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public void setTyreSizeName(String tyreSizeName) {
		this.tyreSizeName = tyreSizeName;
	}

	public void setTreadName(String treadName) {
		this.treadName = treadName;
	}

	public List<TrackTireCheck> getTrackTireChecks() {
		return trackTireChecks;
	}

	public void setTrackTireChecks(List<TrackTireCheck> trackTireChecks) {
		this.trackTireChecks = trackTireChecks;
	}

	public Double getTppTread() {
		return tppTread;
	}

	public void setTppTread(Double tppTread) {
		this.tppTread = tppTread;
	}
    
}