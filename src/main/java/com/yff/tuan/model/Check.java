package com.yff.tuan.model;

import java.util.Date;
import java.util.List;

public class Check {
    private Integer id;

    private Integer customerId;

    private Integer userId;

    private String checkerName;

    private Double guideAirStandard;

    private Double driveAirStandard;

    private Double dragAirStandard;

    private Date date;
    private String dateStr;
    
    private String customerName;
    private String userName;
    private List<CheckVehicle> checkVehicles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCheckerName() {
        return checkerName;
    }

    public void setCheckerName(String checkerName) {
        this.checkerName = checkerName;
    }

    public Double getGuideAirStandard() {
        return guideAirStandard;
    }

    public void setGuideAirStandard(Double guideAirStandard) {
        this.guideAirStandard = guideAirStandard;
    }

    public Double getDriveAirStandard() {
        return driveAirStandard;
    }

    public void setDriveAirStandard(Double driveAirStandard) {
        this.driveAirStandard = driveAirStandard;
    }

    public Double getDragAirStandard() {
        return dragAirStandard;
    }

    public void setDragAirStandard(Double dragAirStandard) {
        this.dragAirStandard = dragAirStandard;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

	public String getCustomerName() {
		return customerName;
	}

	public String getUserName() {
		return userName;
	}

	public List<CheckVehicle> getCheckVehicles() {
		return checkVehicles;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setCheckVehicles(List<CheckVehicle> checkVehicles) {
		this.checkVehicles = checkVehicles;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
    
}