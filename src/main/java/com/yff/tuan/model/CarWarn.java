package com.yff.tuan.model;

public class CarWarn {
    private Integer id;

    private Integer userId;

    private Double pressure;

    private Double temperature;

    private Double temperatureWarning;

    private Double charge;

    private Double chargeWarning;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getTemperatureWarning() {
        return temperatureWarning;
    }

    public void setTemperatureWarning(Double temperatureWarning) {
        this.temperatureWarning = temperatureWarning;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Double getChargeWarning() {
        return chargeWarning;
    }

    public void setChargeWarning(Double chargeWarning) {
        this.chargeWarning = chargeWarning;
    }
}