package com.yff.tuan.model;

import java.util.List;

public class CheckVehicle {
    private Integer id;

    private Integer checkId;

    private String vehicleNo;

    private Integer vehicleTypeId;
    
    private String vehicleTypeName;
    
    private List<CheckVehicleTire> checkVehicleTires;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCheckId() {
        return checkId;
    }

    public void setCheckId(Integer checkId) {
        this.checkId = checkId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

	public String getVehicleTypeName() {
		return vehicleTypeName;
	}

	public void setVehicleTypeName(String vehicleTypeName) {
		this.vehicleTypeName = vehicleTypeName;
	}

	public List<CheckVehicleTire> getCheckVehicleTires() {
		return checkVehicleTires;
	}

	public void setCheckVehicleTires(List<CheckVehicleTire> checkVehicleTires) {
		this.checkVehicleTires = checkVehicleTires;
	}
    
}