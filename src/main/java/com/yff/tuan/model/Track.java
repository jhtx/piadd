package com.yff.tuan.model;

import java.util.Date;
import java.util.List;

public class Track {
    private Integer id;

    private Integer customerId;

    private Integer userId;

    private String contact;

    private String contactPhone;

    private String driverName;

    private String driverPhone;

    private String business;

    private String carNo;

    private String handCarNo;

    private Date inDate;
    
    private String inDateStr;

    private Integer vehicleTypeId;

    private String driveMode;

    private Double baseMiles;

    private String transType;

    private String roads;

    private Double weight;

    private Date createTime;
    
    private String customerName;
    private String userName;
    
    private String vehicleTypeName;
    
    private List<TrackTire> tires;
    
    private String time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public String getHandCarNo() {
        return handCarNo;
    }

    public void setHandCarNo(String handCarNo) {
        this.handCarNo = handCarNo;
    }

    public Date getInDate() {
        return inDate;
    }

    public void setInDate(Date inDate) {
        this.inDate = inDate;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getDriveMode() {
        return driveMode;
    }

    public void setDriveMode(String driveMode) {
        this.driveMode = driveMode;
    }

    public Double getBaseMiles() {
        return baseMiles;
    }

    public void setBaseMiles(Double baseMiles) {
        this.baseMiles = baseMiles;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getRoads() {
        return roads;
    }

    public void setRoads(String roads) {
        this.roads = roads;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public String getCustomerName() {
		return customerName;
	}

	public String getUserName() {
		return userName;
	}

	public String getVehicleTypeName() {
		return vehicleTypeName;
	}

	public List<TrackTire> getTires() {
		return tires;
	}

	public String getTime() {
		return time;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setVehicleTypeName(String vehicleTypeName) {
		this.vehicleTypeName = vehicleTypeName;
	}

	public void setTires(List<TrackTire> tires) {
		this.tires = tires;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getInDateStr() {
		return inDateStr;
	}

	public void setInDateStr(String inDateStr) {
		this.inDateStr = inDateStr;
	}
    
}