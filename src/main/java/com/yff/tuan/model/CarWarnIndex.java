package com.yff.tuan.model;

public class CarWarnIndex {
    private Integer id;

    private Integer warinId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWarinId() {
        return warinId;
    }

    public void setWarinId(Integer warinId) {
        this.warinId = warinId;
    }
}