package com.yff.tuan.model;

import java.util.List;

public class CheckVehicleTire {
	private Integer id;

    private Integer checkVehicleId;

    private Integer vehicleTypeTireId;

    private Integer rX;

    private String retreadType;

    private String treadType;

    private Integer tyreSizeId;

    private Integer brandId;

    private Integer treadId;

    private Double airPress;

    private Double treadDeep1;

    private Double treadDeep2;

    private Double treadDeep3;
    

    private String brandName;
    private String treadName;
    private String vehicleTypeTireName;
    private String tyreSizeName;
    private List<CheckVehicleTireWarn> vehicleTypeTireWarns;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCheckVehicleId() {
        return checkVehicleId;
    }

    public void setCheckVehicleId(Integer checkVehicleId) {
        this.checkVehicleId = checkVehicleId;
    }

    public Integer getVehicleTypeTireId() {
        return vehicleTypeTireId;
    }

    public void setVehicleTypeTireId(Integer vehicleTypeTireId) {
        this.vehicleTypeTireId = vehicleTypeTireId;
    }

    public Integer getrX() {
        return rX;
    }

    public void setrX(Integer rX) {
        this.rX = rX;
    }

    public String getRetreadType() {
        return retreadType;
    }

    public void setRetreadType(String retreadType) {
        this.retreadType = retreadType;
    }

    public String getTreadType() {
        return treadType;
    }

    public void setTreadType(String treadType) {
        this.treadType = treadType;
    }

    public Integer getTyreSizeId() {
        return tyreSizeId;
    }

    public void setTyreSizeId(Integer tyreSizeId) {
        this.tyreSizeId = tyreSizeId;
    }

    public Double getAirPress() {
        return airPress;
    }

    public void setAirPress(Double airPress) {
        this.airPress = airPress;
    }

    public Double getTreadDeep1() {
        return treadDeep1;
    }

    public void setTreadDeep1(Double treadDeep1) {
        this.treadDeep1 = treadDeep1;
    }

    public Double getTreadDeep2() {
        return treadDeep2;
    }

    public void setTreadDeep2(Double treadDeep2) {
        this.treadDeep2 = treadDeep2;
    }

    public Double getTreadDeep3() {
        return treadDeep3;
    }

    public void setTreadDeep3(Double treadDeep3) {
        this.treadDeep3 = treadDeep3;
    }

	public String getVehicleTypeTireName() {
		return vehicleTypeTireName;
	}

	public String getTyreSizeName() {
		return tyreSizeName;
	}

	public void setVehicleTypeTireName(String vehicleTypeTireName) {
		this.vehicleTypeTireName = vehicleTypeTireName;
	}

	public void setTyreSizeName(String tyreSizeName) {
		this.tyreSizeName = tyreSizeName;
	}

	public String getBrandName() {
		return brandName;
	}

	public String getTreadName() {
		return treadName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public void setTreadName(String treadName) {
		this.treadName = treadName;
	}

	public List<CheckVehicleTireWarn> getVehicleTypeTireWarns() {
		return vehicleTypeTireWarns;
	}

	public void setVehicleTypeTireWarns(List<CheckVehicleTireWarn> vehicleTypeTireWarns) {
		this.vehicleTypeTireWarns = vehicleTypeTireWarns;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public Integer getTreadId() {
		return treadId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public void setTreadId(Integer treadId) {
		this.treadId = treadId;
	}
    
}