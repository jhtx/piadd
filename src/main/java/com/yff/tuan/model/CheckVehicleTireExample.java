package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.List;

public class CheckVehicleTireExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CheckVehicleTireExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdIsNull() {
            addCriterion("check_vehicle_id is null");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdIsNotNull() {
            addCriterion("check_vehicle_id is not null");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdEqualTo(Integer value) {
            addCriterion("check_vehicle_id =", value, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdNotEqualTo(Integer value) {
            addCriterion("check_vehicle_id <>", value, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdGreaterThan(Integer value) {
            addCriterion("check_vehicle_id >", value, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("check_vehicle_id >=", value, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdLessThan(Integer value) {
            addCriterion("check_vehicle_id <", value, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdLessThanOrEqualTo(Integer value) {
            addCriterion("check_vehicle_id <=", value, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdIn(List<Integer> values) {
            addCriterion("check_vehicle_id in", values, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdNotIn(List<Integer> values) {
            addCriterion("check_vehicle_id not in", values, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdBetween(Integer value1, Integer value2) {
            addCriterion("check_vehicle_id between", value1, value2, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andCheckVehicleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("check_vehicle_id not between", value1, value2, "checkVehicleId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdIsNull() {
            addCriterion("vehicle_type_tire_id is null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdIsNotNull() {
            addCriterion("vehicle_type_tire_id is not null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id =", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdNotEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id <>", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdGreaterThan(Integer value) {
            addCriterion("vehicle_type_tire_id >", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id >=", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdLessThan(Integer value) {
            addCriterion("vehicle_type_tire_id <", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdLessThanOrEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id <=", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdIn(List<Integer> values) {
            addCriterion("vehicle_type_tire_id in", values, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdNotIn(List<Integer> values) {
            addCriterion("vehicle_type_tire_id not in", values, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdBetween(Integer value1, Integer value2) {
            addCriterion("vehicle_type_tire_id between", value1, value2, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdNotBetween(Integer value1, Integer value2) {
            addCriterion("vehicle_type_tire_id not between", value1, value2, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andRXIsNull() {
            addCriterion("r_x is null");
            return (Criteria) this;
        }

        public Criteria andRXIsNotNull() {
            addCriterion("r_x is not null");
            return (Criteria) this;
        }

        public Criteria andRXEqualTo(Integer value) {
            addCriterion("r_x =", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXNotEqualTo(Integer value) {
            addCriterion("r_x <>", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXGreaterThan(Integer value) {
            addCriterion("r_x >", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXGreaterThanOrEqualTo(Integer value) {
            addCriterion("r_x >=", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXLessThan(Integer value) {
            addCriterion("r_x <", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXLessThanOrEqualTo(Integer value) {
            addCriterion("r_x <=", value, "rX");
            return (Criteria) this;
        }

        public Criteria andRXIn(List<Integer> values) {
            addCriterion("r_x in", values, "rX");
            return (Criteria) this;
        }

        public Criteria andRXNotIn(List<Integer> values) {
            addCriterion("r_x not in", values, "rX");
            return (Criteria) this;
        }

        public Criteria andRXBetween(Integer value1, Integer value2) {
            addCriterion("r_x between", value1, value2, "rX");
            return (Criteria) this;
        }

        public Criteria andRXNotBetween(Integer value1, Integer value2) {
            addCriterion("r_x not between", value1, value2, "rX");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeIsNull() {
            addCriterion("retread_type is null");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeIsNotNull() {
            addCriterion("retread_type is not null");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeEqualTo(String value) {
            addCriterion("retread_type =", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeNotEqualTo(String value) {
            addCriterion("retread_type <>", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeGreaterThan(String value) {
            addCriterion("retread_type >", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeGreaterThanOrEqualTo(String value) {
            addCriterion("retread_type >=", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeLessThan(String value) {
            addCriterion("retread_type <", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeLessThanOrEqualTo(String value) {
            addCriterion("retread_type <=", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeLike(String value) {
            addCriterion("retread_type like", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeNotLike(String value) {
            addCriterion("retread_type not like", value, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeIn(List<String> values) {
            addCriterion("retread_type in", values, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeNotIn(List<String> values) {
            addCriterion("retread_type not in", values, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeBetween(String value1, String value2) {
            addCriterion("retread_type between", value1, value2, "retreadType");
            return (Criteria) this;
        }

        public Criteria andRetreadTypeNotBetween(String value1, String value2) {
            addCriterion("retread_type not between", value1, value2, "retreadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeIsNull() {
            addCriterion("tread_type is null");
            return (Criteria) this;
        }

        public Criteria andTreadTypeIsNotNull() {
            addCriterion("tread_type is not null");
            return (Criteria) this;
        }

        public Criteria andTreadTypeEqualTo(String value) {
            addCriterion("tread_type =", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeNotEqualTo(String value) {
            addCriterion("tread_type <>", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeGreaterThan(String value) {
            addCriterion("tread_type >", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeGreaterThanOrEqualTo(String value) {
            addCriterion("tread_type >=", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeLessThan(String value) {
            addCriterion("tread_type <", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeLessThanOrEqualTo(String value) {
            addCriterion("tread_type <=", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeLike(String value) {
            addCriterion("tread_type like", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeNotLike(String value) {
            addCriterion("tread_type not like", value, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeIn(List<String> values) {
            addCriterion("tread_type in", values, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeNotIn(List<String> values) {
            addCriterion("tread_type not in", values, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeBetween(String value1, String value2) {
            addCriterion("tread_type between", value1, value2, "treadType");
            return (Criteria) this;
        }

        public Criteria andTreadTypeNotBetween(String value1, String value2) {
            addCriterion("tread_type not between", value1, value2, "treadType");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIsNull() {
            addCriterion("tyre_size_id is null");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIsNotNull() {
            addCriterion("tyre_size_id is not null");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdEqualTo(Integer value) {
            addCriterion("tyre_size_id =", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotEqualTo(Integer value) {
            addCriterion("tyre_size_id <>", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdGreaterThan(Integer value) {
            addCriterion("tyre_size_id >", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tyre_size_id >=", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdLessThan(Integer value) {
            addCriterion("tyre_size_id <", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdLessThanOrEqualTo(Integer value) {
            addCriterion("tyre_size_id <=", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIn(List<Integer> values) {
            addCriterion("tyre_size_id in", values, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotIn(List<Integer> values) {
            addCriterion("tyre_size_id not in", values, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdBetween(Integer value1, Integer value2) {
            addCriterion("tyre_size_id between", value1, value2, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tyre_size_id not between", value1, value2, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNull() {
            addCriterion("brand_id is null");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNotNull() {
            addCriterion("brand_id is not null");
            return (Criteria) this;
        }

        public Criteria andBrandIdEqualTo(Integer value) {
            addCriterion("brand_id =", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotEqualTo(Integer value) {
            addCriterion("brand_id <>", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThan(Integer value) {
            addCriterion("brand_id >", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("brand_id >=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThan(Integer value) {
            addCriterion("brand_id <", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThanOrEqualTo(Integer value) {
            addCriterion("brand_id <=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIn(List<Integer> values) {
            addCriterion("brand_id in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotIn(List<Integer> values) {
            addCriterion("brand_id not in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdBetween(Integer value1, Integer value2) {
            addCriterion("brand_id between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotBetween(Integer value1, Integer value2) {
            addCriterion("brand_id not between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andTreadIdIsNull() {
            addCriterion("tread_id is null");
            return (Criteria) this;
        }

        public Criteria andTreadIdIsNotNull() {
            addCriterion("tread_id is not null");
            return (Criteria) this;
        }

        public Criteria andTreadIdEqualTo(Integer value) {
            addCriterion("tread_id =", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdNotEqualTo(Integer value) {
            addCriterion("tread_id <>", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdGreaterThan(Integer value) {
            addCriterion("tread_id >", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tread_id >=", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdLessThan(Integer value) {
            addCriterion("tread_id <", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdLessThanOrEqualTo(Integer value) {
            addCriterion("tread_id <=", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdIn(List<Integer> values) {
            addCriterion("tread_id in", values, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdNotIn(List<Integer> values) {
            addCriterion("tread_id not in", values, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdBetween(Integer value1, Integer value2) {
            addCriterion("tread_id between", value1, value2, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tread_id not between", value1, value2, "treadId");
            return (Criteria) this;
        }

        public Criteria andAirPressIsNull() {
            addCriterion("air_press is null");
            return (Criteria) this;
        }

        public Criteria andAirPressIsNotNull() {
            addCriterion("air_press is not null");
            return (Criteria) this;
        }

        public Criteria andAirPressEqualTo(Double value) {
            addCriterion("air_press =", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressNotEqualTo(Double value) {
            addCriterion("air_press <>", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressGreaterThan(Double value) {
            addCriterion("air_press >", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressGreaterThanOrEqualTo(Double value) {
            addCriterion("air_press >=", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressLessThan(Double value) {
            addCriterion("air_press <", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressLessThanOrEqualTo(Double value) {
            addCriterion("air_press <=", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressIn(List<Double> values) {
            addCriterion("air_press in", values, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressNotIn(List<Double> values) {
            addCriterion("air_press not in", values, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressBetween(Double value1, Double value2) {
            addCriterion("air_press between", value1, value2, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressNotBetween(Double value1, Double value2) {
            addCriterion("air_press not between", value1, value2, "airPress");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1IsNull() {
            addCriterion("tread_deep1 is null");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1IsNotNull() {
            addCriterion("tread_deep1 is not null");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1EqualTo(Double value) {
            addCriterion("tread_deep1 =", value, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1NotEqualTo(Double value) {
            addCriterion("tread_deep1 <>", value, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1GreaterThan(Double value) {
            addCriterion("tread_deep1 >", value, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1GreaterThanOrEqualTo(Double value) {
            addCriterion("tread_deep1 >=", value, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1LessThan(Double value) {
            addCriterion("tread_deep1 <", value, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1LessThanOrEqualTo(Double value) {
            addCriterion("tread_deep1 <=", value, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1In(List<Double> values) {
            addCriterion("tread_deep1 in", values, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1NotIn(List<Double> values) {
            addCriterion("tread_deep1 not in", values, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1Between(Double value1, Double value2) {
            addCriterion("tread_deep1 between", value1, value2, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep1NotBetween(Double value1, Double value2) {
            addCriterion("tread_deep1 not between", value1, value2, "treadDeep1");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2IsNull() {
            addCriterion("tread_deep2 is null");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2IsNotNull() {
            addCriterion("tread_deep2 is not null");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2EqualTo(Double value) {
            addCriterion("tread_deep2 =", value, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2NotEqualTo(Double value) {
            addCriterion("tread_deep2 <>", value, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2GreaterThan(Double value) {
            addCriterion("tread_deep2 >", value, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2GreaterThanOrEqualTo(Double value) {
            addCriterion("tread_deep2 >=", value, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2LessThan(Double value) {
            addCriterion("tread_deep2 <", value, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2LessThanOrEqualTo(Double value) {
            addCriterion("tread_deep2 <=", value, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2In(List<Double> values) {
            addCriterion("tread_deep2 in", values, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2NotIn(List<Double> values) {
            addCriterion("tread_deep2 not in", values, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2Between(Double value1, Double value2) {
            addCriterion("tread_deep2 between", value1, value2, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep2NotBetween(Double value1, Double value2) {
            addCriterion("tread_deep2 not between", value1, value2, "treadDeep2");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3IsNull() {
            addCriterion("tread_deep3 is null");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3IsNotNull() {
            addCriterion("tread_deep3 is not null");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3EqualTo(Double value) {
            addCriterion("tread_deep3 =", value, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3NotEqualTo(Double value) {
            addCriterion("tread_deep3 <>", value, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3GreaterThan(Double value) {
            addCriterion("tread_deep3 >", value, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3GreaterThanOrEqualTo(Double value) {
            addCriterion("tread_deep3 >=", value, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3LessThan(Double value) {
            addCriterion("tread_deep3 <", value, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3LessThanOrEqualTo(Double value) {
            addCriterion("tread_deep3 <=", value, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3In(List<Double> values) {
            addCriterion("tread_deep3 in", values, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3NotIn(List<Double> values) {
            addCriterion("tread_deep3 not in", values, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3Between(Double value1, Double value2) {
            addCriterion("tread_deep3 between", value1, value2, "treadDeep3");
            return (Criteria) this;
        }

        public Criteria andTreadDeep3NotBetween(Double value1, Double value2) {
            addCriterion("tread_deep3 not between", value1, value2, "treadDeep3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}