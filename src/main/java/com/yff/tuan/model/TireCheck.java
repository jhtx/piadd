package com.yff.tuan.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.yff.tuan.util.CustomJsonDateDeserializer;

public class TireCheck {
    private Integer id;

    @NotNull(message="{产品编号不能为空}")
    private String productId;
    
    private Integer carId;
    
    private Integer tyreId;
    
    @NotNull(message="{轮胎编号不能为空}")
    private String tyreNo;
    
    @NotNull(message="{检测气压不能为空}")
    private Double pressure;
    
    @NotNull(message="{检测温度不能为空}")
    private Double temperature;
    
    @NotNull(message="{压力报警不能为空}")
    private Integer pressureWarning;
    
    @NotNull(message="{温度报警不能为空}")
    private Integer temperatureWarning;
    
    @NotNull(message="{漏气报警不能为空}")
    private Integer airWarning;
    
    @NotNull(message="{信号丢失报警不能为空}")
    private Integer signalWarning;
    
    @NotNull(message="{检测时间不能为空}")
    private Date checkTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getTyreId() {
        return tyreId;
    }

    public void setTyreId(Integer tyreId) {
        this.tyreId = tyreId;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Integer getPressureWarning() {
        return pressureWarning;
    }

    public void setPressureWarning(Integer pressureWarning) {
        this.pressureWarning = pressureWarning;
    }

    public Integer getTemperatureWarning() {
        return temperatureWarning;
    }

    public void setTemperatureWarning(Integer temperatureWarning) {
        this.temperatureWarning = temperatureWarning;
    }

    public Integer getAirWarning() {
        return airWarning;
    }

    public void setAirWarning(Integer airWarning) {
        this.airWarning = airWarning;
    }

    public Integer getSignalWarning() {
        return signalWarning;
    }

    public void setSignalWarning(Integer signalWarning) {
        this.signalWarning = signalWarning;
    }

    public Date getCheckTime() {
        return checkTime;
    }
    
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

	public Integer getCarId() {
		return carId;
	}

	public void setCarId(Integer carId) {
		this.carId = carId;
	}
    
}