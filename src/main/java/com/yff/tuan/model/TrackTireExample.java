package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.List;

public class TrackTireExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TrackTireExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTrackIdIsNull() {
            addCriterion("track_id is null");
            return (Criteria) this;
        }

        public Criteria andTrackIdIsNotNull() {
            addCriterion("track_id is not null");
            return (Criteria) this;
        }

        public Criteria andTrackIdEqualTo(Integer value) {
            addCriterion("track_id =", value, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdNotEqualTo(Integer value) {
            addCriterion("track_id <>", value, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdGreaterThan(Integer value) {
            addCriterion("track_id >", value, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("track_id >=", value, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdLessThan(Integer value) {
            addCriterion("track_id <", value, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdLessThanOrEqualTo(Integer value) {
            addCriterion("track_id <=", value, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdIn(List<Integer> values) {
            addCriterion("track_id in", values, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdNotIn(List<Integer> values) {
            addCriterion("track_id not in", values, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdBetween(Integer value1, Integer value2) {
            addCriterion("track_id between", value1, value2, "trackId");
            return (Criteria) this;
        }

        public Criteria andTrackIdNotBetween(Integer value1, Integer value2) {
            addCriterion("track_id not between", value1, value2, "trackId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdIsNull() {
            addCriterion("vehicle_type_tire_id is null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdIsNotNull() {
            addCriterion("vehicle_type_tire_id is not null");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id =", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdNotEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id <>", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdGreaterThan(Integer value) {
            addCriterion("vehicle_type_tire_id >", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id >=", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdLessThan(Integer value) {
            addCriterion("vehicle_type_tire_id <", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdLessThanOrEqualTo(Integer value) {
            addCriterion("vehicle_type_tire_id <=", value, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdIn(List<Integer> values) {
            addCriterion("vehicle_type_tire_id in", values, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdNotIn(List<Integer> values) {
            addCriterion("vehicle_type_tire_id not in", values, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdBetween(Integer value1, Integer value2) {
            addCriterion("vehicle_type_tire_id between", value1, value2, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andVehicleTypeTireIdNotBetween(Integer value1, Integer value2) {
            addCriterion("vehicle_type_tire_id not between", value1, value2, "vehicleTypeTireId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNull() {
            addCriterion("brand_id is null");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNotNull() {
            addCriterion("brand_id is not null");
            return (Criteria) this;
        }

        public Criteria andBrandIdEqualTo(Integer value) {
            addCriterion("brand_id =", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotEqualTo(Integer value) {
            addCriterion("brand_id <>", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThan(Integer value) {
            addCriterion("brand_id >", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("brand_id >=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThan(Integer value) {
            addCriterion("brand_id <", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThanOrEqualTo(Integer value) {
            addCriterion("brand_id <=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIn(List<Integer> values) {
            addCriterion("brand_id in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotIn(List<Integer> values) {
            addCriterion("brand_id not in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdBetween(Integer value1, Integer value2) {
            addCriterion("brand_id between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotBetween(Integer value1, Integer value2) {
            addCriterion("brand_id not between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIsNull() {
            addCriterion("tyre_size_id is null");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIsNotNull() {
            addCriterion("tyre_size_id is not null");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdEqualTo(Integer value) {
            addCriterion("tyre_size_id =", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotEqualTo(Integer value) {
            addCriterion("tyre_size_id <>", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdGreaterThan(Integer value) {
            addCriterion("tyre_size_id >", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tyre_size_id >=", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdLessThan(Integer value) {
            addCriterion("tyre_size_id <", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdLessThanOrEqualTo(Integer value) {
            addCriterion("tyre_size_id <=", value, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdIn(List<Integer> values) {
            addCriterion("tyre_size_id in", values, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotIn(List<Integer> values) {
            addCriterion("tyre_size_id not in", values, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdBetween(Integer value1, Integer value2) {
            addCriterion("tyre_size_id between", value1, value2, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTyreSizeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tyre_size_id not between", value1, value2, "tyreSizeId");
            return (Criteria) this;
        }

        public Criteria andTreadIdIsNull() {
            addCriterion("tread_id is null");
            return (Criteria) this;
        }

        public Criteria andTreadIdIsNotNull() {
            addCriterion("tread_id is not null");
            return (Criteria) this;
        }

        public Criteria andTreadIdEqualTo(Integer value) {
            addCriterion("tread_id =", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdNotEqualTo(Integer value) {
            addCriterion("tread_id <>", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdGreaterThan(Integer value) {
            addCriterion("tread_id >", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tread_id >=", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdLessThan(Integer value) {
            addCriterion("tread_id <", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdLessThanOrEqualTo(Integer value) {
            addCriterion("tread_id <=", value, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdIn(List<Integer> values) {
            addCriterion("tread_id in", values, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdNotIn(List<Integer> values) {
            addCriterion("tread_id not in", values, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdBetween(Integer value1, Integer value2) {
            addCriterion("tread_id between", value1, value2, "treadId");
            return (Criteria) this;
        }

        public Criteria andTreadIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tread_id not between", value1, value2, "treadId");
            return (Criteria) this;
        }

        public Criteria andLeftNoIsNull() {
            addCriterion("left_no is null");
            return (Criteria) this;
        }

        public Criteria andLeftNoIsNotNull() {
            addCriterion("left_no is not null");
            return (Criteria) this;
        }

        public Criteria andLeftNoEqualTo(String value) {
            addCriterion("left_no =", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoNotEqualTo(String value) {
            addCriterion("left_no <>", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoGreaterThan(String value) {
            addCriterion("left_no >", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoGreaterThanOrEqualTo(String value) {
            addCriterion("left_no >=", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoLessThan(String value) {
            addCriterion("left_no <", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoLessThanOrEqualTo(String value) {
            addCriterion("left_no <=", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoLike(String value) {
            addCriterion("left_no like", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoNotLike(String value) {
            addCriterion("left_no not like", value, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoIn(List<String> values) {
            addCriterion("left_no in", values, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoNotIn(List<String> values) {
            addCriterion("left_no not in", values, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoBetween(String value1, String value2) {
            addCriterion("left_no between", value1, value2, "leftNo");
            return (Criteria) this;
        }

        public Criteria andLeftNoNotBetween(String value1, String value2) {
            addCriterion("left_no not between", value1, value2, "leftNo");
            return (Criteria) this;
        }

        public Criteria andRightNoIsNull() {
            addCriterion("right_no is null");
            return (Criteria) this;
        }

        public Criteria andRightNoIsNotNull() {
            addCriterion("right_no is not null");
            return (Criteria) this;
        }

        public Criteria andRightNoEqualTo(String value) {
            addCriterion("right_no =", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoNotEqualTo(String value) {
            addCriterion("right_no <>", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoGreaterThan(String value) {
            addCriterion("right_no >", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoGreaterThanOrEqualTo(String value) {
            addCriterion("right_no >=", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoLessThan(String value) {
            addCriterion("right_no <", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoLessThanOrEqualTo(String value) {
            addCriterion("right_no <=", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoLike(String value) {
            addCriterion("right_no like", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoNotLike(String value) {
            addCriterion("right_no not like", value, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoIn(List<String> values) {
            addCriterion("right_no in", values, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoNotIn(List<String> values) {
            addCriterion("right_no not in", values, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoBetween(String value1, String value2) {
            addCriterion("right_no between", value1, value2, "rightNo");
            return (Criteria) this;
        }

        public Criteria andRightNoNotBetween(String value1, String value2) {
            addCriterion("right_no not between", value1, value2, "rightNo");
            return (Criteria) this;
        }

        public Criteria andAirPressIsNull() {
            addCriterion("air_press is null");
            return (Criteria) this;
        }

        public Criteria andAirPressIsNotNull() {
            addCriterion("air_press is not null");
            return (Criteria) this;
        }

        public Criteria andAirPressEqualTo(Double value) {
            addCriterion("air_press =", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressNotEqualTo(Double value) {
            addCriterion("air_press <>", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressGreaterThan(Double value) {
            addCriterion("air_press >", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressGreaterThanOrEqualTo(Double value) {
            addCriterion("air_press >=", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressLessThan(Double value) {
            addCriterion("air_press <", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressLessThanOrEqualTo(Double value) {
            addCriterion("air_press <=", value, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressIn(List<Double> values) {
            addCriterion("air_press in", values, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressNotIn(List<Double> values) {
            addCriterion("air_press not in", values, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressBetween(Double value1, Double value2) {
            addCriterion("air_press between", value1, value2, "airPress");
            return (Criteria) this;
        }

        public Criteria andAirPressNotBetween(Double value1, Double value2) {
            addCriterion("air_press not between", value1, value2, "airPress");
            return (Criteria) this;
        }

        public Criteria andTreadIsNull() {
            addCriterion("tread is null");
            return (Criteria) this;
        }

        public Criteria andTreadIsNotNull() {
            addCriterion("tread is not null");
            return (Criteria) this;
        }

        public Criteria andTreadEqualTo(Double value) {
            addCriterion("tread =", value, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadNotEqualTo(Double value) {
            addCriterion("tread <>", value, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadGreaterThan(Double value) {
            addCriterion("tread >", value, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadGreaterThanOrEqualTo(Double value) {
            addCriterion("tread >=", value, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadLessThan(Double value) {
            addCriterion("tread <", value, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadLessThanOrEqualTo(Double value) {
            addCriterion("tread <=", value, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadIn(List<Double> values) {
            addCriterion("tread in", values, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadNotIn(List<Double> values) {
            addCriterion("tread not in", values, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadBetween(Double value1, Double value2) {
            addCriterion("tread between", value1, value2, "tread");
            return (Criteria) this;
        }

        public Criteria andTreadNotBetween(Double value1, Double value2) {
            addCriterion("tread not between", value1, value2, "tread");
            return (Criteria) this;
        }

        public Criteria andMilesIsNull() {
            addCriterion("miles is null");
            return (Criteria) this;
        }

        public Criteria andMilesIsNotNull() {
            addCriterion("miles is not null");
            return (Criteria) this;
        }

        public Criteria andMilesEqualTo(Double value) {
            addCriterion("miles =", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotEqualTo(Double value) {
            addCriterion("miles <>", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesGreaterThan(Double value) {
            addCriterion("miles >", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesGreaterThanOrEqualTo(Double value) {
            addCriterion("miles >=", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesLessThan(Double value) {
            addCriterion("miles <", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesLessThanOrEqualTo(Double value) {
            addCriterion("miles <=", value, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesIn(List<Double> values) {
            addCriterion("miles in", values, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotIn(List<Double> values) {
            addCriterion("miles not in", values, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesBetween(Double value1, Double value2) {
            addCriterion("miles between", value1, value2, "miles");
            return (Criteria) this;
        }

        public Criteria andMilesNotBetween(Double value1, Double value2) {
            addCriterion("miles not between", value1, value2, "miles");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}