package com.yff.tuan.model;

public class Market {
    private Integer id;

    private String type;
    
    private String cla;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

	public String getCla() {
		return cla;
	}

	public void setCla(String cla) {
		this.cla = cla;
	}
    
    
}