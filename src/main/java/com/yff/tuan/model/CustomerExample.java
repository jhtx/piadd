package com.yff.tuan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CustomerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andFailureReasonIsNull() {
            addCriterion("failure_reason is null");
            return (Criteria) this;
        }

        public Criteria andFailureReasonIsNotNull() {
            addCriterion("failure_reason is not null");
            return (Criteria) this;
        }

        public Criteria andFailureReasonEqualTo(Integer value) {
            addCriterion("failure_reason =", value, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonNotEqualTo(Integer value) {
            addCriterion("failure_reason <>", value, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonGreaterThan(Integer value) {
            addCriterion("failure_reason >", value, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonGreaterThanOrEqualTo(Integer value) {
            addCriterion("failure_reason >=", value, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonLessThan(Integer value) {
            addCriterion("failure_reason <", value, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonLessThanOrEqualTo(Integer value) {
            addCriterion("failure_reason <=", value, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonIn(List<Integer> values) {
            addCriterion("failure_reason in", values, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonNotIn(List<Integer> values) {
            addCriterion("failure_reason not in", values, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonBetween(Integer value1, Integer value2) {
            addCriterion("failure_reason between", value1, value2, "failureReason");
            return (Criteria) this;
        }

        public Criteria andFailureReasonNotBetween(Integer value1, Integer value2) {
            addCriterion("failure_reason not between", value1, value2, "failureReason");
            return (Criteria) this;
        }

        public Criteria andSupplierIsNull() {
            addCriterion("supplier is null");
            return (Criteria) this;
        }

        public Criteria andSupplierIsNotNull() {
            addCriterion("supplier is not null");
            return (Criteria) this;
        }

        public Criteria andSupplierEqualTo(String value) {
            addCriterion("supplier =", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotEqualTo(String value) {
            addCriterion("supplier <>", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierGreaterThan(String value) {
            addCriterion("supplier >", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierGreaterThanOrEqualTo(String value) {
            addCriterion("supplier >=", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierLessThan(String value) {
            addCriterion("supplier <", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierLessThanOrEqualTo(String value) {
            addCriterion("supplier <=", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierLike(String value) {
            addCriterion("supplier like", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotLike(String value) {
            addCriterion("supplier not like", value, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierIn(List<String> values) {
            addCriterion("supplier in", values, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotIn(List<String> values) {
            addCriterion("supplier not in", values, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierBetween(String value1, String value2) {
            addCriterion("supplier between", value1, value2, "supplier");
            return (Criteria) this;
        }

        public Criteria andSupplierNotBetween(String value1, String value2) {
            addCriterion("supplier not between", value1, value2, "supplier");
            return (Criteria) this;
        }

        public Criteria andWholeCar42IsNull() {
            addCriterion("whole_car42 is null");
            return (Criteria) this;
        }

        public Criteria andWholeCar42IsNotNull() {
            addCriterion("whole_car42 is not null");
            return (Criteria) this;
        }

        public Criteria andWholeCar42EqualTo(Integer value) {
            addCriterion("whole_car42 =", value, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42NotEqualTo(Integer value) {
            addCriterion("whole_car42 <>", value, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42GreaterThan(Integer value) {
            addCriterion("whole_car42 >", value, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42GreaterThanOrEqualTo(Integer value) {
            addCriterion("whole_car42 >=", value, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42LessThan(Integer value) {
            addCriterion("whole_car42 <", value, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42LessThanOrEqualTo(Integer value) {
            addCriterion("whole_car42 <=", value, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42In(List<Integer> values) {
            addCriterion("whole_car42 in", values, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42NotIn(List<Integer> values) {
            addCriterion("whole_car42 not in", values, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42Between(Integer value1, Integer value2) {
            addCriterion("whole_car42 between", value1, value2, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar42NotBetween(Integer value1, Integer value2) {
            addCriterion("whole_car42 not between", value1, value2, "wholeCar42");
            return (Criteria) this;
        }

        public Criteria andWholeCar62IsNull() {
            addCriterion("whole_car62 is null");
            return (Criteria) this;
        }

        public Criteria andWholeCar62IsNotNull() {
            addCriterion("whole_car62 is not null");
            return (Criteria) this;
        }

        public Criteria andWholeCar62EqualTo(Integer value) {
            addCriterion("whole_car62 =", value, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62NotEqualTo(Integer value) {
            addCriterion("whole_car62 <>", value, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62GreaterThan(Integer value) {
            addCriterion("whole_car62 >", value, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62GreaterThanOrEqualTo(Integer value) {
            addCriterion("whole_car62 >=", value, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62LessThan(Integer value) {
            addCriterion("whole_car62 <", value, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62LessThanOrEqualTo(Integer value) {
            addCriterion("whole_car62 <=", value, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62In(List<Integer> values) {
            addCriterion("whole_car62 in", values, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62NotIn(List<Integer> values) {
            addCriterion("whole_car62 not in", values, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62Between(Integer value1, Integer value2) {
            addCriterion("whole_car62 between", value1, value2, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar62NotBetween(Integer value1, Integer value2) {
            addCriterion("whole_car62 not between", value1, value2, "wholeCar62");
            return (Criteria) this;
        }

        public Criteria andWholeCar84IsNull() {
            addCriterion("whole_car84 is null");
            return (Criteria) this;
        }

        public Criteria andWholeCar84IsNotNull() {
            addCriterion("whole_car84 is not null");
            return (Criteria) this;
        }

        public Criteria andWholeCar84EqualTo(Integer value) {
            addCriterion("whole_car84 =", value, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84NotEqualTo(Integer value) {
            addCriterion("whole_car84 <>", value, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84GreaterThan(Integer value) {
            addCriterion("whole_car84 >", value, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84GreaterThanOrEqualTo(Integer value) {
            addCriterion("whole_car84 >=", value, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84LessThan(Integer value) {
            addCriterion("whole_car84 <", value, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84LessThanOrEqualTo(Integer value) {
            addCriterion("whole_car84 <=", value, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84In(List<Integer> values) {
            addCriterion("whole_car84 in", values, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84NotIn(List<Integer> values) {
            addCriterion("whole_car84 not in", values, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84Between(Integer value1, Integer value2) {
            addCriterion("whole_car84 between", value1, value2, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andWholeCar84NotBetween(Integer value1, Integer value2) {
            addCriterion("whole_car84 not between", value1, value2, "wholeCar84");
            return (Criteria) this;
        }

        public Criteria andDrawCar42IsNull() {
            addCriterion("draw_car42 is null");
            return (Criteria) this;
        }

        public Criteria andDrawCar42IsNotNull() {
            addCriterion("draw_car42 is not null");
            return (Criteria) this;
        }

        public Criteria andDrawCar42EqualTo(Integer value) {
            addCriterion("draw_car42 =", value, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42NotEqualTo(Integer value) {
            addCriterion("draw_car42 <>", value, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42GreaterThan(Integer value) {
            addCriterion("draw_car42 >", value, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42GreaterThanOrEqualTo(Integer value) {
            addCriterion("draw_car42 >=", value, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42LessThan(Integer value) {
            addCriterion("draw_car42 <", value, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42LessThanOrEqualTo(Integer value) {
            addCriterion("draw_car42 <=", value, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42In(List<Integer> values) {
            addCriterion("draw_car42 in", values, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42NotIn(List<Integer> values) {
            addCriterion("draw_car42 not in", values, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42Between(Integer value1, Integer value2) {
            addCriterion("draw_car42 between", value1, value2, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar42NotBetween(Integer value1, Integer value2) {
            addCriterion("draw_car42 not between", value1, value2, "drawCar42");
            return (Criteria) this;
        }

        public Criteria andDrawCar62IsNull() {
            addCriterion("draw_car62 is null");
            return (Criteria) this;
        }

        public Criteria andDrawCar62IsNotNull() {
            addCriterion("draw_car62 is not null");
            return (Criteria) this;
        }

        public Criteria andDrawCar62EqualTo(Integer value) {
            addCriterion("draw_car62 =", value, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62NotEqualTo(Integer value) {
            addCriterion("draw_car62 <>", value, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62GreaterThan(Integer value) {
            addCriterion("draw_car62 >", value, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62GreaterThanOrEqualTo(Integer value) {
            addCriterion("draw_car62 >=", value, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62LessThan(Integer value) {
            addCriterion("draw_car62 <", value, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62LessThanOrEqualTo(Integer value) {
            addCriterion("draw_car62 <=", value, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62In(List<Integer> values) {
            addCriterion("draw_car62 in", values, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62NotIn(List<Integer> values) {
            addCriterion("draw_car62 not in", values, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62Between(Integer value1, Integer value2) {
            addCriterion("draw_car62 between", value1, value2, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar62NotBetween(Integer value1, Integer value2) {
            addCriterion("draw_car62 not between", value1, value2, "drawCar62");
            return (Criteria) this;
        }

        public Criteria andDrawCar64IsNull() {
            addCriterion("draw_car64 is null");
            return (Criteria) this;
        }

        public Criteria andDrawCar64IsNotNull() {
            addCriterion("draw_car64 is not null");
            return (Criteria) this;
        }

        public Criteria andDrawCar64EqualTo(Integer value) {
            addCriterion("draw_car64 =", value, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64NotEqualTo(Integer value) {
            addCriterion("draw_car64 <>", value, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64GreaterThan(Integer value) {
            addCriterion("draw_car64 >", value, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64GreaterThanOrEqualTo(Integer value) {
            addCriterion("draw_car64 >=", value, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64LessThan(Integer value) {
            addCriterion("draw_car64 <", value, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64LessThanOrEqualTo(Integer value) {
            addCriterion("draw_car64 <=", value, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64In(List<Integer> values) {
            addCriterion("draw_car64 in", values, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64NotIn(List<Integer> values) {
            addCriterion("draw_car64 not in", values, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64Between(Integer value1, Integer value2) {
            addCriterion("draw_car64 between", value1, value2, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDrawCar64NotBetween(Integer value1, Integer value2) {
            addCriterion("draw_car64 not between", value1, value2, "drawCar64");
            return (Criteria) this;
        }

        public Criteria andDragCar40IsNull() {
            addCriterion("drag_car40 is null");
            return (Criteria) this;
        }

        public Criteria andDragCar40IsNotNull() {
            addCriterion("drag_car40 is not null");
            return (Criteria) this;
        }

        public Criteria andDragCar40EqualTo(Integer value) {
            addCriterion("drag_car40 =", value, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40NotEqualTo(Integer value) {
            addCriterion("drag_car40 <>", value, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40GreaterThan(Integer value) {
            addCriterion("drag_car40 >", value, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40GreaterThanOrEqualTo(Integer value) {
            addCriterion("drag_car40 >=", value, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40LessThan(Integer value) {
            addCriterion("drag_car40 <", value, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40LessThanOrEqualTo(Integer value) {
            addCriterion("drag_car40 <=", value, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40In(List<Integer> values) {
            addCriterion("drag_car40 in", values, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40NotIn(List<Integer> values) {
            addCriterion("drag_car40 not in", values, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40Between(Integer value1, Integer value2) {
            addCriterion("drag_car40 between", value1, value2, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40NotBetween(Integer value1, Integer value2) {
            addCriterion("drag_car40 not between", value1, value2, "dragCar40");
            return (Criteria) this;
        }

        public Criteria andDragCar40DIsNull() {
            addCriterion("drag_car40_d is null");
            return (Criteria) this;
        }

        public Criteria andDragCar40DIsNotNull() {
            addCriterion("drag_car40_d is not null");
            return (Criteria) this;
        }

        public Criteria andDragCar40DEqualTo(Integer value) {
            addCriterion("drag_car40_d =", value, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DNotEqualTo(Integer value) {
            addCriterion("drag_car40_d <>", value, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DGreaterThan(Integer value) {
            addCriterion("drag_car40_d >", value, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DGreaterThanOrEqualTo(Integer value) {
            addCriterion("drag_car40_d >=", value, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DLessThan(Integer value) {
            addCriterion("drag_car40_d <", value, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DLessThanOrEqualTo(Integer value) {
            addCriterion("drag_car40_d <=", value, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DIn(List<Integer> values) {
            addCriterion("drag_car40_d in", values, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DNotIn(List<Integer> values) {
            addCriterion("drag_car40_d not in", values, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DBetween(Integer value1, Integer value2) {
            addCriterion("drag_car40_d between", value1, value2, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar40DNotBetween(Integer value1, Integer value2) {
            addCriterion("drag_car40_d not between", value1, value2, "dragCar40D");
            return (Criteria) this;
        }

        public Criteria andDragCar60IsNull() {
            addCriterion("drag_car60 is null");
            return (Criteria) this;
        }

        public Criteria andDragCar60IsNotNull() {
            addCriterion("drag_car60 is not null");
            return (Criteria) this;
        }

        public Criteria andDragCar60EqualTo(Integer value) {
            addCriterion("drag_car60 =", value, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60NotEqualTo(Integer value) {
            addCriterion("drag_car60 <>", value, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60GreaterThan(Integer value) {
            addCriterion("drag_car60 >", value, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60GreaterThanOrEqualTo(Integer value) {
            addCriterion("drag_car60 >=", value, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60LessThan(Integer value) {
            addCriterion("drag_car60 <", value, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60LessThanOrEqualTo(Integer value) {
            addCriterion("drag_car60 <=", value, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60In(List<Integer> values) {
            addCriterion("drag_car60 in", values, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60NotIn(List<Integer> values) {
            addCriterion("drag_car60 not in", values, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60Between(Integer value1, Integer value2) {
            addCriterion("drag_car60 between", value1, value2, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60NotBetween(Integer value1, Integer value2) {
            addCriterion("drag_car60 not between", value1, value2, "dragCar60");
            return (Criteria) this;
        }

        public Criteria andDragCar60DIsNull() {
            addCriterion("drag_car60_d is null");
            return (Criteria) this;
        }

        public Criteria andDragCar60DIsNotNull() {
            addCriterion("drag_car60_d is not null");
            return (Criteria) this;
        }

        public Criteria andDragCar60DEqualTo(Integer value) {
            addCriterion("drag_car60_d =", value, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DNotEqualTo(Integer value) {
            addCriterion("drag_car60_d <>", value, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DGreaterThan(Integer value) {
            addCriterion("drag_car60_d >", value, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DGreaterThanOrEqualTo(Integer value) {
            addCriterion("drag_car60_d >=", value, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DLessThan(Integer value) {
            addCriterion("drag_car60_d <", value, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DLessThanOrEqualTo(Integer value) {
            addCriterion("drag_car60_d <=", value, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DIn(List<Integer> values) {
            addCriterion("drag_car60_d in", values, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DNotIn(List<Integer> values) {
            addCriterion("drag_car60_d not in", values, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DBetween(Integer value1, Integer value2) {
            addCriterion("drag_car60_d between", value1, value2, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andDragCar60DNotBetween(Integer value1, Integer value2) {
            addCriterion("drag_car60_d not between", value1, value2, "dragCar60D");
            return (Criteria) this;
        }

        public Criteria andOldNewRateIsNull() {
            addCriterion("old_new_rate is null");
            return (Criteria) this;
        }

        public Criteria andOldNewRateIsNotNull() {
            addCriterion("old_new_rate is not null");
            return (Criteria) this;
        }

        public Criteria andOldNewRateEqualTo(Double value) {
            addCriterion("old_new_rate =", value, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateNotEqualTo(Double value) {
            addCriterion("old_new_rate <>", value, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateGreaterThan(Double value) {
            addCriterion("old_new_rate >", value, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateGreaterThanOrEqualTo(Double value) {
            addCriterion("old_new_rate >=", value, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateLessThan(Double value) {
            addCriterion("old_new_rate <", value, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateLessThanOrEqualTo(Double value) {
            addCriterion("old_new_rate <=", value, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateIn(List<Double> values) {
            addCriterion("old_new_rate in", values, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateNotIn(List<Double> values) {
            addCriterion("old_new_rate not in", values, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateBetween(Double value1, Double value2) {
            addCriterion("old_new_rate between", value1, value2, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andOldNewRateNotBetween(Double value1, Double value2) {
            addCriterion("old_new_rate not between", value1, value2, "oldNewRate");
            return (Criteria) this;
        }

        public Criteria andFrontMilesIsNull() {
            addCriterion("front_miles is null");
            return (Criteria) this;
        }

        public Criteria andFrontMilesIsNotNull() {
            addCriterion("front_miles is not null");
            return (Criteria) this;
        }

        public Criteria andFrontMilesEqualTo(Double value) {
            addCriterion("front_miles =", value, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesNotEqualTo(Double value) {
            addCriterion("front_miles <>", value, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesGreaterThan(Double value) {
            addCriterion("front_miles >", value, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesGreaterThanOrEqualTo(Double value) {
            addCriterion("front_miles >=", value, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesLessThan(Double value) {
            addCriterion("front_miles <", value, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesLessThanOrEqualTo(Double value) {
            addCriterion("front_miles <=", value, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesIn(List<Double> values) {
            addCriterion("front_miles in", values, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesNotIn(List<Double> values) {
            addCriterion("front_miles not in", values, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesBetween(Double value1, Double value2) {
            addCriterion("front_miles between", value1, value2, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andFrontMilesNotBetween(Double value1, Double value2) {
            addCriterion("front_miles not between", value1, value2, "frontMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesIsNull() {
            addCriterion("back_miles is null");
            return (Criteria) this;
        }

        public Criteria andBackMilesIsNotNull() {
            addCriterion("back_miles is not null");
            return (Criteria) this;
        }

        public Criteria andBackMilesEqualTo(Integer value) {
            addCriterion("back_miles =", value, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesNotEqualTo(Integer value) {
            addCriterion("back_miles <>", value, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesGreaterThan(Integer value) {
            addCriterion("back_miles >", value, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesGreaterThanOrEqualTo(Integer value) {
            addCriterion("back_miles >=", value, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesLessThan(Integer value) {
            addCriterion("back_miles <", value, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesLessThanOrEqualTo(Integer value) {
            addCriterion("back_miles <=", value, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesIn(List<Integer> values) {
            addCriterion("back_miles in", values, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesNotIn(List<Integer> values) {
            addCriterion("back_miles not in", values, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesBetween(Integer value1, Integer value2) {
            addCriterion("back_miles between", value1, value2, "backMiles");
            return (Criteria) this;
        }

        public Criteria andBackMilesNotBetween(Integer value1, Integer value2) {
            addCriterion("back_miles not between", value1, value2, "backMiles");
            return (Criteria) this;
        }

        public Criteria andFrontPriceIsNull() {
            addCriterion("front_price is null");
            return (Criteria) this;
        }

        public Criteria andFrontPriceIsNotNull() {
            addCriterion("front_price is not null");
            return (Criteria) this;
        }

        public Criteria andFrontPriceEqualTo(Long value) {
            addCriterion("front_price =", value, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNotEqualTo(Long value) {
            addCriterion("front_price <>", value, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceGreaterThan(Long value) {
            addCriterion("front_price >", value, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceGreaterThanOrEqualTo(Long value) {
            addCriterion("front_price >=", value, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceLessThan(Long value) {
            addCriterion("front_price <", value, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceLessThanOrEqualTo(Long value) {
            addCriterion("front_price <=", value, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceIn(List<Long> values) {
            addCriterion("front_price in", values, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNotIn(List<Long> values) {
            addCriterion("front_price not in", values, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceBetween(Long value1, Long value2) {
            addCriterion("front_price between", value1, value2, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNotBetween(Long value1, Long value2) {
            addCriterion("front_price not between", value1, value2, "frontPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceIsNull() {
            addCriterion("back_price is null");
            return (Criteria) this;
        }

        public Criteria andBackPriceIsNotNull() {
            addCriterion("back_price is not null");
            return (Criteria) this;
        }

        public Criteria andBackPriceEqualTo(Long value) {
            addCriterion("back_price =", value, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceNotEqualTo(Long value) {
            addCriterion("back_price <>", value, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceGreaterThan(Long value) {
            addCriterion("back_price >", value, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceGreaterThanOrEqualTo(Long value) {
            addCriterion("back_price >=", value, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceLessThan(Long value) {
            addCriterion("back_price <", value, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceLessThanOrEqualTo(Long value) {
            addCriterion("back_price <=", value, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceIn(List<Long> values) {
            addCriterion("back_price in", values, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceNotIn(List<Long> values) {
            addCriterion("back_price not in", values, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceBetween(Long value1, Long value2) {
            addCriterion("back_price between", value1, value2, "backPrice");
            return (Criteria) this;
        }

        public Criteria andBackPriceNotBetween(Long value1, Long value2) {
            addCriterion("back_price not between", value1, value2, "backPrice");
            return (Criteria) this;
        }

        public Criteria andFrontTreadIsNull() {
            addCriterion("front_tread is null");
            return (Criteria) this;
        }

        public Criteria andFrontTreadIsNotNull() {
            addCriterion("front_tread is not null");
            return (Criteria) this;
        }

        public Criteria andFrontTreadEqualTo(String value) {
            addCriterion("front_tread =", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadNotEqualTo(String value) {
            addCriterion("front_tread <>", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadGreaterThan(String value) {
            addCriterion("front_tread >", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadGreaterThanOrEqualTo(String value) {
            addCriterion("front_tread >=", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadLessThan(String value) {
            addCriterion("front_tread <", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadLessThanOrEqualTo(String value) {
            addCriterion("front_tread <=", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadLike(String value) {
            addCriterion("front_tread like", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadNotLike(String value) {
            addCriterion("front_tread not like", value, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadIn(List<String> values) {
            addCriterion("front_tread in", values, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadNotIn(List<String> values) {
            addCriterion("front_tread not in", values, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadBetween(String value1, String value2) {
            addCriterion("front_tread between", value1, value2, "frontTread");
            return (Criteria) this;
        }

        public Criteria andFrontTreadNotBetween(String value1, String value2) {
            addCriterion("front_tread not between", value1, value2, "frontTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadIsNull() {
            addCriterion("back_tread is null");
            return (Criteria) this;
        }

        public Criteria andBackTreadIsNotNull() {
            addCriterion("back_tread is not null");
            return (Criteria) this;
        }

        public Criteria andBackTreadEqualTo(String value) {
            addCriterion("back_tread =", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadNotEqualTo(String value) {
            addCriterion("back_tread <>", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadGreaterThan(String value) {
            addCriterion("back_tread >", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadGreaterThanOrEqualTo(String value) {
            addCriterion("back_tread >=", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadLessThan(String value) {
            addCriterion("back_tread <", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadLessThanOrEqualTo(String value) {
            addCriterion("back_tread <=", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadLike(String value) {
            addCriterion("back_tread like", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadNotLike(String value) {
            addCriterion("back_tread not like", value, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadIn(List<String> values) {
            addCriterion("back_tread in", values, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadNotIn(List<String> values) {
            addCriterion("back_tread not in", values, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadBetween(String value1, String value2) {
            addCriterion("back_tread between", value1, value2, "backTread");
            return (Criteria) this;
        }

        public Criteria andBackTreadNotBetween(String value1, String value2) {
            addCriterion("back_tread not between", value1, value2, "backTread");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtIsNull() {
            addCriterion("day_change_amt is null");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtIsNotNull() {
            addCriterion("day_change_amt is not null");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtEqualTo(String value) {
            addCriterion("day_change_amt =", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtNotEqualTo(String value) {
            addCriterion("day_change_amt <>", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtGreaterThan(String value) {
            addCriterion("day_change_amt >", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtGreaterThanOrEqualTo(String value) {
            addCriterion("day_change_amt >=", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtLessThan(String value) {
            addCriterion("day_change_amt <", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtLessThanOrEqualTo(String value) {
            addCriterion("day_change_amt <=", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtLike(String value) {
            addCriterion("day_change_amt like", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtNotLike(String value) {
            addCriterion("day_change_amt not like", value, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtIn(List<String> values) {
            addCriterion("day_change_amt in", values, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtNotIn(List<String> values) {
            addCriterion("day_change_amt not in", values, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtBetween(String value1, String value2) {
            addCriterion("day_change_amt between", value1, value2, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayChangeAmtNotBetween(String value1, String value2) {
            addCriterion("day_change_amt not between", value1, value2, "dayChangeAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtIsNull() {
            addCriterion("day_drop_amt is null");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtIsNotNull() {
            addCriterion("day_drop_amt is not null");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtEqualTo(String value) {
            addCriterion("day_drop_amt =", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtNotEqualTo(String value) {
            addCriterion("day_drop_amt <>", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtGreaterThan(String value) {
            addCriterion("day_drop_amt >", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtGreaterThanOrEqualTo(String value) {
            addCriterion("day_drop_amt >=", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtLessThan(String value) {
            addCriterion("day_drop_amt <", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtLessThanOrEqualTo(String value) {
            addCriterion("day_drop_amt <=", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtLike(String value) {
            addCriterion("day_drop_amt like", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtNotLike(String value) {
            addCriterion("day_drop_amt not like", value, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtIn(List<String> values) {
            addCriterion("day_drop_amt in", values, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtNotIn(List<String> values) {
            addCriterion("day_drop_amt not in", values, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtBetween(String value1, String value2) {
            addCriterion("day_drop_amt between", value1, value2, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andDayDropAmtNotBetween(String value1, String value2) {
            addCriterion("day_drop_amt not between", value1, value2, "dayDropAmt");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeIsNull() {
            addCriterion("month_avg_fee is null");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeIsNotNull() {
            addCriterion("month_avg_fee is not null");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeEqualTo(String value) {
            addCriterion("month_avg_fee =", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeNotEqualTo(String value) {
            addCriterion("month_avg_fee <>", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeGreaterThan(String value) {
            addCriterion("month_avg_fee >", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeGreaterThanOrEqualTo(String value) {
            addCriterion("month_avg_fee >=", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeLessThan(String value) {
            addCriterion("month_avg_fee <", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeLessThanOrEqualTo(String value) {
            addCriterion("month_avg_fee <=", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeLike(String value) {
            addCriterion("month_avg_fee like", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeNotLike(String value) {
            addCriterion("month_avg_fee not like", value, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeIn(List<String> values) {
            addCriterion("month_avg_fee in", values, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeNotIn(List<String> values) {
            addCriterion("month_avg_fee not in", values, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeBetween(String value1, String value2) {
            addCriterion("month_avg_fee between", value1, value2, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andMonthAvgFeeNotBetween(String value1, String value2) {
            addCriterion("month_avg_fee not between", value1, value2, "monthAvgFee");
            return (Criteria) this;
        }

        public Criteria andAvgMilesIsNull() {
            addCriterion("avg_miles is null");
            return (Criteria) this;
        }

        public Criteria andAvgMilesIsNotNull() {
            addCriterion("avg_miles is not null");
            return (Criteria) this;
        }

        public Criteria andAvgMilesEqualTo(String value) {
            addCriterion("avg_miles =", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesNotEqualTo(String value) {
            addCriterion("avg_miles <>", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesGreaterThan(String value) {
            addCriterion("avg_miles >", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesGreaterThanOrEqualTo(String value) {
            addCriterion("avg_miles >=", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesLessThan(String value) {
            addCriterion("avg_miles <", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesLessThanOrEqualTo(String value) {
            addCriterion("avg_miles <=", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesLike(String value) {
            addCriterion("avg_miles like", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesNotLike(String value) {
            addCriterion("avg_miles not like", value, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesIn(List<String> values) {
            addCriterion("avg_miles in", values, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesNotIn(List<String> values) {
            addCriterion("avg_miles not in", values, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesBetween(String value1, String value2) {
            addCriterion("avg_miles between", value1, value2, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgMilesNotBetween(String value1, String value2) {
            addCriterion("avg_miles not between", value1, value2, "avgMiles");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeIsNull() {
            addCriterion("avg_oil_fee is null");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeIsNotNull() {
            addCriterion("avg_oil_fee is not null");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeEqualTo(String value) {
            addCriterion("avg_oil_fee =", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeNotEqualTo(String value) {
            addCriterion("avg_oil_fee <>", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeGreaterThan(String value) {
            addCriterion("avg_oil_fee >", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeGreaterThanOrEqualTo(String value) {
            addCriterion("avg_oil_fee >=", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeLessThan(String value) {
            addCriterion("avg_oil_fee <", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeLessThanOrEqualTo(String value) {
            addCriterion("avg_oil_fee <=", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeLike(String value) {
            addCriterion("avg_oil_fee like", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeNotLike(String value) {
            addCriterion("avg_oil_fee not like", value, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeIn(List<String> values) {
            addCriterion("avg_oil_fee in", values, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeNotIn(List<String> values) {
            addCriterion("avg_oil_fee not in", values, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeBetween(String value1, String value2) {
            addCriterion("avg_oil_fee between", value1, value2, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andAvgOilFeeNotBetween(String value1, String value2) {
            addCriterion("avg_oil_fee not between", value1, value2, "avgOilFee");
            return (Criteria) this;
        }

        public Criteria andLikeBrandIsNull() {
            addCriterion("like_brand is null");
            return (Criteria) this;
        }

        public Criteria andLikeBrandIsNotNull() {
            addCriterion("like_brand is not null");
            return (Criteria) this;
        }

        public Criteria andLikeBrandEqualTo(String value) {
            addCriterion("like_brand =", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandNotEqualTo(String value) {
            addCriterion("like_brand <>", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandGreaterThan(String value) {
            addCriterion("like_brand >", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandGreaterThanOrEqualTo(String value) {
            addCriterion("like_brand >=", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandLessThan(String value) {
            addCriterion("like_brand <", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandLessThanOrEqualTo(String value) {
            addCriterion("like_brand <=", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandLike(String value) {
            addCriterion("like_brand like", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandNotLike(String value) {
            addCriterion("like_brand not like", value, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandIn(List<String> values) {
            addCriterion("like_brand in", values, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandNotIn(List<String> values) {
            addCriterion("like_brand not in", values, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandBetween(String value1, String value2) {
            addCriterion("like_brand between", value1, value2, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andLikeBrandNotBetween(String value1, String value2) {
            addCriterion("like_brand not between", value1, value2, "likeBrand");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchIsNull() {
            addCriterion("double_match is null");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchIsNotNull() {
            addCriterion("double_match is not null");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchEqualTo(Integer value) {
            addCriterion("double_match =", value, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchNotEqualTo(Integer value) {
            addCriterion("double_match <>", value, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchGreaterThan(Integer value) {
            addCriterion("double_match >", value, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchGreaterThanOrEqualTo(Integer value) {
            addCriterion("double_match >=", value, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchLessThan(Integer value) {
            addCriterion("double_match <", value, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchLessThanOrEqualTo(Integer value) {
            addCriterion("double_match <=", value, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchIn(List<Integer> values) {
            addCriterion("double_match in", values, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchNotIn(List<Integer> values) {
            addCriterion("double_match not in", values, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchBetween(Integer value1, Integer value2) {
            addCriterion("double_match between", value1, value2, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andDoubleMatchNotBetween(Integer value1, Integer value2) {
            addCriterion("double_match not between", value1, value2, "doubleMatch");
            return (Criteria) this;
        }

        public Criteria andHasTestIsNull() {
            addCriterion("has_test is null");
            return (Criteria) this;
        }

        public Criteria andHasTestIsNotNull() {
            addCriterion("has_test is not null");
            return (Criteria) this;
        }

        public Criteria andHasTestEqualTo(Integer value) {
            addCriterion("has_test =", value, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestNotEqualTo(Integer value) {
            addCriterion("has_test <>", value, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestGreaterThan(Integer value) {
            addCriterion("has_test >", value, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestGreaterThanOrEqualTo(Integer value) {
            addCriterion("has_test >=", value, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestLessThan(Integer value) {
            addCriterion("has_test <", value, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestLessThanOrEqualTo(Integer value) {
            addCriterion("has_test <=", value, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestIn(List<Integer> values) {
            addCriterion("has_test in", values, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestNotIn(List<Integer> values) {
            addCriterion("has_test not in", values, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestBetween(Integer value1, Integer value2) {
            addCriterion("has_test between", value1, value2, "hasTest");
            return (Criteria) this;
        }

        public Criteria andHasTestNotBetween(Integer value1, Integer value2) {
            addCriterion("has_test not between", value1, value2, "hasTest");
            return (Criteria) this;
        }

        public Criteria andCheckRateIsNull() {
            addCriterion("check_rate is null");
            return (Criteria) this;
        }

        public Criteria andCheckRateIsNotNull() {
            addCriterion("check_rate is not null");
            return (Criteria) this;
        }

        public Criteria andCheckRateEqualTo(String value) {
            addCriterion("check_rate =", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateNotEqualTo(String value) {
            addCriterion("check_rate <>", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateGreaterThan(String value) {
            addCriterion("check_rate >", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateGreaterThanOrEqualTo(String value) {
            addCriterion("check_rate >=", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateLessThan(String value) {
            addCriterion("check_rate <", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateLessThanOrEqualTo(String value) {
            addCriterion("check_rate <=", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateLike(String value) {
            addCriterion("check_rate like", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateNotLike(String value) {
            addCriterion("check_rate not like", value, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateIn(List<String> values) {
            addCriterion("check_rate in", values, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateNotIn(List<String> values) {
            addCriterion("check_rate not in", values, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateBetween(String value1, String value2) {
            addCriterion("check_rate between", value1, value2, "checkRate");
            return (Criteria) this;
        }

        public Criteria andCheckRateNotBetween(String value1, String value2) {
            addCriterion("check_rate not between", value1, value2, "checkRate");
            return (Criteria) this;
        }

        public Criteria andPressStandardIsNull() {
            addCriterion("press_standard is null");
            return (Criteria) this;
        }

        public Criteria andPressStandardIsNotNull() {
            addCriterion("press_standard is not null");
            return (Criteria) this;
        }

        public Criteria andPressStandardEqualTo(String value) {
            addCriterion("press_standard =", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardNotEqualTo(String value) {
            addCriterion("press_standard <>", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardGreaterThan(String value) {
            addCriterion("press_standard >", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardGreaterThanOrEqualTo(String value) {
            addCriterion("press_standard >=", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardLessThan(String value) {
            addCriterion("press_standard <", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardLessThanOrEqualTo(String value) {
            addCriterion("press_standard <=", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardLike(String value) {
            addCriterion("press_standard like", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardNotLike(String value) {
            addCriterion("press_standard not like", value, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardIn(List<String> values) {
            addCriterion("press_standard in", values, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardNotIn(List<String> values) {
            addCriterion("press_standard not in", values, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardBetween(String value1, String value2) {
            addCriterion("press_standard between", value1, value2, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andPressStandardNotBetween(String value1, String value2) {
            addCriterion("press_standard not between", value1, value2, "pressStandard");
            return (Criteria) this;
        }

        public Criteria andFleetAmtIsNull() {
            addCriterion("fleet_amt is null");
            return (Criteria) this;
        }

        public Criteria andFleetAmtIsNotNull() {
            addCriterion("fleet_amt is not null");
            return (Criteria) this;
        }

        public Criteria andFleetAmtEqualTo(String value) {
            addCriterion("fleet_amt =", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtNotEqualTo(String value) {
            addCriterion("fleet_amt <>", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtGreaterThan(String value) {
            addCriterion("fleet_amt >", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtGreaterThanOrEqualTo(String value) {
            addCriterion("fleet_amt >=", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtLessThan(String value) {
            addCriterion("fleet_amt <", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtLessThanOrEqualTo(String value) {
            addCriterion("fleet_amt <=", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtLike(String value) {
            addCriterion("fleet_amt like", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtNotLike(String value) {
            addCriterion("fleet_amt not like", value, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtIn(List<String> values) {
            addCriterion("fleet_amt in", values, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtNotIn(List<String> values) {
            addCriterion("fleet_amt not in", values, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtBetween(String value1, String value2) {
            addCriterion("fleet_amt between", value1, value2, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andFleetAmtNotBetween(String value1, String value2) {
            addCriterion("fleet_amt not between", value1, value2, "fleetAmt");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}