package com.yff.tuan.model;

public class Problem {
    private Integer id;

    private String name;
    
    private String cla;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getCla() {
		return cla;
	}

	public void setCla(String cla) {
		this.cla = cla;
	}
    
    
}