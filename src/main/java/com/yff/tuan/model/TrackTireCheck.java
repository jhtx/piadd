package com.yff.tuan.model;

import java.util.Date;

public class TrackTireCheck {
    private Integer id;

    private Integer tireId;

    private Double miles;

    private Double press;

    private Double tread;

    private Double driveMiles;

    private Double expectMiles;

    private Date checkDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTireId() {
        return tireId;
    }

    public void setTireId(Integer tireId) {
        this.tireId = tireId;
    }

    public Double getMiles() {
        return miles;
    }

    public void setMiles(Double miles) {
        this.miles = miles;
    }

    public Double getPress() {
        return press;
    }

    public void setPress(Double press) {
        this.press = press;
    }

    public Double getTread() {
        return tread;
    }

    public void setTread(Double tread) {
        this.tread = tread;
    }

    public Double getDriveMiles() {
        return driveMiles;
    }

    public void setDriveMiles(Double driveMiles) {
        this.driveMiles = driveMiles;
    }

    public Double getExpectMiles() {
        return expectMiles;
    }

    public void setExpectMiles(Double expectMiles) {
        this.expectMiles = expectMiles;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }
}