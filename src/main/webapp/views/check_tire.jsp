<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-multiselect.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<%-- <link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css"> --%>
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">轮位列表</h3>
									<% if(role<4){ %>
									<c:if test="${empty checkVehicleTires}">
										<h3 class="panel-title" style="width: 50%;float: left;text-align: right;">
											<button type="button" id="sub" class="btn btn-primary" >添加</button>
										</h3>
									</c:if>
									<% } %>
								</div>
								<div class="panel-body">
									<form action="" method="post">
										<table class="table table-bordered" id="tab">
											<thead>
												<tr>
													<th>轮位号</th>
													<th>轮胎类型</th>
													<th>翻新类型</th>
													<th>花纹类型</th>
													<th>轮胎品牌</th>
													<th>轮胎规格</th>
													<th>预警标识</th>
													<th>气压</th>
													<th colspan="3">剩余花纹</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="cvt" items="${checkVehicleTires}">
													<tr>
														<td>${cvt.vehicleTypeTireName }
															<input type="hidden" id="vehicleTypeId" value="${cvt.vehicleTypeTireId }">
															<input type="hidden" id="typeTiresJson" value="${typeTiresJson }">
														</td>
														<td>
															<c:if test="${cvt.rX==0 }">R</c:if>
															<c:if test="${cvt.rX==1 }">X</c:if>
														</td>
														<td>
															<c:if test="${cvt.retreadType==0 }">N</c:if>
															<c:if test="${cvt.retreadType==1 }">F</c:if>
														</td>
														<td>
															<c:if test="${cvt.treadType==0 }">H</c:if>
															<c:if test="${cvt.treadType==1 }">Z</c:if>
															<c:if test="${cvt.treadType==2 }">M</c:if>
															<c:if test="${cvt.treadType==3 }">K</c:if>
														</td>
														<td>${cvt.brandName }</td>
														<td>${cvt.tyreSizeName }</td>
														<td>
															<c:forEach var="vttw" items="${cvt.vehicleTypeTireWarns }">
																<c:if test="${vttw.warnType==0 }"><span style="float: left;">W</span></c:if>
																<c:if test="${vttw.warnType==1 }"><span style="float: left;">T</span></c:if>
																<c:if test="${vttw.warnType==2 }">
																<span style="font-size: 20px;float: left; height: 10px; margin-top: -4px;">△</span></c:if>
																<c:if test="${vttw.warnType==3 }"><span style="float: left;">O</span></c:if>
																<c:if test="${vttw.warnType==4 }"><span style="float: left;">X</span></c:if>
															</c:forEach>
														</td>
														<td>${cvt.airPress }</td>
														<td>${cvt.treadDeep1 }</td>
														<td>${cvt.treadDeep2 }</td>
														<td>${cvt.treadDeep3 }</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</form>
								</div>
								<div class="modal-footer" style="text-align: center;">
									<input type="hidden" id="id" value="${id }">
									<button type="button" class="btn btn-primary" id="cancel" onclick="javascript:window.history.back();">返回</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
	 $(document).ready(function() {
	       $('#sbt').hide();
	       $("#sub").click(function () {
		    			 window.location.href='<%=basePath %>check/toAddTire?id='+$("#id").val();
		   });
	 }); 
	 /* $(function () { $('#myModal').modal({
			keyboard: true
	 })}); */
	</script>
</body>
</html>