<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">破损分析</h3>
									<% if(role<4){ %>
									<h3 class="panel-title" style="width: 50%;float: left;text-align: right;">
										<button type="button" id="sub" class="btn btn-primary" data-toggle="modal" data-target="#myModal">添加</button>
									</h3>
									<%} %>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>客户名称</th>
												<th>检测日期</th>
												<th>检测人</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="b" items="${page.list }" varStatus="status">
												<tr>
													<td>${b.customerName}</td>
													<td><fmt:formatDate value="${b.checkDate}" pattern="yyyy-MM-dd" /></td>
													<td>${b.userName}</td>
													<td>
														<a href="<%=basePath %>broken/brokenChecks?id=${b.id}">分析</a>
														<% if(role==1 || role==4){ %>
														<a href="<%=basePath %>broken/exportBroken?id=${b.id}">生成报告</a>
														<%} %>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12 col-md-offset-4">
								<input type="hidden" id="totalCount" value="${page.totalRecord }">
								<input type="hidden" id="pageNum" value="${page.pageNum }">
								<input type="hidden" id="pageSize" value="${page.pageSize }">
								<input type="hidden" id="totalPage" value="${page.totalPage }">
								<ul id='element'></ul>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" 
									aria-hidden="true">×
							</button>
							<h4 class="modal-title" id="myModalLabel">添加客户</h4>
						</div>
						<div class="modal-body">
							<form action="" method="post" id="form01">
								<table class="table table-bordered">
										<tbody>
											<tr>
												<td>客户名称<span style="color: red">*</span></td>
												<td>
													<select name = "customerId" id="customerId">
														<c:forEach var="c" items="${customers }">
															<option value="${c.id }">${c.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>检查人<span style="color: red">*</span></td>
												<td>
													${userName }<input type="hidden" name="userId" id="userId" value="${userId }">
												</td>
											</tr>
										</tbody>
								</table>
							</form>
							<h6 class="modal-title" id="msg" style="color: red"></h6>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"  data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary" id="sbut">保存</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div>
			<!-- end 模态框（Modal） -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
	$("#sbut").click(
	    	 function () {
	    			 if($('#customerId').val()==""){
	    				 $("#msg").html("请添加检查人");
	    				 return;
	    			 }
	    			 var obj = {customerId:$('#customerId').val(),userId:$('#userId').val()};
	    			 console.log(obj);
	    			 $.ajax({
	 				    url:'<%=basePath %>broken/add',
	 				    type:'POST', //GET
	 				    async:true,    //或false,是否异步
	 				    contentType:"application/json",
	 				    data: JSON.stringify(obj), //$("#form01").serializeArray()
	 				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	 				    success:function(data){
	 				        if(data.add==true){
	 				        	alert("添加成功！");
	 				        	window.location.href=window.location.href;
	 				        }else{
	 				        	 $("#msg").html(data.resMsg);
	 				        }
	 				    },
	 				    error:function(xhr){
	 				    	console.log(xhr);
	 				    },
	 				});
		   		 }
		    );
		$(function(){
	        var element = $('#element');
	        var options = {
	            bootstrapMajorVersion:3, 
	            currentPage: $('#pageNum' ).val(), 
	            numberOfPages: $('#pageSize').val(), //每页页数
	            totalPages: $('#totalPage').val(), 
	            itemTexts: function (type, page, current) {
	                switch (type) {
	                    case "first":
	                        return "首页";
	                    case "prev":
	                        return "上一页";
	                    case "next":
	                        return "下一页";
	                    case "last":
	                        return "末页";
	                    case "page":
	                        return page;
	                }
	            },
	            onPageClicked: function (event, originalEvent, type, page) {
            	    var pageNum=page;
            	    var total = $('#totalPage').val();
	            	if(type=="first") pageNum = 1;
	            	if(type=="prev") pageNum = page==1?page:page-1;
	            	if(type=="next") pageNum = total == page ? total : page+1;
	            	if(type=="last") pageNum = total;
	            	if(type=="page") pageNum = page;
	                location.href = "<%=basePath %>broken/list?pageNum=" + pageNum;
	            }
	        };
	        element.bootstrapPaginator(options);
		});
	</script>
	</script>
</body>
</html>