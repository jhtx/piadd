<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">破损分析</h3>
								</div>
								<div class="panel-body">
									<form action="" method="post" id="form01">
										<table class="table table-bordered">
												<tbody>
													<tr>
														<td>车辆品牌</td>
														<td class="tdbold">
															<select name="tyreSizeId" id="tyreSizeId" class="form-control">
																<c:forEach items="${tyreSizes }" var="t">
																	<option value="${t.id }">${t.name }</option>
																</c:forEach>
															</select>
															<input name="brokenId" id="brokenId" type="hidden" value="${id }">
														</td>
														<td>轮胎胎号</td>
														<td class="tdbold">
															<input name="tireNo" id="tireNo" class="form-control">
														</td>
													</tr>
												</tbody>
											</table>
									</form>
									<h6 class="modal-title" id="msg" style="color: red"></h6>
									<div class="modal-footer" style="text-align: center;">
										<button type="button" class="btn btn-default"  data-dismiss="modal" onclick="javascript:history.back();">返回</button>
										<button type="button" class="btn btn-primary" id="sbut">保存</button>
									</div>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- end 模态框（Modal） -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#rX0").click(function(){
			$(this).attr("checked",true);
			$("#rX1").attr("checked",false); 
			$("#rX").val("0");
		});
		$("#rX1").click(function(){
			$(this).attr("checked",true);
			$("#rX0").attr("checked",false);
			$("#rX").val("1");
		});
		$("#primaryRetread0").click(function(){
			$(this).attr("checked",true);
			$("#primaryRetread1").attr("checked",false); 
			$("#primaryRetread").val("0");
		});
		$("#primaryRetread1").click(function(){
			$(this).attr("checked",true);
			$("#primaryRetread0").attr("checked",false);
			$("#primaryRetread").val("1");
		});
		
		$("#sbut").click(
		    	 function () {
		    			 if($('#tyreSizeId').val()==""){
		    				 $("#msg").html("请选择轮胎规格");
		    				 return;
		    			 }
		    			 if($('#tireNo').val()==""){
		    				 $("#msg").html("请输入轮胎胎号");
		    				 return;
		    			 }
		    			 if($('#brandId').val()==""){
		    				 $("#msg").html("请选择轮胎品牌");
		    				 return;
		    			 }
		    			 if($('#tppTreadId').val()==""){
		    				 $("#msg").html("请选择卸胎花纹");
		    				 return;
		    			 }
		    			 if($('#primaryRetread').val()==""){
		    				 $("#msg").html("请选择原始/翻新轮胎");
		    				 return;
		    			 }
		    			 var reg=/^[-\+]?\d+(\.\d+)?$/;
		    			 if($('#retreadCount').val()!= null && !reg.test($('#retreadCount').val())){
		    				 $("#msg").html("请输入导向轮气压标准");
		    				 return;
		    			 }
		    			 if($('#primaryTreadId').val()==""){
		    				 $("#msg").html("请选择原始胎面花纹");
		    				 return;
		    			 }
		    			 if($('#primaryTreadDeep').val()==""){
		    				 $("#msg").html("请输入原始花纹深度");
		    				 return;
		    			 }
		    			 if($('#rX').val()==""){
		    				 $("#msg").html("请选择R/X");
		    				 return;
		    			 }
		    			 if($('#remainTreadDeep1').val()=="" || $('#remainTreadDeep2').val()=="" || $('#remainTreadDeep3').val()==""){
		    				 $("#msg").html("请输入剩余花纹深度");
		    				 return;
		    			 }
		    			 $.ajax({
		 				    url:'<%=basePath %>broken/addCheck',
		 				    type:'POST', //GET
		 				    async:true,    //或false,是否异步
		 				    data: $("#form01").serializeArray(),
		 				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		 				    success:function(data){
		 				        if(data.add==true){
		 				        	alert("添加成功！");
		 				        	window.location.href='<%=basePath %>broken/brokenChecks?id='+$('#brokenId').val();
		 				        }else{
		 				        	 $("#msg").html(data.resMsg);
		 				        }
		 				    },
		 				    error:function(xhr){},
		 				});
			   		 }
			    );
	});
	</script>
</body>
</html>