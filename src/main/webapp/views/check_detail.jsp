<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-multiselect.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">轮胎检查</h3>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th style="text-align: center;">客户名称</th>
												<th style="text-align: center;">检测日期</th>
												<th style="text-align: center;">检测人</th>
												<th colspan="3" style="text-align: center;">气压标准</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>${check.customerName}</td>
												<td><fmt:formatDate value="${check.date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
												<td>${check.checkerName}</td>
												<td>导向轮：${check.guideAirStandard}</td>
												<td>驱动轮：${check.driveAirStandard}</td>
												<td>托车轮：${check.dragAirStandard}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END INPUTS -->
						</div>
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">车辆列表</h3>
									<% if(role<4){ %>
									<h3 class="panel-title" style="text-align: right;">
										<!-- <a href="javascript:void(0);" id="add"> --><!-- </a> -->
										<button type="button" id="sub" class="btn btn-primary" data-toggle="modal" data-target="#myModal">添加车辆</button>
									</h3>
									<%} %>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>车牌号</th>
												<th>车辆类型</th>
												<th>检测</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="cv" items="${check.checkVehicles}">
												<tr>
													<td>${cv.vehicleNo }</td>
													<td>${cv.vehicleTypeName }</td>
													<td><a href="<%=basePath %>check/tire?id=${cv.id}&vehicleTypeId=${cv.vehicleTypeId }">详情</a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="modal-footer" style="text-align: center;">
									<button type="button" class="btn btn-primary" id="cancel" onclick="javascript:window.history.back();">返回</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" 
									aria-hidden="true">×
							</button>
							<h4 class="modal-title" id="myModalLabel">添加车辆</h4>
						</div>
						<div class="modal-body">
							<form action="" method="post" id="form01">
								<table class="table table-bordered">
										<tbody>
											<tr>
												<td>车牌号</td>
												<td>
													<input type="text" name="vehicleNo" id="vehicleNo">
													<input type="hidden" name="checkId" id="checkId" value="${check.id}">
												</td>
											</tr>
											<tr>
												<td>车辆类型</td>
												<td>
													<select name = "vehicleTypeId" id="vehicleTypeId">
														<c:forEach var="vt" items="${vehicleTypes }">
															<option value="${vt.id }">${vt.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
										</tbody>
								</table>
							</form>
							<h6 class="modal-title" id="msg" style="color: red"></h6>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"  data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary" id="sbut">保存</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div>
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
	 $(document).ready(function() {
		 $("#sub").click(
	    	 function () {
	    			 $('#myModal').modal({
	    				keyboard: true
	    		 })
    		 }
         );
		 $("#sbut").click(
	    	 function () {
	    			 if($('#vehicleNo').val()==""){
	    				 $("#msg").html("请添加车牌号");
	    				 return;
	    			 }
	    			 $.ajax({
	 				    url:'<%=basePath %>check/addCheckVehicle',
	 				    type:'POST', //GET
	 				    async:true,    //或false,是否异步
	 				    data: $("#form01").serializeArray(),
	 				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	 				    success:function(data){
	 				        if(data.resCode=="0"){
	 				        	alert("添加成功！");
	 				        	window.location.href=window.location.href;
	 				        }else{
	 				        	 $("#msg").html(data.resMsg);
	 				        }
	 				    },
	 				    error:function(xhr){},
	 				});
    		 }
         );
	 });
	</script>
</body>
</html>