<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top" style="background-image:url(<%=basePath %>assets/img/banner.jpg);background-repeat: no-repeat;background-size: 100%; ">
	<div class="brand bg"></div>
	<div style="position:relative;top:-100%;height:100%;margin:0 20px;">
		<a href="<%=basePath %>index.html" style="">
			<img style="max-width: 30px;float: left" src="<%=basePath %>assets/img/logo.png" alt="派迦科技" class="img-responsive logo">
			<div style="width: 100px;height:30px;float: left;padding-top: 5px;">派迦科技</div>
		</a>
	</div>
	<div class="container-fluid">
		<div class="navbar-btn">
			<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
		</div>
		<%-- <div id="navbar-menu">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span><%=request.getSession().getAttribute("userName") %> 您好! &nbsp;&nbsp;&nbsp;</span>
						<span id="logout" onclick='javascript:window.location.href="<%=basePath %>/logout.html"'>退出登录</span>
					</a>
				</li>
			</ul>
		</div> --%>
	</div>
</nav>
<script>
</script>
<!-- END NAVBAR -->