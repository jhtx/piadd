<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png"> 
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar2.jsp"/>
		<!-- left -->
		<%-- <jsp:include page="left.jsp"/> --%>
		<!-- MAIN -->
		<div class="main" style="width: 100%">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">轮胎检测</h3>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>序号</th>
												<th>产品编号</th>
												<th>纬度</th>
												<th>经度</th>
												<th>检测温度</th>
												<th>车挂匹配</th>
												<th>速度</th>
												<th>尾灯电流电压</th>
												<th>剩余电量</th>
												<th>检测时间</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="tc" items="${page.list }" varStatus="status">
												<tr>
													<td>${status.index+1}</td>
													<td>${tc.productId}</td>
													<td>${tc.lat}</td>
													<td>${tc.lng}</td>
													<td><div id="allmap${status.index+1}" style="width:200px;height:200px;"></div>
														<input type="hidden" id="lat${status.index+1}" value="${tc.lat }">
														<input type="hidden" id="lng${status.index+1}" value="${tc.lng }">
													</td>
													<td>${tc.matchProductId}</td>
													<td>${tc.speed}</td>
													<td>(${tc.taillight})</td>
													<td>${tc.charge}</td>
													<td><fmt:formatDate value="${tc.checkTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12 col-md-offset-4">
								<input type="hidden" id="ln" value="${fn:length(page.list) }">
								<input type="hidden" id="totalCount" value="${page.totalRecord }">
								<input type="hidden" id="pageNum" value="${page.pageNum }">
								<input type="hidden" id="pageSize" value="${page.pageSize }">
								<input type="hidden" id="totalPage" value="${page.totalPage }">
								<ul id='element'></ul>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/getscript?v=2.0&ak=N392KHKB3hPkqGYElL3OYoHIzoqtZkHj"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
		$(function(){
	        var element = $('#element');
	        var options = {
	            bootstrapMajorVersion:3, 
	            currentPage: $('#pageNum' ).val(), 
	            numberOfPages: $('#pageSize').val(), //每页页数
	            totalPages: $('#totalPage').val(), 
	            itemTexts: function (type, page, current) {
	                switch (type) {
	                    case "first":
	                        return "首页";
	                    case "prev":
	                        return "上一页";
	                    case "next":
	                        return "下一页";
	                    case "last":
	                        return "末页";
	                    case "page":
	                        return page;
	                }
	            },
	            onPageClicked: function (event, originalEvent, type, page) {
            	    var pageNum=page;
            	    var total = $('#totalPage').val();
	            	if(type=="first") pageNum = 1;
	            	if(type=="prev") pageNum = page==1?page:page-1;
	            	if(type=="next") pageNum = total == page ? total : page+1;
	            	if(type=="last") pageNum = total;
	            	if(type=="page") pageNum = page;
	                location.href = "<%=basePath %>vehicledata/query?pageNum=" + pageNum;
	            }
	        };
	        element.bootstrapPaginator(options);
		});
		function theLocation(lng,lat){
			map.clearOverlays(); 
			var new_point = new BMap.Point(lng,lat);
			var marker = new BMap.Marker(new_point);  // 创建标注
			map.addOverlay(marker);              // 将标注添加到地图中
			map.panTo(new_point);      
		};
		var ln = $('#ln' ).val();
		if(ln > 0){
			for(var i=0;i<ln;i++){
				var ind = i+1;	
				var  lng = $('#lng'+ind ).val();
				var  lat = $('#lat'+ind ).val();
				var map = new BMap.Map("allmap"+ind);//创建百度地图实例，这里的allmap是地图容器的id  
				map.centerAndZoom(new BMap.Point(lng,lat),12);
				map.enableScrollWheelZoom(true);
				theLocation(lng,lat);
				// 用经纬度设置地图中心点				
			}
		};
</script>
</body>
</html>