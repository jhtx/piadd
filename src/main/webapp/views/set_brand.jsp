<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">车辆品牌</h3>
									<h3 class="panel-title" style="width: 50%;float: left;text-align: right;">
										<button type="button" id="add" class="btn btn-primary" >添加</button>
									</h3>
								</div>
								<div class="panel-body">
									<form id="form01" class="form-horizontal" method="POST">
						            	<table class="table table-striped table-bordered">
						            		<thead>
							                  <tr>
							                    <th>编号</th>
							                    <th>品牌</th>
							                    <th class="td-actions">操作</th>
							                  </tr>
							                </thead>
							                <tbody>
							                	<c:forEach items="${brands }" var="b" varStatus="status">
								                	<tr>
									                    <td>${status.index+1 }</td>
									                    <td class="td-actions">${b.name }<input type="hidden" name="id" id="id" value="${b.id }"></td>
									                    <td><a href="javascript:void(0);" onclick="update(${b.id });"  class="btn btn-danger btn-small">修改</a></td>
								                  	</tr>
							                  	</c:forEach>
							                </tbody>
						                </table>
						               	</form>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- end 模态框（Modal） -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
	$("#add").click(function(){
		location.href="<%=path %>/setting/brand/toadd.html"
	});
	function update(id){
		$("#id").val(id);
		$("#form01").attr('action',"<%=path %>/set/brand/toadd.html");
		$("#form01").submit();
	}
	</script>
</body>
</html>