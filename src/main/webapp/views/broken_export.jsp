<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/fileinput.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/theme.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
    
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	<link rel="stylesheet" href="<%=basePath %>css/common.css">
	<link rel="stylesheet" href="<%=basePath %>css/index.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">导出破损分析报告</h3>
								</div>
								<div class="panel-body">
									<form action="<%=path %>/broken/export" enctype="multipart/form-data" method="post" id="form01">
										<input type="hidden" name="id" value="${id }">
										<div class="form-group col-md-12">
												<div class="img-box full">
													<section class="img-section">
														<p class="up-p">1.请上传2张轮胎使用问题图片</p>
														<div class="z_photo upimg-div clear" >
																 <section class="z_file fl">
																	<img src="<%=basePath %>img/a11.png" class="add-img">
																	<input type="file" name="file1" id="file1" class="file" value="" accept="image/jpg,image/jpeg,image/png" multiple />
																 </section>
														 </div>
													 </section>
												</div>
												<aside class="mask works-mask">
													<div class="mask-content">
														<p class="del-p ">您确定要删除作品图片吗？</p>
														<p class="check-p"><span class="del-com wsdel-ok">确定</span><span class="wsdel-no">取消</span></p>
													</div>
												</aside>
												
								        </div>
								        <div class="form-group col-md-12">
								        		<div class="img-box full" style="margin-top:20px;">
													<div class="img-section" >
														<textarea cols="80%" rows="5" name="comment1" id="comment1"  placeholder="请添加轮胎使用问题"></textarea>
													</div>
												</div>
										</div>
										<div class="form-group col-md-12">
												<div class="img-box full">
													<section class="img-section">
														<p class="up-p">2.请上传2轮胎过度磨损图片</p>
														<div class="z_photo upimg-div clear" >
																 <section class="z_file fl">
																	<img src="<%=basePath %>img/a11.png" class="add-img">
																	<input type="file" name="file2" id="file2" class="file" value="" accept="image/jpg,image/jpeg,image/png" multiple />
																 </section>
														 </div>
													 </section>
												</div>
												<aside class="mask works-mask">
													<div class="mask-content">
														<p class="del-p ">您确定要删除作品图片吗？</p>
														<p class="check-p"><span class="del-com wsdel-ok">确定</span><span class="wsdel-no">取消</span></p>
													</div>
												</aside>
								        </div>
								        <div class="form-group col-md-12">
								        		<div class="img-box full" style="margin-top:20px;">
													<div class="img-section" >
														<textarea cols="80%" rows="5" name="comment2" id="comment2"  placeholder="请添加轮胎过度磨损问题"></textarea>
													</div>
												</div>
										</div>
								        <div class="form-group col-md-12">
												<div class="img-box full">
													<section class="img-section">
														<p class="up-p">3.请上传2张轮胎意外刺伤、划伤问题图片</p>
														<div class="z_photo upimg-div clear" >
																 <section class="z_file fl">
																	<img src="<%=basePath %>img/a11.png" class="add-img">
																	<input type="file" name="file3" id="file3" class="file" value="" accept="image/jpg,image/jpeg,image/png" multiple />
																 </section>
														 </div>
													 </section>
												</div>
												<aside class="mask works-mask">
													<div class="mask-content">
														<p class="del-p ">您确定要删除作品图片吗？</p>
														<p class="check-p"><span class="del-com wsdel-ok">确定</span><span class="wsdel-no">取消</span></p>
													</div>
												</aside>
								        </div>
								        <div class="form-group col-md-12">
								        		<div class="img-box full" style="margin-top:20px;">
													<div class="img-section" >
														<textarea cols="80%" rows="5" name="comment3" id="comment3"  placeholder="请添加轮胎意外刺伤、划伤问题"></textarea>
													</div>
												</div>
										</div>
								        <div class="form-group col-md-12">
												<div class="img-box full">
													<section class="img-section">
														<p class="up-p">4.请上传2张轮胎爆胎图片</p>
														<div class="z_photo upimg-div clear" >
																 <section class="z_file fl">
																	<img src="<%=basePath %>img/a11.png" class="add-img">
																	<input type="file" name="file4" id="file4" class="file" value="" accept="image/jpg,image/jpeg,image/png" multiple />
																 </section>
														 </div>
													 </section>
												</div>
												<aside class="mask works-mask">
													<div class="mask-content">
														<p class="del-p ">您确定要删除作品图片吗？</p>
														<p class="check-p"><span class="del-com wsdel-ok">确定</span><span class="wsdel-no">取消</span></p>
													</div>
												</aside>
								        </div>
								        <div class="form-group col-md-12">
								        		<div class="img-box full" style="margin-top:20px;">
													<div class="img-section" >
														<textarea cols="80%" rows="5" name="comment4" id="comment4"  placeholder="请添加轮胎爆胎问题"></textarea>
													</div>
												</div>
										</div>
									</form>
									<div class="modal-footer" style="text-align: center;">
										<button type="button" class="btn btn-default"  data-dismiss="modal">返回</button>
										<button type="button" class="btn btn-primary" id="sbut" >保存</button>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-md-offset-4">
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/imgUp.js"></script>
	<script type="text/javascript">
		$('#comment1').val("");
		$('#comment2').val("");
		$('#comment3').val("");
		$('#comment4').val("");
		$("#sbut").click(function(){
			$("#form01").submit();
		});
	</script>
	</script>
</body>
</html>