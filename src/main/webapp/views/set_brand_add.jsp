<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">车辆品牌</h3>
								</div>
								<div class="panel-body">
									<form id="form01" class="form-horizontal" method="POST">
								<br>
								<div class="control-group">											
									<label class="control-label" for="no">品牌<span style="color: red">*</span></label>
									<div class="controls">
										<input type="text" class="span6" id="brandName" name="brandName" value="${brand.name }">
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">		
									<label class="control-label" for="no">型号<span style="color: red">*</span></label>
									<div class="controls">
										<c:if test="${!empty brand.treads }">
											<c:forEach items="${brand.treads }" var="m">
												<span style="width: 100%">
													<input type="text" style="float: left" class="span6" id="mname" name="mname" value="${m.name }">
													<input type="hidden" style="float: left" class="span6" id="mid" name="mid" value="${m.name }">
												</span>
											</c:forEach>
										</c:if>
										<c:if test="${empty brand.treads }">
											<span style="width: 100%">
												<input type="text" style="float: left" class="span6" id="mname" name="mname" value="">
												<input type="hidden" style="float: left" class="span6" id="mid" name="mid" value="">
											</span>
										</c:if>
										<span style="width: 100%">
											<button type="button" class="btn btn-primary icon-plus-sign span12" style="float: left;" id="addInput">添加</button>
										</span>
									</div> <!-- /controls -->				
								</div> <!-- /control-group -->
								<div class="control-group">	
									${msg }										
								</div> 
								<div class="form-actions" style="text-align: center;">
									<input type="hidden" id="id" name="id" value="${brand.id }">
									<button type="button"  id="sub" class="btn btn-primary">提交</button> 
									<button type="button" class="btn" onclick="location.href='<%=path%>/set/brand.html'">取消</button>
								</div> <!-- /form-actions -->
						</form>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- end 模态框（Modal） -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
var minheight = $(window).height();
$("#body").css({
  'min-height':minheight +'px'
})
$("#addInput").click(function(){
	$(this).parent().before('<span style="width: 100%"><input type="text" style="float: left" class="span6" id="mname" name="mname" value=""><input type="hidden" style="float: left" class="span6" id="mid" name="mid" value=""></span>');
});

$("#sub").click(function(){
	var id = $("#id").val();
	var brandName = $("#brandName").val();
	if(brandName==""){
		alert("请输入品牌");
		return;
	}
	var mname = $("input[name='mname']");
	var mid = $("input[name='mid']");
	var mAdd = [];
	var mUpdate = [];
	for(var i=0;i<mname.length;i++){
		console.log($(mid[i]));
		console.log($(mid[i]).val());
		if($(mid[i]).val()!=""){
			var obj = {brandId:id,name:$(mname[i]).val()};
			mUpdate.push(obj);
		}else{
			if($(mname[i]).val()!=""){
				var obj = {name:$(mname[i]).val()};
				mAdd.push(obj);
			}
		}
		
	}
    var json = {id:id,name:brandName,mUpdate:mUpdate,mAdd:mAdd};	
	console.log(JSON.stringify(json));
	$.ajax({
	   url:'<%=path%>/set/brand/add',
	   type:'POST', //GET
	   async:true,    //或false,是否异步
	   contentType:"application/json",
	   data:JSON.stringify(json),
	   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	   success:function(data){
	       console.log(data);
	       if(data.add==true){
	    	   if(id==""){
	    		   alert("添加成功！");
	    	   }else{
	    		   alert("修改成功！");
	    	   }
	       	window.location.href="<%=path%>/set/brand.html";
		   }
		},
		error : function(xhr) {
			console.log('错误');
			console.log(xhr);
		},
	});
})
</script>
</body>
</html>