<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">破损分析</h3>
									<% if(role<3) {%>
									<h3 class="panel-title" style="width: 50%;float: left;text-align: right;">
										<button type="button" id="sub" class="btn btn-primary" >添加</button>
									</h3>
									<% } %>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>轮胎规格</th>
												<th>轮胎胎号</th>
												<th>胎体牌号</th>
												<th>卸胎花纹深度</th>
												<th>原始/翻新轮胎</th>
												<th>检查时间</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="b" items="${brokenChecks }" varStatus="status">
												<tr>
													<td>${b.tyreSizeName}</td>
													<td>${b.tireNo}</td>
													<td>${b.brandName}</td>
													<td>${b.tppTread}</td>
													<td>
														<c:if test="${b.primaryRetread==0}">原始轮胎</c:if> 
														<c:if test="${b.primaryRetread==1}">翻新轮胎</c:if>
													</td>
													<td>
														<fmt:formatDate value="${b.updateTime}" pattern="yyyy-MM-dd HH:mm:ss" />
													</td>
													<td>
														<a href="<%=basePath %>broken/brokenCheckDetail?id=${b.id}">详情</a>
													</td>
												</tr>
											</c:forEach>
											<input type="hidden" name="id" id="id" value="${id }">
										</tbody>
									</table>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
							<div class="modal-footer" style="text-align: center;">
									<button type="button" class="btn btn-primary" id="cancel" onclick="javascript:window.history.back();">返回</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#sub").click(
		    	 function () {
		    			 window.location.href='<%=basePath %>broken/toAddCheck?id='+$('#id').val();
			   		 }
			    );
	});
	</script>
</body>
</html>