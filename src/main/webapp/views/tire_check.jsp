<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar2.jsp"/>
		<!-- left -->
		<%-- <jsp:include page="left.jsp"/> --%>
		<!-- MAIN -->
		<div class="main" style="width: 100%">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">轮胎检测</h3>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>序号</th>
												<th>产品编号</th>
												<th>轮胎编号</th>
												<th>检测气压</th>
												<th>检测温度</th>
												<th>压力报警</th>
												<th>温度报警</th>
												<th>漏气报警</th>
												<th>信号丢失报警</th>
												<th>检测时间</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="tc" items="${page.list }" varStatus="status">
												<tr>
													<td>${status.index+1}</td>
													<td>${tc.productId}</td>
													<td>${tc.tyreNo}</td>
													<td>${tc.pressure}</td>
													<td>${tc.temperature}</td>
													<td>(${tc.pressureWarning}):
														<c:if test="${tc.pressureWarning==0}">正常</c:if>
														<c:if test="${tc.pressureWarning==1}">低压1级</c:if>
														<c:if test="${tc.pressureWarning==2}">低压2级</c:if>
														<c:if test="${tc.pressureWarning==3}">低压3级</c:if>
														<c:if test="${tc.pressureWarning==4}">高压</c:if>
													</td>
													<td>(${tc.temperatureWarning}):
														<c:if test="${tc.temperatureWarning==0}">正常</c:if>
														<c:if test="${tc.temperatureWarning==1}">高温</c:if>
													</td>
													<td>(${tc.airWarning}):
														<c:if test="${tc.airWarning==0}">正常</c:if>
														<c:if test="${tc.airWarning==1}">漏气</c:if>
													</td>
													<td>(${tc.signalWarning}):
														<c:if test="${tc.signalWarning==0}">正常</c:if>
														<c:if test="${tc.signalWarning==1}">信号丢失</c:if>
													</td>
													<td><fmt:formatDate value="${tc.checkTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12 col-md-offset-4">
								<input type="hidden" id="totalCount" value="${page.totalRecord }">
								<input type="hidden" id="pageNum" value="${page.pageNum }">
								<input type="hidden" id="pageSize" value="${page.pageSize }">
								<input type="hidden" id="totalPage" value="${page.totalPage }">
								<ul id='element'></ul>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
		$(function(){
	        var element = $('#element');
	        var options = {
	            bootstrapMajorVersion:3, 
	            currentPage: $('#pageNum' ).val(), 
	            numberOfPages: $('#pageSize').val(), //每页页数
	            totalPages: $('#totalPage').val(), 
	            itemTexts: function (type, page, current) {
	                switch (type) {
	                    case "first":
	                        return "首页";
	                    case "prev":
	                        return "上一页";
	                    case "next":
	                        return "下一页";
	                    case "last":
	                        return "末页";
	                    case "page":
	                        return page;
	                }
	            },
	            onPageClicked: function (event, originalEvent, type, page) {
            	    var pageNum=page;
            	    var total = $('#totalPage').val();
	            	if(type=="first") pageNum = 1;
	            	if(type=="prev") pageNum = page==1?page:page-1;
	            	if(type=="next") pageNum = total == page ? total : page+1;
	            	if(type=="last") pageNum = total;
	            	if(type=="page") pageNum = page;
	                location.href = "<%=basePath %>checkdata/query?pageNum=" + pageNum;
	            }
	        };
	        element.bootstrapPaginator(options);
		});
	</script>
	</script>
</body>
</html>