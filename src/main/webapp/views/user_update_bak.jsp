<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- <h3 class="page-title">用户管理</h3> -->
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">用户管理-修改用户</h3>
									<h3 class="panel-title" style="width: 50%;float: left;text-align: right;">
										<button type="button" id="sub" class="btn btn-primary" data-toggle="modal" data-target="#myModal">添加</button>
									</h3>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<tbody>
											<tr>
												<td>名称</td>
												<td><input type="text" name="name" id="name" value="${u.name}"></td>
											</tr>
											<tr>
												<td>角色</td>
												<td ><select name="roleId" id="roleId">
														<c:forEach var="r" items="${roles }">
															<option value="${r.id }" <c:if test="${u.roleId==r.id}">selected</c:if> >${r.name }</option>
														</c:forEach>
													</select></td>
											</tr>
											<tr>
												<td>密码</td>
												<td>${u.wxName}</td>
											</tr>
											<tr>
												<td>绑定微信</td>
												<td>${u.wxName}</td>
											</tr>
											<tr>
												<td>用户名称<span style="color: red">*</span></td>
												<td>
													<input type="text" name="name" id="name" value="${u.name}">
												</td>
											</tr>
											<tr>
												<td>密码<span style="color: red">*</span></td>
												<td>
													<input type="password" name="password" id="password">
												</td>
											</tr>
											<tr>
												<td>角色<span style="color: red">*</span></td>
												<td>
													<select name="roleId" id="roleId">
														<c:forEach var="r" items="${roles }">
															<option value="${r.id }"  <c:if test="${u.roleId==r.id}">selected</c:if> >${r.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td >绑定微信<span style="color: red">*</span></td>
												<td>
													<select name="wxId" id="wxId">
														<c:forEach var="w" items="${wxUsers }">
															<option value="${w.id }"  <c:if test="${u.roleId==r.id}">selected</c:if> >${w.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">Copyright &copy; 2017.Company name All rights reserved.</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>js/bootstrap-paginator.js"></script>
	<%-- <script src="<%=basePath %>js/qunit-1.11.0.js"></script> --%>
	<script type="text/javascript">
	function del(id){
		if(confirm("确定删除?")){
			$.ajax({
			    url:'<%=basePath %>user/del',
			    type:'POST', //GET
			    async:true,    //或false,是否异步
			    data: {id:id},
			    dataType:'json',
			    success:function(data){
			        if(data.resCode=="0"){
			        	alert("删除成功！");
			        	window.location.href=window.location.href;
			        }else{
			        	 $("#msg").html(data.resMsg);
			        }
			    },
			    error:function(xhr){},
			});
		}
	};
	$("#sbut").click(
    	 function () {
    			 if($('#name').val()==""){
    				 $("#msg").html("请输入用户名");
    				 return;
    			 }
    			 if($('#password').val()==""){
    				 $("#msg").html("请输入密码");
    				 return;
    			 }
    			 $.ajax({
 				    url:'<%=basePath %>user/add',
 				    type:'POST', //GET
 				    async:true,    //或false,是否异步
 				    data: $("#form01").serializeArray(),
 				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
 				    success:function(data){
 				        if(data.resCode=="0"){
 				        	alert("添加成功！");
 				        	window.location.href=window.location.href;
 				        }else{
 				        	 $("#msg").html(data.resMsg);
 				        }
 				    },
 				    error:function(xhr){},
 				});
	   		 }
	    );
	</script>
	</script>
</body>
</html>