<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-multiselect.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" >跟踪详情</h3>
									<%if(role <4) {%>
									<h3 class="panel-title" style="text-align: right;">
										<a href="javascript:void(0);" id="mf">修改</a>
									</h3>
									<% } %>
								</div>
								<div class="panel-body">
									<table class="table table-bordered" id="t1">
										<tbody>
											<tr>
												<td>客户名称</td>
												<td class="tdbold" colspan="3">${track.customerName}</td>
											</tr>
											<tr>
												<td>联系人</td>
												<td class="tdbold">${track.contact}</td>
												<td>联系电话</td>
												<td class="tdbold">${track.contactPhone}</td>
											</tr>
											<tr>
												<td>司机名称</td>
												<td class="tdbold">${track.driverName}</td>
												<td>司机电话</td>
												<td class="tdbold">${track.driverPhone}</td>
											</tr>
											<tr>
												<td>主车牌号</td>
												<td class="tdbold">${track.carNo}</td>
												<td>挂车牌号</td>
												<td class="tdbold">${track.handCarNo}</td>
											</tr>
											<tr>
												<td>开始日期</td>
												<td class="tdbold"><fmt:formatDate value="${track.inDate}" pattern="yyyy-MM-dd" /></td>
												<td>车辆类型</td>
												<td class="tdbold">${track.vehicleTypeName}</td>
											</tr>
											<tr>
												<td>公里表数</td>
												<td class="tdbold">${track.baseMiles}</td>
												<td>运输类型</td>
												<td class="tdbold">${track.transType}</td>
											</tr>
											<tr>
												<td>使用环境</td>
												<td class="tdbold">${track.roads}</td>
												<td>车货重量</td>
												<td class="tdbold">${track.weight}</td>
											</tr>
											<tr>
												<td>业务状况</td>
												<td class="tdbold" colspan="3">${track.business}</td>
											</tr>
										</tbody>
									</table>
									<table class="table table-bordered" id="t2">
										<tbody>
											<tr>
												<td>客户名称</td>
												<td class="tdbold" >
													${track.customerName}
													<input type="hidden" name="id" id="id" value="${track.id}">
													<input type="hidden" name="customerId"  id="customerId" value="${track.customerId}">
												</td>
												<td>检测人</td>
												<td class="tdbold" >
													${track.userName}
													<input type="hidden" name="userId"  id="userId" value="${track.userId}">
												</td>
											</tr>
											<tr>
												<td>联系人</td>
												<td class="tdbold">
													<input type="text" name="contact"  id="contact" value="${track.contact}">
												</td>
												<td>联系电话</td>
												<td class="tdbold"><input type="text" name="contactPhone"  id="contactPhone" value="${track.contactPhone}"></td>
											</tr>
											<tr>
												<td>司机名称</td>
												<td class="tdbold"><input type="text" name="driverName"  id="driverName"  value="${track.driverName}"></td>
												<td>司机电话</td>
												<td class="tdbold"><input type="text" name="driverPhone" id="driverPhone"  value="${track.driverPhone}"></td>
											</tr>
											<tr>
												<td>主车牌号</td>
												<td class="tdbold"><input type="text" name="carNo" id="carNo"  value="${track.carNo}"></td>
												<td>挂车牌号</td>
												<td class="tdbold"><input type="text" name="handCarNo"  id="handCarNo" value="${track.handCarNo}"></td>
											</tr>
											<tr>
												<td>开始日期</td>
												<td class="tdbold"><input type="text" name="inDate"  id="datetimepicker" data-date-format="yyyy-mm-dd" value="<fmt:formatDate value="${track.inDate}" pattern="yyyy-MM-dd" />"></td>
												<td>车辆类型</td>
												<td class="tdbold">
													${track.vehicleTypeName}
													<input type="hidden" name="vehicleTypeId"  id="vehicleTypeId" value="${track.vehicleTypeId}">
												</td>
											</tr>
											<tr>
												<td>公里表数</td>
												<td class="tdbold"><input type="text" name="baseMiles" id="baseMiles"  value="${track.baseMiles}"></td>
												<td>运输类型</td>
												<td class="tdbold">
													<select name="transType" id="transType" class="form-control">
															<option value="">请选择</option>
															<c:forEach items="${markets }" var="m">
																<option value="${m.type }" <c:if test="${track.transType==m.type}">selected</c:if> >${m.type }</option>
															</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>使用环境</td>
												<td class="tdbold">
													<select name="roads" id="roads" class="form-control">
															<option value="">请选择</option>
															<c:forEach items="${evns }" var="e">
																<option value="${e.name }"  <c:if test="${track.roads==e.name}">selected</c:if> >${e.name }</option>
															</c:forEach>
													</select>
												</td>
												<td>车货重量</td>
												<td class="tdbold"><input type="text" name="weight"  id="weight" value="${track.weight}"></td>
											</tr>
											<tr>
												
												<td>业务状况</td>
												<td class="tdbold" colspan="3"><input type="text" name="business"  id="business" value="${track.business}"></td>
											</tr>
											<tr>
												<td colspan="4">
													<button type="button" id="sub" class="btn btn-success">保存</button>
													<button type="button" id="cal" class="btn btn-primary">取消</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END INPUTS -->
						</div>
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">轮胎列表</h3>
									<%if(role <4) {%>
									<h3 class="panel-title" style="text-align: right;">
										<a href="javascript:void(0);"  data-toggle="modal" data-target="#add">添加</a>
									</h3>
									<%} %>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>轮胎编号</th>
												<th>轮胎品牌</th>
												<th>轮胎规格</th>
												<th>花纹代号</th>
												<th>检测</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="tire" items="${track.tires}">
												<tr>
													<td>${tire.vehicleTypeTireName }</td>
													<td>${tire.brandName }</td>
													<td>${tire.tyreSizeName }</td>
													<td>${tire.treadName }</td>
													<td><a href="<%=basePath %>track/check?id=${tire.id}">检测</a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
					<button type="button" class="btn btn-primary" id="cancel" onclick="javascript:window.history.back();">返回</button>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" 
									aria-hidden="true">×
							</button>
							<h4 class="modal-title" id="addLabel">添加轮胎</h4>
						</div>
						<div class="modal-body">
							<form action="" method="post" id="form01">
								<table class="table table-bordered">
										<tbody>
											<tr>
												<td>轮胎编号<span style="color: red">*</span></td>
												<td>
													<select name="vehicleTypeTireId" id="vehicleTypeTireId">
														<c:forEach items="${typeTires }" var="t">
															<option value="${t.id }">${t.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>轮胎品牌<span style="color: red">*</span></td>
												<td>
													<select name="brandId" id="brandId">
														<c:forEach items="${brands }" var="b">
															<option value="${b.id }">${b.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>轮胎规格<span style="color: red">*</span></td>
												<td>
													<select name="tyreSizeId" id="tyreSizeId">
														<c:forEach items="${tyreSize }" var="ts">
															<option value="${ts.id }">${ts.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>花纹代号<span style="color: red">*</span></td>
												<td>
													<select name="treadId" id="treadId">
														<c:forEach items="${treads }" var="td">
															<option value="${td.id }">${td.name }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>胎号<span style="color: red">*</span></td>
												<td>
													<input name="leftNo" id="leftNo">
													<input name="rightNo" id="rightNo">
												</td>
											</tr>
											<tr>
												<td>推荐气压<span style="color: red">*</span></td>
												<td>
													<div class="input-group">
															<input class="form-control" name="airPress" id="airPress">
															<span class="input-group-addon">KP</span>
													</div>
												</td>
											</tr>
											<tr>
												<td>原始花纹深度<span style="color: red">*</span></td>
												<td>
														<div class="input-group">
															<input class="form-control" name="tread" id="tread">
															<span class="input-group-addon">mm</span>
														</div>
												</td>
											</tr>
											<tr>
												<td>卸胎花纹深度</td>
												<td>
													<div class="input-group">
															<input class="form-control" name="tppTread" id="tppTread">
															<span class="input-group-addon">mm</span>
													</div>
												</td>
											</tr>
										</tbody>
								</table>
							</form>
							<h6 class="modal-title" id="msg" style="color: red"></h6>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="sbut">保存</button>
							<button type="button" class="btn btn-default"  data-dismiss="modal">关闭</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div>
			<!-- end 模态框（Modal） -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-multiselect.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-datetimepicker.min.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-datetimepicker.zh-CN.js"></script>
	<script type="text/javascript">
	 $(document).ready(function() {
		 $('#datetimepicker').datetimepicker({
		        language: 'zh-CN',
		        showMeridian: true,
		        autoclose: true,
		        todayBtn: true
		    });
		 
		 $("#t2").hide();
	     $('#mf').click(
	    	function(){
	    		$("#t2").show();
	    		$("#t1").hide();
	    	}		 
	     );
	     $('#cal').click(
	    	function(){
	    		$("#t1").show();
	    		$("#t2").hide();
	    	}		 
	     );
	     
	     $("#sub").click(function(){	
				if($('#customerId').val()==""){
					$("#msg").html("请选择客户");
					return;
				}
				if($('#contact').val()==""){
					$("#msg").html("请输入联系人");
					return;
				}
				if($('#carNo').val()==""){
					$("#msg").html("请输入主车牌号");
					return;
				}
				if($('#datetimepicker').val()==""){
					$("#msg").html("请输入安装日期");
					return;
				}
				if($('#vehicleTypeId').val()==""){
					$("#msg").html("请输入车辆类型");
					return;
				}
				if($('#baseMiles').val()==""){
					$("#msg").html("请输入公里表数");
					return;
				}
				
				var obj = {
						id:$('#id').val(),
						customerId:$('#customerId').val(),
						userId:$('#userId').val(),
						contact:$('#contact').val(),
						contactPhone:$('#contactPhone').val(),
						driverName:$('#driverName').val(),
						driverPhone:$('#driverPhone').val(),
						carNo:$('#carNo').val(),
						handCarNo:$('#handCarNo').val(),
						inDate:$('#datetimepicker').val(),
						vehicleTypeId:$('#vehicleTypeId').val(),
						baseMiles:$('#baseMiles').val(),
						transType:$('#transType').val(),
						roads:$('#roads').val(),
						weight:$('#weight').val(),
						business:$('#business').val()
				};
				
				$.ajax({
				   url:'<%=basePath%>track/update',
				   type:'POST', //GET
				   async:true,    //或false,是否异步
				   contentType:"application/json",
				   data:JSON.stringify(obj),
				   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				   success:function(data){
				       console.log(data);
				       if(data.update==true){
				       	alert("修改成功！");
				       	window.location.href="<%=basePath%>track/detail?id="+data.id;
					   }else{
						   alert("修改失败！"); 
					   }
					},
					error : function(xhr) {
						console.log('错误');
						console.log(xhr);
					},
				});
			});
	     $("#sbut").click(function(){	
				if($('#vehicleTypeTireId').val()==""){
					$("#msg").html("轮胎编号");
					return;
				}
				if($('#brandId').val()==""){
					$("#msg").html("轮胎品牌");
					return;
				}
				if($('#tyreSizeId').val()==""){
					$("#msg").html("轮胎规格");
					return;
				}
				if($('#treadId').val()==""){
					$("#msg").html("花纹代号");
					return;
				}
				
				var obj = {
						trackId:$('#id').val(),
						vehicleTypeTireId:$('#vehicleTypeTireId').val(),
						brandId:$('#brandId').val(),
						tyreSizeId:$('#tyreSizeId').val(),
						treadId:$('#treadId').val(),
						leftNo:$('#leftNo').val(),
						rightNo:$('#rightNo').val(),
						airPress:$('#airPress').val(),
						tread:$('#tread').val(),
						tppTread:$('#tppTread').val()
				};
				
				$.ajax({
				   url:'<%=basePath%>track/addTire',
				   type:'POST', //GET
				   async:true,    //或false,是否异步
				   contentType:"application/json",
				   data:JSON.stringify(obj),
				   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				   success:function(data){
				       console.log(data);
				       if(data.add==true){
				       	alert("添加成功！");
				       	window.location.href="<%=basePath%>track/detail?id="+$('#id').val();
					   }else{
						   alert("修改失败！"); 
					   }
					},
					error : function(xhr) {
						console.log('错误');
						console.log(xhr);
					},
				});
			});
	     $("#brandId").change(function(){
	    	 var id = $(this).children('option:selected').val();
	    	 $.ajax({
				   url:'<%=basePath%>track/tread',
				   type:'POST', //GET
				   async:true,    //或false,是否异步
				   data:"id="+id,
				   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				   success:function(data){
				       console.log(data);
			    	   var optStr = "";
			    	   var treads = data.treads;
			    	   $.each(treads, function (i) {
			    		   var id= treads[i].id;
			    		   var name = treads[i].name;
				            optStr=optStr + '<option value='+id+'>'+name+'</option>'
				    	});
			    	   $("#treadId").html(optStr);
					},
					error : function(xhr) {
						console.log('错误');
						console.log(xhr);
					},
			});
	     });
	     
	 });
	</script>
</body>
</html>