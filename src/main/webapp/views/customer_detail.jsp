<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-multiselect.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">客户详情</h3>
					<div class="row">
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-body">
									<table class="table table-bordered">
										<tbody>
											<tr>
												<td>客户名称</td>
												<td colspan="2" class="tdbold">${customer.name}
													<input type="hidden" value="${customer.id}" id="customerId">
												</td>
												<td>销售代表</td>
												<td class="tdbold">${customer.userName}</td>
											</tr>
											<tr>
												<td>客户状态</td>
												<td colspan="2" class="tdbold">${customer.statusName}</td>
												<td>生意放弃/失败原因</td>
												<td class="tdbold">${customer.failureReasonName}</td>
											</tr>
											<tr>
												<td>当前供应商</td>
												<td colspan="4" class="tdbold">${customer.supplier}</td>
											</tr>
											<c:if test="${empty customer.addresses}">
												<tr>
													<td >联系地址</td>
													<td colspan="4" class="tdbold"></td>
												</tr>
											</c:if>
											<c:if test="${!empty customer.addresses}">
												<c:forEach var="address" items="${customer.addresses}" varStatus="status">
													<tr>
														<c:if test="${status.index == 0 }">
														<td style="vertical-align:middle;" rowspan="${fn:length(customer.addresses)}" >联系地址</td>
														</c:if>
														<td colspan="4" class="tdbold">${address.address }</td>
													</tr>
												</c:forEach>
											</c:if>
											<c:if test="${empty customer.contacts}">
												<tr>
													<td >联系人</td>
													<td colspan="4" class="tdbold">暂无</td>
												</tr>
											</c:if>
											<c:if test="${!empty customer.contacts}">
												<tr>
													<td>联系人</td><td>职位</td><td>电话</td><td>手机</td><td>电子邮件</td>
												</tr>
												<c:forEach var="contact" items="${customer.contacts}" varStatus="status">
													<tr>
														<td class="tdbold">${contact.name}</td>
														<td class="tdbold">${contact.position}</td>
														<td class="tdbold">${contact.phone}</td>
														<td class="tdbold">${contact.mobile}</td>
														<td class="tdbold">${contact.email}</td>
													</tr>
												</c:forEach>
											</c:if>
											<tr>
												<td colspan="4"  style="vertical-align:middle;">车辆类型</td>
											</tr>
											<tr>
												<td >一体车(个)</td>
												<td class="tdbold">4x2:${customer.wholeCar42 }辆</td>
												<td class="tdbold">6x2:${customer.wholeCar62 }辆</td>
												<td class="tdbold" colspan="2">8x4:${customer.wholeCar84 }辆</td>
											</tr>
											<tr>
												<td >牵引车(个)</td>
												<td class="tdbold">4x2:${customer.drawCar42 }辆</td>
												<td class="tdbold">6x2:${customer.drawCar62 }辆</td>
												<td class="tdbold" colspan="2">6x4:${customer.drawCar64 }辆</td>
											</tr>
											<tr>
												<td >拖车(个)</td>
												<td class="tdbold">4x0:${customer.dragCar40 }辆</td>
												<td class="tdbold">4x0单胎:${customer.dragCar40D }辆</td>
												<td class="tdbold">6x0:${customer.dragCar60 }辆</td>
												<td class="tdbold">6x0单胎:${customer.dragCar60D }辆</td>
											</tr>
											<tr>
												<td >主要轮胎品牌</td>
												<td class="tdbold">
														<c:forEach var="brand" items="${customer.brands}" varStatus="status">
																<c:forEach var="brands" items="${brands}" > 
																	 	<c:if test="${brand.id == brands.id }">${brands.name }</c:if>
																</c:forEach>
														</c:forEach>
												</td>
												<td >轮胎使用规格</td>
												<td  class="tdbold" colspan="2">
													<c:forEach var="tyreSize" items="${customer.tyreSizes}" varStatus="status">
														<c:forEach var="tyreSizes" items="${tyreSizes}" >   
																 	<c:if test="${tyreSize.tyreSizeId == tyreSizes.id }">[${tyreSizes.name }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
											</tr>
											<tr>
												<td >翻新轮胎与新轮胎使用比例</td>
												<td class="tdbold" colspan="4">${customer.oldNewRate}%</td>
											</tr>
											<tr>
												<td >翻新轮胎里程(平均)</td>
												<td class="tdbold" colspan="1">驱动:${customer.frontMiles}</td>
												<td class="tdbold" colspan="3">拖轮:${customer.backMiles}</td>
											</tr>
											<tr>
												<td >翻新轮胎价格</td>
												<td class="tdbold" colspan="1">驱动:${customer.frontPrice}</td>
												<td class="tdbold" colspan="3">拖轮:${customer.backPrice}</td>
											</tr>
											<tr>
												<td >翻新轮胎花纹要求</td>
												<td class="tdbold" colspan="1">驱动:${customer.frontTread}</td>
												<td class="tdbold" colspan="3">拖轮:${customer.backTread}</td>
											</tr>
											<tr>
												<td >日均换胎量</td>
												<td class="tdbold">${customer.dayChangeAmt}</td>
												<td >月均报废胎量</td>
												<td class="tdbold"  colspan="2">${customer.dayDropAmt}</td>
											</tr>
											<tr>
												<td >里程/每月/每车</td>
												<td class="tdbold">${customer.avgMiles}</td>
												<td >油费/每月/每车</td>
												<td class="tdbold"  colspan="2">${customer.avgOilFee}</td>
											</tr>
											<tr>
												<td >每月轮胎费用/每车</td>
												<td class="tdbold">${customer.monthAvgFee}</td>
												<td >推崇新轮胎品牌</td>
												<td class="tdbold"  colspan="2">${customer.likeBrand}</td>
											</tr>
											<tr>
												<td >市场分类</td>
												<td class="tdbold">
													<c:forEach var="market" items="${customer.markets}" varStatus="status">
														<c:forEach var="markets" items="${markets}" >   
																 	<c:if test="${market.marketId == markets.id }">[${markets.type }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
												<td >使用环境</td>
												<td class="tdbold"  colspan="2">
													<c:forEach var="evn" items="${customer.evns}" varStatus="status">
														<c:forEach var="evns" items="${evns}" >   
																 	<c:if test="${evn.evnId == evns.id }">[${evns.name }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
											</tr>
											<tr>
												<td >轮胎主要问题</td>
												<td class="tdbold" colspan="4">
													<c:forEach var="problem" items="${customer.problems}" varStatus="status">
														<c:forEach var="problems" items="${problems}" >   
																 	<c:if test="${problem.problemId == problems.id }">[${problems.name }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
											</tr>
											
											<tr>
												<td rowspan="3" style="vertical-align:middle;">服务要求</td>
												<td >体系</td>
												<td class="tdbold" colspan="3">
													<c:forEach var="require" items="${customer.requires}" varStatus="status">
														<c:forEach var="requires" items="${requires}" >   
																 	<c:if test="${require.requireId == requires.id }">[${requires.name }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
											</tr>
											<tr>
												<td >服务</td>
												<td class="tdbold" colspan="3">
													<c:forEach var="service" items="${customer.services}" varStatus="status">
														<c:forEach var="services" items="${services}" >   
																 	<c:if test="${service.serviceId == services.id }">[${services.name }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
											</tr>
											<tr>
												<td >信息</td>
												<td class="tdbold" colspan="3">
													<c:forEach var="info" items="${customer.infos}" varStatus="status">
														<c:forEach var="infos" items="${infos}" >   
																 	<c:if test="${info.infoId == infos.id }">[${infos.name }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
											</tr>
											<tr>
												<td >翻新点</td>
												<td class="tdbold">
													<c:forEach var="point" items="${customer.points}" varStatus="status">
														<c:forEach var="points" items="${points}" >   
																 	<c:if test="${point.pointId == points.id }">[${points.name }&nbsp;&nbsp;]</c:if>
														</c:forEach>
													</c:forEach>
												</td>
												<td >双轮匹配</td>
												<td class="tdbold" colspan="2">
													<c:if test="${customer.doubleMatch==0}">否</c:if>
													<c:if test="${customer.doubleMatch==1}">是</c:if>
												</td>
											</tr>
											<tr>
												<td >是否有轮胎实验</td>
												<td class="tdbold">
													<c:if test="${customer.hasTest==0}">否</c:if>
													<c:if test="${customer.hasTest==1}">是</c:if>
												</td>
												<td >气压检查</td> 
												<td class="tdbold"  colspan="2">
													<c:if test="${customer.checkRate==0}">每日</c:if>
													<c:if test="${customer.checkRate==1}">每周</c:if>
													<c:if test="${customer.checkRate==2}">每月</c:if>
													<c:if test="${customer.checkRate==3}">每年</c:if>
												</td>
											</tr>
											<tr>
												<td >气压标准</td>
												<td class="tdbold">${customer.pressStandard}</td>
												<td >车队轮胎规模</td> 
												<td class="tdbold"  colspan="2">${customer.fleetAmt}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END INPUTS -->
						</div>
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">讨论区</h3>
								</div>
								<div class="tab-content">
									<div class="tab-pane fade active in" id="tab-bottom-left1">
										<ul class="list-unstyled activity-timeline">
											<c:forEach items="${comments }" var="comment">
												<li>
													<i class="fa fa-comment activity-icon"></i>
													<p>${comment.userName } <span class="timestamp"><fmt:formatDate value="${comment.createTime}" pattern="yyyy-MM-dd HH:mm:ss" /></span></p>
													<p>${comment.content }</p>
												</li>
											</c:forEach>
										</ul>
									</div>
								</div>
								
								<div class="panel-body ">
									<textarea class="form-control" name="content" id="content" placeholder="请添加评论" rows="4"></textarea>
									<button type="button" class="btn btn-success" id="comment">评论</button>
									<button type="button" class="btn btn-primary" onclick="javascript:window.history.back();">返回</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#comment").click(function(){
			if($("#content").val()==""){
				alert("请输入评论内容");
				return;
			}
			var obj = {content:$("#content").val(),customerId:$("#customerId").val()}
			$.ajax({
			   url:'<%=basePath%>customer/addComment',
			   type:'POST', //GET
			   async:true,    //或false,是否异步
			   contentType:"application/json",
			   data:JSON.stringify(obj),
			   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			   success:function(data){
			       console.log(data);
			       if(data.add==true){
			       	alert("添加成功！");
			       	window.location.href=window.location.href;
				   }
				},
				error : function(xhr) {
					console.log('错误');
					console.log(xhr);
				},
			});
		});
	});
	</script>
</body>
</html>