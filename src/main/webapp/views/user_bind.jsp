<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/toastr/toastr.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">绑定微信用户</h3>
					<div class="row">
						<div class="col-md-6">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<!-- <div class="panel-heading">
									<h3 class="panel-title">Bordered Table</h3>
								</div> -->
								<div class="panel-body" id="toastr-demo">
									<form action="" >
									<table class="table table-bordered">
										<tr>
											<td align="right">微信号：</td>
											<td>
												<select class="form-control" id="wxId" name="wxId">
													<c:forEach var="wu" items="${wxusers }" varStatus="status">
														<option value="${wu.id }"
															<c:if test="${user.wxId == wu.id  }">selected</c:if>
														>${wu.name }</option>
													</c:forEach>
												</select>
												<input type="hidden" id="id" name="id" value="${user.id }">
											</td>
										<tr>
										<tr>
											<td colspan="2" align="center">
												<button type="button" style="display: none" id="success" class="btn btn-default btn-toastr" data-context="info" data-message="绑定成功！" data-position="top-center" ></button>
												<button type="button" id="sub" class="btn btn-success">确定</button>
												<button type="button" onclick="javascrpt:history.go(-1);" class="btn btn-primary">返回</button>
											</td>
										<tr>
									</table>
									</form>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/vendor/toastr/toastr.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		  $("#sub").click(function(){			  
			  $.ajax({
				    url:'<%=basePath %>user/bind',
				    type:'POST', //GET
				    async:true,    //或false,是否异步
				    data:{
				    	id:$("#id").val(),wxId:$("#wxId").val()
				    },
				    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				    success:function(data){
				        console.log(data);
				        if(data.binded==true){
				        	alert("绑定成功！");
				        	window.location.href="<%=basePath %>user/list";
				        }
				    },
				    error:function(xhr){
				        console.log('错误');
				        console.log(xhr);
				    },
				});
		  });
		});
	</script>
</body>
</html>