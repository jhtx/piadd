<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/toastr/toastr.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-select.min.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">修改合作客户</h3>
					<div class="row">
						<div class="col-md-6">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<!-- <div class="panel-heading">
									<h3 class="panel-title">Bordered Table</h3>
								</div> -->
								<div class="panel-body" id="toastr-demo">
									<form action="" id="form01">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>客户名称<span style="color: red">*</span></td>
													<td>
														<input type="text" name="nickName" id="nickName" value="${couser.nickName }">
														<input type="hidden" name="id"  id="id" value="${couser.id }">
													</td>
												</tr>
												<tr>
													<td>客户账号<span style="color: red">*</span></td>
													<td>
														${couser.name }
													</td>
												</tr>
												<tr>
													<td>密码<span style="color: red">*</span></td>
													<td>
														<input type="password" name="password" id="password" value="${couser.password }">
													</td>
												</tr>
												<tr>
													<td>客户编号</td>
													<td>
														<input type="text" name="no" id="no" value="${couser.no }">
													</td>
												</tr>
												<tr>
													<td>客户地址</td>
													<td>
														<input type="text" name="address" id="address" value="${couser.address }">
													</td>
												</tr>
												<tr>
													<td>联系人<span style="color: red">*</span></td>
													<td>
														<input type="text" name="contactor" id="contactor" value="${couser.contactor }">
													</td>
												</tr>
												<tr>
													<td>联系电话<span style="color: red">*</span></td>
													<td>
														<input type="text" name="phone" id="phone" value="${couser.phone }">
													</td>
												</tr>
												<tr>
													<td colspan="2" align="center">
														<button type="button" id="sub" class="btn btn-success">确定</button>
														<button type="button" onclick="javascrpt:history.go(-1);" class="btn btn-primary">返回</button>
													</td>
												<tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/vendor/toastr/toastr.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-select.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#sub").click(function(){	
			if($('#nickName').val()==""){
				$("#msg").html("请输入客户名称");
				return;
			}
			if($('#password').val()==""){
				$("#msg").html("请输入密码");
				return;
			}
			if($('#contactor').val()==""){
				$("#msg").html("请输入联系人");
				return;
			}
			if($('#phone').val()==""){
				$("#msg").html("请输入联系电话");
				return;
			}
	 		var obj = {id: $("#id").val(),nickName:$("#nickName").val(),password:$("#password").val(),no:$("#no").val(),address:$("#address").val(),contactor:$("#contactor").val(),phone:$("#phone").val()};
			$.ajax({
			   url:'<%=basePath %>couser/update',
			   type:'POST', //GET
			   async:true,    //或false,是否异步
			   contentType:"application/json",
			   data:JSON.stringify(obj),
			   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			   success:function(data){
			       console.log(data);
			       if(data.update==true){
			       	alert("修改成功！");
			       	window.location.href="<%=basePath %>couser/list";
			       }
			   },
			   error:function(xhr){
			       console.log('错误');
			       console.log(xhr);
			   },
			});
		});
	});
	</script>
</body>
</html>