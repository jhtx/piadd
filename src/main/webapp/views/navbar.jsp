<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top" id="navtop" style="background-image:url(<%=basePath %>assets/img/banner.jpg);background-repeat: no-repeat;background-size: 100%; margin-left: 210px">
	<div class="brand bg"></div>
	<div class="container-fluid">
		<div class="navbar-btn">
			<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
		</div>
		<div id="navbar-menu">
			<ul class="nav navbar-nav navbar-right" style="padding: 16px 0;margin-top: 8px;margin-bottom: 8px;">
				<li class="dropdown">
						<span>${userName } 您好!&nbsp;&nbsp;&nbsp;</span>
						<span><a href="javascript:void(0)" data-toggle="modal" data-target="#passwordModal">修改密码&nbsp;&nbsp;&nbsp;</a></span>
						<span><a id="logout" onclick='javascript:window.location.href="<%=basePath %>/logout.html"' style="cursor: pointer;">退出登录</a></span>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!-- 模态框（Modal） -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="passwordModalLabel">修改密码</h4>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="formupdatepassword">
					<table class="table table-bordered">
							<tbody>
								<tr>
									<td>用户名称<span style="color: red">*</span></td>
									<td>${userName }<input type="hidden" name="userId" value="${userId }"></td>
								</tr>
								<tr>
									<td>密码<span style="color: red">*</span></td>
									<td>
										<input type="text" name="password" id="navpassword" placehoder="请输入新密码">
									</td>
								</tr>
							</tbody>
					</table>
				</form>
				<h6 class="modal-title" id="navmsg" style="color: red"></h6>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="closeBtn">关闭</button>
				<button type="button" class="btn btn-primary" id="updatepassword">提交更改</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal -->
</div>
<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
<script>
$("#updatepassword").click(function(){
	if($("#navpassword").val()==""){
		$("#navmsg").html("密码不能为空");
		return;
	}
	$.ajax({
		   url:'<%=path %>/user/updatepassword',
		   type:'POST', //GET
		   async:true,    //或false,是否异步
		   data:$("#formupdatepassword").serializeArray(),
		   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
		   success:function(data){
		       console.log(data);
		       if(data.resCode=="0"){
		       	alert("修改成功！");
		       	$("#closeBtn").click();
			   }
			},
			error : function(xhr) {
				console.log('错误');
				console.log(xhr);
			},
		});
	
});
</script>
<!-- END NAVBAR -->