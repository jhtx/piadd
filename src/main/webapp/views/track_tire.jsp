<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-multiselect.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">跟踪详情-轮胎跟踪</h3>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>轮胎编号</th>
												<th>轮胎品牌</th>
												<th>轮胎规格</th>
												<th>花纹代号</th>
												<th colspan="2">胎号</th>
												<th>推荐气压</th>
												<th>原始花纹深度</th>
												<th>卸胎花纹深度</th>
												<th>预计新轮胎行驶总里程</th>
											</tr>
										</thead>
										<tbody>
												<tr>
													<td>${trackTire.vehicleTypeTireName }</td>
													<td>${trackTire.brandName }</td>
													<td>${trackTire.tyreSizeName }</td>
													<td>${trackTire.treadName }</td>
													<td>${trackTire.leftNo }</td>
													<td>${trackTire.rightNo }</td>
													<td>${trackTire.airPress }</td>
													<td>${trackTire.tread }</td>
													<td>${trackTire.tppTread }</td>
													<td>${trackTire.miles }</td>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END INPUTS -->
						</div>
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">检查数据</h3>
									<% if(role <4) {%>
									<h3 class="panel-title" style="text-align: right;">
										<a href="javascript:void(0);" id="mf" data-toggle="modal" data-target="#add">添加</a>
									</h3>
									<% } %>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>检测日期</th>
												<th>气压</th>
												<th>花纹深度</th>
												<th>公里表读数</th>
												<th>已行驶里程</th>
												<th>预计剩余行驶里程</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="trackTireCheck" items="${trackTire.trackTireChecks}">
												<tr>
													<td>
														<fmt:formatDate value="${trackTireCheck.checkDate}" pattern="yyyy-MM-dd HH:mm:ss" />
													</td>
													<td>${trackTireCheck.press }</td>
													<td>${trackTireCheck.tread }</td>
													<td>${trackTireCheck.miles }</td>
													<td>${trackTireCheck.driveMiles }</td>
													<td>${trackTireCheck.expectMiles }</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"  onclick="javascript:window.history.back();">返回</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" 
									aria-hidden="true">×
							</button>
							<h4 class="modal-title" id="addLabel">添加检查数据</h4>
						</div>
						<div class="modal-body">
							<form action="" method="post" id="form01">
								<table class="table table-bordered">
										<tbody>
											<tr>
												<td>公里表读数<span style="color: red">*</span></td>
												<td>
													<input name="miles" id="miles">
													<input name="tireId" id="tireId" value="${trackTire.id}" type="hidden">
													<input name="baseMiles" id="baseMiles" value="${track.baseMiles}" type="hidden">
													<input name="basePress" id="basePress" value="${trackTire.airPress }" type="hidden">
													<input name="baseTread" id="baseTread" value="${trackTire.tread }" type="hidden">
												</td>
											</tr>
											<tr>
												<td>气压<span style="color: red">*</span></td>
												<td>
													<input name="press" id="press">
												</td>
											</tr>
											<tr>
												<td rowspan="6">花纹深度<span style="color: red">*</span></td>
												<td>
													<input class="col-md-4" name="tread" id="tread"><input class="col-md-4" name="tread" id="tread"><input class="col-md-4" name="tread" id="tread">
												</td>
											</tr>
											<tr>
												<td>
													<input class="col-md-4"  name="tread" id="tread"><input class="col-md-4"  name="tread" id="tread"><input class="col-md-4"  name="tread" id="tread">
												</td>
											</tr>
											<tr>
												<td>
													<input class="col-md-4"  name="tread" id="tread"><input class="col-md-4"  name="tread" id="tread"><input class="col-md-4"  name="tread" id="tread">
												</td>
											</tr>
											<tr>
												<td>
													<input class="col-md-4"  name="tread" id="tread"><input class="col-md-4"  name="tread" id="tread"><input class="col-md-4"  name="tread" id="tread">
												</td>
											</tr>
										</tbody>
								</table>
							</form>
							<h6 class="modal-title" id="msg" style="color: red"></h6>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="sbut">保存</button>
							<button type="button" class="btn btn-default"  data-dismiss="modal">关闭</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div>
			<!-- end 模态框（Modal） -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
	 $(document).ready(function() {
		 $("#sbut").click(function(){	
				if($('#miles').val()==""){
					$("#msg").html("请输入公里表读数");
					return;
				}
				if($('#press').val()==""){
					$("#msg").html("请输入气压");
					return;
				}
				
				var treads  = $("input[name='tread']");
				var arrays = new Array();
				var re = /^[0-9]+.?[0-9]*$/;
				$.each(treads, function (i) {
		    		   var value= $(treads[i]).val();
		    		   if(!re.test(value)){
		    			   $("#msg").html("请输入数值");
		    			   return;
		    		   }
		    		   arrays.push(value);
			    });
				arrays.sort();
				var tread = arrays[0];
				var baseTread = $('#baseTread').val();
				var miles = $('#miles').val();
				var press = $('#press').val();
				var basePress = $('#basePress').val();
				var baseMiles = $('#baseMiles').val();
				var driveMiles = miles-baseMiles;
				
				var obj = {
						tireId:$('#tireId').val(),
						miles:miles,
						press:press,
						tread:tread,
						driveMiles:driveMiles,
				};
				
				$.ajax({
				   url:'<%=basePath%>track/addCheck',
				   type:'POST', //GET
				   async:true,    //或false,是否异步
				   contentType:"application/json",
				   data:JSON.stringify(obj),
				   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				   success:function(data){
				       console.log(data);
				       if(data.add==true){
				       	alert("添加成功！");
				       	window.location.href="<%=basePath%>track/check?id="+$('#tireId').val();
					   }else{
						   alert("修改失败！"); 
					   }
					},
					error : function(xhr) {
						console.log('错误');
						console.log(xhr);
					},
				});
			});
	 });
	</script>
</body>
</html>