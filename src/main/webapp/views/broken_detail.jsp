<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-multiselect.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css">
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">破损详情</h3>
								</div>
								<div class="panel-body">
									<table class="table table-bordered">
										<tbody>
											<tr>
												<td>检测日期</td>
												<td class="tdbold"><fmt:formatDate value="${brokenCheck.updateTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
												<td>轮胎规格</td>
												<td class="tdbold">${brokenCheck.tyreSizeName}</td>
											</tr>
											<tr>
												<td>轮胎胎号</td>
												<td class="tdbold">${brokenCheck.tireNo}</td>
												<td>胎体牌号</td>
												<td class="tdbold">${brokenCheck.brandName}</td>
											</tr>
											<tr>
												<td>卸胎花纹深度(mm)</td>
												<td class="tdbold">${brokenCheck.tppTread}</td>
												<td>原始/翻新轮胎</td>
												<td class="tdbold">
													<c:if test="${brokenCheck.primaryRetread==0}">原始轮胎</c:if> 
													<c:if test="${brokenCheck.primaryRetread==1}">翻新轮胎</c:if> 
												</td>
											</tr>
											<tr>
												<td>翻新品牌</td>
												<td class="tdbold">${brokenCheck.retreadBrandName}</td>
												<td>翻新次数</td>
												<td class="tdbold">${brokenCheck.retreadCount}</td>
											</tr>
											<tr>
												<td>翻新胎花纹</td>
												<td class="tdbold">
													<c:if test="${brokenCheck.retreadTreadId==1}">Z</c:if>
													<c:if test="${brokenCheck.retreadTreadId==2}">H</c:if>
													<c:if test="${brokenCheck.retreadTreadId==3}">K</c:if>
													<c:if test="${brokenCheck.retreadTreadId==4}">M</c:if>
												</td>
												<td>原始胎面花纹</td>
												<td class="tdbold">${brokenCheck.primaryTreadName}</td>
											</tr>
											<tr>
												<td>原始花纹深度</td>
												<td class="tdbold">${brokenCheck.primaryTreadDeep}</td>
												<td>R/X</td>
												<td class="tdbold">
													<c:if test="${brokenCheck.rX==0}">R</c:if> 
													<c:if test="${brokenCheck.rX==1}">X</c:if> 
												</td>
											</tr>
											<tr>
												<td>剩余花纹深度</td>
												<td class="tdbold" colspan="3">${brokenCheck.remainTreadDeep1} |${brokenCheck.remainTreadDeep2} |${brokenCheck.remainTreadDeep3} </td>
											</tr>
    										<tr>
												<td>使用问题</td>
												<td class="tdbold">${brokenCheck.useProblemName}</td>
												<td>胎体问题</td>
												<td class="tdbold">${brokenCheck.tireProblemName}</td>
											</tr>
											<tr>
												<td>管理规程问题</td>
												<td class="tdbold">${brokenCheck.manageProblemName}</td>
												<td>修补问题</td>
												<td class="tdbold">${brokenCheck.repairProblemName}</td>
											</tr>
											<tr>
												<td>翻新问题</td>
												<td class="tdbold" >${brokenCheck.retreadProblemName}</td>
												<td>轮胎采购价格(元)</td>
												<td class="tdbold" >${brokenCheck.price}</td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
							<!-- END INPUTS -->
							<div class="modal-footer" style="text-align: center;">
									<button type="button" class="btn btn-primary" id="cancel" onclick="javascript:window.history.back();">返回</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
	 $(document).ready(function() {
	       // $('#brand_select').multiselect();
	 });
	</script>
</body>
</html>