<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/toastr/toastr.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-select.min.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">修改用户</h3>
					<div class="row">
						<div class="col-md-6">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<!-- <div class="panel-heading">
									<h3 class="panel-title">Bordered Table</h3>
								</div> -->
								<div class="panel-body" id="toastr-demo">
									<form action="" id="form01">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>用户名称<span style="color: red">*</span></td>
													<td>
														${user.name }
														<input type="hidden" name="name" id="name" value="${user.name }">
														<input type="hidden" name="id"  id="id" value="${user.id }">
														<input type="hidden"  id="wxusers" value="${user.wxUsersJson }">
													</td>
												</tr>
												<tr>
													<td>密码<span style="color: red">*</span></td>
													<td>
														<input type="password" name="password" id="password" value="${user.password }">
													</td>
												</tr>
												<tr>
													<td>角色<span style="color: red">*</span></td>
													<td>
														<select name="roleId" id="roleId">
															<c:forEach var="r" items="${roles }">
																<option  value="${r.id }" <c:if test="${r.id==user.roleId }">selected</c:if> >${r.name }</option>
															</c:forEach>
														</select>
														<select name="topUserId" id="topUserId" style="display: none">
															<option>请选择归属总监</option>
															<c:forEach var="r" items="${roles }">
																<option value="${r.id }">${r.name }</option>
															</c:forEach>
													</select>
													</td>
												</tr>
												<tr>
													<td >绑定微信<span style="color: red">*</span></td>
													<td>
														<select name="wxIds" id="wxIds"  class="selectpicker bla bla bli" multiple data-live-search="true">
															<c:forEach var="w" items="${wxUsers }">
																<option value='${w.id }'>${w.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td colspan="2" align="center">
														<button type="button" id="sub" class="btn btn-success">确定</button>
														<button type="button" onclick="javascrpt:history.go(-1);" class="btn btn-primary">返回</button>
													</td>
												<tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/vendor/toastr/toastr.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-select.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker({
	        'selectedText': 'cat'
	    });
		var users = ${user.wxUsersJson };
		var oldnumber = new Array();
	    $.each(users, function (i) {
	            oldnumber.push(users[i].id);
	    });
	    $('.selectpicker').selectpicker('val', oldnumber);//默认选中
	    $('.selectpicker').selectpicker('refresh');
	    $("#roleId").change(function(){
			if($("#roleId").val()==3){
				$("#topUserId").css("display","block");
			}else{
				$("#topUserId").css("display","none");
			}
		});
		$("#sub").click(function(){	
			if($('#name').val()==""){
				$("#msg").html("请输入用户名");
				return;
			}
			if($('#password').val()==""){
				$("#msg").html("请输入密码");
				return;
			}
			if($("#roleId").val()==3 && $("#topUserId").val()==""){
				 $("#msg").html("请选择归属总监");
				 return;
			 }
			 var arr = $("#wxId").val();
			console.log(arr);
	 		var obj = {id: $("#id").val(),name:$("#name").val(),password:$("#password").val(),roleId:$("#roleId").val(),wxIds:$("#wxIds").val()};
			$.ajax({
			   url:'<%=basePath %>user/update',
			   type:'POST', //GET
			   async:true,    //或false,是否异步
			   contentType:"application/json",
			   data:JSON.stringify(obj),
			   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			   success:function(data){
			       console.log(data);
			       if(data.update==true){
			       	alert("修改成功！");
			       	window.location.href="<%=basePath %>user/list";
			       }
			   },
			   error:function(xhr){
			       console.log('错误');
			       console.log(xhr);
			   },
			});
		});
	});
	</script>
</body>
</html>