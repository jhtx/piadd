<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	String uri = (String)request.getSession().getAttribute("uri");
	String activeUser="",activeSet="",activeCustomer="", activeTrack="" ,activeBroken="" ,activeReport= "",activeCoUser="";
	if(uri.startsWith("/couser")){
		activeCoUser = "active";
	}
	if(uri.startsWith("/set")){
		activeSet = "active";
	}
	if(uri.startsWith("/customer")){
		activeCustomer = "active";
	}
	if(uri.startsWith("/track")){
		activeTrack = "active";
	}
	if(uri.startsWith("/broken")){
		activeBroken = "active";
	}
	if(uri.startsWith("/check")){
		activeReport = "active";
	}
	int role = (Integer)request.getSession().getAttribute("role");
%>
<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
	<div style="position:relative;    height: 80px;margin-top: -80px;">
		<a href="<%=basePath %>index.html" style="">
			<img style="max-width: 30px;float: left;margin: 25px 0px 25px 40px;" src="<%=basePath %>assets/img/logo.png" alt="TTMS" class="img-responsive logo">
			<div style="width: 100px;height:30px;float: left;margin-top: 30px;">TTMS</div>
		</a>
	</div>
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<!-- <li><a href="index.html"  class="active"><i class="lnr lnr-home"></i> <span>首页</span></a></li> -->
				<% if(role==1) {%>
				<li>
					<a href="<%=basePath %>user/list.html" class="<%=activeUser %>">
						<i class="lnr lnr-home"></i> <span>用户管理</span>
					</a>
				</li>
				<li>
					<a href="<%=basePath %>/set/list.html" class="<%=activeSet %>">
						<i class="lnr lnr-home"></i> <span>参数设定</span>
					</a>
				</li>
				<% } %>
				<li>
					<a href="<%=basePath %>customer/list.html" class="<%=activeCustomer %>">
						<i class="fa fa-user"></i> <span>咨询客户管理</span>
					</a>
				</li>
				<li>
					<a href="<%=basePath %>track/list.html" class="<%=activeTrack %>">
						<i class="lnr lnr-car"></i> <span>轮胎跟踪</span>
					</a>
				</li>
				<li>
					<a href="<%=basePath %>broken/list.html" class="<%=activeBroken %>">
						<i class="lnr lnr-chart-bars"></i> <span>破损分析</span>
					</a>
				</li>
				<li>
					<a href="<%=basePath %>check/list.html" class="<%=activeReport %>">
						<i class="fa fa-truck"></i> <span>轮胎检查</span>
					</a>
				</li>
				<% if(role==1) {%>
				<li>
					<a href="<%=basePath %>couser/list.html" class="<%=activeCoUser %>">
						<i class="fa fa-user-circle"></i> <span>合作用户管理</span>
					</a>
				</li>
				<% } %>
				<!-- <li>
					<a href="notifications.html" class="">
						<i class="lnr lnr-alarm"></i> <span>Notifications</span>
					</a>
				</li>
				<li>
					<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Pages</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subPages" class="collapse ">
						<ul class="nav">
							<li><a href="page-profile.html" class="">Profile</a></li>
							<li><a href="page-login.html" class="">Login</a></li>
							<li><a href="page-lockscreen.html" class="">Lockscreen</a></li>
						</ul>
					</div>
				</li>
				<li><a href="tables.html" class=""><i class="lnr lnr-dice"></i> <span>Tables</span></a></li>
				<li><a href="typography.html" class=""><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li>
				<li><a href="icons.html" class=""><i class="lnr lnr-linearicons"></i> <span>Icons</span></a></li> -->
			</ul>
		</nav>
	</div>
</div>
<!-- END LEFT SIDEBAR -->
