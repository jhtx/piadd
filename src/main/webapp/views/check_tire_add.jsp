<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<%=basePath %>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link rel="stylesheet" href="<%=basePath %>css/google.css">
	<%-- <link rel="stylesheet" href="<%=basePath %>css/bootstrap-responsive.css">
	<link rel="stylesheet" href="<%=basePath %>css/qunit-1.11.0.css"> --%>
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp"/>
		<!-- left -->
		<jsp:include page="left.jsp"/>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<!-- INPUTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%;float: left;">轮位检测</h3>
								</div>
								<div class="panel-body">
									<form action="" method="post">
										<table class="table table-bordered" id="tab">
											<thead>
												<tr>
													<th width="5%">轮位号</th>
													<th width="10%">轮胎类型</th>
													<th width="10%">翻新类型</th>
													<th width="10%">花纹类型</th>
													<th width="10%">轮胎品牌</th>
													<th width="10%">轮胎规格</th>
													<th width="15%">预警标识</th>
													<th width="10%">气压</th>
													<th colspan="3">剩余花纹</th>
												</tr>
											</thead>
											<tbody>
													<c:forEach var="t" items="${typeTires }" varStatus="status">
														<tr>
															<td>
																${t.name }
																<input type="hidden" value="${t.id }" name="vehicleTypeTireId" id="vehicleTypeTireId">
																<input type="hidden" value="${vehicle.id }" name="checkVehicleId" id="checkVehicleId">
															</td>
															<td>
																<select id="rX" name="rX" class="form-control" style="padding: 6px 2px;">
																	<option value="0">R</option>
																	<option value="1">X</option>
																</select>
															</td>
															<td>
																<select id="retreadType" name="retreadType" class="form-control" style="padding: 6px 2px;">
																	<option value="0">N</option>
																	<option value="1">F</option>
																</select>
															</td>
															<td>
																<select id="treadType" name="treadType" class="form-control" style="padding: 6px 2px;">
																	<option value="0">H</option>
																	<option value="1">Z</option>
																	<option value="2">M</option>
																	<option value="3">K</option>
																</select>
															</td>
															<td>${cvt.brandName }
																<select id="brandId" name="brandId" class="form-control" style="padding: 6px 2px;">
																	<c:forEach items="${brands }" var="b">
																		<option value="${b.id }">${b.name }</option>
																	</c:forEach>
																</select>
															</td>
															<td>
																<select name="tyreSizeId" id="tyreSizeId" class="form-control" style="padding: 6px 2px;">
																	<c:forEach items="${tyreSizes }" var="t">
																		<option value="${t.id }">${t.name }</option>
																	</c:forEach>
																</select>
															</td>
															<td>
																<select name="vehicleTypeTireWarns" id="vehicleTypeTireWarns"  class="selectpicker bla bla bli" multiple data-live-search="true" style="padding: 6px 2px;">
																		<option value="0">W</option>
																		<option value="1">T</option>
																		<option value="2">△</option>
																		<option value="3">O</option>
																		<option value="4">X</option>
																</select>
															</td>
															<td>
																<input  name="airPress" id="airPress" class="form-control">
															</td>
															<td><input  name="treadDeep1" id="treadDeep1" class="form-control"></td>
															<td><input  name="treadDeep2" id="treadDeep2" class="form-control"></td>
															<td><input  name="treadDeep3" id="treadDeep3" class="form-control"></td>
														</tr>
													</c:forEach>
											</tbody>
										</table>
										</form>
									<h6 class="modal-title" id="msg" style="color: red"></h6>
								</div>
								<div class="modal-footer" style="text-align: center;">
									<input type="hidden" value="${id }" name="id" id="id">
									<input type="hidden" value="${vehicle.vehicleTypeId }" name="vehicleTypeId" id="vehicleTypeId">
									<button type="button" class="btn btn-success"  id="sub">保存</button>
									<button type="button" class="btn btn-primary" id="cancel" onclick="javascript:window.history.back();">取消</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath %>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath %>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath %>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-select.min.js"></script>
	<script type="text/javascript">
	 $(document).ready(function() {
		   $('.selectpicker').selectpicker({
		        'selectedText': 'cat'
		    });
	       $('#sbt').hide();
	       $("#sub").click(function () {
	    	   
	    	   var checkVehicleIds = $("input[name='checkVehicleId']");
	    	   var vehicleTypeTireIds = $("input[name='vehicleTypeTireId']");
	    	   var rX=document.getElementsByName('rX');
	    	   var retreadType=document.getElementsByName('retreadType');
	    	   var treadType=document.getElementsByName('treadType');
	    	   var brandId=document.getElementsByName('brandId');
	    	   var tyreSizeId=document.getElementsByName('tyreSizeId');
	    	   var vehicleTypeTireWarns=document.getElementsByName('vehicleTypeTireWarns');
	    	   var airPress = $("input[name='airPress']");
	    	   var treadDeep1 = $("input[name='treadDeep1']");
	    	   var treadDeep2 = $("input[name='treadDeep2']");
	    	   var treadDeep3 = $("input[name='treadDeep3']");
	    	   var arr=[];
	    	   
	    	   for(var i=0;i<vehicleTypeTireIds.length;i++){
	    		   var warnArr=[];
	    		   var vls = $(vehicleTypeTireWarns[i]).val()
	    		   for(var j=0;j<vls.length;j++){
	    			   var obj = {
		    				   warnType:vls[j]
		    		   }
	    			   warnArr.push(obj);
	    		   }
		    		  
	    		   var obj = {
	    				   checkVehicleId:$(checkVehicleIds[i]).val(),
	    				   vehicleTypeTireId:$(vehicleTypeTireIds[i]).val(),
	    				   rX:$(rX[i]).val(),
	    				   retreadType:$(retreadType[i]).val(),
	    				   treadType:$(treadType[i]).val(),
	    				   brandId:$(brandId[i]).val(),
	    				   tyreSizeId:$(tyreSizeId[i]).val(),
	    				   airPress:$(airPress[i]).val(),
	    				   treadDeep1:$(treadDeep1[i]).val(),
	    				   treadDeep2:$(treadDeep2[i]).val(),
	    				   treadDeep3:$(treadDeep3[i]).val(),
	    				   vehicleTypeTireWarns:warnArr
	    		   } 
	    		   arr.push(obj);
	    	   }
		       console.log(arr);		
		       $.ajax({
				   url:'<%=basePath%>check/addTire',
				   type:'POST', //GET
				   async:true,    //或false,是否异步
				   contentType:"application/json",
				   data:JSON.stringify(arr),
				   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
				   success:function(data){
				       console.log(data);
				       if(data.add==true){
				       	alert("添加成功！");
				       	window.location.href='<%=basePath%>check/tire?id='+$("#id").val()+'&vehicleTypeId='+$("#vehicleTypeId").val();
					   }
					},
					error : function(xhr) {
						console.log('错误');
						console.log(xhr);
					},
				});	 
	      });
	 }); 
	 /* $(function () { $('#myModal').modal({
			keyboard: true
	 })}); */
	</script>
</body>
</html>