<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<%=basePath%>assets/css/bootstrap.min.css">
<link rel="stylesheet"
	href="<%=basePath%>assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="<%=basePath%>assets/vendor/linearicons/style.css">
<!-- MAIN CSS -->
<link rel="stylesheet" href="<%=basePath%>assets/css/main.css">
<link rel="stylesheet"
	href="<%=basePath%>assets/css/bootstrap-select.min.css">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet" href="<%=basePath%>assets/css/demo.css">
<!-- GOOGLE FONTS -->
<link rel="stylesheet" href="<%=basePath%>css/google.css">
<link rel="stylesheet" href="<%=basePath%>css/bootstrap-responsive.css">
<link rel="stylesheet" href="<%=basePath%>css/qunit-1.11.0.css">

<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp" />
		<!-- left -->
		<jsp:include page="left.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- <h3 class="page-title">客户管理</h3> -->
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%; float: left;">客户管理-添加客户</h3>
								</div>
								<div class="panel-body">
									<form action="" method="post" id="form01">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td>客户名称</td>
													<td colspan="2" class="tdbold">
														<input class="form-control" type="hidden" id="id" value="${customer.id}">
														<input class="form-control" type="text" id="name" name="name" value="${customer.name}">
													</td>
													<td>销售代表</td>
													<td class="tdbold">
														<select class="form-control" name="userId" id="userId">
															<c:forEach var="u" items="${users }">
																<option <c:if test="${customer.userId==u.id}">selected</c:if> value="${u.id}">${u.name}</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>客户状态</td>
													<td colspan="2" class="tdbold">
														<select class="form-control" name="status" id="status">
															<option <c:if test="${customer.status==1}">selected</c:if> value="1">目标客户</option>
															<option <c:if test="${customer.status==2}">selected</c:if> value="2">初次拜访</option>
															<option <c:if test="${customer.status==3}">selected</c:if> value="3">机会评估</option>
															<option <c:if test="${customer.status==4}">selected</c:if> value="4">信息收集</option>
															<option <c:if test="${customer.status==5}">selected</c:if> value="5">方案确定</option>
															<option <c:if test="${customer.status==6}">selected</c:if> value="6">方案实施</option>
															<option <c:if test="${customer.status==7}">selected</c:if> value="7">PISIP未实施</option>
															<option <c:if test="${customer.status==8}">selected</c:if> value="8">PISIP实施</option>
															<option <c:if test="${customer.status==9}">selected</c:if> value="9">生意失败</option>
															<option <c:if test="${customer.status==10}">selected</c:if> value="10">重新评估</option>
														</select>
													</td>
													<td>生意放弃/失败原因</td>
													<td class="tdbold">
														<select class="form-control" name="failureReason" id="failureReason">
															<option <c:if test="${customer.failureReason==1}">selected</c:if> value="1">服务</option>
															<option <c:if test="${customer.failureReason==2}">selected</c:if> value="2">产品</option>
															<option <c:if test="${customer.failureReason==3}">selected</c:if> value="3">资金</option>
															<option <c:if test="${customer.failureReason==4}">selected</c:if> value="4">PSIP</option>
															<option <c:if test="${customer.failureReason==5}">selected</c:if> value="5">关系</option>
															<option <c:if test="${customer.failureReason==6}">selected</c:if> value="6">规模</option>
															<option <c:if test="${customer.failureReason==7}">selected</c:if> value="7">其他</option>
														</select>
													</td>
												</tr>
												<tr>
													<td>当前供应商</td>
													<td colspan="4" class="tdbold"><input class="form-control" type="text" id="supplier" name="supplier" value="${customer.supplier}"></td>
												</tr>
												<tr>
													<td rowspan="2">联系地址</td>
													<td colspan="4" class="tdbold"><input class="form-control" type="text" name="address" id="address0" ></td>
												</tr>
												<tr>
													<td colspan="4" class="tdbold"><input class="form-control" type="text" name="address" id="address1" ></td>
												</tr>
												<tr>
													<td>联系人</td>
													<td>职位</td>
													<td>电话</td>
													<td>手机</td>
													<td>电子邮件</td>
												</tr>
												<tr>
													<td class="tdbold" width="15%"><input class="form-control" type="text" id="contactName0" name="contact.name"></td>
													<td class="tdbold" width="20%"><input class="form-control" type="text" id="contactPosition0" name="contact.position"></td>
													<td class="tdbold" width="20%"><input class="form-control" type="text" id="contactPhone0" name="contact.phone"></td>
													<td class="tdbold" width="20%"><input class="form-control" type="text" id="contactMobile0" name="contact.mobile"></td>
													<td class="tdbold" width="20%"><input class="form-control" type="text" id="contactEmail0" name="contact.email"></td>
												</tr>
												<tr>
													<td class="tdbold"><input class="form-control" type="text" id="contactName1" name="contact.name"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactPosition1" name="contact.position"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactPhone1" name="contact.phone"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactMobile1" name="contact.mobile"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactEmail1" name="contact.email"></td>
												</tr>
												<tr>
													<td class="tdbold"><input class="form-control" type="text" id="contactName2" name="contact.name"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactPosition2" name="contact.position"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactPhone2" name="contact.phone"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactMobile2" name="contact.mobile"></td>
													<td class="tdbold"><input class="form-control" type="text" id="contactEmail2" name="contact.email"></td>
												</tr>
												<tr>
													<td colspan="5" style="text-align: center;">车辆类型</td>
												</tr>
												<tr>
													<td>一体车(个)</td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">4x2</span>
															<input class="form-control" type="text" name="wholeCar42" id="wholeCar42" value="${customer.wholeCar42}" >
														</div>
													</td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">6x2</span>
															<input class="form-control" type="text" name="wholeCar62" id="wholeCar62" value="${customer.wholeCar62}">
														</div>
													</td>
													<td class="tdbold" colspan="2">
														<div class="input-group">
															<span class="input-group-addon">8x4</span>
															<input class="form-control" type="text" name="wholeCar84" id="wholeCar84" value="${customer.wholeCar84}">
														</div>
													</td>
												</tr>
												<tr>
													<td>牵引车(个)</td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">4x2</span>
															<input class="form-control" type="text" name="drawCar42" id="drawCar42" value="${customer.drawCar42}">
														</div>
													</td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">6x2</span>
															<input class="form-control" type="text" name="drawCar62" id="drawCar62" value="${customer.drawCar62}">
														</div></td>
													<td class="tdbold" colspan="2">
														<div class="input-group">
															<span class="input-group-addon">6x4</span>
															<input class="form-control" type="text" name="drawCar64" id="drawCar64" value="${customer.drawCar64}">
														</div></td>
												</tr>
												<tr>
													<td>拖车(个)</td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">4x0</span>
															<input class="form-control" type="text" name="dragCar40" id="dragCar40" value="${customer.dragCar40}">
														</div></td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">4x0单胎</span>
															<input class="form-control" type="text" name="dragCar40D" id="dragCar40D" value="${customer.dragCar40D}">
														</div></td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">6x0</span>
															<input class="form-control" type="text" name="dragCar60" id="dragCar60" value="${customer.dragCar60}">
														</div></td>
													<td class="tdbold">
														<div class="input-group">
															<span class="input-group-addon">6x0单胎</span>
															<input class="form-control" type="text" name="dragCar60D" id="dragCar60D" value="${customer.dragCar60D}">
														</div></td>
												</tr>
												<tr>
													<td>主要轮胎品牌</td>
													<td class="tdbold">
														<select id="brand" name="brand" class="selectpicker bla bla bli" multiple data-live-search="true" title="请选择">
															<c:forEach var="brand" items="${brands}">
																<option value="${brand.id }">${brand.name }</option>
															</c:forEach>
														</select>
													</td>
													<td>轮胎使用规格</td>
													<td class="tdbold" colspan="2">
														<select id="tyreSize" name="tyreSize" class="selectpicker bla bla bli" multiple data-live-search="true" title="请选择">
															<c:forEach var="tyreSize" items="${tyreSizes}">
																<option value="${tyreSize.id }">${tyreSize.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>翻新轮胎与新轮胎使用比例</td>
													<td class="tdbold" colspan="4">
														<div class="input-group">
															<input class="form-control" type="text" name="oldNewRate" id="oldNewRate" value="${customer.oldNewRate}">
															<span class="input-group-addon">%</span>
														</div>
													</td>
												</tr>
												<tr>
													<td>翻新轮胎里程(平均)</td>
													<td class="tdbold" colspan="1">
														<div class="input-group">
															<span class="input-group-addon">驱动</span>
															<input class="form-control" type="text" name="frontMiles" id="frontMiles" value="${customer.frontMiles}">
															<span class="input-group-addon">KM</span>
														</div>
													</td>
													<td class="tdbold" colspan="3">
														<div class="input-group">
															<span class="input-group-addon">拖轮</span>
															<input class="form-control" type="text" name="backMiles" id="backMiles" value="${customer.backMiles}">
															<span class="input-group-addon">KM</span>
														</div>
													</td>
												</tr>
												<tr>
													<td>翻新轮胎价格</td>
													<td class="tdbold" colspan="1">
														<div class="input-group">
															<span class="input-group-addon">驱动</span>
															<input class="form-control" type="text" name="frontPrice" id="frontPrice" value="${customer.frontPrice}">
															<span class="input-group-addon">元</span>
														</div>
													</td>
													<td class="tdbold" colspan="3">
														<div class="input-group">
															<span class="input-group-addon">拖轮</span>
															<input class="form-control" type="text" name="backPrice" id="backPrice" value="${customer.backPrice}">
															<span class="input-group-addon">元</span>
														</div>
													</td>
												</tr>
												<tr>
													<td>翻新轮胎花纹要求</td>
													<td class="tdbold" colspan="1">
														<div class="input-group">
															<span class="input-group-addon">驱动</span>
															<select class="form-control"  name="frontTread" id="frontTread">
																<option value="">请选择</option>
																<option value="H" <c:if test="${customer.frontTread=='H'}">selected</c:if> >H</option>
																<option value="Z" <c:if test="${customer.frontTread=='Z'}">selected</c:if> >Z</option>
																<option value="K" <c:if test="${customer.frontTread=='K'}">selected</c:if> >K</option>
																<option value="M" <c:if test="${customer.frontTread=='M'}">selected</c:if> >M</option>
															</select>
														</div>
													</td>
													<td class="tdbold" colspan="3">
														<div class="input-group">
															<span class="input-group-addon">拖轮</span>
															<select class="form-control"  name="backTread" id="backTread">
																<option value="">请选择</option>
																<option value="H" <c:if test="${customer.backTread=='H'}">selected</c:if> >H</option>
																<option value="Z" <c:if test="${customer.backTread=='Z'}">selected</c:if> >Z</option>
																<option value="K" <c:if test="${customer.backTread=='K'}">selected</c:if> >K</option>
																<option value="M" <c:if test="${customer.backTread=='M'}">selected</c:if> >M</option>
															</select>
														</div>
													</td>
												</tr>
												<tr>
													<td>日均换胎量</td>
													<td class="tdbold">
														<div class="input-group">
															<input class="form-control" type="text" name="dayChangeAmt" id="dayChangeAmt" value="${customer.dayChangeAmt}">
															<span class="input-group-addon">条</span>
														</div>
													</td>
													<td>月均报废胎量</td>
													<td class="tdbold" colspan="2">
														<div class="input-group">
															<input class="form-control" type="text" name="dayDropAmt" id="dayDropAmt" value="${customer.dayDropAmt}">
															<span class="input-group-addon">条</span>
														</div>
													</td>
												</tr>
												<tr>
													<td>里程/每月/每车</td>
													<td class="tdbold">
														<div class="input-group">
															<input class="form-control" type="text" name="avgMiles" id="avgMiles" value="${customer.avgMiles}">
															<span class="input-group-addon">KM</span>
														</div>
													</td>
													<td>油费/每月/每车</td>
													<td class="tdbold" colspan="2">
														<div class="input-group">
															<input class="form-control" type="text" name="avgOilFee" id="avgOilFee" value="${customer.avgOilFee}">
															<span class="input-group-addon">元</span>
														</div>
													</td>
												</tr>
												<tr>
													<td>每月轮胎费用/每车</td>
													<td class="tdbold">
														<div class="input-group">
															<input class="form-control" type="text" name="monthAvgFee" id="monthAvgFee" value="${customer.monthAvgFee}">
															<span class="input-group-addon">元</span>
														</div>
													</td>
													<td>推崇新轮胎品牌</td>
													<td class="tdbold" colspan="2"><input class="form-control" type="text" name="likeBrand" id="likeBrand" value="${customer.likeBrand}"></td>
												</tr>
												<tr>
													<td>市场分类</td>
													<td class="tdbold">
														<select id="market"  name="market" class="selectpicker bla bla bli" multiple data-live-search="true" title="请选择">
															<c:forEach var="market" items="${markets}">
																<option value="${market.id }">${market.type }</option>
															</c:forEach>
														</select>
													</td>
													<td>使用环境</td>
													<td class="tdbold" colspan="2">
														<select id="evn" name="evn" class="selectpicker bla bla bli" multiple data-live-search="true" title="请选择">
															<c:forEach var="evn" items="${evns}">
																<option value="${evn.id }">${evn.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>轮胎主要问题</td>
													<td class="tdbold" colspan="4">
														<select id="problem" name="problem" class="selectpicker bla bla bli"  data-width="100%"   multiple data-live-search="true" title="请选择">
															<c:forEach var="problem" items="${problems}">
																<option value="${problem.id }">${problem.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>

												<tr>
													<td rowspan="3" style="vertical-align: middle;">服务要求</td>
													<td>体系</td>
													<td class="tdbold" colspan="3">
														<select id="require" name="require" class="selectpicker bla bla bli" multiple data-live-search="true" title="请选择">
															<c:forEach var="require" items="${requires}">
																<option value="${require.id }">${require.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>服务</td>
													<td class="tdbold" colspan="3">
														<select id="service" name="service" class="selectpicker bla bla bli" multiple data-live-search="true"  title="请选择">
															<c:forEach var="service" items="${services}">
																<option value="${service.id }">${service.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>信息</td>
													<td class="tdbold" colspan="3">
														<select id="info" name="info" class="selectpicker bla bla bli" multiple data-live-search="true" title="请选择">
															<c:forEach var="info" items="${infos}">
																<option value="${info.id }">${info.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>翻新点</td>
													<td class="tdbold">
														<select id="point" name="point" class="selectpicker bla bla bli" multiple data-live-search="true" title="请选择">
															<c:forEach var="point" items="${points}">
																<option value="${point.id }">${point.name }</option>
															</c:forEach>
														</select>
													</td>
													<td>双轮匹配</td>
													<td class="tdbold" colspan="2">
														<label class="fancy-checkbox"> 
															<input type="checkbox" id="doubleMatch0"  <c:if test="${customer.doubleMatch==0 }">checked</c:if> value="0"><span>否</span>
														</label>
														<label class="fancy-checkbox"> 
															<input type="checkbox" id="doubleMatch1" value="1" <c:if test="${customer.doubleMatch==1 }">checked</c:if> ><span>是</span>
														</label>
														<input type="hidden" id="doubleMatch" name="doubleMatch" value="0">
													</td>
												</tr>
												<tr>
													<td>是否有轮胎实验</td>
													<td class="tdbold">
														<label class="fancy-checkbox">
															<input type="checkbox" id="hasTest0" <c:if test="${customer.hasTest==0 }">checked</c:if> value="0"><span>否</span>
														</label>
														<label class="fancy-checkbox"> 
															<input type="checkbox" id="hasTest1" <c:if test="${customer.hasTest==1 }">checked</c:if> value="1"><span>是</span>
														</label>
														<input type="hidden" id="hasTest" name="hasTest" value="0"></td>
													<td>气压检查</td>
													<td class="tdbold" colspan="2">
														<select class="form-control" name="checkRate" id="checkRate">
															<option <c:if test="${customer.checkRate==0 }">selected</c:if> value="0">每日</option>
															<option <c:if test="${customer.checkRate==1 }">selected</c:if> value="1">每周</option>
															<option <c:if test="${customer.checkRate==2 }">selected</c:if> value="2">每月</option>
															<option <c:if test="${customer.checkRate==3 }">selected</c:if> value="3">每年</option>

														</select>
													</td>
												</tr>
												<tr>
													<td>气压标准</td>
													<td class="tdbold"><input class="form-control" name="pressStandard" id="pressStandard"  value="${customer.pressStandard}"></td>
													<td>车队轮胎规模</td>
													<td class="tdbold" colspan="2"><input class="form-control" name="fleetAmt" id="fleetAmt"  value="${customer.fleetAmt}"></td>
												</tr>
												<tr>
													<td colspan="5" align="center">
														<button type="button" id="sub" class="btn btn-success">保存</button>
														<button type="button" onclick="javascrpt:history.go(-1);" class="btn btn-primary">取消</button>
													</td>
												<tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath%>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath%>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script
		src="<%=basePath%>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath%>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath%>js/bootstrap-paginator.js"></script>
	<script src="<%=basePath%>assets/scripts/bootstrap-select.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker({
	        'selectedText': 'cat'
	    });
		
		var addresses = ${customer.addressesJson};
	    $.each(addresses, function (i) {
	            $("#address"+i).val(addresses[i].address);
	    });
	    
	    var contacts = ${customer.contactsJson};
	    $.each(contacts, function (i) {
	            $("#contactName"+i).val(contacts[i].name);
	            $("#contactPosition"+i).val(contacts[i].position);
	            $("#contactPhone"+i).val(contacts[i].phone);
	            $("#contactMobile"+i).val(contacts[i].mobile);
	            $("#contactEmail"+i).val(contacts[i].email);
	    });
	    
		var tyreSizes = ${customer.tyreSizesJson };
		var tyreSizesArray = new Array();
	    $.each(tyreSizes, function (i) {
	    	tyreSizesArray.push(tyreSizes[i].tyreSizeId);
	    });
		$('#tyreSize').selectpicker('val', tyreSizesArray);//默认选中
	    $('#tyreSize').selectpicker('refresh');
		
		var brands = ${customer.brandsJson };
		var brandsArray = new Array();
	    $.each(brands, function (i) {
	    	brandsArray.push(brands[i].brandId);
	    });
		$('#brand').selectpicker('val', brandsArray);//默认选中
	    $('#brand').selectpicker('refresh');
		
		var markets = ${customer.marketsJson };
		var marketsArray = new Array();
	    $.each(markets, function (i) {
	    	marketsArray.push(markets[i].marketId);
	    });
		$('#market').selectpicker('val', marketsArray);//默认选中
	    $('#market').selectpicker('refresh');
		
		var evns = ${customer.evnsJson };
		var evnsArray = new Array();
	    $.each(evns, function (i) {
	    	evnsArray.push(evns[i].evnId);
	    });
		$('#evn').selectpicker('val', evnsArray);//默认选中
	    $('#evn').selectpicker('refresh');
		
		var infos = ${customer.infosJson };
		var infosArray = new Array();
	    $.each(infos, function (i) {
	    	infosArray.push(infos[i].infoId);
	    });
		$('#info').selectpicker('val', infosArray);//默认选中
	    $('#info').selectpicker('refresh');
		
		var problems = ${customer.problemsJson };
		var problemsArray = new Array();
	    $.each(problems, function (i) {
	    	problemsArray.push(problems[i].problemId);
	    });
		$('#problem').selectpicker('val', problemsArray);//默认选中
	    $('#problem').selectpicker('refresh');
		
		var requires = ${customer.requiresJson };
		var requiresArray = new Array();
	    $.each(requires, function (i) {
	    	requiresArray.push(requires[i].requireId);
	    });
		$('#require').selectpicker('val', requiresArray);//默认选中
	    $('#require').selectpicker('refresh');
		
		var services = ${customer.servicesJson };
		var servicesArray = new Array();
	    $.each(services, function (i) {
	    	servicesArray.push(services[i].serviceId);
	    });
		$('#service').selectpicker('val', servicesArray);//默认选中
	    $('#service').selectpicker('refresh');
		
		var points = ${customer.pointsJson };
		var pointsArray = new Array();
	    $.each(points, function (i) {
	    	pointsArray.push(points[i].pointId);
	    });
		$('#point').selectpicker('val', pointsArray);//默认选中
	    $('#point').selectpicker('refresh');
		
		$("#doubleMatch0").click(function(){
			if($(this).prop("checked")){
				$("#doubleMatch1").prop("checked",false); 
				$("#doubleMatch").val("0");
			}
			if(!$(this).prop("checked")){
				$("#doubleMatch1").prop("checked",'checked'); 
				$("#doubleMatch").val("1");
			}
		});
		$("#doubleMatch1").click(function(){
			if($(this).prop("checked")){
				$("#doubleMatch0").prop("checked",false); 
				$("#doubleMatch").val("1");
			}
			if(!$(this).prop("checked")){
				$("#doubleMatch0").prop("checked",'checked'); 
				$("#doubleMatch").val("0");
			}
		});
		
		$("#hasTest0").click(function(){
			if($(this).prop("checked")){
				$("#hasTest1").prop("checked",false); 
				$("#hasTest").val("0");
			}
			if(!$(this).prop("checked")){
				$("#hasTest1").prop("checked",'checked'); 
				$("#hasTest").val("1");
			}
		});
		$("#hasTest1").click(function(){
			if($(this).prop("checked")){
				$("#hasTest0").prop("checked",false); 
				$("#hasTest").val("1");
			}
			if(!$(this).prop("checked")){
				$("#hasTest0").prop("checked",'checked'); 
				$("#hasTest").val("0");
			}
		});
		
		$("#sub").click(function(){	
			if($('#name').val()==""){
				$("#msg").html("请输入客户名称");
				return;
			}
			if($('#userName').val()==""){
				$("#msg").html("请输入销售代表");
				return;
			}
			
			var addresses = $("input[name='address']");
			var addressObj = [];
			for(var i=0;i<addresses.length;i++){
				var obj = {address:$(addresses[i]).val()};
				addressObj.push(obj);
			}
			
			var contactNames = $("input[name='contact.name']");
			var contactPositions = $("input[name='contact.position']");
			var contactPhones = $("input[name='contact.phone']");
			var contactMobiles = $("input[name='contact.mobile']");
			var contactEmails = $("input[name='contact.email']");
			var contacts = [];
			for(var i=0;i<contactNames.length;i++){
				var contactName = $(contactNames[i]).val();
				var contactPosition = $(contactPositions[i]).val();
				var contactPhone = $(contactPhones[i]).val();
				var contactMobile = $(contactMobiles[i]).val();
				var contactEmail = $(contactEmails[i]).val();
				var contact = {name:contactName,position:contactPosition,phone:contactPhone,mobile:contactMobile,email:contactEmail};
				contacts.push(contact);
			}
			
			var tyreSizes = $("#tyreSize").val();
			var tyreSizeObj = [];
			for(var i=0;i<tyreSizes.length;i++){
				var obj = {tyreSizeId:tyreSizes[i]};
				tyreSizeObj.push(obj);
			}
			
			var brands = $("#brand").val();
			var brandObj = [];
			for(var i=0;i<brands.length;i++){
				var obj = {brandId:brands[i]};
				brandObj.push(obj);
			}
			
			var markets = $("#market").val();
			var marketObj = [];
			for(var i=0;i<markets.length;i++){
				var obj = {marketId:markets[i]};
				marketObj.push(obj);
			}
			
			var evns = $("#evn").val();
			var evnObj = [];
			for(var i=0;i<evns.length;i++){
				var obj = {evnId:evns[i]};
				evnObj.push(obj);
			}
			
			var infos = $("#info").val();
			var infoObj = [];
			for(var i=0;i<infos.length;i++){
				var obj = {infoId:infos[i]};
				infoObj.push(obj);
			}
			
			var problems = $("#problem").val();
			var problemObj = [];
			for(var i=0;i<problems.length;i++){
				var obj = {problemId:problems[i]};
				problemObj.push(obj);
			}
			
			var requires = $("#require").val();
			var requireObj = [];
			for(var i=0;i<requires.length;i++){
				var obj = {requireId:requires[i]};
				requireObj.push(obj);
			}
			
			var services = $("#service").val();
			var serviceObj = [];
			for(var i=0;i<services.length;i++){
				var obj = {serviceId:services[i]};
				serviceObj.push(obj);
			}
			
			var points = $("#point").val();
			var pointObj = [];
			for(var i=0;i<points.length;i++){
				var obj = {pointId:points[i]};
				pointObj.push(obj);
			}
			
	 		var obj = {
	 					customer:{
		 					id: $("#id").val(),
		 					name: $("#name").val(),
		 					userId:$("#userId").val(),
		 					userName:$("#userName").val(),
		 					status:$("#status").val(),
		 					failureReason:$("#failureReason").val(),
		 					supplier:$("#supplier").val(),
		 					wholeCar42:$("#wholeCar42").val(),
		 					wholeCar62:$("#wholeCar62").val(),
		 					wholeCar84:$("#wholeCar84").val(),
		 					drawCar42:$("#drawCar42").val(),
		 					drawCar62:$("#drawCar62").val(),
		 					drawCar64:$("#drawCar64").val(),
		 					dragCar40:$("#dragCar40").val(),
		 					dragCar40D:$("#dragCar40D").val(),
		 					dragCar60:$("#dragCar60").val(),
		 					dragCar60D:$("#dragCar60D").val(),
		 					oldNewRate:$("#oldNewRate").val(),
		 					frontMiles:$("#frontMiles").val(),
		 					backMiles:$("#backMiles").val(),
		 					frontPrice:$("#frontPrice").val(),
		 					backPrice:$("#backPrice").val(),
		 					frontTread:$("#frontTread").val(),
		 					backTread:$("#backTread").val(),
		 					dayChangeAmt:$("#dayChangeAmt").val(),
		 					dayDropAmt:$("#dayDropAmt").val(),
		 					monthAvgFee:$("#monthAvgFee").val(),
		 					avgMiles:$("#avgMiles").val(),
		 					avgOilFee:$("#avgOilFee").val(),
		 					likeBrand:$("#likeBrand").val(),
		 					doubleMatch:$("#doubleMatch").val(),
		 					hasTest:$("#hasTest").val(),
		 					checkRate:$("#checkRate").val(),
		 					pressStandard:$("#pressStandard").val(),
		 					fleetAmt:$("#fleetAmt").val()
	 					},
	 					addresses:addressObj,
	 					contacts:contacts,
	 					tyreSizes:tyreSizeObj,
	 					brands:brandObj,
	 					markets:marketObj,
	 					evns:evnObj,
	 					infos:infoObj,
	 					problems:problemObj,
	 					requires:requireObj,
	 					services:serviceObj,
	 					points:pointObj
	 		          };
	 		console.log(JSON.stringify(obj));
			$.ajax({
			   url:'<%=basePath%>customer/update',
			   type:'POST', //GET
			   async:true,    //或false,是否异步
			   contentType:"application/json",
			   data:JSON.stringify(obj),
			   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			   success:function(data){
			       console.log(data);
			       if(data.add==true){
			       	alert("添加成功！");
			       	window.location.href="<%=basePath%>customer/list";
				   }
				},
				error : function(xhr) {
					console.log('错误');
					console.log(xhr);
				},
			});
		});
	});
	</script>
	</script>
</body>
</html>