<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>卡客途安轮卫管家</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<%=basePath%>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=basePath%>assets/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="<%=basePath%>assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=basePath%>assets/vendor/linearicons/style.css">
<!-- MAIN CSS -->
<link rel="stylesheet" href="<%=basePath%>assets/css/main.css">
<link rel="stylesheet" href="<%=basePath%>assets/css/bootstrap-select.min.css">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet" href="<%=basePath%>assets/css/demo.css">
<!-- GOOGLE FONTS -->
<link rel="stylesheet" href="<%=basePath%>css/google.css">
<link rel="stylesheet" href="<%=basePath%>css/bootstrap-responsive.css">
<link rel="stylesheet" href="<%=basePath%>css/qunit-1.11.0.css">

<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath %>assets/img/logo.png">
<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath %>assets/img/logo.png">
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- navbar -->
		<jsp:include page="navbar.jsp" />
		<!-- left -->
		<jsp:include page="left.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- <h3 class="page-title">客户管理</h3> -->
					<div class="row">
						<div class="col-md-12 col-center-block">
							<!-- BORDERED TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title" style="width: 50%; float: left;">轮胎跟踪-添加跟踪客户</h3>
								</div>
								<div class="panel-body">
									<form action="" method="post" id="form01">
										<table class="table table-bordered" id="t2">
											<tbody>
												<tr>
													<td>客户名称</td>
													<td class="tdbold" >
														<select name="customerId" id="customerId" class="form-control">
															<option value="">请选择</option>
															<c:forEach items="${customer }" var="c">
																<option value="${c.id }">${c.name }</option>
															</c:forEach>
														</select>
													</td>
													<td>检测人</td>
													<td class="tdbold" >
														${userName }<input type="hidden" name="userId" id="userId" value="${userId }">
													</td>
												</tr>
												<tr>
													<td>联系人</td>
													<td class="tdbold"><input class="form-control" type="text" name="contact"  id="contact" >
													</td>
													<td>联系电话</td>
													<td class="tdbold"><input class="form-control" type="text" name="contactPhone"  id="contactPhone" ></td>
												</tr>
												<tr>
													<td>司机名称</td>
													<td class="tdbold"><input class="form-control" type="text" name="driverName"  id="driverName" ></td>
													<td>司机电话</td>
													<td class="tdbold"><input class="form-control" type="text" name="driverPhone"  id="driverPhone" ></td>
												</tr>
												<tr>
													<td>主车牌号</td>
													<td class="tdbold"><input class="form-control" type="text" name="carNo"  id="carNo" ></td>
													<td>挂车牌号</td>
													<td class="tdbold"><input class="form-control" type="text" name="handCarNo"  id="handCarNo"  ></td>
												</tr>
												<tr>
													<td>开始日期</td>
													<td class="tdbold"><input type="text" name="inDate"  id="datetimepicker" data-date-format="yyyy-mm-dd"></td>
													<td>车辆类型</td>
													<td class="tdbold">
														<select name="vehicleTypeId" id="vehicleTypeId" class="form-control">
															<option value="">请选择</option>
															<c:forEach items="${vehicleType }" var="v">
																<option value="${v.id }">${v.name }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>公里表数</td>
													<td class="tdbold"><input class="form-control" type="text" name="baseMiles"   id="baseMiles"></td>
													<td>运输类型</td>
													<td class="tdbold">
														<select name="transType" id="transType" class="form-control">
															<option value="">请选择</option>
															<c:forEach items="${markets }" var="m">
																<option value="${m.type }">${m.type }</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<td>使用环境</td>
													<td class="tdbold">
														<select name="roads" id="roads" class="form-control">
															<option value="">请选择</option>
															<c:forEach items="${evns }" var="e">
																<option value="${e.name }">${e.name }</option>
															</c:forEach>
														</select>
													</td>
													<td>车货重量</td>
													<td class="tdbold"><input class="form-control" type="text" name="weight"  id="weight"></td>
												</tr>
												<tr>
													<td>业务状况</td>
													<td class="tdbold" colspan="3"><input class="form-control" type="text" name="business"  id="business"></td>
												</tr>
											</tbody>
										</table>
									</form>
									<h6 class="modal-title" id="msg" style="color: red"></h6>
								</div>
								<div class="modal-footer" style="text-align: center;">
									<button type="button" class="btn btn-success"  id="sub">保存</button>
									<button type="button" class="btn btn-primary" id="cancel">取消</button>
								</div>
							</div>
							<!-- END BORDERED TABLE -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<%=basePath%>assets/vendor/jquery/jquery.min.js"></script>
	<script src="<%=basePath%>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<%=basePath%>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath%>assets/scripts/klorofil-common.js"></script>
	<script src="<%=basePath%>js/bootstrap-paginator.js"></script>
	<script src="<%=basePath%>assets/scripts/bootstrap-select.min.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-datetimepicker.min.js"></script>
	<script src="<%=basePath %>assets/scripts/bootstrap-datetimepicker.zh-CN.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#datetimepicker').datetimepicker({
		       language: 'zh-CN',
		       autoclose: true,
		       todayBtn: true
		});
		$("#sub").click(function(){	
			if($('#customerId').val()==""){
				$("#msg").html("请选择客户");
				return;
			}
			if($('#contact').val()==""){
				$("#msg").html("请输入联系人");
				return;
			}
			if($('#carNo').val()==""){
				$("#msg").html("请输入主车牌号");
				return;
			}
			if($('#datetimepicker').val()==""){
				$("#msg").html("请输入安装日期");
				return;
			}
			if($('#vehicleTypeId').val()==""){
				$("#msg").html("请输入车辆类型");
				return;
			}
			if($('#baseMiles').val()==""){
				$("#msg").html("请输入公里表数");
				return;
			}
			
			var obj = {
					customerId:$('#customerId').val(),
					userId:$('#userId').val(),
					contact:$('#contact').val(),
					contactPhone:$('#contactPhone').val(),
					driverName:$('#driverName').val(),
					driverPhone:$('#driverPhone').val(),
					carNo:$('#carNo').val(),
					handCarNo:$('#handCarNo').val(),
					inDate:$('#datetimepicker').val(),
					vehicleTypeId:$('#vehicleTypeId').val(),
					baseMiles:$('#baseMiles').val(),
					transType:$('#transType').val(),
					roads:$('#roads').val(),
					weight:$('#weight').val(),
					business:$('#business').val()
			};
			
			$.ajax({
			   url:'<%=basePath%>track/add',
			   type:'POST', //GET
			   async:true,    //或false,是否异步
			   contentType:"application/json",
			   data:JSON.stringify(obj),
			   dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
			   success:function(data){
			       console.log(data);
			       if(data.add==true){
			       	alert("添加成功！");
			       	window.location.href="<%=basePath%>track/detail?id="+data.id;
				   }
				},
				error : function(xhr) {
					console.log('错误');
					console.log(xhr);
				},
			});
		});
		$("#cancel").click(function(){	
			window.history.back();
		});
	});
	</script>
	</script>
</body>
</html>