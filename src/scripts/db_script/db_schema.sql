-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: jjk_mst
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doctor_working_q`
--

DROP TABLE IF EXISTS `doctor_working_q`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor_working_q` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doctor_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `timeout` bigint(20) DEFAULT NULL,
  `create_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `family`
--

DROP TABLE IF EXISTS `family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family` (
  `family_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `phone_number` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `user_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `sex` bit(1) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `id_type` enum('0','1','2','3','4') CHARACTER SET latin1 DEFAULT NULL,
  `id_no` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `role` varchar(20) CHARACTER SET latin1 DEFAULT 'owner',
  `is_delete` bit(1) DEFAULT b'0',
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`family_id`),
  KEY `fk_mst_family_userid_idx` (`user_id`),
  CONSTRAINT `fk_mst_family_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=488 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `im_token`
--

DROP TABLE IF EXISTS `im_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `im_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `token` varchar(500) NOT NULL,
  `create_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jjk_report_router`
--

DROP TABLE IF EXISTS `jjk_report_router`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjk_report_router` (
  `ReportId` bigint(20) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `ShardNo` tinyint(4) NOT NULL,
  PRIMARY KEY (`ReportId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login_token`
--

DROP TABLE IF EXISTS `login_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `logint_token` varchar(500) NOT NULL,
  `create_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=637 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_3rd_user`
--

DROP TABLE IF EXISTS `mst_3rd_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_3rd_user` (
  `UserId` bigint(20) NOT NULL,
  `OpenId` varchar(50) CHARACTER SET utf8 NOT NULL,
  `UnionId` varchar(50) CHARACTER SET utf8 NOT NULL,
  `AccessToken` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_activity`
--

DROP TABLE IF EXISTS `mst_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activityId` bigint(20) NOT NULL,
  `serviceId` bigint(20) NOT NULL,
  `title` varchar(200) NOT NULL,
  `subTitle` varchar(200) NOT NULL,
  `imgUrl` varchar(500) NOT NULL,
  `contents` varchar(1000) DEFAULT NULL,
  `signSdate` date DEFAULT NULL,
  `signEdate` date DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `totalDistance` int(11) DEFAULT NULL,
  `appUrl` varchar(500) DEFAULT NULL,
  `activityCreateTime` date DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `idType` enum('1','2') DEFAULT NULL,
  `idNumber` varchar(20) DEFAULT NULL,
  `createdTime` timestamp NULL DEFAULT NULL,
  `updatedTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_imported_user`
--

DROP TABLE IF EXISTS `mst_imported_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_imported_user` (
  `UserID` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `UserName` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '登陆名',
  `Pwd` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '登陆密码',
  `NickName` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '昵称',
  `TrueName` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '真实姓名',
  `CardIDType` int(10) DEFAULT NULL COMMENT '证件类型:1身份证，2军官证，3其他',
  `CardID` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证号',
  `Birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `Sex` varchar(1) CHARACTER SET utf8 DEFAULT NULL COMMENT '性别：0是男，1是女',
  `BodyHeight` decimal(10,0) DEFAULT '0' COMMENT '身高',
  `BodyWeight` decimal(10,0) DEFAULT '0' COMMENT '体重',
  `BloodType` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '血型',
  `Education` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '学历',
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '邮箱',
  `Phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机号',
  `Photo` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '图片',
  `ProvinceID` int(10) DEFAULT NULL COMMENT '省份id',
  `CityID` int(10) DEFAULT NULL COMMENT '城市id',
  `CompanyID` int(10) DEFAULT NULL COMMENT '公司id',
  `DeptID` int(10) DEFAULT NULL COMMENT '部门id',
  `Grade` int(10) DEFAULT '1' COMMENT '级别：1普通用户，2管理用户，这里区分的是圈子',
  `Stars` varchar(1) CHARACTER SET utf8 DEFAULT NULL COMMENT '标星，1，2，3，4，5颗星',
  `IsLock` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '是否锁定，0未锁定，1锁定',
  `CreateTime` int(11) DEFAULT NULL COMMENT '创建时间',
  `DiseaseID` int(10) DEFAULT NULL COMMENT '疾病id',
  `Age` int(4) unsigned DEFAULT NULL COMMENT '客户的年龄',
  `Test1` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Remark` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `IsCheck` int(255) DEFAULT '0' COMMENT '是否体检',
  `ChatToken` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `UserType` int(2) DEFAULT NULL COMMENT '1公司，2个人',
  PRIMARY KEY (`UserID`),
  KEY `userid` (`UserID`),
  KEY `cardid` (`CardID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户基本信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_report`
--

DROP TABLE IF EXISTS `mst_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_report` (
  `report_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `phone_number` varchar(11) CHARACTER SET utf8 NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `id_type` enum('0','1','2','3','4') CHARACTER SET utf8 DEFAULT NULL COMMENT '05护照  01身份证 02警察证03军官证04其他',
  `id_no` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `check_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel` enum('CiMing','MeiNian') CHARACTER SET utf8 NOT NULL,
  `channel_report_id` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `media_type` enum('Html5','Json') CHARACTER SET utf8 NOT NULL,
  `url` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `check_type` enum('HealthyHut','Checkup') CHARACTER SET utf8 NOT NULL,
  `template_id` bigint(20) DEFAULT '1',
  `score` int(11) DEFAULT '0',
  `encrypt_type` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`report_id`),
  KEY `PhoneNumber` (`phone_number`),
  KEY `UserId` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_report_template`
--

DROP TABLE IF EXISTS `mst_report_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_report_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_report_version_control`
--

DROP TABLE IF EXISTS `mst_report_version_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_report_version_control` (
  `ControlId` int(11) NOT NULL AUTO_INCREMENT COMMENT '//TODO move to cache',
  `UserId` bigint(20) DEFAULT NULL,
  `AppVersion` varchar(50) CHARACTER SET utf8 NOT NULL,
  `CurrentVersion` varchar(50) CHARACTER SET utf8 NOT NULL,
  `CreatedTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ControlId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_user_push_info`
--

DROP TABLE IF EXISTS `mst_user_push_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_user_push_info` (
  `push_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `notification_token` varchar(64) COLLATE utf8_bin NOT NULL,
  `device_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `device_os` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `service_provider` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`push_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_user_report_mapping`
--

DROP TABLE IF EXISTS `mst_user_report_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_user_report_mapping` (
  `UserReportMappingId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ReportId` bigint(20) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `CreatedTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserReportMappingId`),
  KEY `ReportId` (`ReportId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_user_service`
--

DROP TABLE IF EXISTS `mst_user_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_user_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `serviceId` bigint(20) NOT NULL,
  `serviceType` enum('1','2') NOT NULL,
  `serviceName` varchar(100) NOT NULL,
  `serviceIntro` varchar(1000) DEFAULT NULL,
  `createdTime` timestamp NULL DEFAULT NULL,
  `updatedTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_user_service_mapping`
--

DROP TABLE IF EXISTS `mst_user_service_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_user_service_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serviceId` bigint(20) NOT NULL,
  `idType` enum('1','2') NOT NULL,
  `idNumber` varchar(20) NOT NULL,
  `userType` enum('1','2') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_vcode_log`
--

DROP TABLE IF EXISTS `mst_vcode_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_vcode_log` (
  `LogId` bigint(20) NOT NULL AUTO_INCREMENT,
  `PhoneNumber` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Vcode` int(6) NOT NULL,
  `Scenario` enum('Register','ChangePhone','FindPassword','ModifyPassword','LoginPhoneChange','BindPhone','ZzaQueryReport') CHARACTER SET utf8 NOT NULL,
  `CreatedTimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedTimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` enum('Used','Unused') CHARACTER SET utf8 DEFAULT 'Unused',
  PRIMARY KEY (`LogId`),
  KEY `PhoneNumber` (`PhoneNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=42664 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `imported_user_id` bigint(20) DEFAULT '0' COMMENT '导入用户表中的用户ID',
  `phone_number` varchar(11) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  `id_type` enum('0','1','2','3','4') DEFAULT NULL COMMENT '1 身份证 2 ',
  `id_no` varchar(20) DEFAULT NULL,
  `status` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  `bound` char(1) NOT NULL DEFAULT 'N' COMMENT '是否绑定',
  `is_delete` bit(1) DEFAULT b'0',
  `profile_img_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `access_token` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `refresh_token` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `open_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `union_id` varchar(100) DEFAULT NULL,
  `wx_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `wx_portrait_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login_timestamp` timestamp NULL DEFAULT NULL,
  `update_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `PhoneNumber` (`phone_number`),
  KEY `IdType_IdNo` (`id_type`,`id_no`)
) ENGINE=InnoDB AUTO_INCREMENT=519 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-06 22:03:12
-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: jjk_rpt
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rpt_global_id`
--

DROP TABLE IF EXISTS `rpt_global_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rpt_global_id` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stub` char(1) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stub` (`stub`)
) ENGINE=MyISAM AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rpt_report`
--

DROP TABLE IF EXISTS `rpt_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rpt_report` (
  `report_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `version` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `phone_number` varchar(11) CHARACTER SET utf8 NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `id_type` enum('0','1','2','3','4') CHARACTER SET utf8 DEFAULT NULL COMMENT '05护照  01身份证 02警察证03军官证04其他',
  `id_no` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `check_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel` enum('CiMing','MeiNian') CHARACTER SET utf8 NOT NULL,
  `channel_report_id` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `media_type` enum('Html5','Json') CHARACTER SET utf8 NOT NULL,
  `url` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `check_type` enum('HealthyHut','Checkup') CHARACTER SET utf8 NOT NULL,
  `bound` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'N' COMMENT '''Y'',''N''',
  `template_id` bigint(20) DEFAULT '1',
  `score` int(11) DEFAULT '0',
  `encrypt_type` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`report_id`),
  KEY `PhoneNumber` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-06 22:04:32
