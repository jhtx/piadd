
CREATE TABLE `mst_channel` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '递增id',
  `channel_key` varchar(10) DEFAULT NULL COMMENT '渠道key',
  `channel_name` varchar(20) DEFAULT NULL COMMENT '渠道名称（如微信转发）',
  `created_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `created_date` datetime DEFAULT NULL COMMENT '创建日期',
  `modified_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  `modified_date` datetime DEFAULT NULL COMMENT '修改日期',
  `is_available` tinyint(3) DEFAULT NULL COMMENT '有效标识（0：无效，1：有效）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_channel
-- ----------------------------

INSERT INTO `mst_channel` VALUES ('1', 'REGISTER', '注册', '', '2015-09-11 16:29:49', '123', '2015-09-11 16:29:52', '1');



CREATE TABLE `mst_coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '递增id',
  `coupon_code` varchar(10) DEFAULT NULL COMMENT '优惠券编码',
  `coupon_name` varchar(20) DEFAULT NULL COMMENT '优惠券名称',
  `coupon_value` int(10) DEFAULT NULL COMMENT '优惠券面值,分为单位',
  `type_id` bigint(20) DEFAULT NULL COMMENT '关联类型id(品牌、品类)',
  `type` varchar(20) DEFAULT NULL COMMENT '优惠券类型(大品类、具体商品等)',
  `img_url` varchar(50) DEFAULT NULL COMMENT '图片地址',
  `created_by` varchar(10) DEFAULT NULL COMMENT '创建人',
  `created_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_by` varchar(10) DEFAULT NULL COMMENT '修改人',
  `modified_date` datetime DEFAULT NULL COMMENT ' 修改时间',
  `is_available` tinyint(3) DEFAULT NULL COMMENT '是否有效',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_coupon
-- ----------------------------
INSERT INTO `mst_coupon` VALUES ('1', 'zza', '早找癌优惠券', '10000', '1', 'PRODUCT', '/zza/picture/Coupon1', 'system', '2015-09-09 17:55:44', 'system', '2015-09-09 17:55:47', '1', '1');


CREATE TABLE `mst_user_coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '递增id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `coupon_id` bigint(20) DEFAULT NULL COMMENT '优惠券id',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态码(0：新下发，1:已锁定，2：已使用，3：已过期，4：已作废)',
  `exp_date` datetime DEFAULT NULL COMMENT '优惠券有效结束时间',
  `channel_key` varchar(10) DEFAULT NULL COMMENT '渠道_key',
  `order_id` varchar(60) DEFAULT NULL COMMENT '关联订单号',
  `created_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_date` datetime DEFAULT NULL COMMENT '优惠券状态更新时间',
  `is_available` tinyint(3) NOT NULL DEFAULT '1' COMMENT '数据是否有效 1有效，0无效',
  `description` varchar(50) DEFAULT NULL COMMENT '状态描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;

CREATE TABLE `mst_coupon_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_count` int(11) DEFAULT NULL,
  `coupon_Id` bigint(20) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `channel_key` varchar(20) DEFAULT NULL,
  `img_url` varchar(500) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_coupon_activity
-- ----------------------------
INSERT INTO `mst_coupon_activity` VALUES ('1', '99999991', '1', 'REGISTER', 'REGISTER', 'http://jjkimage.bj.bcebos.com/couponactivity/1442476467218.jpg', '1');