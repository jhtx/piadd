/*
Navicat MySQL Data Transfer

Source Server         : 10.2.201.47
Source Server Version : 50625
Source Host           : 10.2.201.47:3306
Source Database       : jjk_mst

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2015-08-28 10:17:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mst_goods
-- ----------------------------
DROP TABLE IF EXISTS `mst_goods`;
CREATE TABLE `mst_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `ext` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for mst_order_iterm
-- ----------------------------
DROP TABLE IF EXISTS `mst_order_iterm`;
CREATE TABLE `mst_order_iterm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `order_id` varchar(60) DEFAULT NULL,
  `pay_type` varchar(20) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_num` int(11) DEFAULT NULL COMMENT '产品数量',
  `price` int(10) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `delivery_status` tinyint(4) DEFAULT '0' COMMENT '是否为用户添加商品 1：是 ，0：否',
  `delivery_retry_num` int(11) DEFAULT NULL,
  `delivery_time` datetime DEFAULT NULL,
  `refund_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for mst_product
-- ----------------------------
DROP TABLE IF EXISTS `mst_product`;
CREATE TABLE `mst_product` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `price` int(11) DEFAULT NULL COMMENT '单位分',
  `total` int(11) DEFAULT NULL COMMENT '数量  暂时冗余',
  `status` int(11) DEFAULT '1',
  `ext` varchar(100) DEFAULT NULL COMMENT '描述',
  `category_id` int(11) DEFAULT NULL COMMENT '类型id，暂时冗余',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for mst_refund_reason
-- ----------------------------
DROP TABLE IF EXISTS `mst_refund_reason`;
CREATE TABLE `mst_refund_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `msg` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for mst_refund_record
-- ----------------------------
DROP TABLE IF EXISTS `mst_refund_record`;
CREATE TABLE `mst_refund_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(60) DEFAULT NULL,
  `refund_code` int(11) DEFAULT NULL,
  `ext` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  `refund_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4;


ALTER TABLE `mst_product`
ADD COLUMN `img_url`  varchar(100) NULL AFTER `status`;

